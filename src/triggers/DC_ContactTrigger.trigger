// 
// Contact trigger for DoubleClick connector
//
trigger DC_ContactTrigger on Contact (after insert, after update, before insert, before update) {
	if (Trigger.isAfter && Trigger.isUpdate &&  goog_dclk_dsm__DC_ConnectorApplicationSettings__c.getInstance().goog_dclk_dsm__DC_AllowContactTriggerPushToDSM__c && !DC_ContactContactConnector.connectorSyncUpdate) {
    DC_ContactTriggerController.performExistingContactSync(Trigger.new); 
  }
}