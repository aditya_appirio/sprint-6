// 
// Account trigger for DoubleClick connector
//
trigger DC_AccountTrigger on Account (after insert, after update, before insert, 
before update) {

  if (Trigger.isAfter && Trigger.isUpdate && goog_dclk_dsm__DC_ConnectorApplicationSettings__c.getInstance().goog_dclk_dsm__DC_AllowAccountTriggerPushToDSM__c && !DC_AccountCompanyConnector.connectorSyncUpdate) {
    DC_AccountTriggerController.performExistingAccountSync(Trigger.new); 
  }
}