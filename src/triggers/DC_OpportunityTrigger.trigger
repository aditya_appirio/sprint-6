// 
// Opportunity trigger for DoubleClick connector
//
trigger DC_OpportunityTrigger on Opportunity (after update) {
  if (Trigger.isAfter && Trigger.isUpdate && goog_dclk_dsm__DC_ConnectorApplicationSettings__c.getInstance().goog_dclk_dsm__DC_AllowOpportunityTriggerToDSM__c && !DC_OpportunityProposalConnector.connectorSyncUpdate) {
    DC_OpportunityTriggerController.performExistingOpportunitySync(Trigger.new); 
  }
}