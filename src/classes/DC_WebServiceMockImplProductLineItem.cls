// 
// (c) 2014 Appirio
//
// WebServiceMockImpl_ProductLineItem : implements WebServiceMock for webServicecallout test classes 
//
// 24 Jan 2014    Ankit Goyal (JDC)      Original
//
@IsTest
global class DC_WebServiceMockImplProductLineItem implements WebServiceMock{
  global void doInvoke(Object stub, Object request, Map<String, Object> response,
                       String endpoint, String soapAction, String requestName,
                       String responseNS, String responseName, String responseType){
    DC_CustomFieldService.CustomField custFld = new DC_CustomFieldService.CustomField();
	  custFld.name = 'custFld1';
	  custFld.id = 12345;
	  custFld.CustomField_Type = 'Integer';
	  
	  DC_CustomFieldService.CustomField custFld1 = new DC_CustomFieldService.CustomField();
	  custFld1.name = 'custFld2';
	  custFld1.CustomField_Type = 'Integer';
	  custFld1.id = 123456;
	  
	  DC_CustomFieldService.CustomFieldPage custFldPage = new DC_CustomFieldService.CustomFieldPage();
	  custFldPage.startIndex = 0;
	  custFldPage.results = new List<DC_CustomFieldService.CustomField>{custFld, custFld1};
	  custFldPage.totalResultSetSize = 2;
    
    DC_CustomFieldService.CustomFieldOption custFldOption = new DC_CustomFieldService.CustomFieldOption();
	  custFldOption.displayName = 'custFldOpt1';
	  custFldOption.id = 12345;
	  custFldOption.customFieldId = 12345;
	  
	  DC_CustomFieldService.CustomFieldOption custFldOption1 = new DC_CustomFieldService.CustomFieldOption();
	  custFldOption1.displayName = 'custFldOpt2';
	  custFldOption1.customFieldId = 12345;
    
    DC_ProposalLineItemService.Value bVal = new DC_ProposalLineItemService.BooleanValue();
    bVal.value = 'true';
    bVal.value_type = 'BooleanValue';
    
    DC_ProposalLineItemService.Value textVal = new DC_ProposalLineItemService.TextValue();
    textVal.value = 'test';
    textVal.value_type = 'TextValue';
    DC_ProposalLineItemService.BaseCustomFieldValue custFldValue = new DC_ProposalLineItemService.CustomFieldValue();
    custFldValue.customFieldId = 12345;
    custFldValue.BaseCustomFieldValue_Type = 'CustomFieldValue';
    custFldValue.value = bVal;
    
    DC_ProposalLineItemService.BaseCustomFieldValue dropDownCustFldValue = new DC_ProposalLineItemService.DropDownCustomFieldValue();
    dropDownCustFldValue.customFieldId = 123456;
    dropDownCustFldValue.BaseCustomFieldValue_Type = 'DropDownCustomFieldValue';
    dropDownCustFldValue.value = textVal;
    
    DC_ProposalLineItemService.Money m = new DC_ProposalLineItemService.Money();
    m.currencyCode = 'USD';
    m.microAmount = 123;
    
    DC_ProposalLineItemService.ProposalLineItem proposalLI = new DC_ProposalLineItemService.ProposalLineItem();
    proposalLI.name = 'testProposalLI';
    proposalLI.proposalId = 12345;
    proposalLI.productId = String.valueOf(12345);
    proposalLI.cost = m;
    proposalLI.baseRate = m;
    proposalLI.isArchived = true;
    proposalLI.customFieldValues = new List<DC_ProposalLineItemService.BaseCustomFieldValue>{custFldValue};
    
    DC_ProposalLineItemService.ProposalLineItem proposalLI1 = new DC_ProposalLineItemService.ProposalLineItem();
    proposalLI1.name = 'testProposalLI1';
    proposalLI1.proposalId = 12345;
    proposalLI1.id = 12345;
    proposalLI1.productId = String.valueOf(12345);
    proposalLI1.cost = m;
    proposalLI1.baseRate = m;
    proposalLI1.isArchived = false;
    proposalLI1.customFieldValues = new List<DC_ProposalLineItemService.BaseCustomFieldValue>{dropDownCustFldValue};
    
    DC_ProposalLineItemService.ProposalLineItem proposalLI2 = new DC_ProposalLineItemService.ProposalLineItem();
    proposalLI2.name = 'testProposalLI1';
    proposalLI2.proposalId = 12345;
    proposalLI2.id = 123456;
    proposalLI2.productId = String.valueOf(12345);
    proposalLI2.cost = m;
    proposalLI2.baseRate = m;
    proposalLI2.dropDownCustomFieldValues = new List<DC_ProposalLineItemService.BaseCustomFieldValue>{dropDownCustFldValue};
    
    DC_ProposalLineItemService.ProposalLineItemPage proposalLIPage = new DC_ProposalLineItemService.ProposalLineItemPage();
    proposalLIPage.results = new List<DC_ProposalLineItemService.ProposalLineItem>{proposalLI, proposalLI1, proposalLI2};    
    proposalLIPage.totalResultSetSize = 3;
    proposalLIPage.startIndex = 0;
      
    
          
    if(request instanceof DC_ProposalLineItemService.updateProposalLineItem_element){
      DC_ProposalLineItemService.updateProposalLineItemResponse_element resp = new DC_ProposalLineItemService.updateProposalLineItemResponse_element();
      resp.rval = proposalLI;
      response.put('response_x', resp);
    }else if(request instanceof DC_ProposalLineItemService.createProposalLineItems_element){
      DC_ProposalLineItemService.createProposalLineItemsResponse_element resp = new DC_ProposalLineItemService.createProposalLineItemsResponse_element();
      resp.rval = new List<DC_ProposalLineItemService.ProposalLineItem>{proposalLI} ;
      response.put('response_x', resp);
    }else if(request instanceof DC_ProposalLineItemService.updateProposalLineItems_element){
      DC_ProposalLineItemService.updateProposalLineItemsResponse_element resp = new DC_ProposalLineItemService.updateProposalLineItemsResponse_element();
      resp.rval = new List<DC_ProposalLineItemService.ProposalLineItem>{proposalLI1} ;
      response.put('response_x', resp);
    }else if(request instanceof DC_ProposalLineItemService.getProposalLineItem_element){
      DC_ProposalLineItemService.getProposalLineItemResponse_element resp = new DC_ProposalLineItemService.getProposalLineItemResponse_element();
      resp.rval = proposalLI1 ;
      response.put('response_x', resp);
    }else if(request instanceof DC_ProposalLineItemService.performProposalLineItemAction_element)
        response.put('response_x', new DC_ProposalLineItemService.performProposalLineItemActionResponse_element());
    else if(request instanceof DC_ProposalLineItemService.createProposalLineItem_element){
      DC_ProposalLineItemService.createProposalLineItemResponse_element resp = new DC_ProposalLineItemService.createProposalLineItemResponse_element();
      resp.rval = proposalLI1 ;
      response.put('response_x', resp);
    }else if(request instanceof DC_ProposalLineItemService.getProposalLineItemsByStatement_element){
      DC_ProposalLineItemService.getProposalLineItemsByStatementResponse_element resp = new DC_ProposalLineItemService.getProposalLineItemsByStatementResponse_element();
      DC_ProposalLineItemService.Statement statement = new DC_ProposalLineItemService.Statement();
      resp.rval = proposalLIPage;
      response.put('response_x', resp);
    }else if(request instanceof DC_CustomFieldService.updateCustomFieldOptions_element){
          DC_CustomFieldService.updateCustomFieldOptionsResponse_element resp = new DC_CustomFieldService.updateCustomFieldOptionsResponse_element();
          resp.rval = new List<DC_CustomFieldService.CustomFieldOption>{custFldOption};
          response.put('response_x', resp);
          //response.put('response_x', new DC_CustomFieldService.updateCustomFieldOptionsResponse_element());
      }else if(request instanceof DC_CustomFieldService.getCustomFieldsByStatement_element){
          DC_CustomFieldService.getCustomFieldsByStatementResponse_element resp = new DC_CustomFieldService.getCustomFieldsByStatementResponse_element();
          resp.rval = custFldPage;
          response.put('response_x', resp);
          
      }else if(request instanceof DC_CustomFieldService.createCustomFieldOptions_element){
          DC_CustomFieldService.createCustomFieldOptionsResponse_element resp = new DC_CustomFieldService.createCustomFieldOptionsResponse_element();
          resp.rval = new List<DC_CustomFieldService.CustomFieldOption>{custFldOption1};
          response.put('response_x', resp);
      }else if(request instanceof DC_CustomFieldService.getCustomFieldOption_element){
          DC_CustomFieldService.getCustomFieldOptionResponse_element resp = new DC_CustomFieldService.getCustomFieldOptionResponse_element();
          resp.rval = custFldOption;
          response.put('response_x', resp);
      }else if(request instanceof DC_CustomFieldService.createCustomFields_element){
          DC_CustomFieldService.createCustomFieldsResponse_element resp = new DC_CustomFieldService.createCustomFieldsResponse_element();
          resp.rval = new List<DC_CustomFieldService.CustomField>{custFld1};
          response.put('response_x', resp);
      }else if(request instanceof DC_CustomFieldService.updateCustomFields_element){
          DC_CustomFieldService.updateCustomFieldsResponse_element resp = new DC_CustomFieldService.updateCustomFieldsResponse_element();
          resp.rval = new List<DC_CustomFieldService.CustomField>{custFld};
          response.put('response_x', resp);
      }else if(request instanceof DC_CustomFieldService.performCustomFieldAction_element)
          response.put('response_x', new DC_CustomFieldService.performCustomFieldActionResponse_element());  
      else if(request instanceof DC_ReportService.getReportDownloadURL_element){
	        DC_ReportService.getReportDownloadURLResponse_element resp = new DC_ReportService.getReportDownloadURLResponse_element();
	        resp.rval = 'sfid,DoubleClickId\nsfid,DoubleClickId\nsfid,DoubleClickId';
	        response.put('response_x', resp);
	    }
    return;
  }
}