// 
//  Custom Opportunity Team handler handles the DoubleClick Opportunity team members related to the 
//  the Opportunity.  The process turns the related records into an array when the target is DoubleClick
//  and turns it into related records when the target is Salesforce
//
public with sharing class DC_CustomOpportunityTeamHandler extends DC_CustomTeamHandler{

  // items to be deleted (teams that have been removed in DoubleClick)
	private List<goog_dclk_dsm__DC_DoubleClickOpportunityTeam__c> deleteItems;

  // teams to insert by opportunity team
	private Map<Id, Set<Long>> dcInsertTeamsByOppId;

  // teams to retrieve based on those found
	private Set<String> teamsForRetrieval;
	
  // DC_CustomTeamHandler methods =====================

  // when this mapping is selected, return this addition to the query that is performed
  public override string getSalesforceQueryString() {
    return '(SELECT id, goog_dclk_dsm__DC_Team__r.goog_dclk_dsm__DC_DoubleClickId__c FROM goog_dclk_dsm__DoubleClick_Opportunity_Teams__r)'; 
  }
  
  // initialize class level variables
  public override void initialize(goog_dclk_dsm__DC_ObjectMapping__c objMapping, List<goog_dclk_dsm__DC_FieldMapping__c> fldMappings, DC_GenericWrapper src, DC_GenericWrapper tgt) {
  	super.initialize(objMapping, fldMappings, src, tgt);
  	deleteItems = new List<goog_dclk_dsm__DC_DoubleClickOpportunityTeam__c>();
  	dcInsertTeamsByOppId = new Map<Id, Set<Long>>();
  	teamsForRetrieval = new Set<String>();
  }
  
  // process the field that was passed.  
  public override void processField(goog_dclk_dsm__DC_FieldMapping__c currentField, DC_GenericWrapper source, DC_GenericWrapper target) {
    // bi-directional routing
  	if (toSalesforce) { 
      processFieldDCtoSF(currentField, source, target);
  	} else {
  		processFieldSFtoDC(currentField, source, target);
  	} 
  }

  public override void finish() {
    super.finish();

    // no actions to take place if the target is not salesforce -- all updates processed
    // as part of the processField method
    if (!toSalesforce) {
      return;
    }

    // create the means to lookup the teams by the doubleclick team id (goog_dclk_dsm__dc_doubleclickid__c)
    Map<Long, Id> teamIdsByDCTeamId = new Map<Long, Id>();
    for (goog_dclk_dsm__DC_Team__c team : [SELECT id, goog_dclk_dsm__DC_DoubleClickId__c FROM goog_dclk_dsm__DC_Team__c WHERE goog_dclk_dsm__DC_DoubleClickId__c IN :teamsForRetrieval]) {
      teamIdsByDCTeamId.put(long.valueOf(team.goog_dclk_dsm__DC_DoubleClickId__c), team.Id);
    }

    List<goog_dclk_dsm__DC_DoubleClickOpportunityTeam__c> inserts = new List<goog_dclk_dsm__DC_DoubleClickOpportunityTeam__c>();
    
    // loop through each of the opportunities, create inserts for all team members for those opportunities
    // at this point, the items being looped through are only those that need to be inserted
    for (Id oppId : dcInsertTeamsByOppId.keySet()) {
      for (Long dcTeamId : dcInsertTeamsByOppId.get(oppId)) {
        inserts.add(new goog_dclk_dsm__DC_DoubleClickOpportunityTeam__c(goog_dclk_dsm__DC_Team__c = teamIdsByDCTeamId.get(dcTeamId), goog_dclk_dsm__DC_Opportunity__c = oppId));
      }
    }

    // perform dml    
    DC_SalesforceDML.getInstance().registerInsert(inserts);
    DC_SalesforceDML.getInstance().registerDelete(deleteItems);
  }  

  // END DC_CustomTeamHandler methods =====================
  
  private void processFieldDCtoSF(goog_dclk_dsm__DC_FieldMapping__c currentField, DC_GenericWrapper source, DC_GenericWrapper target) {
  	Opportunity o = (Opportunity)target.getCurrentObject();
  	//Set<Long> dcTeamIds = new Set<Long>(((DC_ProposalService.Proposal) target.getCurrentObject()).appliedTeamIds);
  	Set<Long> dcTeamIds;
  	Set<Long> insertDCIds;
  	Set<Long> deleteDCIds;
  	if(((DC_ProposalService.Proposal) source.getCurrentObject()).appliedTeamIds != null){
  		dcTeamIds = new Set<Long>(((DC_ProposalService.Proposal) source.getCurrentObject()).appliedTeamIds);
  	}
  	
  	Map<Long, goog_dclk_dsm__DC_DoubleClickOpportunityTeam__c> existingTeams = getOppTeamByDCTeamId(o);
  	
  	// delete list is whatever is remaining in existing after removing incoming ids 
  	if(existingTeams != null && existingTeams.size() > 0){
  		deleteDCIds = existingTeams.keySet().clone();
  		deleteDCIds.removeAll(dcTeamIds);
  		updateDeleteItemsList(deleteDCIds, existingTeams);
  	}
  	if(dcTeamIds != null && dcTeamIds.size() > 0){
  		insertDCIds = dcTeamIds.clone();
  	}
  	
  	// insert list is whatever doesn't exist in the target
  	if(insertDCIds != null && insertDCIds.size() > 0){
	  	insertDCIds.removeAll(existingTeams.keySet());
	    updateInsertItems(o.Id, insertDCIds);
  	}

  }

  // update class level variables teamsForRetrieval and dcInsertTeamsByOppId to be used
  // in the finish method
  private void updateInsertItems(Id oppId, Set<Long> insertDCIds) {
    for (Long l : insertDCIds) {
      teamsForRetrieval.add(string.valueOf(l));
    }
    dcInsertTeamsByOppId.put(oppId, insertDCIds);
  }
  
  // add items to our list for deletion
  private void updateDeleteItemsList(Set<Long> delIds, Map<Long, goog_dclk_dsm__DC_DoubleClickOpportunityTeam__c> existingTeams) {
  	for (Long l : delIds) {
  		deleteItems.add(existingTeams.get(l));
  	}
  }
  
  // process the field from salesforce to doubleclick
  // take the source (sf), collect the teams and create the necessary array
  private void processFieldSFtoDC(goog_dclk_dsm__DC_FieldMapping__c currentField, DC_GenericWrapper source, DC_GenericWrapper target) {
    Opportunity o = (Opportunity)source.getCurrentObject();
    
    List<Long> teamIds = new List<Long>();
    for (goog_dclk_dsm__DC_DoubleClickOpportunityTeam__c team : o.goog_dclk_dsm__DoubleClick_Opportunity_Teams__r) { 
      teamids.add(long.valueOf(team.goog_dclk_dsm__dc_team__r.goog_dclk_dsm__DC_DoubleClickId__c));     
    }
    ((DC_ProposalService.Proposal) target.getCurrentObject()).appliedTeamIds = teamIds;
  }  
  
  // get a map of doubleclick team records by doubleclick id
  private Map<Long, goog_dclk_dsm__DC_DoubleClickOpportunityTeam__c> getOppTeamByDCTeamId(Opportunity o) {
  	Map<Long, goog_dclk_dsm__DC_DoubleClickOpportunityTeam__c> retMap = new Map<Long, goog_dclk_dsm__DC_DoubleClickOpportunityTeam__c>(); 
  	for (goog_dclk_dsm__DC_DoubleClickOpportunityTeam__c team : o.goog_dclk_dsm__DoubleClick_Opportunity_Teams__r) { 
  		retMap.put(long.valueOf(team.goog_dclk_dsm__dc_team__r.goog_dclk_dsm__dc_doubleclickid__c), team);
  	}
  	
  	return retMap;
  }
  

}