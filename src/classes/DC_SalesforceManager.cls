// 
// (c) 2014 Appirio
//
// DC_SalesforceManager
// base class for salesforce manager classes
//
public virtual with sharing class DC_SalesforceManager {
  
  public void updateCurrent(DC_GenericWrapper obj) {
  	DC_SalesforceDML.getInstance().registerUpdate((SObject)obj.getCurrentObject());
  }

  public void updateAll(DC_GenericWrapper obj) {
    DC_SalesforceDML dml = DC_SalesforceDML.getInstance();
    dml.registerUpdate(dml.objectListToSObjectList(obj.getObjects()));  	
  }
    
  public void updateSFRecords(DC_GenericWrapper accounts) {
  	DC_SalesforceDML dml = DC_SalesforceDML.getInstance();
    dml.registerUpdate(dml.objectListToSObjectList(accounts.getObjects()));
  }

}