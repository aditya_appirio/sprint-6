/**
 */
@isTest
private class Test_DC_SFTeamDCTeamConnector {

	static testMethod void testTeamConnector() {
		goog_dclk_dsm__DC_ObjectMapping__c dcobjMapping1 = DC_TestUtils.createObjectMapping('Team', 'Team', 'goog_dclk_dsm__DC_Team__c', true);
		List<goog_dclk_dsm__DC_FieldMapping__c> fldMappings = new List<goog_dclk_dsm__DC_FieldMapping__c>();
    
    fldMappings.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId'));
    insert fldMappings;
    
    
		goog_dclk_dsm__DC_Team__c sfTeam = new goog_dclk_dsm__DC_Team__c(name = 'Team 1', goog_dclk_dsm__DC_DoubleClickId__c = '32193392');
    insert sfTeam;
		Test.startTest();
		Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplTeamService());
		DC_SFTeamDCTeamConnector connInstance = DC_SFTeamDCTeamConnector.getInstance();
		DC_Connector.DC_DoubleClickPageProcessingResult res = connInstance.syncTeamsByPage(DateTime.now(), 10, 0);
		System.assert(res != null);
		System.assertEquals(res.totalResultSetSize , 2);
		Test.stopTest();
	}
}