// 
// (c) 2014 Appirio
//
// DC_AccountCompanyConnector
//
// 12 Feb 2014    Ankit Goyal (JDC)      Original
//
public with sharing class DC_ContactContactConnector extends DC_Connector {
  public static boolean connectorSyncUpdate = false;  
  private static DC_ContactContactConnector instance;
  
  //constructor
  private DC_ContactContactConnector(){
    super();
  }
  public static DC_ContactContactConnector getInstance() {
    if(instance == null) {
      instance = new DC_ContactContactConnector();
    } 
    return instance;
  }
  
  // contact mapper instance
  private DC_ContactMapper contactMapper {
    get {
      if(contactMapper == null) {
        contactMapper = DC_MapperFactory.getInstance().getContactMapper();
      }
      return contactMapper;
    }
    set;
  }
  
   // DoubleClick contact manager instance 
  public DC_DoubleClickContactManager dcContactManager {
    get {
      if(dcContactManager == null) {
        dcContactManager  = managerFactory.getContactManager();
      }
      return dcContactManager ;
    }
    set;
  }
  
  // Salesforce contact manager isntance
  private DC_SalesforceContactManager contactManager {
    get {
      if(contactManager == null) {
        contactManager = managerFactory.getSalesforceContactManager();
      }
      return contactManager;
    }
    set;
  }
  
  // Sync the Contacts passed with DoubleClick -- update only.  
  // Records that don't exist get ignored
  // contactList    List<Contact>   Contacts to sync with DoubleClick
  public void syncExistingSF_Records(List<Contact> contactList) {
    DC_GenericWrapper contactsToSync = contactMapper.wrap(contactList);

    if(contactsToSync.size() > 0) {
      contactsToSync = contactsToSync.getExistingRecords();

      if(contactsToSync.size() > 0) {
        syncSFRecordsToDCRecords(contactsToSync, DC_Constants.CONTACT_CONTEXT);
      }
    }
  }
  
  
  // sync the salesforce contacts to the DoubleClick contacts
  public DC_GenericWrapper syncSFContactsToDCContacts(List<Contact> contactList) {
    connectorSyncUpdate = true;
    DC_GenericWrapper connectorQueriedContacts = contactManager.getContacts(contactList); 
     
    DC_GenericWrapper wrap = syncSFRecordsToDCRecords(connectorQueriedContacts, DC_Constants.CONTACT_CONTEXT);
    return wrap;
  }
  
  // find companies by matching on name of account that is provided
  // account  Account  account whose name will be used to find a matching companys
  public DC_GenericWrapper findMatchingContacts(Contact contact) {
    return dcContactManager.getMatchingContacts(
        contact.goog_dclk_dsm__DC_AccountDoubleClickId__c, contact.Name, contact.Email
      );
  }
  public Set<String> findMatchingContacts(List<String> companyIds, List<String> names, List<String> emails) {
    DC_GenericWrapper matchingContactsWrapper = dcContactManager.getMatchingContacts1(companyIds, names, emails);
    Set<String> dcContactIds = dcContactManager.getDCRecordsIds(matchingContactsWrapper);
    return dcContactIds;
  }

  // link the Salesforce Account and the DoubleClick Company
  // contact    Contact           Salesforce contact to link
  // dcContact  DC_GenericWrapper wrapper containing DoubleClick contact to link against
  public void linkSFContactsAndDCContacts(List<Contact> contacts, DC_GenericWrapper dcContact) { 
    connectorSyncUpdate = true;
    DC_GenericWrapper wrappedContacts = contactMapper.wrap(contacts);
    System.debug('wrappedContacts::' + wrappedContacts);
    System.debug('dcContact::' + dcContact);
    contactMapper.linkSFRecordsAndDCRecords(wrappedContacts, dcContact);

    dcContactManager.updateDCRecords(dcContact);
    contactManager.updateAll(wrappedContacts);
  }  

  // ** DEPRECATED for 201403 API ====================================
  /*  
  
  // link the Salesforce Account and the DoubleClick Company
  // contact    Contact           Salesforce contact to link
  // dcContact  DC_GenericWrapper wrapper containing DoubleClick contact to link against
  public void linkSFContactAndDCContact(Contact contact, DC_GenericWrapper dcContact) {
    DC_GenericWrapper wrappedContact = contactMapper.wrap(contact);
    wrappedContact.next();
    contactMapper.linkSFRecordAndDCRecord(wrappedContact, dcContact);

    dcContactManager.updateContact(dcContact);
    contactManager.updateCurrent(wrappedContact);
  }
  */
  // END DEPRECATED ====================================
}