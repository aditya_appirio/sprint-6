//
//
//  Class for managing deferred DML.  This class stores as commands the dml to be performed,
//  which is finally executed with the flush.  Purpose is to manage Salesforce's inability 
//  to execute callouts following dml in the context of a single invocation.
//
//  Brandon Raines & JDC  20140206
//
public without sharing class DC_SalesforceDML {
	// single instance of this class
	private static DC_SalesforceDML instance;
	
	public DC_SalesforceDMLCommand lastExceptionCommand { get; set; }
	
	// the command queue that is being maintained
	private List<DC_SalesforceDMLCommand> commandQueue = new List<DC_SalesforceDMLCommand>(); 
	
	// singleton implementation
	public static DC_SalesforceDML getInstance() {
		if (instance == null) 
		  instance = new DC_SalesforceDML();
		  
		return instance; 
	}
	
	// flush method to process all DML 
	// commandQueue removes processed items to ensure that, if additional processing
	// is required due to an exception, items aren't processed a second time
	public void flush() {
    DC_SalesforceDMLCommand current;
    List<DC_SalesforceDMLCommand> errorCommands = new List<DC_SalesforceDMLCommand>(); 
    System.debug('## Command Queue Size: ' + commandQueue.size());
		try {
			while (commandQueue.size() > 0) {
			  current = commandQueue.remove(0);
			  current.executeDML();
			  
			  current.executed = true;
			  if (!(current.successful = !current.hasErrors)) {
          errorCommands.add(current);
			  } 
			}
		} catch (Exception e) {
			lastExceptionCommand = current;
			current.executed = true;
			current.successful = false;
			current.lastException = e;
			errorCommands.add(current);
      DC_ConnectorLogger.log('DML Execution', e, current.records);
      throw new DC_SalesforceDMLException('DML Exception', e, errorCommands);
		}
		
		if (errorCommands.size() > 0) {
		  throw new DC_SalesforceDMLException('DML Exception', errorCommands);
		}
	}
	
  public List<sObject> objectListToSObjectList(List<Object> sObjectRecords) {
  	List<sObject> retList = new List<sObject>();
  	for (Object o : sObjectRecords) {
      retList.add((sObject)o);
  	}  	
  	return retList;
  }	
	
	// provide a method to create a list of records from 
	// a single record
	// record  sObject   Record to create list around
	private List<sObject> createList(sObject record){
    if (record == null) {
      return null;
    }
    List<sObject> records = new List<sObject>();
    records.add(record);
    return records;
  }
  
  // register insert
  // records  List<sObject>   records to insert
  public DC_SalesforceDMLStatus registerInsert(List<sObject> records) {
  	if (records == null || records.size() == 0) {
  		return null;
  	}
  	DC_SalesforceDMLCommand cmd = new DC_SalesforceDMLInsert(records);
  	commandQueue.add(cmd);
  	return cmd;
  }
  
  // insert override for single object
  // record   sObject  record to insert 
  public DC_SalesforceDMLStatus registerInsert(sObject record) {
    return registerInsert(createList(record));
  }
  
  // register update
  // records  List<sObject>   records to update
  public DC_SalesforceDMLStatus registerUpdate(List<sObject> records) {
  	System.debug('records:::' + records);
  	if (records == null || records.size() == 0) {
      return null;
    }
    DC_SalesforceDMLCommand cmd = new DC_SalesforceDMLUpdate(records);
    commandQueue.add(cmd);
    return cmd;
  }
  
  // update override for single object
  // record   sObject  record to update
  public DC_SalesforceDMLStatus registerUpdate(sObject record) {
    return registerUpdate(createList(record));
  }
  
  
  // register update
  // records  List<sObject>   records to update
  public DC_SalesforceDMLStatus registerUpsert(List<sObject> records) {
    if (records == null || records.size() == 0) {
      return null;
    }
    DC_SalesforceDMLCommand cmd = new DC_SalesforceDMLUpsert(records);
    commandQueue.add(cmd);
    return cmd;
  }
  
  // register update
  // records  List<sObject>   records to update
  public DC_SalesforceDMLStatus registerUpsert(List<sObject> records, Schema.sObjectField externalKeyField) {
    if (records == null || records.size() == 0) {
      return null;
    }
    DC_SalesforceDMLCommand cmd = new DC_SalesforceDMLUpsert(records, externalKeyField);
    commandQueue.add(cmd);
    return cmd;
  }  
  
  // update override for single object
  // record   sObject  record to update
  public DC_SalesforceDMLStatus registerUpsert(sObject record) {
    return registerUpsert(createList(record));
  }  
  
  // register delete
  // records  List<sObject>   records to delete  
  public DC_SalesforceDMLStatus registerDelete(List<sObject> records) {
  	if (records == null || records.size() == 0) {
      return null;
    }
    DC_SalesforceDMLCommand cmd = new DC_SalesforceDMLDelete(records);
    commandQueue.add(cmd);
    return cmd;
  }
  
  // delete override for single object
  // record   sObject  record to delete    
  public DC_SalesforceDMLStatus registerDelete(sObject record) {
    return registerDelete(createList(record));
  }
  
  public interface DC_SalesforceDMLStatus {
  	boolean getExecuted();
  	boolean getSuccessful();
	
  	object[] getResults();

    List<sObject> getInsertRecords();
    List<sObject> getUpdateRecords();  
  	List<Database.SaveResult> getInsertResults();
  	List<Database.SaveResult> getUpdateResults();
    List<sObject> getSuccessfulInsertRecords();
    List<sObject> getSuccessfulUpdateRecords();
    List<sObject> getSuccessfulDeleteRecords();
  	
  	integer getSuccessCount();
  	integer getErrorCount();
  	
  	List<sObject> getRecords();
  	Exception getLastException();
  }
  
  /*************************************************************************
  * DML COMMAND CLASSES
  *************************************************************************/

  // abstract class defining the generalized DML Command  
  public abstract class DC_SalesforceDMLCommand implements DC_SalesforceDMLStatus {
  	private List<sObject> insertRecords;
  	private List<sObject> updateRecords;
    private List<sObject> successfulInsertRecords;
    private List<sObject> successfulUpdateRecords;
    private List<sObject> successfulDeleteRecords;      	
   	private List<Database.SaveResult> insertResults;
  	private List<Database.SaveResult> updateResults;
  	private integer successCount;
  	
  	public Schema.sObjectType objectType { get; set; }
    public boolean executed { get; set; }
    public boolean successful { get; set; }   
    public Exception lastException { get; set; }
    
    // results following execution 
    public object[] results { get; set; }
    
    // records registered for execution
    public List<sObject> records { get; set; }
    
  	//index of record errored out during DML operation
  	public Integer[] errorRecordIndex { get; set; }
  	public Boolean hasErrors = false;
  	
  	// DC_SalesforceDMLStatus interface
  	public boolean getExecuted() {
  		return executed;
  	}
  	
  	public boolean getSuccessful() {
  		return successful;
  	}
  	
  	public integer getSuccessCount() {
  		return successCount;
  	}
  	
  	public integer getErrorCount() {
  		return errorRecordIndex.size();
  	}
  	
  	public object[] getResults() {
  		return results;
  	}
  	
  	public List<sObject> getRecords() {
  		return records;
  	}

    public Exception getLastException() {
    	return lastException;
    }
        
    public List<sObject> getInsertRecords() {
      return insertRecords;
    }    
    
    public List<sObject> getUpdateRecords() {
      return updateRecords;
    }
    
    public List<sObject> getSuccessfulInsertRecords() {
      return successfulInsertRecords;
    }    
    
    public List<sObject> getSuccessfulUpdateRecords() {
      return successfulUpdateRecords;
    }
    
    public List<sObject> getSuccessfulDeleteRecords() {
      return successfulDeleteRecords;
    }    
            
    public List<Database.SaveResult> getInsertResults() {
      return insertResults;
    }    
    
    public List<Database.SaveResult> getUpdateResults() {
    	return updateResults;
    }
    
    // constructor
    // inRecords  List<sObject>   records to process when executed 
    public DC_SalesforceDMLCommand(List<sObject> inRecords) {
    	executed = false;
      records = inRecords;
      if (records != null && records.size() > 0) {
        objectType = records[0].getsObjectType();
      }
    }
  	
  	// execute dml action -- will execute specific dml
  	private void executeDML(){
  		errorRecordIndex = new List<Integer>();
      successCount = 0;

  		executeCommandDML();
  		for(Integer i=0; i<results.size(); i++){
  			if( ((results[i] instanceOf Database.SaveResult) && ((Database.SaveResult)results[i]).isSuccess()== false) || 
  			    ((results[i] instanceOf Database.DeleteResult) && ((Database.DeleteResult)results[i]).isSuccess()== false) ||
  			    ((results[i] instanceOf Database.UpsertResult) && ((Database.UpsertResult)results[i]).isSuccess()== false))
  			{
  				hasErrors = true;
  				errorRecordIndex.add(i);
  			} else {
  				successCount++;
  			}
  		} 
  	}
  	private abstract void executeCommandDML();
  }
  
  // Class to handle Insert DML
  public class DC_SalesforceDMLInsert extends DC_SalesforceDMLCommand {
  	public DC_SalesforceDMLInsert(List<sObject> inRecords) {
  		super(inRecords);
  		insertRecords = inRecords;
  	}
  	
  	// perform execution of outstanding dml
    public override void executeCommandDML() {
    	results = insertResults = database.insert(records, false);
    	successfulInsertRecords = new List<sObject>();
    	for (Integer i = 0; i < insertResults.size(); i++) {
    		if (insertResults[i].isSuccess()) { 
    			successfulInsertRecords.add(records[i]);
    		}
    	}
    }
  }
  
  // Class to handle Update DML
  private class DC_SalesforceDMLUpdate extends DC_SalesforceDMLCommand {
    public DC_SalesforceDMLUpdate(List<sObject> upRecords) {
      super(upRecords);
      updateRecords = upRecords;
    }

    // perform execution of outstanding dml
    private override void executeCommandDML() {
      results = updateResults = database.update(records, false);
      System.debug('Update Results: ' + updateResults);
      successfulUpdateRecords = new List<sObject>();
      for (Integer i = 0; i < updateResults.size(); i++) {
        if (updateResults[i].isSuccess()) {
          successfulUpdateRecords.add(records[i]);
        }
      }      
    }
  }
  
  // Class to handle Upserts.
  
  // NOTE: Upserts handled as insert / update as generic sobject cannot be used with upsert command
  
  public class DC_SalesforceDMLUpsert extends DC_SalesforceDMLCommand {
  	private Schema.sObjectField externalKeyField;
    public DC_SalesforceDMLUpsert(List<sObject> upRecords) {
      super(upRecords);
    }
    
    public DC_SalesforceDMLUpsert(List<sObject> upRecords, Schema.sObjectField keyField) {
    	super(upRecords);
    	externalKeyField = keyField;
    }
    
    private void processUpsertResults(List<Database.upsertResult> upResults) {
      successfulInsertRecords = new List<sObject>();
      successfulUpdateRecords = new List<sObject>();
      
      for (integer i = 0; i < upResults.size(); i++) {
        if (upResults[i].isSuccess()) {
        	if (upResults[i].isCreated()) {
            successfulInsertRecords.add(records[i]);
        	} else {
            successfulUpdateRecords.add(records[i]);
        	}
        }            
      }
    }

    private void executeOpportunityLineItemUpsert() {
    	List<Database.upsertResult> upResults;
      if (externalKeyField == null) {
        results = upResults = database.upsert((List<OpportunityLineItem>)records, false);
      } else {
        results = upResults = database.upsert((List<OpportunityLineItem>)records, externalKeyField, false);
      }
      processUpsertResults(upResults);
    }
    
    private void executeOpportunityTeamMemberUpsert() {
      List<Database.upsertResult> upResults;
      if (externalKeyField == null) {
        results = upResults = database.upsert((List<OpportunityTeamMember>)records, false);
      } else {
        results = upResults = database.upsert((List<OpportunityTeamMember>)records, externalKeyField, false);
      }
      processUpsertResults(upResults);
    }    
    
    private void executeDoubleClickTeamMemberUpsert() {
      List<Database.upsertResult> upResults;
      if (externalKeyField == null) {
        results = upResults = database.upsert((List<goog_dclk_dsm__DC_DoubleClickTeamMember__c>)records, false);
      } else {
      	results = upResults = database.upsert((List<goog_dclk_dsm__DC_DoubleClickTeamMember__c>)records, externalKeyField, false);
      }
      processUpsertResults(upResults);
    }        

    // perform execution of outstanding dml
    public override void executeCommandDML() {
    	successfulInsertRecords = new List<sObject>();
    	successfulUpdateRecords = new List<sObject>();
    	if (objectType != null && objectType == OpportunityLineItem.sObjectType) {
        executeOpportunityLineItemUpsert();
    	} else if (objectType != null && objectType == OpportunityTeamMember.sObjectType) {
    		executeOpportunityTeamMemberUpsert();
    	} else if (objectType != null && objectType == goog_dclk_dsm__DC_DoubleClickTeamMember__c.sObjectType) {
    		executeDoubleClickTeamMemberUpsert();
    	} else {   	
    		insertRecords = new List<sObject>();
        updateRecords = new List<sObject>();
        
        for (sObject o : records) {
	    		if (o.Id == null) {
	    			insertRecords.add(o);
	    		} else {
	    			updateRecords.add(o);
	    		}
	    	}
	    	insertResults = database.insert(insertRecords, false);
	      updateResults = database.update(updateRecords, false);
	      
	      results = new List<Database.saveResult>();
	
	      for (integer i = 0; i < insertResults.size(); i++){
	      	results.add(insertResults[i]);
	      	
	      	if (insertResults[i].isSuccess()) {
	      		successfulInsertRecords.add(insertRecords[i]);
	      	}
	      } 
	      
        for (integer i = 0; i < updateResults.size(); i++){
          results.add(updateResults[i]);
          
          if (updateResults[i].isSuccess()) {
            successfulUpdateRecords.add(updateRecords[i]);
          }
        } 	      
	    }
	    System.debug(results); 
    }
    
  }  
  
  // Class to handle Upsert DML
  private class DC_SalesforceDMLDelete extends DC_SalesforceDMLCommand {
    public DC_SalesforceDMLDelete(List<sObject> delRecords) {
      super(delRecords);
    }

    // perform execution of outstanding dml
    private override void executeCommandDML() {
      results = database.delete(records, false);
      successfulDeleteRecords = new List<sObject>();
      for (Integer i = 0; i < results.size(); i++) {
        if (((Database.deleteResult)results[i]).isSuccess()) {
          successfulDeleteRecords.add(records[i]);
        }
      }        
    }
  }
  
  public class DC_SalesforceDMLException extends Exception {
  	public List<DC_SalesforceDMLCommand> errorCommands { get; set; }
    public DC_SalesforceDMLException(String message, Exception e, List<DC_SalesforceDMLCommand> errCommands) {
    	this(message, errCommands);
    	this.initCause(e);
    }
    
    public DC_SalesforceDMLException(String message, List<DC_SalesforceDMLCommand> errCommands) {
      errorCommands = errCommands;
      addCommandExceptionsToMessage(errCommands);
    }    
    
    private void addCommandExceptionsToMessage(List<DC_SalesforceDMLCommand> errCommands) {
      
    	for (DC_SalesforceDMLCommand cmd : errCommands) { 
    		if (cmd.errorRecordIndex != null) {
    		  for (integer i : cmd.errorRecordIndex) {
    			  this.setMessage(this.getMessage() + '\n' + cmd.records[i].Id + ': ' + cmd.getResults()[i]); 
    			  System.debug('Error: ' + cmd.getResults()[i]); 
    			  System.debug('Error Record: ' + cmd.records[i]);
    		  }
    		}
    	}
    }
  }
}