public with sharing class DC_WorkflowDAO extends DC_DoubleClickDAO {

  protected override void tokenRefreshed() {
    this.workflowService = null;
  }

  public DC_WorkflowDAO(DC_DoubleClickOAuthHandler authHandler) {
    super(authHandler);
  }

  // get a configured doubleclick company interface
  private DC_WorkflowService.WorkflowRequestServiceInterfacePort workflowService {
    get {
      if(this.workflowService == null) {
        this.workflowService = DC_ServicesFactory.getWorkflowService(this.authHandler);
      } 

      return this.workflowService;
    }
    set;
  }

  public DC_WorkflowService.Statement createStatement(String query) {
    DC_WorkflowService.Statement result = new DC_WorkflowService.Statement();

    result.query = query;
    
    return result;
  }

  private class GetWorkflowRequestByStatementCommand implements DC_DoubleClickDAO.Command {
    public DC_WorkflowService.WorkflowRequestPage result;
    public DC_WorkflowDAO context;
    public DC_WorkflowService.Statement statement;
    
    public GetWorkflowRequestByStatementCommand(DC_WorkflowDAO context, DC_WorkflowService.Statement statement) {
      this.context = context;
      this.statement = statement;
    }
    
    public void execute() {
      this.result = context.workflowService.getWorkflowRequestsByStatement(statement);
    }
  }
  
  // retrieve a company using the passed in query string
  public DC_WorkflowService.WorkflowRequestPage getWorkflowRequestByStatement(String query) {
  	System.debug('query:::' + query);
    DC_WorkflowService.Statement statement = this.createStatement(query);
    System.debug('statement:::' + statement);
    GetWorkflowRequestByStatementCommand cmd = new GetWorkflowRequestByStatementCommand(this, statement);
    
    this.invokeAPI(cmd, true);
    
    return cmd.result;
  }

  private class ApproveWorkflowRequestsCommand implements DC_DoubleClickDAO.Command {
    public Integer result;
    public DC_WorkflowDAO context;
    public DC_WorkflowService.Statement statement;
    
    public ApproveWorkflowRequestsCommand(DC_WorkflowDAO context, DC_WorkflowService.Statement statement) {
      this.context = context;
      this.statement = statement;
    } 
    
    public void execute() {
      this.result = context.workflowService.performWorkflowRequestAction(new DC_WorkflowService.ApproveWorkflowApprovalRequests(), statement).numChanges;
    	System.debug('result:::' + result);
    }
  }
  
  // retrieve a company using the passed in query string
  public Integer approveWorkflowRequests(String query) {
    DC_WorkflowService.Statement statement = this.createStatement(query);
    
    ApproveWorkflowRequestsCommand cmd = new ApproveWorkflowRequestsCommand(this, statement);
    
    this.invokeAPI(cmd, true);
    
    return cmd.result;
  }

  private class RejectWorkflowRequestsCommand implements DC_DoubleClickDAO.Command {
    public Integer result;
    public DC_WorkflowDAO context;
    public DC_WorkflowService.Statement statement;
    
    public RejectWorkflowRequestsCommand(DC_WorkflowDAO context, DC_WorkflowService.Statement statement) {
      this.context = context;
      this.statement = statement;
    }
    
    public void execute() {
      this.result = context.workflowService.performWorkflowRequestAction(new DC_WorkflowService.RejectWorkflowApprovalRequests(), statement).numChanges;
    	System.debug('result:rej::' + result);
    }
  }
  
  // retrieve a company using the passed in query string
  public Integer rejectWorkflowRequests(String query) {
    DC_WorkflowService.Statement statement = this.createStatement(query);
    
    RejectWorkflowRequestsCommand cmd = new RejectWorkflowRequestsCommand(this, statement);
    
    this.invokeAPI(cmd, true);
    
    return cmd.result;
  }

}