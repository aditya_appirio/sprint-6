@isTest
private class Test_DC_QueueableBatchTeamSync {
    
    @isTest static void testQueueableBatchTeamSync() {
    
    goog_dclk_dsm__DC_ObjectMapping__c dcObjMapping = DC_TestUtils.createObjectMapping('Team', 'Team', 'goog_dclk_dsm__DC_Team__c', false);
    insert dcObjMapping;
    
    goog_dclk_dsm__DC_FieldMapping__c fldMapping1 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', 
      goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId');
    insert new List<goog_dclk_dsm__DC_FieldMapping__c>{fldMapping1};
    
    goog_dclk_dsm__DC_ConnectorApplicationSettings__c connAppSetting = DC_TestUtils.createConnAppSetting(1, true);
    List<goog_dclk_dsm__DC_ProcessingQueue__c> processQueues = new List<goog_dclk_dsm__DC_ProcessingQueue__c>();
    processQueues.add(DC_TestUtils.createDCProcessingQueue(false, false, false, 'Team Service', false));
    insert processQueues;
    

    Test.startTest();
    
    Test.setMock(WebServiceMock.class,new DC_WebServiceMockImplTeamService());
    Test.setMock(HttpCalloutMock.class,new DC_HttpCalloutMockImploAuth());
    DC_QueueableBatchTeamSync asb = new DC_QueueableBatchTeamSync();
    DC_QueueableProcess.DC_QueueableProcessParameter inparams;
    asb.initialize(inparams);
    asb.updatesyncDate(Datetime.now());
    asb.getParameterType();
    asb.getResultString();
    
    asb.getRecordsToProcessPerPage();
    asb.getBasisDate();
    asb.processRecords(1,1);

    Test.stopTest();

  }
}