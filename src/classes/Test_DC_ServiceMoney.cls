// 
// (c) 2014 Appirio
//
// Test_DC_ServiceMoney : test class for DC_ServiceMoney
//
@isTest
private class Test_DC_ServiceMoney {

  static testMethod void testDCServiceMoney() {
    DC_ServiceMoney money = new DC_ServiceMoney();
    money.setCurrencyCode('test');
    money.setMicroAmount(10000000);
    system.assertEquals('test', money.getCurrencyCode());
    system.assertEquals(10000000, money.getMicroAmount());
    
  }

}