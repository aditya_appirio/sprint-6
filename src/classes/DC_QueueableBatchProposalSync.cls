//
// queueable batch proposal sync process --
// 
public with sharing class DC_QueueableBatchProposalSync extends DC_QueueableBatchProcess implements
  Database.batchable<long>, 
  Database.AllowsCallouts,
  Database.Stateful {
      
  public DC_RetrieveProposalsParams pparams;
  
  // the count of proposals to process on each page
  public override long getRecordsToProcessPerPage() {
		return (long)goog_dclk_dsm__DC_ConnectorApplicationSettings__c.getInstance().goog_dclk_dsm__DC_ProposalsToProcessPerPage__c;
  }
    
  // DC_QueueableProcess ===============
  public override void initialize(DC_QueueableProcess.DC_QueueableProcessParameter inparams) {
    params = pparams = (DC_RetrieveProposalsParams)inparams;
  }
    
  // the basis date to use -- usually the last sync date
  public override DateTime getBasisDate() {
  	System.debug('@@ basis date: ' + DC_SyncDateManager.opportunityLastSyncDate);
    return DC_SyncDateManager.opportunityLastSyncDate;
  }  

  // kick off the batch.  batch size will always be 1 as we are processing pages of records
  public override void execute() {
    Database.executeBatch(this, 1);
  } 

  // return the type of parameter to use
  public override System.Type getParameterType() {
     return DC_RetrieveProposalsParams.class;
  }
  // Database.batchable ================
    
  // process the records that have been 
  public override DC_Connector.DC_DoubleClickPageProcessingResult processRecords(long pageSize, long pageNo) {
  	System.debug('@@ pageSize: ' + pageSize + '; pageNo: ' + pageNo);
  	return DC_OpportunityProposalConnector.getInstance().syncProposalsByPage(basisDate, pageSize, pageNo * pageSize);
  }

  // update the sync date for the 
  public override void updateSyncDate(DateTime dt) {
    DC_SyncDateManager.opportunityLastSyncDate = dt;
  }
  
  // return the result string to log
  public override String getResultString() {
    return String.format('Total Proposals Processed: {0}\nUpdated Records: {1}\nError Records: {2}', 
      new String[] { 
        (recordsProcessed == null) ? '' : recordsProcessed.format() ,
        String.valueOf(updatedRecordCount),
        String.valueOf(errorCount)     
      });
  }    
  
  // parameter class -- note that this is empty as no parameters are required
  public class DC_RetrieveProposalsParams extends DC_QueueableProcess.DC_QueueableProcessParameter { }
     
}