// 
// (c) 2014 Appirio
//
// Controller for managing Opportunity updates in DCLK based on trigger
//
//
public class DC_OpportunityTriggerController {
  public static void performExistingOpportunitySync(List<Opportunity> updatedOpportunities) {
    List<Id> opportunityIds = new List<Id>();
    
    for(Opportunity opp : updatedOpportunities) { 
      if (opp.goog_dclk_dsm__DC_DoubleClickId__c != null && !opp.goog_dclk_dsm__DC_isSold__c) {
        opportunityIds.add(opp.Id);
      }
    }
 
    if (opportunityIds.size() > 0) {
      DC_OpportunityTriggerController.syncExistingOpportunities(opportunityIds);
    }
  }
  // sync existing opportunities with DoubleClick
  // note that this is called as an async @future call as triggers cannot perform callouts
  // contactIds   List<Id>  ids of contacts to sync.
  @future(callout=true)
  public static void syncExistingOpportunities(List<Id> opportunityIds) {
    DC_OpportunityProposalConnector connector = DC_OpportunityProposalConnector.getInstance();
    
    connector.syncSFOpportunitiesToDCProposals([SELECT Id from Opportunity where id in :opportunityIds]);
  }
}