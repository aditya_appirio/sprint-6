// 
// (c) 2014 Appirio
//
// Test_DC_ProposalService : test class for DC_ProposalService 
//
// 24 Jan 2014    Anjali K (JDC)      Original
// 
@isTest
private class Test_DCProposalService {

  static testMethod void testDCProposalService() {
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplProposal());
    Test.startTest(); 
    DC_ProposalService.CommonError ce = new DC_ProposalService.CommonError();
    System.assert(ce != null);
    //DC_ProposalService.createProposal_element createPropEle = new DC_ProposalService.createProposal_element();
    DC_ProposalService.InternalApiError iae = new DC_ProposalService.InternalApiError();
    System.assert(iae != null);
    DC_ProposalService.ProposalLineItemError pliErr = new DC_ProposalService.ProposalLineItemError();
    System.assert(pliErr != null);
    
    DC_ProposalService.Authentication au = new DC_ProposalService.Authentication();
    DC_ProposalService.SetValue sv = new DC_ProposalService.SetValue();
    DC_ProposalService.StringLengthError sle = new DC_ProposalService.StringLengthError();
    
    DC_ProposalService.WorkflowActionError wfError = new DC_ProposalService.WorkflowActionError();
    DC_ProposalService.RequiredNumberError reqNumErr = new DC_ProposalService.RequiredNumberError();
    DC_ProposalService.CustomFieldValueError custFldValErr = new DC_ProposalService.CustomFieldValueError();
    DC_ProposalService.ProposalServiceInterfacePort port = new DC_ProposalService.ProposalServiceInterfacePort();
    System.assert(port != null);
    
    //Methods of ProposalServiceInterfacePort
    //DC_ProposalService.Proposal proposal = port.updateProposal(new DC_ProposalService.Proposal());
    //DC_ProposalService.Proposal createPro = port.createProposal(new DC_ProposalService.Proposal());
    DC_ProposalService.ProposalPage proposalPageVar = port.getProposalsByStatement(new DC_ProposalService.Statement());
    //DC_ProposalService.Proposal getProp = port.getProposal(Long.valueOf('12345'));
    DC_ProposalService.Proposal[] proposalArr = port.updateProposals(new List<DC_ProposalService.Proposal>{new DC_ProposalService.Proposal()});
    DC_ProposalService.Proposal[] createProposalArr = port.createProposals(new List<DC_ProposalService.Proposal>{new DC_ProposalService.Proposal()});
    DC_ProposalService.UpdateResult res = port.performProposalAction(new DC_ProposalService.ProposalAction(), new DC_ProposalService.Statement());
    
    DC_ProposalService.LineItemOperationError liOperationErr = new DC_ProposalService.LineItemOperationError();
    DC_ProposalService.DropDownCustomFieldValue ddCustFldVal = new DC_ProposalService.DropDownCustomFieldValue();
    DC_ProposalService.PrecisionError precisionErr = new DC_ProposalService.PrecisionError();
    DC_ProposalService.UnarchiveProposals unarchiveProposal = new DC_ProposalService.UnarchiveProposals();
    DC_ProposalService.InvalidUrlError invalidURLErr = new DC_ProposalService.InvalidUrlError();
    DC_ProposalService.BaseCustomFieldValue basecustFldVal = new DC_ProposalService.BaseCustomFieldValue();
    DC_ProposalService.ArchiveProposals archiveProposal = new DC_ProposalService.ArchiveProposals();
    DC_ProposalService.ProposalAction propsalAction = new DC_ProposalService.ProposalAction();
    DC_ProposalService.SalespersonSplit salesPersonSplit = new DC_ProposalService.SalespersonSplit();
    DC_ProposalService.ProposalPage propPage = new DC_ProposalService.ProposalPage();
    DC_ProposalService.RetractProposals retractProposal = new DC_ProposalService.RetractProposals();
    DC_ProposalService.ProposalActionError propActionErr = new DC_ProposalService.ProposalActionError();
    DC_ProposalService.ProductError prodErr = new DC_ProposalService.ProductError();
    DC_ProposalService.ForecastError forecastErr = new DC_ProposalService.ForecastError();
    DC_ProposalService.RangeError rangeErr = new DC_ProposalService.RangeError();
    DC_ProposalService.Money money = new DC_ProposalService.Money();
    DC_ProposalService.RequiredCollectionError requiredCollectionErr  = new DC_ProposalService.RequiredCollectionError();
    DC_ProposalService.BillingError billErr = new DC_ProposalService.BillingError();
    DC_ProposalService.ProposalCompanyAssociation propCompAssociation = new DC_ProposalService.ProposalCompanyAssociation();
    DC_ProposalService.UpdateResult updateRes = new DC_ProposalService.UpdateResult();
    DC_ProposalService.ProposalError propErr = new DC_ProposalService.ProposalError();
    DC_ProposalService.SubmitProposalsForApproval propforApproval = new DC_ProposalService.SubmitProposalsForApproval();
    
    DC_ProposalService.BooleanValue boolVal = new DC_ProposalService.BooleanValue();
    DC_ProposalService.AppliedLabel al = new DC_ProposalService.AppliedLabel();
    //DC_ProposalService.updateProposalResponse_element updateCompRes = new DC_ProposalService.updateProposalResponse_element();
    DC_ProposalService.UniqueError ue = new DC_ProposalService.UniqueError();
    DC_ProposalService.RequiredError re = new DC_ProposalService.RequiredError();
    DC_ProposalService.ProposalError compErr = new DC_ProposalService.ProposalError();
    DC_ProposalService.FeatureError fe = new DC_ProposalService.FeatureError();
    DC_ProposalService.AuthenticationError ae = new DC_ProposalService.AuthenticationError();
    DC_ProposalService.PermissionError pe = new DC_ProposalService.PermissionError();
    DC_ProposalService.PublisherQueryLanguageSyntaxError PQLSyntaxErr = new DC_ProposalService.PublisherQueryLanguageSyntaxError();
    //DC_ProposalService.getProposal_element getcompEle = new DC_ProposalService.getProposal_element();
    DC_ProposalService.String_ValueMapEntry valMapEntry = new DC_ProposalService.String_ValueMapEntry();
    DC_ProposalService.Value val = new DC_ProposalService.Value(); 
    DC_ProposalService.OAuth oAuth = new DC_ProposalService.OAuth();
    //DC_ProposalService.createProposal_element compElement = new DC_ProposalService.createProposal_element();
    DC_ProposalService.ProposalPage compPage = new DC_ProposalService.ProposalPage();
    DC_ProposalService.TypeError typeErr = new DC_ProposalService.TypeError();
    DC_ProposalService.NumberValue numVal = new DC_ProposalService.NumberValue();
    DC_ProposalService.SoapResponseHeader soapRespHeader = new DC_ProposalService.SoapResponseHeader();
    DC_ProposalService.NotNullError nne = new DC_ProposalService.NotNullError();
    //DC_ProposalService.createProposalResponse_element createCompResEle = new DC_ProposalService.createProposalResponse_element();
    DC_ProposalService.DateTimeValue dtTimeVal = new DC_ProposalService.DateTimeValue();
    DC_ProposalService.Statement statement = new DC_ProposalService.Statement();
    DC_ProposalService.PublisherQueryLanguageContextError PQLContentErr = new DC_ProposalService.PublisherQueryLanguageContextError();
    DC_ProposalService.ApiError apiErr = new DC_ProposalService.ApiError();
    DC_ProposalService.Date_x dateX = new DC_ProposalService.Date_x();
    DC_ProposalService.SoapRequestHeader soapReqHeader = new DC_ProposalService.SoapRequestHeader();
    DC_ProposalService.TeamError teamErr = new DC_ProposalService.TeamError();
    DC_ProposalService.ApiException apiExce = new DC_ProposalService.ApiException();
    DC_ProposalService.Proposal compV = new DC_ProposalService.Proposal();
    DC_ProposalService.QuotaError quotaErr = new DC_ProposalService.QuotaError();
    DC_ProposalService.DateValue dtVal = new DC_ProposalService.DateValue();
    DC_ProposalService.ApplicationException appExcep = new DC_ProposalService.ApplicationException();
    DC_ProposalService.ApiVersionError apiVersionErr = new DC_ProposalService.ApiVersionError();
    DC_ProposalService.LabelEntityAssociationError lblEntityAssErr = new DC_ProposalService.LabelEntityAssociationError();
    DC_ProposalService.ServerError serverErr = new DC_ProposalService.ServerError();
    DC_ProposalService.DateTime_x datetimeX = new DC_ProposalService.DateTime_x();
    DC_ProposalService.StatementError statementErr = new DC_ProposalService.StatementError();
    DC_ProposalService.TextValue textVal = new DC_ProposalService.TextValue();
    Test.stopTest();
  }
}