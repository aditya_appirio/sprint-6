// 
// (c) 2014 Appirio
//
// Test_DC_DoubleClickUserManager : test class for DC_DoubleClickUserManager 
//
// 14 March 2014   Karun Kumar(JDC) Original
//
@isTest 
private class Test_DC_DoubleClickUserManager {

  static testMethod void myUnitTest() {
    goog_dclk_dsm__DC_ObjectMapping__c dcObjMapping = DC_TestUtils.createObjectMapping('User', 'User', true);
    
    goog_dclk_dsm__DC_FieldMapping__c fldMapping1 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId');
    insert new List<goog_dclk_dsm__DC_FieldMapping__c>{fldMapping1};
    
    Profile sysAdminProfile = [Select Id From Profile Where Name ='System Administrator'];
    
    User userObj = new User(FirstName='test first', LastName='test last', Alias='Mr.', Email='test@gmail.com', 
                            UserName='testuser@gmail.com.testClass', ProfileId =sysAdminProfile.Id, 
                            TimeZoneSidKey='Asia/Kolkata', LocaleSidKey='en_US', LanguageLocaleKey='en_US', EmailEncodingKey='ISO-8859-1');
    insert userObj;
    
    system.runAs(userObj) {
      Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplUserService());
      Test.startTest();
      DC_DoubleClickOAuthHandler oAuthHandler = new DC_DoubleClickOAuthHandler(Userinfo.getUserId());     
      DC_DoubleClickUserManager dcumObj = new DC_DoubleClickUserManager(oAuthHandler);  
      
      system.debug('Point 1');
          
      DC_GenericWrapper gwObj = dcumObj.getUsersUpdatedSinceLastSync(DateTime.now(), 10, 0);
      system.assertEquals(gwObj <> null, true);
      system.debug('Point 2');
      
      gwObj = dcumObj.getUsersUpdatedSinceLastSync(Datetime.now().addDays(-1));
      system.assertEquals(gwObj <> null, true);
      system.debug('Point 3');
      
      //gwObj = dcumObj.getUser(1234);
      //system.assertEquals(gwObj <> null, true);
      //system.debug('Point 4');
      
      gwObj = dcumObj.getUsersUpdatedSinceLastSync(DateTime.now(), 10, 0);
      
      //dcumObj.updateUser(gwObj);
      
      gwObj = dcumObj.getUnmatchedUsersByName('test');
      system.assertEquals(gwObj <> null, true);
      system.debug('Point 5');
      
      gwObj = dcumObj.getDCRecords(gwObj);
      system.assertEquals(gwObj <> null, true);
      system.debug('Point 6');
      
      gwObj = dcumObj.createDCRecords(gwObj);
      system.assertEquals(gwObj <> null, true);
      system.debug('Point 7');
      
      dcumObj.updateDCRecords(gwObj);
      system.assertEquals(gwObj <> null, true);
      system.debug('Point 8');
      
      Test.stopTest();
    }    
  }
}