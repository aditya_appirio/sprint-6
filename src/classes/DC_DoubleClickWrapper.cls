// 
// (c) 2014 Appirio
//
// DC_DoubleClickWrapper
// DC Connector
//
// 20 Jan 2014    Anjali K (JDC)      Original
//
public abstract with sharing class DC_DoubleClickWrapper extends DC_GenericWrapper {

  public override Boolean isSalesforce() {
    return false;
  }

  public override Boolean isDoubleClick() {
    return true;
  }

  public DC_DoubleClickWrapper(goog_dclk_dsm__DC_ObjectMapping__c objectMapping, List<goog_dclk_dsm__DC_FieldMapping__c> fieldMappings, List<Object> objects) {
    super(objectMapping, fieldMappings, objects);
    System.debug('objectMapping::' + objectMapping);
  }
  
  protected String getDoubleClickFieldName(String fieldName) {
  	System.debug('fieldNameMap::' + fieldNameMap);
    goog_dclk_dsm__DC_FieldMapping__c mapping = this.fieldNameMap.get(fieldName.toLowerCase());
    
    if(mapping != null) {
      return mapping.goog_dclk_dsm__DC_DoubleClickFieldName__c;
    } else {
      throw new DC_ConnectorException(DC_Constants.MAPPING_STR + fieldName + DC_Constants.NOT_FOUND_STR);
    }
  }

  protected Long getCustomFieldId(String fieldName) {
    for(goog_dclk_dsm__DC_FieldMapping__c fieldMapping : fieldMappings) {
      if(fieldMapping.goog_dclk_dsm__DC_DoubleClickFieldName__c != null && fieldMapping.goog_dclk_dsm__DC_DoubleClickFieldName__c.equalsIgnoreCase(fieldName)) {
        return fieldMapping.goog_dclk_dsm__DC_DoubleClickCustomFieldID__c.longValue();
      }
    }

    return null;
  }
  
  //added to handle custom field value of type boolean
  protected Schema.DisplayType getCustomFieldType(String fieldName1,String sobjectType) {
  	Schema.DisplayType customfldType;
  	System.debug('------sobjectType----------' + sobjectType + '----- ' + fieldName1);
    for(goog_dclk_dsm__DC_FieldMapping__c fieldMapping : fieldMappings) {
      if(fieldMapping.goog_dclk_dsm__DC_DoubleClickFieldName__c != null && fieldMapping.goog_dclk_dsm__DC_DoubleClickFieldName__c.equalsIgnoreCase(fieldName1)) {
      	Map<String, Schema.SObjectField> M;
      	if(sobjectType == 'Opportunity')
 			M = Schema.SObjectType.Opportunity.fields.getMap();
 		else if(sobjectType == 'Account')	
 			M = Schema.SObjectType.Account.fields.getMap();
 		else if(sobjectType == 'Contact')		
 			M = Schema.SObjectType.Contact.fields.getMap();
		Schema.SObjectField field = M.get(fieldMapping.DC_SalesforceFieldName__c);
		customfldType = field.getDescribe().getType();
		System.debug('------field.getDescribe().getType()----------' + customfldType + fieldMapping.DC_SalesforceFieldName__c);
      }
    }
    return customfldType;
  }
  
  
  public abstract DC_GenericWrapper createWrapper(goog_dclk_dsm__DC_ObjectMapping__c objectMapping, List<goog_dclk_dsm__DC_FieldMapping__c> fieldMappings, List<Object> objects);

  public override DC_GenericWrapper getUnmatchedRecords(String fieldName) {
    this.gotoBeforeFirst();
    List<Object> result = new List<Object>();
    
    while(this.hasNext()) {
      this.next();
      if(this.getField(fieldName) == null) {
        result.add(this.currentObject);
      }
    }
    
    return createWrapper(this.objectMapping, this.fieldMappings, result);
  }
  
  public override DC_GenericWrapper getMatchedRecords(String fieldName) {
    this.gotoBeforeFirst();
    List<Object> result = new List<Object>();
    
    while(this.hasNext()) {
      this.next();
      if(this.getField(fieldName) != null) {
        result.add(this.currentObject);
      }
    }
    
    return createWrapper(this.objectMapping, this.fieldMappings, result);
  }
}