/**
 */
@isTest
private class Test_DC_DoubleClickTeamDAO {

	static testMethod void testDCDoubleClickTeamDAO() {
		
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new DC_HttpCalloutMockImploAuth());
		Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplTeamService());
		
    DC_DoubleClickOAuthHandler oAuthHandler = new DC_DoubleClickOAuthHandler(Userinfo.getUserId());
    
    DC_DoubleClickTeamDAO teamDAO = new DC_DoubleClickTeamDAO(oAuthHandler);
    String query = null;
    
    DC_TeamService.String_ValueMapEntry[] params = new List<DC_TeamService.String_ValueMapEntry>();
    DC_TeamService.Statement statement = teamDAO.createStatement(query, params);
    System.assert(statement != null);
     
    DC_TeamService.Team team = new DC_TeamService.Team();
    team.id = 123456;
    team.name = 'testComp';
    
    //DC_TeamService.Team[] teams = teamDAO.createTeams(new List<DC_TeamService.Team>{team});
    
    
    //teamDAO.updateTeam(team);
    
    //teamDAO.updateTeams(new List<DC_TeamService.Team>{team});
    
    query = 'WHERE Name = \'testTeam\'';
    teamDAO.getTeamsByStatement(query);
    
    //teamDAO.getTeam(123456, true);
    Test.stopTest();
	}
}