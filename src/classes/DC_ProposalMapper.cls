// 
// (c) 2014 Appirio
//
// DC_ProposalMapper
// T-246423
// 06 Feb 2014    Ankit Goyal (JDC)      Original 
//
public with sharing class DC_ProposalMapper extends DC_ObjectMapper {
  
  private static DC_ProposalMapper instance;
  
  //constructor
  private DC_ProposalMapper(){
    super();
  }
  public static DC_ProposalMapper getInstance() {
    if(instance == null) {
      instance = new DC_ProposalMapper();
    } 
    return instance;
  }
  
  // Overloaded method wrap : takes DC_ProposalService.Proposal as param and call wrap(List<DC_ProposalService.Proposal>)
  public DC_GenericWrapper wrap(DC_ProposalService.Proposal proposal) {
    List<DC_ProposalService.Proposal> proposalList = new List<DC_ProposalService.Proposal>();
    proposalList.add(proposal);
    DC_GenericWrapper result = wrap(proposalList);
    
    return result;
  }
  
  // Overloaded method wrap : takes List<DC_ProposalService.Proposal> as param and convert into DC_DoubleClickProposalWrapper instance
  public DC_GenericWrapper wrap(List<DC_ProposalService.Proposal> proposals) {
  	System.debug('proposal:::' + proposals);
    return new DC_DoubleClickProposalWrapper(this.getObjectMappingByName('Opportunity'), this.getFieldMappingsByName('Opportunity'), proposals);
  }
   
  // Overloaded method wrap : takes List<DC_ProposalService.Proposal> as param and convert into DC_DoubleClickProposalWrapper instance
  public DC_GenericWrapper wrap(DC_ProposalService.ProposalPage page) {
    DC_GenericWrapper wrapper = new DC_DoubleClickProposalWrapper(this.getObjectMappingByName('Opportunity'), this.getFieldMappingsByName('Opportunity'), page.results);
    wrapper.totalResultSetSize = page.totalResultSetSize;
    return wrapper;
  }  

  /* // Overrided method wrap : takes Proposal as param and call wrap(List<Proposal> proposals)
  public DC_GenericWrapper wrap(Opportunity opp) {
    List<Opportunity> oppList = new List<Opportunity>();
    oppList.add(opp);
    DC_GenericWrapper result = wrap(oppList);
    
    return result;
  }
  
  // Overrided method wrap : takes List<Proposal> as param and convert into DC_SalesforceWrapper instance
  public DC_GenericWrapper wrap(List<Opportunity> oppList) {
    return new DC_SalesforceWrapper(this.getObjectMappingByName('Opportunity'), this.getFieldMappingsByName('Opportunity'), oppList);
  }
  */
  
  //Create new DC proposals for SF Opportunities
  public DC_GenericWrapper createNewDCProposals(DC_GenericWrapper proposals) {
    DC_GenericWrapper dc_proposals = this.wrap(new List<DC_ProposalService.Proposal>());
    proposals.gotoBeforeFirst();
    
    while(proposals.hasNext()) {
      proposals.next();
      dc_proposals.add(new DC_ProposalService.Proposal());
      dc_proposals.gotoLast();
      //dc_proposals.updateFields(proposals);
      updateFields(proposals, dc_proposals);
    } 
    return dc_proposals;
  }

  // Links the current company with the current account in the wrappers
  public void linkOpportunityAndProposal(DC_GenericWrapper opportunity, DC_GenericWrapper proposal) {
      opportunity.setField('DoubleClickId', proposal.getField('DoubleClickId'));
      proposal.setField('sfid', proposal.getField('sfid'));
  }

  public override void updateField(DC_GenericWrapper target, goog_dclk_dsm__DC_FieldMapping__c fieldMapping, Object newValue) {
    if(fieldMapping.name.equalsIgnoreCase('probability')) {
      if(newValue instanceof Double) {
        newValue = ((Double)newValue).round().intValue();
      }

      newValue = ((Integer)newValue / 10) * 10000;
    } else if (fieldMapping.name.equalsIgnoreCase('Agency Commission')) {
      newValue = Decimal.valueOf((long)newValue / 1000);
    } else if (fieldMapping.name.equalsIgnoreCase('Exchange Rate')) {
      decimal d = 10000000000.0;
      newValue = decimal.valueOf(((long)newValue)) / d;
    }

    super.updateField(target, fieldMapping, newValue);
  }
  
  public DateTime getLatestUpdateDate(DC_DoubleClickProposalWrapper wrapper) {
  	if (wrapper.size() == 0)
      return null;
      
  	wrapper.gotoBeforeFirst();
  	
  	DateTime curMax = DateTime.newInstance(1901, 1, 1);
  	DateTime cur;
  	while (wrapper.hasNext()) {
      wrapper.next();
      cur = ((DC_ServiceDateTime)((DC_ProposalService.Proposal)wrapper.getCurrentObject()).lastModifiedDateTime).getSalesforceDateTime();

      if (cur > curMax) {
        curMax = cur;
      }
  	}
  	
  	return curMax;
  }
  
  
  // of the wrappers passed, provide those that are pending approval or are approved.
  public DC_GenericWrapper getApprovedProposals(DC_GenericWrapper wrapper) {
  	DC_GenericWrapper retwrap = this.wrap(new List<DC_ProposalService.Proposal>());
    wrapper.gotoBeforeFirst();
    while (wrapper.hasNext()) {
      wrapper.next();
      if ((string)wrapper.getField('status') == 'APPROVED' || (string)wrapper.getField('status') == 'PENDING_APPROVAL') {
      	retwrap.add(wrapper.getCurrentObject());
      }
    }
    return retwrap;
  } 
  
  //
}