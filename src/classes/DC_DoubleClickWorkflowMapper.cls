public with sharing class DC_DoubleClickWorkflowMapper extends DC_ObjectMapper {

  private static DC_DoubleClickWorkflowMapper instance;

  private DC_DoubleClickWorkflowMapper(){ 
    super();
  }
  public static DC_DoubleClickWorkflowMapper getInstance() {
    if(instance == null) {
      instance = new DC_DoubleClickWorkflowMapper();
    } 
    return instance;
  }

  public DC_GenericWrapper wrap(DC_WorkflowService.WorkflowRequest workflow) {
    List<DC_WorkflowService.WorkflowRequest> workflowList = new List<DC_WorkflowService.WorkflowRequest>();
    workflowList.add(workflow);
    DC_GenericWrapper result = wrap(workflowList);

    return result;
  }
  
  public DC_GenericWrapper wrap(List<DC_WorkflowService.WorkflowRequest> workflows) {
    return new DC_DoubleClickworkflowWrapper(this.getObjectMappingByName('workflowrequest'), this.getFieldMappingsByName('workflowrequest'), workflows);
  }

  public override DC_GenericWrapper createNewDCRecords(DC_GenericWrapper accounts) {
    throw new DC_ConnectorException('Unsupported method');
  }

  public override void linkSFRecordAndDCRecord(DC_GenericWrapper account, DC_GenericWrapper workflow) {
    throw new DC_ConnectorException('Unsupported method');
  }
}