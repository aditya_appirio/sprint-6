public abstract with sharing class DC_CustomTeamHandler implements DC_FieldHandlerFactory.DC_FieldMappingCustomHandler {
	protected goog_dclk_dsm__DC_ObjectMapping__c objectMapping { get; set; }
	protected List<goog_dclk_dsm__DC_FieldMapping__c> fieldMappings { get; set; }
	protected DC_GenericWrapper source { get; set; }
	protected DC_GenericWrapper target { get; set; }
	protected boolean toSalesforce { get; set; }
	
  public abstract string getSalesforceQueryString();
  public abstract void processField(goog_dclk_dsm__DC_FieldMapping__c currentField, DC_GenericWrapper source, DC_GenericWrapper target);
  
  public virtual void initialize(goog_dclk_dsm__DC_ObjectMapping__c objMapping, List<goog_dclk_dsm__DC_FieldMapping__c> fldMappings, DC_GenericWrapper src, DC_GenericWrapper tgt) { 
    objectMapping = objMapping;
    fieldMappings = fldMappings;
    source = src;
    target = tgt;    
    
    toSalesforce = src.isDoubleClick(); 
  }
  
  public virtual void finish() {}
  
}