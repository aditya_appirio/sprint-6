/**
 */
@isTest
private class Test_DC_DeliveryDataProcess {

	static testMethod void testDCDeliveryProcess() {
		goog_dclk_dsm__DC_ObjectMapping__c objMapping1 = new goog_dclk_dsm__DC_ObjectMapping__c(goog_dclk_dsm__DC_DoubleClickObjectName__c='Proposal', goog_dclk_dsm__DC_SalesforceObjectName__c = 'Opportunity', name = 'opportunity');
		goog_dclk_dsm__DC_ObjectMapping__c dcDelDataObjMapping = new goog_dclk_dsm__DC_ObjectMapping__c(goog_dclk_dsm__DC_DoubleClickObjectName__c='DeliveryDataReport', goog_dclk_dsm__DC_SalesforceObjectName__c = 'OpportunityLineItem', name = 'DeliveryData');
    insert new List<goog_dclk_dsm__DC_ObjectMapping__c>{objMapping1, dcDelDataObjMapping};
    goog_dclk_dsm__DC_FieldMapping__c fldMapping3 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping4 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'custom.SFDC_Opportunity_ID', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Id', Name = 'sfid');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping5 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'probabilityToClose', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Probability', Name = 'probability');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping6 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'status', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345,  Name = 'status');
    
    goog_dclk_dsm__DC_FieldMapping__c ddFldMapping1 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcDelDataObjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'Dimension.PROPOSAL_LINE_ITEM_ID', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId');
    goog_dclk_dsm__DC_FieldMapping__c ddFldMapping2 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcDelDataObjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'DimensionAttribute.LINE_ITEM_LIFETIME_IMPRESSIONS', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_LifetimeImpressions__c', Name = 'LifetimeImpressions');
    goog_dclk_dsm__DC_FieldMapping__c ddFldMapping3 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcDelDataObjMapping.id, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Id', Name = 'sfid');
    goog_dclk_dsm__DC_FieldMapping__c ddFldMapping4 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcDelDataObjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'DimensionAttribute.LINE_ITEM_GOAL_QUANTITY', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_GoalQuantity__c', Name = 'GoalQuantity');
    
    insert new List<goog_dclk_dsm__DC_FieldMapping__c>{fldMapping3, fldMapping4, fldMapping5, fldMapping6,ddFldMapping1, ddFldMapping2, ddFldMapping3, ddFldMapping4};
    
    List<goog_dclk_dsm__DC_FieldMapping__c> ddFldMappingList = new List<goog_dclk_dsm__DC_FieldMapping__c>{ddFldMapping1, ddFldMapping2, ddFldMapping3, ddFldMapping4};
    
		goog_dclk_dsm__DC_ProcessingQueue__c pQueue = DC_TestUtils.createDCProcessingQueue(false, true, false, 'Delivery Data Process', true);
		Test.startTest();
    Test.setMock(WebServiceMock.class,new DC_WebServiceMockImplProposal());
    Test.setMock(HttpCalloutMock.class,new DC_HttpCalloutMockImploAuth());
		DC_DeliveryDataProcess delDataProcess = new DC_DeliveryDataProcess();
		
		DC_QueueableProcess.DC_QueueableProcessParameter inparams = new DC_DeliveryDataProcess.DC_DeliveryDataProcessParameters();
		inparams.queueItem = pQueue;
    delDataProcess.initialize(inparams);
    System.Type typeX = delDataProcess.getParameterType();
    System.assert(typeX != null);
    delDataProcess.execute();
    List<goog_dclk_dsm__DC_ConnectorLog__c> dc = [Select id, goog_dclk_dsm__DC_Status__c from goog_dclk_dsm__DC_ConnectorLog__c];
    //System.assertEquals(dc[0].goog_dclk_dsm__DC_Status__c, 'Success');
    System.assertEquals(dc[0].goog_dclk_dsm__DC_Status__c, null);
    Test.stopTest();
	}
	
	static testMethod void testDCDeliveryProcessException() {
    
		goog_dclk_dsm__DC_ProcessingQueue__c pQueue = DC_TestUtils.createDCProcessingQueue(false, true, false, 'Delivery Data Process', true);
		Test.startTest();
    Test.setMock(WebServiceMock.class,new DC_WebServiceMockImplProposal());
    Test.setMock(HttpCalloutMock.class,new DC_HttpCalloutMockImploAuth());
		DC_DeliveryDataProcess delDataProcess = new DC_DeliveryDataProcess();
		
		DC_QueueableProcess.DC_QueueableProcessParameter inparams = new DC_DeliveryDataProcess.DC_DeliveryDataProcessParameters();
		inparams.queueItem = pQueue;
    delDataProcess.initialize(inparams);
    delDataProcess.getParameterType();
    delDataProcess.execute();
    Test.stopTest();
    //check for exception
    List<goog_dclk_dsm__DC_ConnectorLog__c> dc = [Select id from goog_dclk_dsm__DC_ConnectorLog__c];
    System.assert(dc.size() > 0);
	}
}