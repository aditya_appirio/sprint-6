public with sharing class DC_DeliveryDataMapper extends DC_ObjectMapper {
  
  private static DC_DeliveryDataMapper instance;

  private DC_DeliveryDataMapper() {
    super();
  }

  public static DC_DeliveryDataMapper getInstance() {
    if(instance == null) {
      instance = new DC_DeliveryDataMapper();
    }

    return instance;
  }

  public DC_GenericWrapper wrap(String report) {
    return new DC_DeliveryDataWrapper(this.getObjectMappingByName('DeliveryData'), this.getFieldMappingsByName('DeliveryData'), report);
  }
}