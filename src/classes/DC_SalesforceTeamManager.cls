public with sharing class DC_SalesforceTeamManager {
	// enforce singleton 
	private DC_TeamMapper mapper {
    get {
      if(mapper == null) {
        mapper = DC_TeamMapper.getInstance();
      }
      return mapper;
    }
    set; 
  }

  // upsert teams into the DC_Teams object
  public DC_SalesforceDML.DC_SalesforceDMLStatus upsertTeams(DC_GenericWrapper teams) {
    List<goog_dclk_dsm__DC_Team__c> teamList = new List<goog_dclk_dsm__DC_Team__c>();
    
    for(Object u : teams.getObjects()) {
      teamList.add((goog_dclk_dsm__DC_Team__c)u);
    }

    DC_SalesforceDML.DC_SalesforceDMLStatus status;
    if(teamList.size() > 0) {
      status = DC_SalesforceDML.getInstance().registerUpsert(teamList);
    }
    
    return status;
  }  

  // takes wrapper with dc records and gets the related dc_team records
  public DC_GenericWrapper getMatchingSalesforceRecords(DC_GenericWrapper wrapper) {
    
    wrapper.gotoBeforeFirst();
    List<String> dcIds = new List<String>();
    
    while(wrapper.hasNext()) {
      wrapper.next();
      dcIds.add(String.valueOf(wrapper.toLong(wrapper.getField('doubleclickid'))));
    }

    return mapper.wrap(
      (List<goog_dclk_dsm__DC_Team__c>)database.query('SELECT ' + this.mapper.getCommaSeparatedSalesforceFieldList('Team') + ' FROM goog_dclk_dsm__DC_Team__c WHERE goog_dclk_dsm__dc_doubleclickid__c IN :dcIds')); 
  }  


  // get the sfdc dc_user related to the passed in id
  // id   string    user id for the DC_User to retrieve
  /*
  public DC_GenericWrapper getTeam(String id) {
    String fieldNames = this.mapper.getCommaSeparatedSalesforceFieldList('Team');
    System.debug('fieldNames::' + fieldNames);
    String query = 'SELECT ' + fieldNames + ' FROM goog_dclk_dsm__DC_Team__c where id = :id';
     
    return mapper.wrap((List<goog_dclk_dsm__DC_Team__c>)Database.query(query));
  }
 
  // get the sfdc dc users for the ids provided and return in a generic wrapper
  // ids  List<Id>  list of ids for which to retrieve sfdc dc users
  public DC_GenericWrapper getTeams(List<Id> ids) {
    String fieldNames = this.mapper.getCommaSeparatedSalesforceFieldList('Team');
    String query = 'SELECT ' + fieldNames + ' FROM goog_dclk_dsm__DC_Team__c where id in :ids';

    return mapper.wrap((List<goog_dclk_dsm__DC_Team__c>)Database.query(query));
  }

  // get the sfdc dc user records from the passed sfdc dc users 
  public DC_GenericWrapper getTeams(List<goog_dclk_dsm__DC_Team__c> teams) {
    if(teams == null || teams.size() == 0) {
      return null;
    }
    List<Id> ids = new List<Id>(new Map<Id, goog_dclk_dsm__DC_Team__c>(teams).keySet());
    return getTeams(ids);
  }
  
  //update User
  public void updateTeams(DC_GenericWrapper teams) {
    List<goog_dclk_dsm__DC_Team__c> teamList = new List<goog_dclk_dsm__DC_Team__c>();
    for(Object o : teams.getObjects()) {
      teamList.add((goog_dclk_dsm__DC_Team__c)o);
    }
    if(teamList.size() > 0) {
      DC_SalesforceDML.getInstance().registerUpdate(teamList);
    }
  }
  
  //get team
  public DC_GenericWrapper getTeams(DC_GenericWrapper wrapper) {
    wrapper.gotoBeforeFirst();
    List<Id> teamIds = new List<Id>();
    while(wrapper.hasNext()) {
      wrapper.next();
      teamIds.add((String)wrapper.getField('sfid'));
    }
    return getTeams(teamIds);
  }
  

  */  
}