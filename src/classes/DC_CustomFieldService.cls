//Generated by wsdl2apex

public class DC_CustomFieldService {
    public class CommonError {
        public String reason;
        private String[] reason_type_info = new String[]{'reason',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'reason'};
    }
    public class updateCustomFieldOptions_element {
        public DC_CustomFieldService.CustomFieldOption[] customFieldOptions;
        private String[] customFieldOptions_type_info = new String[]{'customFieldOptions',DC_Constants.SERVICE_END_POINT,null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'customFieldOptions'};
    }
    public class getCustomFieldOptionResponse_element {
        public DC_CustomFieldService.CustomFieldOption rval;
        private String[] rval_type_info = new String[]{'rval',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'rval'};
    }
    public class DeactivateCustomFields {
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{};
    }
    public class InternalApiError {
        public String reason;
        private String[] reason_type_info = new String[]{'reason',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'reason'};
    }
    public class createCustomFieldsResponse_element {
        public DC_CustomFieldService.CustomField[] rval;
        private String[] rval_type_info = new String[]{'rval',DC_Constants.SERVICE_END_POINT,null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'rval'};
    }
    public class Authentication {
        public String Authentication_Type;
        private String[] Authentication_Type_type_info = new String[]{'Authentication.Type',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'Authentication_Type'};
    }
    public class SetValue {
        public DC_CustomFieldService.Value[] values;
        private String[] values_type_info = new String[]{'values',DC_Constants.SERVICE_END_POINT,null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'values'};
    }
    public class CustomFieldPage {
        public Integer totalResultSetSize;
        public Integer startIndex;
        public DC_CustomFieldService.CustomField[] results;
        private String[] totalResultSetSize_type_info = new String[]{'totalResultSetSize',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] startIndex_type_info = new String[]{'startIndex',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] results_type_info = new String[]{'results',DC_Constants.SERVICE_END_POINT,null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'totalResultSetSize','startIndex','results'};
    }
    public class BooleanValue {
        public Boolean value;
        private String[] value_type_info = new String[]{'value',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'value'};
    }
    public class UniqueError {
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{};
    }
    public class CustomFieldError {
        public String reason;
        private String[] reason_type_info = new String[]{'reason',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'reason'};
    }
    public class RequiredError {
        public String reason;
        private String[] reason_type_info = new String[]{'reason',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'reason'};
    }
    public class FeatureError {
        public String reason;
        private String[] reason_type_info = new String[]{'reason',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'reason'};
    }
    public class AuthenticationError {
        public String reason;
        private String[] reason_type_info = new String[]{'reason',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'reason'};
    }
    public class PermissionError {
        public String reason;
        private String[] reason_type_info = new String[]{'reason',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'reason'};
    }
    public class PublisherQueryLanguageSyntaxError {
        public String reason;
        private String[] reason_type_info = new String[]{'reason',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'reason'};
    }
    public class String_ValueMapEntry {
        public String key;
        public DC_CustomFieldService.Value value;
        private String[] key_type_info = new String[]{'key',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] value_type_info = new String[]{'value',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'key','value'};
    }
    public class CustomField {
        public Long id;
        public String name;
        public String description;
        public Boolean isActive;
        public String entityType;
        public String dataType;
        public String visibility;
        public String CustomField_Type;
        private String[] id_type_info = new String[]{'id',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] name_type_info = new String[]{'name',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] description_type_info = new String[]{'description',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] isActive_type_info = new String[]{'isActive',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] entityType_type_info = new String[]{'entityType',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] dataType_type_info = new String[]{'dataType',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] visibility_type_info = new String[]{'visibility',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] CustomField_Type_type_info = new String[]{'CustomField.Type',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'id','name','description','isActive','entityType','dataType','visibility','CustomField_Type'};
    }
    public class Value {
        public String Value_Type;
        private String[] Value_Type_type_info = new String[]{'Value.Type',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'Value_Type'};
    }
    public class OAuth {
        public String parameters;
        private String[] parameters_type_info = new String[]{'parameters',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'parameters'};
    }
    public class CustomFieldOption {
        public Long id;
        public Long customFieldId;
        public String displayName;
        private String[] id_type_info = new String[]{'id',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] customFieldId_type_info = new String[]{'customFieldId',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] displayName_type_info = new String[]{'displayName',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'id','customFieldId','displayName'};
    }
    public class createCustomFieldOptionsResponse_element {
        public DC_CustomFieldService.CustomFieldOption[] rval;
        private String[] rval_type_info = new String[]{'rval',DC_Constants.SERVICE_END_POINT,null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'rval'};
    }
    public class CustomFieldAction {
        public String CustomFieldAction_Type;
        private String[] CustomFieldAction_Type_type_info = new String[]{'CustomFieldAction.Type',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'CustomFieldAction_Type'};
    }
    public class getCustomFieldsByStatementResponse_element {
        public DC_CustomFieldService.CustomFieldPage rval;
        private String[] rval_type_info = new String[]{'rval',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'rval'};
    }
    public class CustomFieldServiceInterfacePort {
        public String endpoint_x = DC_Constants.WSDL_SOAP_LOCATION +'/CustomFieldService';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        public DC_CustomFieldService.SoapResponseHeader ResponseHeader;
        public DC_CustomFieldService.SoapRequestHeader RequestHeader;
        private String ResponseHeader_hns = 'ResponseHeader='+DC_Constants.SERVICE_END_POINT;
        private String RequestHeader_hns = 'RequestHeader='+DC_Constants.SERVICE_END_POINT;
        private String[] ns_map_type_info = new String[]{DC_Constants.SERVICE_END_POINT, 'DC_CustomFieldService'};
        public DC_CustomFieldService.CustomFieldPage getCustomFieldsByStatement(DC_CustomFieldService.Statement filterStatement) {
            DC_CustomFieldService.getCustomFieldsByStatement_element request_x = new DC_CustomFieldService.getCustomFieldsByStatement_element();
            DC_CustomFieldService.getCustomFieldsByStatementResponse_element response_x;
            request_x.filterStatement = filterStatement;
            Map<String, DC_CustomFieldService.getCustomFieldsByStatementResponse_element> response_map_x = new Map<String, DC_CustomFieldService.getCustomFieldsByStatementResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              DC_Constants.SERVICE_END_POINT,
              'getCustomFieldsByStatement',
              DC_Constants.SERVICE_END_POINT,
              'getCustomFieldsByStatementResponse',
              'DC_CustomFieldService.getCustomFieldsByStatementResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.rval;
        }
        public DC_CustomFieldService.CustomFieldOption getCustomFieldOption(Long customFieldOptionId) {
            DC_CustomFieldService.getCustomFieldOption_element request_x = new DC_CustomFieldService.getCustomFieldOption_element();
            DC_CustomFieldService.getCustomFieldOptionResponse_element response_x;
            request_x.customFieldOptionId = customFieldOptionId;
            Map<String, DC_CustomFieldService.getCustomFieldOptionResponse_element> response_map_x = new Map<String, DC_CustomFieldService.getCustomFieldOptionResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              DC_Constants.SERVICE_END_POINT,
              'getCustomFieldOption',
              DC_Constants.SERVICE_END_POINT,
              'getCustomFieldOptionResponse',
              'DC_CustomFieldService.getCustomFieldOptionResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.rval;
        }
        public DC_CustomFieldService.CustomField[] createCustomFields(DC_CustomFieldService.CustomField[] customFields) {
            DC_CustomFieldService.createCustomFields_element request_x = new DC_CustomFieldService.createCustomFields_element();
            DC_CustomFieldService.createCustomFieldsResponse_element response_x;
            request_x.customFields = customFields;
            Map<String, DC_CustomFieldService.createCustomFieldsResponse_element> response_map_x = new Map<String, DC_CustomFieldService.createCustomFieldsResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              DC_Constants.SERVICE_END_POINT,
              'createCustomFields',
              DC_Constants.SERVICE_END_POINT,
              'createCustomFieldsResponse',
              'DC_CustomFieldService.createCustomFieldsResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.rval;
        }
        public DC_CustomFieldService.CustomFieldOption[] createCustomFieldOptions(DC_CustomFieldService.CustomFieldOption[] customFieldOptions) {
            DC_CustomFieldService.createCustomFieldOptions_element request_x = new DC_CustomFieldService.createCustomFieldOptions_element();
            DC_CustomFieldService.createCustomFieldOptionsResponse_element response_x;
            request_x.customFieldOptions = customFieldOptions;
            Map<String, DC_CustomFieldService.createCustomFieldOptionsResponse_element> response_map_x = new Map<String, DC_CustomFieldService.createCustomFieldOptionsResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              DC_Constants.SERVICE_END_POINT,
              'createCustomFieldOptions',
              DC_Constants.SERVICE_END_POINT,
              'createCustomFieldOptionsResponse',
              'DC_CustomFieldService.createCustomFieldOptionsResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.rval;
        }
        public DC_CustomFieldService.UpdateResult performCustomFieldAction(DC_CustomFieldService.CustomFieldAction customFieldAction,DC_CustomFieldService.Statement filterStatement) {
            DC_CustomFieldService.performCustomFieldAction_element request_x = new DC_CustomFieldService.performCustomFieldAction_element();
            DC_CustomFieldService.performCustomFieldActionResponse_element response_x;
            request_x.customFieldAction = customFieldAction;
            request_x.filterStatement = filterStatement;
            Map<String, DC_CustomFieldService.performCustomFieldActionResponse_element> response_map_x = new Map<String, DC_CustomFieldService.performCustomFieldActionResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              DC_Constants.SERVICE_END_POINT,
              'performCustomFieldAction',
              DC_Constants.SERVICE_END_POINT,
              'performCustomFieldActionResponse',
              'DC_CustomFieldService.performCustomFieldActionResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.rval;
        }
        public DC_CustomFieldService.CustomField[] updateCustomFields(DC_CustomFieldService.CustomField[] customFields) {
            DC_CustomFieldService.updateCustomFields_element request_x = new DC_CustomFieldService.updateCustomFields_element();
            DC_CustomFieldService.updateCustomFieldsResponse_element response_x;
            request_x.customFields = customFields;
            Map<String, DC_CustomFieldService.updateCustomFieldsResponse_element> response_map_x = new Map<String, DC_CustomFieldService.updateCustomFieldsResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              DC_Constants.SERVICE_END_POINT,
              'updateCustomFields',
              DC_Constants.SERVICE_END_POINT,
              'updateCustomFieldsResponse',
              'DC_CustomFieldService.updateCustomFieldsResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.rval;
        }
        public DC_CustomFieldService.CustomFieldOption[] updateCustomFieldOptions(DC_CustomFieldService.CustomFieldOption[] customFieldOptions) {
            DC_CustomFieldService.updateCustomFieldOptions_element request_x = new DC_CustomFieldService.updateCustomFieldOptions_element();
            DC_CustomFieldService.updateCustomFieldOptionsResponse_element response_x;
            request_x.customFieldOptions = customFieldOptions;
            Map<String, DC_CustomFieldService.updateCustomFieldOptionsResponse_element> response_map_x = new Map<String, DC_CustomFieldService.updateCustomFieldOptionsResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              DC_Constants.SERVICE_END_POINT,
              'updateCustomFieldOptions',
              DC_Constants.SERVICE_END_POINT,
              'updateCustomFieldOptionsResponse',
              'DC_CustomFieldService.updateCustomFieldOptionsResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.rval;
        }
    }
    public class TypeError {
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{};
    }
    public class NumberValue {
        public String value;
        private String[] value_type_info = new String[]{'value',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'value'};
    }
    public class SoapResponseHeader {
        public String requestId;
        public Long responseTime;
        private String[] requestId_type_info = new String[]{'requestId',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] responseTime_type_info = new String[]{'responseTime',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'requestId','responseTime'};
    }
    public class ActivateCustomFields {
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{};
    }
    public class getCustomFieldsByStatement_element {
        public DC_CustomFieldService.Statement filterStatement;
        private String[] filterStatement_type_info = new String[]{'filterStatement',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'filterStatement'};
    }
    public class NotNullError {
        public String reason;
        private String[] reason_type_info = new String[]{'reason',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'reason'};
    }
    public class updateCustomFields_element {
        public DC_CustomFieldService.CustomField[] customFields;
        private String[] customFields_type_info = new String[]{'customFields',DC_Constants.SERVICE_END_POINT,null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'customFields'};
    }
    public class EntityLimitReachedError {
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{};
    }
    public class DateTimeValue {
        public DC_CustomFieldService.DateTime_x value;
        private String[] value_type_info = new String[]{'value',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'value'};
    }
    public class Statement {
        public String query;
        public DC_CustomFieldService.String_ValueMapEntry[] values;
        private String[] query_type_info = new String[]{'query',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] values_type_info = new String[]{'values',DC_Constants.SERVICE_END_POINT,null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'query','values'};
    }
    public class PublisherQueryLanguageContextError {
        public String reason;
        private String[] reason_type_info = new String[]{'reason',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'reason'};
    }
    public class ApiError {
        public String fieldPath;
        public String trigger_x;
        public String errorString;
        public String ApiError_Type;
        private String[] fieldPath_type_info = new String[]{'fieldPath',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] trigger_x_type_info = new String[]{'trigger',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] errorString_type_info = new String[]{'errorString',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] ApiError_Type_type_info = new String[]{'ApiError.Type',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'fieldPath','trigger_x','errorString','ApiError_Type'};
    }
    public class Date_x {
        public Integer year;
        public Integer month;
        public Integer day;
        private String[] year_type_info = new String[]{'year',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] month_type_info = new String[]{'month',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] day_type_info = new String[]{'day',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'year','month','day'};
    }
    public class updateCustomFieldOptionsResponse_element {
        public DC_CustomFieldService.CustomFieldOption[] rval;
        private String[] rval_type_info = new String[]{'rval',DC_Constants.SERVICE_END_POINT,null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'rval'};
    }
    public class performCustomFieldActionResponse_element {
        public DC_CustomFieldService.UpdateResult rval;
        private String[] rval_type_info = new String[]{'rval',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'rval'};
    }
    public class performCustomFieldAction_element {
        public DC_CustomFieldService.CustomFieldAction customFieldAction;
        public DC_CustomFieldService.Statement filterStatement;
        private String[] customFieldAction_type_info = new String[]{'customFieldAction',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] filterStatement_type_info = new String[]{'filterStatement',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'customFieldAction','filterStatement'};
    }
    public class SoapRequestHeader {
        public String networkCode;
        public String applicationName;
        public DC_CustomFieldService.Authentication authentication;
        private String[] networkCode_type_info = new String[]{'networkCode',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] applicationName_type_info = new String[]{'applicationName',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] authentication_type_info = new String[]{'authentication',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'networkCode','applicationName','authentication'};
    }
    public class NullError {
        public String reason;
        private String[] reason_type_info = new String[]{'reason',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'reason'};
    }
    public class ApiException extends Exception {
        public DC_CustomFieldService.ApiError[] errors;
        private String[] errors_type_info = new String[]{'errors',DC_Constants.SERVICE_END_POINT,null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'errors'};
    }
    public class createCustomFieldOptions_element {
        public DC_CustomFieldService.CustomFieldOption[] customFieldOptions;
        private String[] customFieldOptions_type_info = new String[]{'customFieldOptions',DC_Constants.SERVICE_END_POINT,null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'customFieldOptions'};
    }
    public class QuotaError {
        public String reason;
        private String[] reason_type_info = new String[]{'reason',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'reason'};
    }
    public class createCustomFields_element {
        public DC_CustomFieldService.CustomField[] customFields;
        private String[] customFields_type_info = new String[]{'customFields',DC_Constants.SERVICE_END_POINT,null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'customFields'};
    }
    public class DateValue {
        public DC_CustomFieldService.Date_x value;
        private String[] value_type_info = new String[]{'value',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'value'};
    }
    public class ApplicationException extends Exception {
        public String message;
        public String ApplicationException_Type;
        private String[] message_type_info = new String[]{'message',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] ApplicationException_Type_type_info = new String[]{'ApplicationException.Type',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'message','ApplicationException_Type'};
    }
    public class getCustomFieldOption_element {
        public Long customFieldOptionId;
        private String[] customFieldOptionId_type_info = new String[]{'customFieldOptionId',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'customFieldOptionId'};
    }
    public class ParseError {
        public String reason;
        private String[] reason_type_info = new String[]{'reason',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'reason'};
    }
    public class ApiVersionError {
        public String reason;
        private String[] reason_type_info = new String[]{'reason',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'reason'};
    }
    public class ServerError {
        public String reason;
        private String[] reason_type_info = new String[]{'reason',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'reason'};
    }
    public class updateCustomFieldsResponse_element {
        public DC_CustomFieldService.CustomField[] rval;
        private String[] rval_type_info = new String[]{'rval',DC_Constants.SERVICE_END_POINT,null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'rval'};
    }
    public class DateTime_x {
        public DC_CustomFieldService.Date_x date_x;
        public Integer hour;
        public Integer minute;
        public Integer second;
        public String timeZoneID;
        private String[] date_x_type_info = new String[]{'date',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] hour_type_info = new String[]{'hour',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] minute_type_info = new String[]{'minute',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] second_type_info = new String[]{'second',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] timeZoneID_type_info = new String[]{'timeZoneID',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'date_x','hour','minute','second','timeZoneID'};
    }
    public class StatementError {
        public String reason;
        private String[] reason_type_info = new String[]{'reason',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'reason'};
    }
    public class TextValue {
        public String value;
        private String[] value_type_info = new String[]{'value',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'value'};
    }
    public class DropDownCustomField {
        public DC_CustomFieldService.CustomFieldOption[] options;
        private String[] options_type_info = new String[]{'options',DC_Constants.SERVICE_END_POINT,null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'options'};
    }
    public class UpdateResult {
        public Integer numChanges;
        private String[] numChanges_type_info = new String[]{'numChanges',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'numChanges'};
    }
}