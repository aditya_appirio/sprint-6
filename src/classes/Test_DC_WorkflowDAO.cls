/**
 */
@isTest
private class Test_DC_WorkflowDAO {

	static testMethod void myUnitTest() {
		
		DC_DoubleClickWorkflowMapper mapper = DC_DoubleClickWorkflowMapper.getInstance();
		
		DC_WorkflowService.WorkflowRequest wfRequest = new DC_WorkflowService.WorkflowRequest();
	  wfRequest.entityId = 12345;
	  wfRequest.id = 12345;
	  wfRequest.status = 'PENDING_APPROVAL';
	  wfRequest.workflowRuleName = 'test rule';
	  wfRequest.entityType = 'PROPOSAL';
    
		goog_dclk_dsm__DC_ObjectMapping__c dcObjMapping1 = DC_TestUtils.createObjectMapping('WorkflowRequest', 'WorkflowRequest', null, false);
    insert new List<goog_dclk_dsm__DC_ObjectMapping__c>{dcObjMapping1};
    
    goog_dclk_dsm__DC_FieldMapping__c fldMapping5 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'entityType',  Name = 'entityType');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping6 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id',  Name = 'DoubleClickId');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping7 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'workflowRuleName',  Name = 'workflowRuleName');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping8 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'status', Name = 'status');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping9 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'workflowRuleName',  Name = 'name');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping10 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'entityId',  Name = 'entityId');
    insert new List<goog_dclk_dsm__DC_FieldMapping__c>{fldMapping5, fldMapping6, fldMapping7, fldMapping8, fldMapping9};
    Test.startTest();
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplWorkflowService());
		Test.setMock(HttpCalloutMock.class, new DC_HttpCalloutMockImploAuth());
    DC_DoubleClickOAuthHandler oAuthHandler = new DC_DoubleClickOAuthHandler(Userinfo.getUserId());
    
    DC_WorkflowDAO wfDAO = new DC_WorkflowDAO(oAuthHandler);
    String query = null;
    
    DC_WorkflowService.Statement statement = wfDAO.createStatement(query);
    System.assert(statement != null);
     
    
    query = 'WHERE id in (12345)';
    DC_WorkflowService.WorkflowRequestPage p = wfDAO.getWorkflowRequestByStatement(query);
    System.assertEquals(p.results[0].workflowrulename, 'test rule');
    System.assertEquals(1, wfDao.approveWorkflowRequests(query));
    System.assertEquals(1, wfDAO.rejectWorkflowRequests(query));
    
    
    DC_WorkflowManager wManager = new DC_WorkflowManager(oAuthHandler);
    
    DC_GenericWrapper gw = mapper.wrap(wfRequest);
    wManager.approveWorkflowRequests(gw);
    wManager.rejectWorkflowRequests(gw);
    Test.stopTest();
	}
	
}