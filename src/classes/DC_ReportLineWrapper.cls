public virtual with sharing class DC_ReportLineWrapper extends DC_DoubleClickWrapper {

  public DC_ReportLineWrapper(goog_dclk_dsm__DC_ObjectMapping__c objectMapping, List<goog_dclk_dsm__DC_FieldMapping__c> fieldMappings, String report) {
    super(objectMapping, fieldMappings, new List<Map<String, String>>());

    String[] lines = report.split('\n');

    if(lines.size() > 1) {
      String[] fieldNames = lines[0].split(',');
      
      for(Integer i = 1; i < lines.size(); i++) {
        String[] fields = lines[i].split(',');
        Map<String, String> obj = new Map<String, String>();

        for(Integer j = 0; j < fieldNames.size(); j++) {
          obj.put(fieldNames[j].toLowerCase(), fields[j]);
        }

        this.objects.add(obj);
      }
    }
    System.debug('this.objects:::' + this.objects);
  }

  private Map<String, String> getCurentReportLine() {
    if(this.currentObject == null){
      throw new DC_ConnectorException(DC_Constants.ARR_OUT_OF_BOUND_ERR);
    } else {
    	System.debug('this.currentObject::' + this.currentObject);
      return (Map<String, String>)this.currentObject;
    }

  }

  public override Object getField(String field) {
    String fieldName = getDoubleClickFieldName(field.toLowerCase());

    System.debug('field: ' + field);
    System.debug('fieldName: ' + fieldName);

    return getCurentReportLine().get(fieldName.toLowerCase());
  }

  public override void setField(String field, Object value) {
    String fieldName = getDoubleClickFieldName(field.toLowerCase());

    getCurentReportLine().put(fieldName.toLowerCase(), this.toString(value));
  }

  public override DC_GenericWrapper getNewRecords() {
    throw new DC_ConnectorException('Unsupported method invoked');
  }

  // TODO US: Deprecate this and use getMatchedRecords instead
  public override DC_GenericWrapper getExistingRecords() {
    throw new DC_ConnectorException('Unsupported method invoked');
  }

  public override DC_GenericWrapper getCurrentWrapped() { 
    throw new DC_ConnectorException('Unsupported method invoked');
  }

  public override DC_GenericWrapper createWrapper(goog_dclk_dsm__DC_ObjectMapping__c objectMapping, List<goog_dclk_dsm__DC_FieldMapping__c> fieldMappings, List<Object> objects) {
    throw new DC_ConnectorException('Unsupported method invoked');
  }
}