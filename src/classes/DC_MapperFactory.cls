//
//
//  Factory to return instances of mapper classes
//
//
public with sharing class DC_MapperFactory {
  private static DC_MapperFactory instance;
  
  //constructor
  private DC_MapperFactory(){
  }
  
  // enforce singleton instance
  public static DC_MapperFactory getInstance() {
    if(instance == null) {
      instance = new DC_MapperFactory();
    } 
    return instance;
  }
  
  // return instance of account mapper
  public DC_CompanyMapper getAccountMapper(){
  	return (DC_CompanyMapper.getInstance());
  }
  
  // return instance of contact mapper
  public DC_ContactMapper getContactMapper(){
    return (DC_ContactMapper.getInstance());
  }
  
  // return instance of opportunity mapper
  public DC_ProposalMapper getOpportunityMapper(){
    return (DC_ProposalMapper.getInstance());
  }
  
  // return instance of opportunity Line Item mapper
  public DC_ProposalLineItemMapper getOpportunityLIMapper(){
    return (DC_ProposalLineItemMapper.getInstance());
  }
  
  // return instance of opportunity Line Item mapper
  public DC_UserMapper getUserMapper(){
    return (DC_UserMapper.getInstance());
  }
  
  // return instance of account mapper
  public DC_TeamMapper getTeamMapper(){
  	return (DC_TeamMapper.getInstance());
  }
}