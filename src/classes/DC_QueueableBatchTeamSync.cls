public with sharing class DC_QueueableBatchTeamSync extends DC_QueueableBatchProcess implements
  Database.batchable<long>, 
  Database.AllowsCallouts,
  Database.Stateful {
      
  public DC_RetrieveTeamsParams uparams;

  public override long getRecordsToProcessPerPage() {
  	System.debug('goog_dclk_dsm__DC_ConnectorApplicationSettings__c.getInstance().goog_dclk_dsm__DC_TeamsToProcessPerPage__c:::' + goog_dclk_dsm__DC_ConnectorApplicationSettings__c.getInstance().goog_dclk_dsm__DC_TeamsToProcessPerPage__c);
    return (long)goog_dclk_dsm__DC_ConnectorApplicationSettings__c.getInstance().goog_dclk_dsm__DC_TeamsToProcessPerPage__c;
  }
   
  public override datetime getBasisDate() { 
  	return DateTime.now(); 
  }
    
  // DC_QueueableProcess ===============
  public override void initialize(DC_QueueableProcess.DC_QueueableProcessParameter inparams) {
    params = uparams = (DC_RetrieveTeamsParams)inparams;
  }

  public override void execute() {
    Database.executeBatch(this, 1);
  } 

  public override System.Type getParameterType() {
     return DC_RetrieveTeamsParams.class;
  }
  
  public override DC_Connector.DC_DoubleClickPageProcessingResult processRecords(long pageSize, long pageNo) {
    return DC_SFTeamDCTeamConnector.getInstance().syncTeamsByPage(basisDate, pageSize, pageNo * pageSize);
  }
  
  public override void updateSyncDate(DateTime dt) {
    return;

    // teams do not have sync dates-
  }
  public override String getResultString() {
    return String.format('Total Records Processed: {0}\nInserted Records: {1}\nUpdated Records: {2}\nError Records: {3}', 
      new String[] { 
        (recordsProcessed == null) ? '' : recordsProcessed.format(),
        String.valueOf(insertedRecordCount),
        String.valueOf(updatedRecordCount),
        String.valueOf(errorCount)     
      });
  }   
  

  public class DC_RetrieveTeamsParams extends DC_QueueableProcess.DC_QueueableProcessParameter { }
}