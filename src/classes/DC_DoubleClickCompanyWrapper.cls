//
// (c) 2014 Appirio
//
// DC_DoubleClickCompanyWrapper 
// 
//
public with sharing class DC_DoubleClickCompanyWrapper extends DC_DoubleClickWrapper {

  public DC_DoubleClickCompanyWrapper(goog_dclk_dsm__DC_ObjectMapping__c objectMapping, List<goog_dclk_dsm__DC_FieldMapping__c> fieldMappings, List<Object> objects) {
    super(objectMapping, fieldMappings, objects);
  }

  private DC_CompanyService.Company getCurrentCompany() {
    System.debug('this.currentObject::' + this.currentObject);
    if(this.currentObject == null){
      throw new DC_ConnectorException(DC_Constants.ARR_OUT_OF_BOUND_ERR);
    } else if(!(this.currentObject instanceOf DC_CompanyService.Company)){
        throw new DC_ConnectorException(DC_Constants.INVALID_COMPANY_MAPPING);
    } else{
        return (DC_CompanyService.Company) this.currentObject;
    }
  }

  public override Object getField(String field) {
    String fieldName = getDoubleClickFieldName(field.toLowerCase());
    System.debug('getCurrentCompany()::' + getCurrentCompany());
    System.debug('fieldName::' + fieldName);
    if(fieldName.toLowerCase().equals('id')) {
      return getCurrentCompany().id;
    } else if(fieldName.toLowerCase().equals('name')) {
      return getCurrentCompany().name;
    } else if(fieldName.toLowerCase().equals('type_x')) {
      return getCurrentCompany().type_x;
    } else if(fieldName.toLowerCase().equals('address')) {
        return getCurrentCompany().address;
    } else if(fieldName.toLowerCase().equals('email')) {
        return getCurrentCompany().email;
    } else if(fieldName.toLowerCase().equals('faxphone')) {
        return getCurrentCompany().faxPhone;
    } else if(fieldName.toLowerCase().equals('primaryphone')) {
        return getCurrentCompany().primaryPhone;
    } else if(fieldName.toLowerCase().equals('externalid')) {
        return getCurrentCompany().externalId;
    } else if(fieldName.toLowerCase().equals('creditstatus')) {
        return getCurrentCompany().creditStatus;
    } else if(fieldName.toLowerCase().equals('primarycontactid')) {
        return getCurrentCompany().primaryContactId;
    } else if(fieldName.toLowerCase().equals('thirdpartycompanyid')) {
        return getCurrentCompany().thirdPartyCompanyId;
    } else if(fieldName.toLowerCase().equals('appliedteamids')) {
        return getCurrentCompany().appliedTeamIds;
    } else if(fieldName.toLowerCase().equals('appliedlabels')) {
        return getCurrentCompany().appliedLabels;
    } else if(fieldName.toLowerCase().equals('enablesameadvertisercompetitiveexclusion')) {
        return getCurrentCompany().enableSameAdvertiserCompetitiveExclusion;
    } else if(fieldName.toLowerCase().equals('lastmodifieddatetime')) {
        return getCurrentCompany().lastModifiedDateTime;
    } else if(fieldName.toLowerCase().equals('comment')) {
        return getCurrentCompany().comment;
    }
    else {
      throw new DC_ConnectorException(DC_Constants.getInvalid_Mapping_Company(field));
    }
  }
  
  public override void setField(String field, Object value) {
    String fieldName = getDoubleClickFieldName(field);
    
    try{
      if(fieldName.equalsIgnoreCase('id')) {
        getCurrentCompany().id = this.toLong(value);
      } else if(fieldName.equalsIgnoreCase('name')) {
          getCurrentCompany().name = this.toString(value);
      } else if(fieldName.equalsIgnoreCase('type_x')) {
          getCurrentCompany().type_x = this.toString(value);
      } else if(fieldName.equalsIgnoreCase('address')) {
          getCurrentCompany().address = this.toString(value);
      } else if(fieldName.equalsIgnoreCase('email')) {
          getCurrentCompany().email = this.toString(value);
      } else if(fieldName.equalsIgnoreCase('faxphone')) {
          getCurrentCompany().faxPhone = this.toString(value);
      } else if(fieldName.equalsIgnoreCase('primaryphone')) {
          getCurrentCompany().primaryPhone = this.toString(value);
      } else if(fieldName.equalsIgnoreCase('externalid')) {
          getCurrentCompany().externalId = this.toString(value);
      } else if(fieldName.equalsIgnoreCase('comment')) {
          getCurrentCompany().comment = this.toString(value);
      } else if(fieldName.equalsIgnoreCase('creditstatus')) {
          getCurrentCompany().creditStatus = this.toString(value);
      } else if(fieldName.equalsIgnoreCase('primarycontactid')) {
          getCurrentCompany().primaryContactId = this.toLong(value);
      } else if(fieldName.equalsIgnoreCase('thirdpartycompanyid')) {
          getCurrentCompany().thirdPartyCompanyId = this.toInteger(value);
      } else if(fieldName.equalsIgnoreCase('enablesameadvertisercompetitiveexclusion')) {
          getCurrentCompany().enableSameAdvertiserCompetitiveExclusion = this.toBoolean(value);
      // TODO figure out how to handle these complex objects          
      } else if(fieldName.equalsIgnoreCase('appliedlabels')) {
          getCurrentCompany().appliedLabels = (DC_CompanyService.AppliedLabel[]) value;
      } else if(fieldName.equalsIgnoreCase('appliedteamids')) {
          getCurrentCompany().appliedTeamIds = (Long[]) value;
      } else if(fieldName.equalsIgnoreCase('lastmodifieddatetime')) {
          getCurrentCompany().lastModifiedDateTime = toDC_DateTime(value);
      }
      else {
        throw new DC_ConnectorException(DC_Constants.getInvalid_Mapping_Company(field));
      }
    }
    catch(System.TypeException e){
      throw new DC_ConnectorException(e+DC_Constants.FOR_FLD_STR+field);
    }
  }

  private DC_CompanyService.DateTime_x toDC_DateTime(Object obj){
    if(obj == null) {
      return null;
    }
    if(obj instanceof DateTime) {
      DateTime dt= (Datetime)obj;
      DC_CompanyService.DateTime_x dc_DateTime = new DC_CompanyService.DateTime_x();
      DC_CompanyService.Date_x dc_Date = new DC_CompanyService.Date_x();
      dc_Date.day =dt.day();
      dc_Date.month=dt.month();
      dc_Date.year=dt.year();
      
      dc_DateTime.date_x = dc_Date;
      dc_DateTime.hour = dt.hour();
      dc_DateTime.minute = dt.minute();
      dc_DateTime.second = dt.second();
      
      return (DC_CompanyService.DateTime_x) dc_DateTime;
    } 
    throw new DC_ConnectorException(DC_Constants.UNSUPPORTED_DATA_CONVERT_ERR);
  }
  
  public override DC_GenericWrapper getNewRecords() {
    this.gotoBeforeFirst();
    List<Object> result = new List<Object>();
    
    while(this.hasNext()) {
      this.next();
      
      if(this.getField('DoubleClickId') == null) {
        result.add(this.currentObject);
      }
    }
    
    return new DC_DoubleClickCompanyWrapper(this.objectMapping, this.fieldMappings, result);
  }
  
  public override DC_GenericWrapper getExistingRecords() {
    this.gotoBeforeFirst();
    List<Object> result = new List<Object>();
    
    while(this.hasNext()) {
      this.next();
      
      if(this.getField('DoubleClickId') != null) {
        result.add(this.currentObject);
      }
    }
    
    return new DC_DoubleClickCompanyWrapper(this.objectMapping, this.fieldMappings, result);
  }
  
  public override DC_GenericWrapper getCurrentWrapped() {
    return new DC_DoubleClickCompanyWrapper(this.objectMapping, this.fieldMappings, new object[] { this.currentObject });
  }
   
  public override DC_GenericWrapper createWrapper(goog_dclk_dsm__DC_ObjectMapping__c objectMapping, List<goog_dclk_dsm__DC_FieldMapping__c> fieldMappings, List<Object> objects) {
    return new DC_DoubleClickCompanyWrapper(objectMapping, fieldMappings, objects);
  }
  
}