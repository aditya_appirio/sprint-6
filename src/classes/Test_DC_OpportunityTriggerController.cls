// 
// (c) 2014 Appirio
//
// Test_DC_OpportunityTriggerController : test class for DC_OpportunityTriggerController 
//
// 13 March 2014   Karun Kumar(JDC) Original
//
@isTest
private class Test_DC_OpportunityTriggerController {

  static testMethod void myUnitTest() {
    goog_dclk_dsm__DC_ConnectorApplicationSettings__c connAppSetting = DC_TestUtils.createConnAppSetting(1, false);
    connAppSetting.goog_dclk_dsm__DC_AllowOpportunityTriggerToDSM__c = true;
    insert connAppSetting;
    
    goog_dclk_dsm__DC_ObjectMapping__c dcObjMapping = DC_TestUtils.createObjectMapping('Proposal', 'Opportunity', true);
    
    goog_dclk_dsm__DC_FieldMapping__c fldMapping1 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping2 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'custom.SFDC_Opportunity_ID', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Id', Name = 'sfid');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping3 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'probabilityToClose', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Probability', Name = 'probability');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping4 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'status', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345,  Name = 'status');
    insert new List<goog_dclk_dsm__DC_FieldMapping__c>{fldMapping1,fldMapping2,fldMapping3,fldMapping4};
    
    system.debug('Setup success.');
    
    Account acc = DC_TestUtils.createAccount('testCompany', true);
     
    try {
      Test.startTest();
      Opportunity opp = DC_TestUtils.createOpportunity('testOpp',System.now().date(),acc.Id,'Prospecting',true);

      opp.goog_dclk_dsm__DC_ReadyForDSM__c = true;
      opp.goog_dclk_dsm__dc_doubleclickid__c = '1';
      update opp;
      
      opp = [Select goog_dclk_dsm__DC_ReadyForDSM__c, goog_dclk_dsm__DC_DoubleClickId__c, goog_dclk_dsm__DC_DSMRecord__c From Opportunity Where Id = :opp.Id ];
      system.assertEquals(opp.goog_dclk_dsm__DC_ReadyForDSM__c, true);
      system.assertEquals(opp.goog_dclk_dsm__DC_DoubleClickId__c, '1');
      system.assert(opp.goog_dclk_dsm__DC_DSMRecord__c.contains('Exists in DSM'));
      Test.stopTest();
    } catch(Exception  e) {
      system.debug('Exception :: '+e);
    }    
  }
}