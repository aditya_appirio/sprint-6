// 
// (c) 2014 Appirio
//
// Data container for authentication information
//
// 15 Jan 2014    Mauricio Desiderio (Appirio)      Original
public virtual with sharing class DC_DoubleClickOAuthHandler {
  
  private String userId;
  
  public DC_DoubleClickOAuthHandler(String userId){
    this.userId = userId;
  }
  
    public goog_dclk_dsm__DC_ConnectorAuthenticationSettings__c settings { 
        get {
            if(this.settings == null) {
            	
                this.settings = goog_dclk_dsm__DC_ConnectorAuthenticationSettings__c.getOrgDefaults();
            } 
            return this.settings;
        }
        set;
    }
    
    // TODO: this has to be per user, probably should pass as parameter in the constructor, should default to org wide default
    //:: @Anjali comment :> Please review below, do we need to check for current user (UserInfo.getUserId()) 
    //or will it be passed from DC_ConnectorAdmincontroller class in constructor like currently done in code.
    public goog_dclk_dsm__DC_Authentication__c auth {
        get {
            if(this.auth == null) {
                if(userId != null && goog_dclk_dsm__DC_Authentication__c.getValues(userId) != null){ //T-275477
                    this.auth = goog_dclk_dsm__DC_Authentication__c.getValues(userId); //T-275477
                    system.debug('@@userId@@'+userId);
                    system.debug('@@this.auth'+this.auth);
                }
                else if(userId == null){
                    this.auth = goog_dclk_dsm__DC_Authentication__c.getOrgDefaults();
                }
            }
            return this.auth;
        }
        set;
    }
  //create a new User to authenticate
  public void saveAuthValues(){
    this.auth =  new goog_dclk_dsm__DC_Authentication__c(SetupOwnerId = UserInfo.getUserId());
    insert this.auth;
  } 
 
  public goog_dclk_dsm__DC_ConnectorLog__c connectorLog {
    get{
        if(this.connectorLog == null){
            connectorLog = new goog_dclk_dsm__DC_ConnectorLog__c();
        }
        return connectorLog;
    }
    set;
  }
    // exchange the provided code to get the access and refresh tokens
    public goog_dclk_dsm__DC_Authentication__c exchangeCode(String code, Boolean admin) {
        System.debug('this.auth:::' + this.auth);

    if(!admin && this.auth == null) {
      this.auth = goog_dclk_dsm__DC_Authentication__c.getInstance(userId);
    }

    goog_dclk_dsm__DC_Authentication__c result = this.auth;
    HttpRequest req = new HttpRequest();
    Http http = new Http();
    req.setEndpoint(DC_Constants.END_POINT);
    req.setMethod(DC_Constants.POST_METHOD);
    String redirectURI;
    
    //T-275477
    //if(userId != null){ 
        //redirectURI = settings.goog_dclk_dsm__DC_UserRedirectURI__c;
    //}else{
        redirectURI = settings.goog_dclk_dsm__DC_RedirectURI__c;
    //}
    
    req.setBody('code='+ EncodingUtil.urlDecode(code, 'UTF-8') +
      '&client_id='+settings.goog_dclk_dsm__DC_ClientID__c +
      '&client_secret='+settings.goog_dclk_dsm__DC_ClientSecret__c +
      '&redirect_uri='+settings.goog_dclk_dsm__DC_RedirectURI__c +
      '&grant_type='+DC_Constants.AUTH_CODE);
      
    System.debug('--------------------------------------------------------');
    System.debug('code='+ EncodingUtil.urlDecode(code, 'UTF-8') +
      '&client_id='+settings.goog_dclk_dsm__DC_ClientID__c +
      '&client_secret='+settings.goog_dclk_dsm__DC_ClientSecret__c +
      '&redirect_uri='+settings.goog_dclk_dsm__DC_RedirectURI__c +
      '&grant_type='+DC_Constants.AUTH_CODE);
    
    	
    
      
    // parse authentication response into DC_Authentication instance
    result = parseAuthResponse(result, http.send(req));
    
    goog_dclk_dsm__DC_ConnectorApplicationSettings__c appSettings;
    if(userId != null && goog_dclk_dsm__DC_ConnectorApplicationSettings__c.getValues(userId) != null){ //T-275477
        appSettings = goog_dclk_dsm__DC_ConnectorApplicationSettings__c.getValues(userId); //T-275477
    }else{ 
        appSettings = goog_dclk_dsm__DC_ConnectorApplicationSettings__c.getOrgDefaults();
    }
                
    //DC_ConnectorApplicationSettings__c appSettings = goog_dclk_dsm__DC_ConnectorApplicationSettings__c.getOrgDefaults();
	
    DC_ManagerFactory.getInstance().setAuthHandler(this);
    
    if(admin) {
      appSettings.goog_dclk_dsm__DC_PublisherTimezoneInDSM__c = DC_ManagerFactory.getInstance().getNetworkManager().getCurrentNetworkTimezone();    
      DC_SalesforceDML.getInstance().registerUpsert(appSettings);
    }

    return result;   
    }
    
    // refresh the access token using the refresh token in auth parameter
    public goog_dclk_dsm__DC_Authentication__c refreshAccessToken() {
    HttpRequest req = new HttpRequest();
    Http http = new Http();
    
    req.setMethod(DC_Constants.POST_METHOD);
    req.setEndpoint(DC_Constants.END_POINT);
    
    System.debug('==auth===' + 'client_id='+settings.goog_dclk_dsm__DC_ClientID__c +
                '&client_secret='+settings.goog_dclk_dsm__DC_ClientSecret__c +
                '&refresh_token=' + auth.goog_dclk_dsm__DC_RefreshToken__c +
                '&grant_type='+DC_Constants.REFRESH_TOKEN);
    
    req.setBody('client_id='+settings.goog_dclk_dsm__DC_ClientID__c +
                '&client_secret='+settings.goog_dclk_dsm__DC_ClientSecret__c +
                '&refresh_token=' + auth.goog_dclk_dsm__DC_RefreshToken__c +
                '&grant_type='+DC_Constants.REFRESH_TOKEN);
      
    this.auth = parseAuthResponse(this.auth, http.send(req));
    
    return auth;
    }
    
    // creates new DC_Authentication instance and uses it for response assignment
    private goog_dclk_dsm__DC_Authentication__c parseAuthResponse(HttpResponse resp) {
    return parseAuthResponse(this.auth, resp); 
    }   
    
    // takes DC_Authentication instance and HTTP Response and assigns HTTP Response values to 
    // DC_Authentication values
    private goog_dclk_dsm__DC_Authentication__c parseAuthResponse(goog_dclk_dsm__DC_Authentication__c auth, HttpResponse res) {
    
    Map<String, Object> authResult;
    try{
        authResult = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());
        system.debug('@@authResult' +authResult );
    }catch(Exception e){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
    }
    if(authResult != null && authResult.get(DC_Constants.ACCESS_TOKEN) != null) {
        auth.goog_dclk_dsm__DC_AccessToken__c = (String) authResult.get(DC_Constants.ACCESS_TOKEN);
    }
    if(authResult != null && authResult.get(DC_Constants.TOKEN_TYPE) != null) {
        auth.goog_dclk_dsm__DC_TokenType__c = (String) authResult.get(DC_Constants.TOKEN_TYPE);  
    }
    if(authResult != null && authResult.get(DC_Constants.EXPIRES_IN) != null) {
        auth.goog_dclk_dsm__DC_ExpiresIn__c = (Integer) authResult.get(DC_Constants.EXPIRES_IN);
    }
    if(authResult != null && authResult.get(DC_Constants.REFRESH_TOKEN) != null) {
        auth.goog_dclk_dsm__DC_RefreshToken__c = (String) authResult.get(DC_Constants.REFRESH_TOKEN);
        system.debug('###auth.goog_dclk_dsm__DC_RefreshToken__c##'+auth.goog_dclk_dsm__DC_RefreshToken__c);
    }
    if(authResult != null && (authResult.get(DC_Constants.ERROR) != null || authResult.get(DC_Constants.ERR_DESC) != null)) {
        connectorLog = DC_ConnectorLogger.log('Fail', (String) authResult.get(DC_Constants.ERR_DESC), null, (String) authResult.get(DC_Constants.ERROR));
    }
    return auth;        
    }
    
    public void saveSettings() {
    DC_SalesforceDML.getInstance().registerUpsert(this.auth);
    system.debug('@@@UserId'+this.auth);
    //upsert this.auth;
    DC_SalesforceDML.getInstance().registerUpsert(this.settings);
    //upsert this.settings;
    }
}