//
//  Controller giving access to batch record processing for installation
//
public with sharing class DC_BatchRecordProcessorController {
  private final PageReference jobStatus = new PageReference('/apexpages/setup/listAsyncApexJobs.apexp');

	public DC_BatchRecordProcessorController() {
		
	}

  public PageReference startAccountBatch() {
    Database.executeBatch(new DC_PushAccountToDSMBatch(), 10);
    return jobStatus;
  }

  public PageReference startContactBatch() {
    Database.executeBatch(new DC_PushContactToDSMBatch(), 10);
    return jobStatus;
  }  

  public PageReference startUserBatch() {
    Database.executeBatch(new DC_PushUserToDSMBatch(), 10);
    return jobStatus;
  }
}