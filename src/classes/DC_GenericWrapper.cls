// 
// (c) 2014 Appirio
//
// DC_GenericWrapper : Contains Generalized and abstract methods
// 
//
public abstract with sharing class DC_GenericWrapper {

  public goog_dclk_dsm__DC_ObjectMapping__c objectMapping {
    public get;
    protected set;
  }

  public List<goog_dclk_dsm__DC_FieldMapping__c> fieldMappings {
    public get;
    protected set;
  }

  public List<Object> objects;
  protected Object currentObject;
  protected Integer currentIndex;
  protected Map<String, goog_dclk_dsm__DC_FieldMapping__c> fieldNameMap;

  public abstract Boolean isSalesforce();
  public abstract Boolean isDoubleClick();

	//Construtor
  public DC_GenericWrapper(goog_dclk_dsm__DC_ObjectMapping__c objectMapping, List<goog_dclk_dsm__DC_FieldMapping__c> fieldMappings, List<Object> objects) {
    this.objectMapping = objectMapping;
    this.fieldMappings = fieldMappings;
    this.objects = objects != null ? objects : new List<Object>();
    this.currentIndex = -1;

    this.fieldNameMap = new Map<String, goog_dclk_dsm__DC_FieldMapping__c>();
    
    for(goog_dclk_dsm__DC_FieldMapping__c fieldMapping : fieldMappings) {
    	System.debug('fieldMapping::' + fieldMapping);
      fieldNameMap.put(fieldMapping.name.toLowerCase(), fieldMapping);
    }
  }
	
	//Abstract methods
  public abstract Object getField(String field);
  public abstract void setField(String field, Object value);
  public abstract DC_GenericWrapper getNewRecords();
  // TODO US: Deprecate this and use getMatchedRecords instead
  public abstract DC_GenericWrapper getExistingRecords();
  // TODO US: Deprecate this and use getMatchedRecords instead
  public abstract DC_GenericWrapper getCurrentWrapped();
  public abstract DC_GenericWrapper getUnmatchedRecords(String fieldName);
  public abstract DC_GenericWrapper getMatchedRecords(String fieldName);
  
  public long totalResultSetSize { get; set; }
  
  // returns records wrapped singularly
  public List<DC_GenericWrapper> getSingleWrappedRecords() {
  	List<DC_GenericWrapper> returnWrappers = new List<DC_GenericWrapper>();
  	this.gotoBeforeFirst();
  	
  	while (this.hasNext()) {
  		this.next();
  		returnWrappers.add(this.getCurrentWrapped());
  	}
  	
  	return returnWrappers;
  }
  
  //methods to go next/prev in collection : START
  public boolean hasNext() {
  	System.debug('this.currentIndex::' + this.currentIndex);
  	System.debug('this.size()::' + this.size());
    return (this.currentIndex + 1) < this.size();
  }
  
  public boolean hasPrevious() {
    return this.currentIndex > 0;
  }

  public void next() {
    if(this.hasNext()) {
      this.currentIndex++;
      System.debug('this.currentIndex:after increment:' + this.currentIndex);
      this.currentObject = this.objects.get(this.currentIndex);      
      System.debug('this.size():in next:' + this.size()); 
    } else {
      throw new DC_ConnectorException(DC_Constants.ARR_OUT_OF_BOUND_ERR);
    }
    
  }
  
  public void previous() {
    if(this.hasPrevious()) {
      this.currentIndex--;
      this.currentObject = this.objects.get(this.currentIndex);
    } else {
      throw new DC_ConnectorException(DC_Constants.ARR_OUT_OF_BOUND_ERR);
    }
  }
  
  public void gotoObject(Integer index) {
    if(index >= 0 && index < this.objects.size()) {
      this.currentIndex = index;
      this.currentObject = this.objects.get(this.currentIndex);
    } else if(index == -1) {
    	this.currentIndex = index;
      this.currentObject = null;
    } else {
      throw new DC_ConnectorException(DC_Constants.ARR_OUT_OF_BOUND_ERR);
    }
  }

  // Positions wrapper in the object identified by a given sfid, returns true if found, false otherwise
  public boolean gotoSfid(String sfid) {
    this.gotoBeforeFirst();

    string fieldText;
    while(this.hasNext()) {
      this.next();
      if((fieldText = (string)this.getField('sfid')) != null && fieldText.equalsIgnoreCase(sfid)) {
        return true;
      }
    }

    return false;
  }
  
  // Positions wrapper in the object identified by a given sfid, returns true if found, false otherwise
  public boolean gotoDCId(String dcid) {
    this.gotoBeforeFirst();

    string fieldText;
    while(this.hasNext()) {
      this.next();
      System.debug('hhsdjhsjdh::' + string.valueOf(this.getField('doubleclickid')));
      if((fieldText = string.valueOf(this.getField('doubleclickid'))) != null && fieldText.equalsIgnoreCase(dcid)) {
        return true;
      }
    }

    return false;
  }  
  
  public void gotoBeforeFirst() {
    this.gotoObject(-1);
  }
  
  public List<Object> getObjects() {
    return objects;
  }
  
  public Integer size() {
    return objects.size();
  }
  
  public void add(Object value) {
    this.objects.add(value);
  }
  
  public void addAll(List<Object> values) {
  	this.objects.addAll(values);
  }
  
  // return last entry in the collection
  public void gotoLast() {
    this.gotoObject(this.objects.size() - 1);
  }
	
	//methods to go next/prev in collection : END
	
  // update the fields on the current object with those of the provided source
  // source   DC_GenericWrapper   source object to use for fields to update on this target object
  // Use DC_ObjectMapper.updateFields instead
  /*
  public void updateFields(DC_GenericWrapper source) {
    for(goog_dclk_dsm__DC_FieldMapping__c fieldMapping : fieldMappings) {
      this.setField(fieldMapping.Name, source.getField(fieldMapping.Name));
    }
  }*/
  
  public Object getCurrentObject() {
    return this.currentObject;
  }
  
  public Integer getCurrentIndex() {
    return this.currentIndex;
  }
  
  //conversion to String
  public String toString(Object obj) {
    if(obj == null) {
      return null;
    }

    if(obj instanceof String) {
      return (String)obj;
    } else if(obj instanceof Integer) {
      return String.valueOf(obj);
    } else if(obj instanceof DC_CompanyService.Date_x) {
      // TO DO-Done handle Date_x type
      //Doubt-Does it really needed now?
      DC_CompanyService.Date_x d= (DC_CompanyService.Date_x)obj;
      return(toString(d.year)+'-'+toString(d.month)+'-'+toString(d.day));
    } else if(obj instanceof DC_CompanyService.DateTime_x) {
	    // TO DO-Done handle DateTime_x type
	    //Doubt-Does it really needed now?
	    DC_CompanyService.DateTime_x dt= (DC_CompanyService.DateTime_x)obj;
      return(toString(dt.date_x.year)+'-'+toString(dt.date_x.month)+'-'+toString(dt.date_x.day)+toString(dt.hour)+':'+toString(dt.minute)+':'+toString(dt.second)+'Z');
    } else if(obj instanceOf DC_ProposalLineItemService.CreativePlaceholder[]) {
      string returnValue = '';
      DC_ProposalLineItemService.CreativePlaceholder[] placeholders = (DC_ProposalLineItemService.CreativePlaceholder[])obj;
      for (DC_ProposalLineItemService.CreativePlaceholder placeholder : placeholders) {
        returnValue += ((returnValue == '') ? '' : ', ') + placeholder.size.width + 'x' + placeholder.size.height;
      }
      return returnValue;
    } else {
      return String.valueOf(obj);
    }

    throw new DC_ConnectorException(DC_Constants.UNSUPPORTED_DATA_CONVERT_ERR + ' - ' + obj);
  }

  public Long toLong(Object obj) {
    if(obj == null) {
      return null;
    }

    if(obj instanceof Long || obj instanceof Integer) {
      return (Long) obj;
    } else if(obj instanceof String) {
      return Long.valueOf((String)obj);
    } else if(obj instanceof Boolean) {
      return (Boolean) obj ? 1 : 0;
    } else if(obj instanceof Double) {
      return ((Double)obj).round();
    }

    throw new DC_ConnectorException(DC_Constants.UNSUPPORTED_DATA_CONVERT_ERR + ' - ' + obj);
  }
  
  public Decimal toDecimal(Object obj) {
    if(obj == null) {
      return null;
    }
    if(obj instanceof Decimal) {
      return (Decimal) obj;
    } else if(obj instanceof Long) {
      return Decimal.valueOf((long)obj);
    } else if(obj instanceof String) {
      return Decimal.valueOf((String)obj);
    } else if(obj instanceof Boolean) {
      return (Boolean) obj ? 1 : 0;
    } else if(obj instanceof Double) {
      return ((Double)obj).longValue();
    } else if(obj instanceof DC_ServiceMoney) {
    	return ((DC_ServiceMoney)obj).microAmount / 1000000;
    }

    throw new DC_ConnectorException(DC_Constants.UNSUPPORTED_DATA_CONVERT_ERR + ' - ' + obj);    
  }
  
  //conversion to Integer
  public Integer toInteger(Object obj) {
    if(obj == null) {
      return null;
    }
    if(obj instanceof Integer) {
      return (Integer) obj;
    } else if(obj instanceof Long) {
      return ((Long)obj).intValue();
    } else if(obj instanceof String) {
      return Integer.valueOf((String)obj);
    } else if(obj instanceof Boolean) {
      return (Boolean) obj ? 1 : 0;
    } else if(obj instanceof Double) {
      return ((Double)obj).round().intValue();
    }

    throw new DC_ConnectorException(DC_Constants.UNSUPPORTED_DATA_CONVERT_ERR + ' - ' + obj);
  }
	
	//conversion to boolean
  public Boolean toBoolean(Object obj) {
    if(obj == null) {
      return null;
    }

    if(obj instanceof Boolean) {
      return (Boolean) obj;
    } else if(obj instanceof Long) {
      return ((Long)obj) == 1;
    } else if(obj instanceof Integer) {
      return ((Integer)obj) == 1;
    } else if(obj instanceof String) {
      return Boolean.valueOf((String) obj);
    }

    throw new DC_ConnectorException(DC_Constants.UNSUPPORTED_DATA_CONVERT_ERR + ' - ' + obj);
  }
  
  public Long[] toLongArr(Object obj) {
    if(obj == null) {
      return null;
    }
    if(obj instanceof Long[] || obj instanceof Integer[]) {
      return (Long[]) obj;
    } 
    throw new DC_ConnectorException(DC_Constants.UNSUPPORTED_DATA_CONVERT_ERR + ' - ' + obj);
  }
  
  public String[] toStringArr(Object obj) {
    if(obj == null) {
      return null;
    }
    if(obj instanceof String[]) {
      return (String[]) obj;
    } 
    throw new DC_ConnectorException(DC_Constants.UNSUPPORTED_DATA_CONVERT_ERR + ' - ' + obj);
  }

}