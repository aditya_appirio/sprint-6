// 
// DoubleClick Connector Queued Process Dispatcher
// Process dequeues items and processes them
// 
global class DC_QueueDispatch implements Schedulable {
	private id queueItemScheduled;
	
	public DC_QueueDispatch() {}
	// schedule the following queue item for immediate execution
	public DC_QueueDispatch(Id queueItem) {
		queueItemScheduled = queueItem;
	}
	
	// schedule for immediate execution the queue item that is passed
	public static void scheduleForImmediateExecution(Id queueItem) {
		DateTime n = DateTime.now();
		n.addMinutes(1);
		
		string scheduleString = String.format('{0} {1} {2} {3} {4} {5}', new String[] { //Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
        String.valueOf(n.second()),
        String.valueOf(n.minute()),
        String.valueOf(n.hour()),
        String.valueOf(n.day()),
        String.valueOf(n.month()),
        '?'//String.valueOf(n.year())
		  });
		
		System.schedule('DC Connector Immediate - ' + queueItem, scheduleString, new DC_QueueDispatch(queueItem));
	}
	
	// schedule the process to execute certain times per hour
	// this will create multiple scheduled processes
	public static void scheduleQueue(integer timesPerHour){
		if (timesPerHour > 60 || timesPerHour < 0)
		  throw new DC_ConnectorException('Value for timesPerHour must be between 1 and 60');
		
		// number of minutes between executions
		integer minutes = (60/timesPerHour);
		
		// schedule process
		for (integer i = 0; i < timesPerHour; i++) {
			integer minute = i * minutes;
      System.schedule('DC Connector Sync ' + (((minute) < 10) ? '0' + (minute): string.valueOf(minute)), '0 ' + minute + ' * * * ?', new DC_QueueDispatch());
		}
	}
	
	// execute process defined by the Schedulable interface. Get the queued item and execute.
  public void execute(SchedulableContext SC) {
  	if (queueItemScheduled != null) {
      startQueueItem(queueItemScheduled);
      return;
  	}
  	
  	integer ct = (integer)goog_dclk_dsm__DC_ConnectorApplicationSettings__c.getInstance().goog_dclk_dsm__DC_SimultaneousQueueExecutionCount__c;
  	if (ct == null)
      ct = 1;
      
    // replace LIMIT with number from custom setting for number of concurrent
    List<goog_dclk_dsm__DC_ProcessingQueue__c> queueItems = [SELECT id, goog_dclk_dsm__DC_Process__c, goog_dclk_dsm__DC_Executing__c, goog_dclk_dsm__DC_Asynchronous__c 
                                              FROM goog_dclk_dsm__DC_ProcessingQueue__c 
                                              WHERE goog_dclk_dsm__DC_Active__c = true AND goog_dclk_dsm__DC_Status__c = 'Queued' 
                                              ORDER BY goog_dclk_dsm__DC_LastExecution__c ASC 
                                              LIMIT :ct];
                                              
    // if nothing to dequeue, exit
    if (queueItems.size() == 0)
      return;
    
    executeQueueItems(queueItems);
  }
  
  private void executeQueueItems(List<goog_dclk_dsm__DC_ProcessingQueue__c> queueItems) {
    // loop through queueu items, indicate execution and set last execution
    for (goog_dclk_dsm__DC_ProcessingQueue__c queue : queueItems) { 
      queue.goog_dclk_dsm__DC_Executing__c = true;
      queue.goog_dclk_dsm__DC_LastExecution__c = datetime.now(); 
    }

    // register update of queue items    
    DC_SalesforceDML.getInstance().registerUpdate(queueItems); 
     
    // execute queued items
    for (goog_dclk_dsm__DC_ProcessingQueue__c queueItem : queueItems) {
      // if asynchronous, send for async dispatch
      if (queueItem.goog_dclk_dsm__DC_Asynchronous__c) {
        asyncDispatch(queueItem.Id);
      } else { 
        dispatch(queueItem);
      }
    }
    try {
      DC_SalesforceDML.getInstance().flush();
    } catch (Exception e) {
      DC_ConnectorLogger.log('Queue Dispatch', e);
    }
  }
  
  public void startQueueItem(Id queueItemId) {
    List<goog_dclk_dsm__DC_ProcessingQueue__c> queueItems = [SELECT id, goog_dclk_dsm__DC_Process__c, goog_dclk_dsm__DC_Executing__c, goog_dclk_dsm__DC_Asynchronous__c 
                                              FROM goog_dclk_dsm__DC_ProcessingQueue__c 
                                              WHERE id = :queueItemId];   
    if (queueItems.size() == 0)
      return;
      
    executeQueueItems(queueItems);
  }
  
  webService static void remoteStartProcess(id queueItemId) {
  	DC_QueueDispatch q = new DC_QueueDispatch();
  	q.startQueueItem(queueItemId);

  }     

  // get the process related to the queueitem and execute it
  public static void dispatch(goog_dclk_dsm__DC_ProcessingQueue__c queueItem) {
    
    try {
      DC_QueueableProcess execprocess = DC_QueueableProcessFactory.getQueueableProcess(queueItem, queueItem.goog_dclk_dsm__DC_Process__c, null);
      execprocess.execute();
    } catch (Exception e) {
    	DC_ConnectorLogger.log(queueItem.goog_dclk_dsm__DC_Process__c, e, queueItem.Id);
    }
  }
  
  // asynchronously execute the process related to the queue item id
  @future(callout=true)
  public static void asyncDispatch(id queueItemId) {
  	List<goog_dclk_dsm__DC_ProcessingQueue__c> items = [SELECT id, goog_dclk_dsm__DC_Process__c FROM goog_dclk_dsm__DC_ProcessingQueue__c WHERE id = :queueItemId];
  	
    if (items == null || items.size() == 0)
      return;

    try {
      DC_QueueableProcess execprocess = DC_QueueableProcessFactory.getQueueableProcess(items[0], items[0].goog_dclk_dsm__DC_Process__c, null);    
      execprocess.execute();
    } catch (Exception e) {
    	DC_ConnectorLogger.log(items[0].goog_dclk_dsm__DC_Process__c, e, queueItemId);
    }
  } 
  
  // called by the process when completed to have the queue updated.
  public static void queueItemCompleted(goog_dclk_dsm__DC_ProcessingQueue__c queueItem, DC_ExecutionStatus lastExecutionStatus, String details) {
    DC_SalesforceDML.getInstance().registerUpdate(new goog_dclk_dsm__DC_ProcessingQueue__c(id=queueItem.Id, goog_dclk_dsm__DC_Executing__c = false, goog_dclk_dsm__DC_LastExecutionStatus__c = executionStatusStrings.get(lastExecutionStatus)));
    //List<DC_ProcessingQueue__c> items = [SELECT id, DC_Process__c FROM DC_ProcessingQueue__c WHERE id = :queueId];
    //DC_ConnectorLogger.log(items[0].DC_Process__c + ' Completed', details, queueId);
     
    DC_ConnectorLogger.log(queueItem.goog_dclk_dsm__DC_Process__c, queueItem.goog_dclk_dsm__DC_Process__c + ' ' + executionLogStrings.get(lastExecutionStatus) ,
      (lastExecutionStatus == DC_ExecutionStatus.Success) ? DC_ConnectorLogger.Status.Success : DC_ConnectorLogger.Status.Error, details, queueItem.Id);
  }
  
  public static Map<DC_ExecutionStatus, String> executionStatusStrings = new Map<DC_ExecutionStatus, String> {
    DC_ExecutionStatus.Success => 'Successful', 
    DC_ExecutionStatus.Failure => 'Failed',
    DC_ExecutionStatus.Error => 'Error',
    DC_ExecutionStatus.Recordset_Updated => 'Updated Records'
  };
  
  public static Map<DC_ExecutionStatus, String> executionLogStrings = new Map<DC_ExecutionStatus, String> {
  	DC_ExecutionStatus.Success => 'completed successfully', 
  	DC_ExecutionStatus.Failure => 'failed',
  	DC_ExecutionStatus.Error => 'encountered errors',
  	DC_ExecutionStatus.Recordset_Updated => '- records updated while executing'
  };
  
  public enum DC_ExecutionStatus {
    Success,
    Failure,
    Error,
    Recordset_Updated    
  }

  // add queue management methods
}