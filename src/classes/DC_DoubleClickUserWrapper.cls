// 
// (c) 2014 Appirio
//
// DC_DoubleClickUserWrapper 
// T-251378
// 20 Feb 2014   Anjali K (JDC)      Original
//
public with sharing class DC_DoubleClickUserWrapper extends DC_DoubleClickWrapper{
	
	//constructor : passing values to DC_DoubleClickWrapper constructor
	public DC_DoubleClickUserWrapper(goog_dclk_dsm__DC_ObjectMapping__c objectMapping, List<goog_dclk_dsm__DC_FieldMapping__c> fieldMappings, List<Object> objects) {
    super(objectMapping, fieldMappings, objects);
  }
  
  // Method to get current User
  private DC_UserService.User_x getCurrentUser() {
    if(this.currentObject == null){
      throw new DC_ConnectorException(DC_Constants.ARR_OUT_OF_BOUND_ERR);
    } else if(!(this.currentObject instanceOf DC_UserService.User_x)){
        throw new DC_ConnectorException(DC_Constants.INVALID_USER_MAPPING);
    } else{
        return (DC_UserService.User_x) this.currentObject;
    } 
  }
  
  // Overridden getter for User fields 
  public override Object getField(String field) {
  	System.debug('field::' + field);
    String fieldName = getDoubleClickFieldName(field.toLowerCase());
    System.debug('fieldName::' + fieldName +'==' + fieldName.toLowerCase().equals('roleId'));
    if(fieldName.toLowerCase().equals('id')) {
      return getCurrentUser().id;
    } else if(fieldName.toLowerCase().equals('name')) {
        return getCurrentUser().name;
    } else if(fieldName.toLowerCase().equals('email')) {
        return getCurrentUser().email;
    } else if(fieldName.toLowerCase().equals('roleid')) {
        return getCurrentUser().roleId;
    } else if(fieldName.toLowerCase().equals('rolename')) {
        return getCurrentUser().roleName;
    } else if(fieldName.toLowerCase().equals('externalid')) {
        return getCurrentUser().externalId;        
    } else if(fieldName.toLowerCase().equals('preferredlocale')) {
        return getCurrentUser().preferredLocale;
    } else if(fieldName.toLowerCase().equals('userrecord_type')) {
        return getCurrentUser().UserRecord_Type;
		} else if(fieldName.equalsIgnoreCase('ordersuilocaltimezoneid')) {
	      return getCurrentUser().ordersUiLocalTimeZoneId;
		} else if(fieldName.equalsIgnoreCase('isemailnotificationallowed')) {
	      return getCurrentUser().isEmailNotificationAllowed;
    } else if(fieldName.equalsIgnoreCase('isactive')) {
        return getCurrentUser().isActive;	      
		} else if(fieldName.startsWithIgnoreCase('custom')) {
      Long customFieldId = getCustomFieldId(fieldName);

      if(customFieldId != null) {
        return getTextCustomFieldValue(customFieldId);
      } else {
        throw new DC_ConnectorException(DC_Constants.CUSTOM_FIELD_ERROR_STR);
      }
    }
    else {
      throw new DC_ConnectorException(DC_Constants.getInvalid_Mapping_User(field));
    }
  }
  
  // method to get custom type text field value : 19th March Anjali
  private String getTextCustomFieldValue(Long fieldId) {
    if(getCurrentUser().customFieldValues != null) {

      for(DC_UserService.BaseCustomFieldValue fieldValue : getCurrentUser().customFieldValues) {
        
        if (fieldValue instanceOf DC_UserService.CustomFieldValue) {
          DC_UserService.CustomFieldValue curfield = (DC_UserService.CustomFieldValue)fieldValue;
          if(curfield.customFieldId == fieldId) {    
            if (curfield.value instanceOf DC_UserService.TextValue) {
              return ((DC_UserService.TextValue)curfield.value).value;
            }
          }
        }
      } 
    }
    return null;
  }

	// method to set custom type text field value
  private void setTextCustomFieldValue(Long fieldId, String value) {
    DC_UserService.CustomFieldValue field = new DC_UserService.CustomFieldValue();
    field.value = new DC_UserService.TextValue();
    field.customFieldId = fieldId;
    ((DC_UserService.TextValue)field.value).value = this.toString(value);

    if(getCurrentUser().customFieldValues == null) {
      getCurrentUser().customFieldValues = new List<DC_UserService.BaseCustomFieldValue>();
    } else {
      Integer idx = 0;
      for(DC_UserService.BaseCustomFieldValue fieldValue : getCurrentUser().customFieldValues) {
        if(fieldValue.customFieldId == fieldId) {
          getCurrentUser().customFieldValues.remove(idx);
          break;
        } 
        idx++;
      }
    }

    getCurrentUser().customFieldValues.add(field);
  }
  
  
  // Overridden setter for User fields 
  public override void setField(String field, Object value) {
    String fieldName = getDoubleClickFieldName(field);
    System.debug('getCurrentUser():::' + getCurrentUser());
    try{
      if(fieldName.equalsIgnoreCase('id')) {
      	getCurrentUser().id = this.toInteger(value);  
      } else if(fieldName.equalsIgnoreCase('name')) {
        getCurrentUser().name = this.toString(value);
      } else if(fieldName.equalsIgnoreCase('email')) {
        getCurrentUser().email = this.toString(value); 
      } else if(fieldName.toLowerCase().equals('roleid')) {
      	getCurrentUser().roleId = this.toLong(value);
	    } else if(fieldName.toLowerCase().equals('rolename')) {
	    	getCurrentUser().roleName = this.toString(value);
	    } else if(fieldName.toLowerCase().equals('preferredlocale')) {
	    	getCurrentUser().preferredLocale = this.toString(value);
	    } else if(fieldName.toLowerCase().equals('userrecord_type')) {
	    	getCurrentUser().UserRecord_Type = this.toString(value);
      } else if(fieldName.toLowerCase().equals('externalid')) {
        getCurrentUser().externalid = this.toString(value);	    	
	    } else if(fieldName.equalsIgnoreCase('ordersuilocaltimezoneid')) {
        getCurrentUser().ordersUiLocalTimeZoneId = this.toString(value); 
      } else if(fieldName.equalsIgnoreCase('isemailnotificationallowed')) {
        getCurrentUser().isEmailNotificationAllowed = this.toBoolean(value); 
      } else if(fieldName.equalsIgnoreCase('isActive')) {
      	getCurrentUser().isActive = this.toBoolean(value);
			} else if(fieldName.startsWithIgnoreCase('custom')) {
        Long customFieldId = getCustomFieldId(fieldName);

        if(customFieldId != null) {
          setTextCustomFieldValue(customFieldId, (String)value);
        } else {
          // TO DO:Done Jaipur externalize this message
          throw new DC_ConnectorException(DC_Constants.CUSTOM_FIELD_ERROR_STR);
        }
      }
      else {
        throw new DC_ConnectorException(DC_Constants.getInvalid_Mapping_User(field));
      }
    }
    catch(System.TypeException e){
      throw new DC_ConnectorException(e+DC_Constants.FOR_FLD_STR+field);
    }
  }
  
  // Overridden for getting new user wrapper
  public override DC_GenericWrapper getNewRecords() {
    this.gotoBeforeFirst();
    List<Object> result = new List<Object>();
    
    while(this.hasNext()) {
      this.next();
      System.debug('this.getField(DoubleClickId):new::' + this.getField('DoubleClickId'));
      if(this.getField('DoubleClickId') == null) {
        result.add(this.currentObject);
      }
    } 
    
    return new DC_DoubleClickUserWrapper(this.objectMapping, this.fieldMappings, result);
  }
  
  // Overridden for getting existing user wrapper
  public override DC_GenericWrapper getExistingRecords() {
    this.gotoBeforeFirst();
    List<Object> result = new List<Object>();
    
    while(this.hasNext()) {
      this.next();
      System.debug('this.getField(DoubleClickId):exist::' + this.getField('DoubleClickId'));
      if(this.getField('DoubleClickId') != null) {
        result.add(this.currentObject);
      }
    }
    
    return new DC_DoubleClickUserWrapper(this.objectMapping, this.fieldMappings, result);
  } 
  
  // Overridden for getting current user wrapper
  public override DC_GenericWrapper getCurrentWrapped() {
    return new DC_DoubleClickUserWrapper(this.objectMapping, this.fieldMappings, new object[] { this.currentObject });
  }  
	
	// Overridden for creating user wrapper
	public override DC_GenericWrapper createWrapper(goog_dclk_dsm__DC_ObjectMapping__c objectMapping, List<goog_dclk_dsm__DC_FieldMapping__c> fieldMappings, List<Object> objects) {
    return new DC_DoubleClickUserWrapper(objectMapping, fieldMappings, objects);
  }
}