// 
// (c) 2014 Appirio
//
// custom handler for the pushing and pulling of opportunity/proposal team members
//
//
public with sharing class DC_CustomProposalTeamMemberHandler implements DC_FieldHandlerFactory.DC_FieldMappingCustomHandler {
  private goog_dclk_dsm__DC_ObjectMapping__c objectMapping;
  private List<goog_dclk_dsm__DC_FieldMapping__c> fieldMappings;
  private DC_GenericWrapper source, target; 
    
  private boolean toSalesforce;

  private List<Id> opportunityIds;
  private List<String> proposalIds;
  private List<String> dcUserIds;
  private Map<String, DC_UserRoleInfo> userInfoByProposalId; 
  private Map<Id, Map<String, DC_UserRoleInfo>> keyedUserRoleInfoByOppId;    
  private Map<String, DC_UserRoleInfo> userRoleInfoByKey;
  //private static Schema.sObjectField dctmKey = goog_dclk_dsm__DoubleClick_Team_Member__c.goog_dclk_dsm__DC_RecordKey__c.getDescribe().getSObjectField();

  private String currentOppId;
  private Map<String, String> dcUserRoleMap; //Map of dc user id and role string
  private Map<String, String> proposalToOppMap; //Map of Proposal and Opportunity id
  private Map<String, Integer> proposalToSplitMap; //Map of Proposal and Split percent
  private List<goog_dclk_dsm__DC_DoubleClickTeamMember__c> dcUsersListToUpsert;
  private   List<OpportunityTeamMember> oppTeamMemberListToUpsert; 
  private Set<String> oppListSource;
  
  private Map<Id, Integer> targetIndexByOppId;
  
  String proposalId;  
  public DC_CustomProposalTeamMemberHandler(){ }
  
  public string getSalesforceQueryString() { 
  	return ''; 
  }
    
  /* INITIALIZE ============================ */
  
  public void initialize(goog_dclk_dsm__DC_ObjectMapping__c inObjectMapping, List<goog_dclk_dsm__DC_FieldMapping__c> inFieldMappings, DC_GenericWrapper inSource, DC_GenericWrapper inTarget){
    toSalesforce = inSource.isDoubleClick(); 
    objectMapping = inObjectMapping;
    fieldMappings = inFieldMappings;
    source = inSource;
    target = inTarget;
    if (toSalesforce) {
      initializeDCtoSF();
    } else {
      initializeSFtoDC();
    }
  }

  public void finish(){
    // At this point, with all values collected from processField, perform necessary queries and creation of upsert records
    if (toSalesforce) {
      finishDCtoSF();
    } else {
      finishSFtoDC();
    }       
  } 
  
  public void processField(goog_dclk_dsm__DC_FieldMapping__c currentField, DC_GenericWrapper source, DC_GenericWrapper target){
    // Anjali -- in this method collect together into a class level variable the values you'll need for the final queries, to be performed in finish() 
    // NOTE: you don't need to only collect the field mentioned in currentField -- you can collect all that you need from the record.
    // this will be called for each new record.
    if (toSalesforce) {
      processFieldDCtoSF();
    } else {
      processFieldSFtoDC();
    }   
  }  
  
  // DC TO SF ==================================================
  
  //init variables
  private void initializeDCtoSF() {
    if(dcUserIds == null){
      dcUserIds = new List<String>();
    }
    if(userInfoByProposalId == null){
      userInfoByProposalId = new Map<String, DC_UserRoleInfo>();
    }
    if(dcUserRoleMap == null){
      dcUserRoleMap = new Map<String, String>();
    }
    if(proposalToOppMap == null){
      proposalToOppMap = new Map<String, String>();
    }
    if(proposalToSplitMap == null){
      proposalToSplitMap = new Map<String, Integer>();
    }
    if(dcUsersListToUpsert == null){
      dcUsersListToUpsert = new List<goog_dclk_dsm__DC_DoubleClickTeamMember__c>();
    }
    if(oppTeamMemberListToUpsert == null){
      oppTeamMemberListToUpsert = new List<OpportunityTeamMember>();
    }
      
    opportunityIds = new List<Id>();
    proposalIds = new List<String>();
    dcUserIds = new List<String>();
    userInfoByProposalId = new Map<String, DC_UserRoleInfo>();
    keyedUserRoleInfoByOppId = new Map<Id, Map<String, DC_UserRoleInfo>>();    
    userRoleInfoByKey = new Map<String, DC_UserRoleInfo>();
  }
    

  private DC_UserRoleInfo recordUserRole(string oppId, long userId, string roleName, decimal split) {  
    DC_UserRoleInfo info = recordUserRole(oppId, userId, roleName);
    info.splitPercent = split;
    
    return info;    
  }
  
  private DC_UserRoleInfo recordUserRole(string oppId, long userId, string roleName) {
    string tkey = oppId + '^' + String.valueOf(userId) + '^' + roleName;
    dcUserIds.add(string.valueOf(userId));
    DC_UserRoleInfo tmpRoleInfo = new DC_UserRoleInfo(string.valueOf(userId), roleName);
    keyedUserRoleInfoByOppId.get(oppId).put(tkey, tmpRoleInfo);
    userRoleInfoByKey.put(tkey, tmpRoleInfo); 
    return tmpRoleInfo;
  }


  
  private void processFieldDCtoSF() {
     
    String oppId = String.valueOf(source.getField('sfid'));
    if (oppId == null || oppId == '')
      return;
      
    keyedUserRoleInfoByOppId.put(oppId, new Map<String, DC_UserRoleInfo>());
    opportunityIds.add(oppId);
    string tkey;
    if ((Long)source.getField('primaryTraffickerId') != null) {
      recordUserRole(oppId, (Long)source.getField('primaryTraffickerId'), 'Primary Trafficker');
    }
    
    if ((Long[])source.getField('secondaryTraffickerIds') != null) {
      for(Long tId : (Long[])source.getField('secondaryTraffickerIds')){
        recordUserRole(oppId, tid, 'Secondary Trafficker');
      }
    }  
    
    if ((Long[])source.getField('salesPlannerIds') != null) {
      for(Long spId : (Long[])source.getField('salesPlannerIds')){
        recordUserRole(oppId, spid, 'Sales Planner');
      }
    }
    
    if (source.getField('primarySalesperson') != null) {
      DC_ProposalService.SalespersonSplit s = (DC_ProposalService.SalespersonSplit)source.getField('primarySalesperson');
      recordUserRole(oppId, s.userId, 'Primary Sales Rep', s.split);
    }
    
    if (source.getField('secondarySalespeople') != null) {
      for(DC_ProposalService.SalespersonSplit s : (DC_ProposalService.SalespersonSplit[])source.getField('secondarySalespeople')){
        recordUserRole(oppId, s.userId, 'Secondary Sales Rep', s.split);
      }
    }     
  
   
  }  
  
  // complete the dc to sf process
  private void finishDCtoSF() {
    DC_SalesforceDML dmlInstance = DC_SalesforceDML.getInstance();
    Map<String, goog_dclk_dsm__DC_User__c> usersByDCUserId = new Map<String, goog_dclk_dsm__DC_User__c>();
    Set<String> dcUserIdSet = new Set<String>(dcUserIds);
    
    System.debug('DC User Ids: ' + dcUserIds);
    
    List<goog_dclk_dsm__DC_User__c> dcUsers = [SELECT id, Name, goog_dclk_dsm__DC_DoubleClickId__c, goog_dclk_dsm__DC_SalesforceUserRecord__c, goog_dclk_dsm__DC_SalesforceUserRecord__r.Id FROM goog_dclk_dsm__DC_User__c WHERE goog_dclk_dsm__DC_DoubleClickId__c IN :dcUserIds];
    
    // get the salesforce user ids
    List<Id> salesforceUserIds = getSalesforceUserIds(dcUsers);
        
    // loop through the users retrieved and create a handy map by dc user id
    for (goog_dclk_dsm__DC_User__c u : dcUsers) {
      usersByDCUserId.put(u.goog_dclk_dsm__DC_DoubleClickId__c, u);
      // track our original list to see if anything is left that will need to be added
      dcUserIdSet.remove(u.goog_dclk_dsm__DC_DoubleClickId__c);
    }
    
    // if we have user ids that weren't returned, we'll insert them here with dummy info
    if (dcUserIdSet.size() > 0) {
      List<goog_dclk_dsm__DC_User__c> usersForInsert = new List<goog_dclk_dsm__DC_User__c>();
      for (String uid : dcUserIdSet) {
        goog_dclk_dsm__DC_User__c newUser = new goog_dclk_dsm__DC_User__c(name = 'Not Yet Defined', goog_dclk_dsm__DC_DoubleClickId__c = uid);
        usersByDCUserId.put(uid, newUser);
        usersForInsert.add(newUser);
      }
      // the insert should populate the dc user records with a salesforce id and allow the process to continue without issue
      dmlInstance.registerInsert(usersForInsert);
      dmlInstance.flush();
    }

    Map<String, OpportunityTeamMember> otmsForUpsert = new Map<String, OpportunityTeamMember>();
    Map<String, goog_dclk_dsm__DC_DoubleClickTeamMember__c> dctmsForUpsert = new Map<String, goog_dclk_dsm__DC_DoubleClickTeamMember__c>();
    List<string> otmKeys = new List<string>();
    List<string> dctmKeys = new List<string>();

    
    
    
    // loop through the different keys to generate upsertable rows
    for (Id oppId : keyedUserRoleInfoByOppId.keySet()) {
      for (String rkey : keyedUserRoleInfoByOppId.get(oppId).keySet()) {
        DC_UserRoleInfo info = keyedUserRoleInfoByOppId.get(oppId).get(rkey); 
          
        info.userRecord = usersByDCUserId.get(info.dcUserId);
        System.debug('User Record for Id ' + info.dcUserId + ': ' + info.userRecord); 
        if (info.userRecord.goog_dclk_dsm__DC_SalesforceUserRecord__c != null) {
          otmsForUpsert.put(rkey, createOpportunityTeamMember(rkey, oppId, info));
          otmKeys.add(rkey);
        } else {
          dctmsForUpsert.put(rkey, createDoubleClickTeamMember(rkey, oppId, info));
          dctmKeys.add(rkey);
        }
      }
    }
    /*
    
    //dmlInstance.registerUpsert(otmsForUpsert, otmKey); 
    dmlInstance.registerUpsert(dctmsForUpsert, dctmKey);
    
    // delete query for records no longer relevant
    dmlInstance.registerDelete(
      [SELECT id FROM goog_dclk_dsm__DoubleClick_Team_Member__c WHERE goog_dclk_dsm__opportunity__c IN :keyedUserRoleInfoByOppId.keySet() AND goog_dclk_dsm__DC_RecordKey__c NOT IN :dctmKeys AND goog_dclk_dsm__DC_RecordKey__c != null]
    );
    */
    reconcileDoubleClickTeamMembers(dctmsForUpsert);
    reconcileOpportunityTeamMembers(otmsForUpsert);
    dmlInstance.flush();
    
  }   

  private void reconcileDoubleClickTeamMembers(Map<String, goog_dclk_dsm__DC_DoubleClickTeamMember__c> incomingDCTM) {
    // get doubleClick team members 
    System.debug('@@ reconcileDoubleClickTeamMembers -- opportunities: ' + opportunityIds);
    List<goog_dclk_dsm__DC_DoubleClickTeamMember__c> existingTeam = [SELECT id, goog_dclk_dsm__DC_Opportunity__r.Id, goog_dclk_dsm__dc_user__r.goog_dclk_dsm__dc_doubleclickid__c, goog_dclk_dsm__DC_Role__c FROM goog_dclk_dsm__DC_DoubleClickTeamMember__c WHERE goog_dclk_dsm__DC_Opportunity__c IN :opportunityIds];

    Map<String, goog_dclk_dsm__DC_DoubleClickTeamMember__c> membersByKey = new Map<String, goog_dclk_dsm__DC_DoubleClickTeamMember__c>();
    // key existing team by concatenated key
    for (goog_dclk_dsm__DC_DoubleClickTeamMember__c member : existingTeam) {
      membersByKey.put(
          member.goog_dclk_dsm__DC_Opportunity__r.Id + '^' + member.goog_dclk_dsm__dc_user__r.goog_dclk_dsm__dc_doubleclickid__c + '^' + member.goog_dclk_dsm__DC_Role__c, 
          member);
    }

    System.debug('@@ members by key: ' + membersByKey); 
    
    Set<String> retentionKeys = membersByKey.keySet().clone();
    Set<String> deleteKeys = membersByKey.keySet().clone();
    Set<String> incomingKeys = incomingDCTM.keySet().clone();
    
    retentionKeys.retainAll(incomingKeys); // these are the keys to be updated
    deleteKeys.removeAll(incomingKeys); // the remaining keys are those to be deleted
    
    // update doubleclick team members with opportunity ids
    for (String key : retentionKeys) { 
      incomingDCTM.get(key).id = membersByKey.get(key).id;
    }
    System.debug('@@ incoming keys: ' + incomingDCTM.keySet());
    System.debug('@@ upsert values: ' + incomingDCTM.values());
    DC_SalesforceDML.getInstance().registerUpsert(incomingDCTM.values());
    System.debug('@@ delete keys: ' + deleteKeys);
    membersByKey.keySet().retainAll(deleteKeys);
    DC_SalesforceDML.getInstance().registerDelete(membersByKey.values());

  }

  // used to handle opportunity team members upsert and delete
  private void reconcileOpportunityTeamMembers(Map<String, OpportunityTeamMember> incomingOTM) {
    // get opportunity team members -- key by special key
    // construct the keys
    
    // only contains existing users
    
    
    // need dc users keyed by 
    List<OpportunityTeamMember> members = [SELECT id, userid, opportunityId, teammemberrole FROM OpportunityTeamMember WHERE opportunityId IN :keyedUserRoleInfoByOppId.keySet()];
    System.debug('@@ otm retrieved: ' + members);
    Map<String, OpportunityTeamMember> membersByKey = new Map<String, OpportunityTeamMember>();
    
    // collect sf user ids
    List<String> dcUserIds = new List<String>();
    for (OpportunityTeamMember member : members) {
      dcUserIds.add(member.userid);
    }
    
    // get dc users by sf id
    Map<Id, goog_dclk_dsm__DC_User__c> dcUsersBySFUserId = getDCUsersBySFUserId([SELECT goog_dclk_dsm__dc_doubleclickid__c, goog_dclk_dsm__DC_SalesforceUserRecord__c, goog_dclk_dsm__DC_SalesforceUserRecord__r.Id FROM goog_dclk_dsm__DC_User__c where goog_dclk_dsm__DC_SalesforceUserRecord__c IN :dcUserIds]);
    
    System.debug('@@ dc users by sf user id: ' + dcUsersBySFUserId);
    for (OpportunityTeamMember member : members) {
      if (dcUsersBySFUserId.containsKey(member.userid)) {
        membersByKey.put(
          member.opportunityId + '^' + dcUsersBySFUserId.get(member.userId).goog_dclk_dsm__dc_doubleclickid__c + '^' + member.teammemberrole, 
          member);
      }
    }  
    
    System.debug('@@ members by key: ' + membersByKey);
    
    Set<String> retentionKeys = membersByKey.keySet().clone();
    Set<String> deleteKeys = membersByKey.keySet().clone();
    Set<String> incomingKeys = incomingOTM.keySet().clone();
    
    retentionKeys.retainAll(incomingKeys); // these are the keys to be updated
    deleteKeys.removeAll(incomingKeys); // the remaining keys are those to be deleted
    
    // update opportunity team members with opportunity ids
    for (String key : retentionKeys) {
      incomingOTM.get(key).id = membersByKey.get(key).id;
    }
    System.debug('@@ incoming keys: ' + incomingOTM.keySet());
    DC_SalesforceDML.getInstance().registerUpsert(incomingOTM.values());
    System.debug('@@ delete keys: ' + deleteKeys);
    membersByKey.keySet().retainAll(deleteKeys);
    DC_SalesforceDML.getInstance().registerDelete(membersByKey.values());
  }

  
  private List<Id> getSalesforceUserIds(List<goog_dclk_dsm__DC_User__c> dcUserRecords) {
    List<id> returnIds = new List<Id>();
    for (goog_dclk_dsm__DC_User__c dcUser : dcUserRecords) {
      if (dcUser.goog_dclk_dsm__DC_SalesforceUserRecord__c != null) {
        returnIds.add(dcUser.goog_dclk_dsm__DC_SalesforceUserRecord__r.Id);
      }
    }
    
    return returnIds;
  }
  
  private goog_dclk_dsm__DC_DoubleClickTeamMember__c createDoubleClickTeamMember(String key, String oppId, DC_UserRoleInfo roleInfo){
    goog_dclk_dsm__DC_DoubleClickTeamMember__c dtm = new goog_dclk_dsm__DC_DoubleClickTeamMember__c();
    //dtm.goog_dclk_dsm__DC_RecordKey__c = key;
    dtm.goog_dclk_dsm__DC_Opportunity__c = oppId;
    dtm.goog_dclk_dsm__DC_User__c = roleInfo.userRecord.Id;
    dtm.goog_dclk_dsm__DC_Role__c = roleInfo.role;
    if (roleInfo.splitPercent != null) {
      dtm.goog_dclk_dsm__DC_SplitPercent__c = (roleInfo.splitPercent / 10000) * 10; 
    }       
    return dtm;
  }
  
  private OpportunityTeamMember createOpportunityTeamMember(String key, String oppId, DC_UserRoleInfo roleInfo){ 
    OpportunityTeamMember otm = new OpportunityTeamMember();
    //otm.goog_dclk_dsm__DC_RecordKey__c = key;
    otm.OpportunityId = oppId;
    otm.UserId = roleInfo.userRecord.goog_dclk_dsm__DC_SalesforceUserRecord__c;
    otm.TeamMemberRole = roleInfo.role;
    if (roleInfo.splitPercent != null) {
      otm.goog_dclk_dsm__DC_SplitPercent__c = (roleInfo.splitPercent / 10000) * 10; 
    }
        
    return otm;
  }    

  // SF TO DC =================================================================
  
    
  private void initializeSFtoDC() {
    targetIndexByOppId = new Map<Id, Integer>();
    opportunityIds = new List<Id>();
    proposalIds = new List<String>();
    dcUserIds = new List<String>();
    userInfoByProposalId = new Map<String, DC_UserRoleInfo>();
    keyedUserRoleInfoByOppId = new Map<Id, Map<String, DC_UserRoleInfo>>();    
    userRoleInfoByKey = new Map<String, DC_UserRoleInfo>();
    oppListSource = new Set<String>();
  } 
  
  private void processFieldSFtoDC() {
    currentOppId = String.valueOf(source.getField('sfid'));
    System.debug('currentOppId::' + currentOppId);
    
    
    targetIndexByOppId.put(currentOppId, target.getCurrentIndex());
    
    oppListSource.add(currentOppId);
    System.debug('oppListSource::' + oppListSource);
    //proposalId = (String)source.getField('DoubleClickId');
    //if(proposalId == null || proposalId == ''){ //Anjali commented on 18th march
    //  return;
    //}
    System.debug('proposalId::' + proposalId);
    keyedUserRoleInfoByOppId.put(currentOppId , new Map<String, DC_UserRoleInfo>());
  
  }  
  
  private Map<Id, goog_dclk_dsm__DC_User__c> getDCUsersBySFUserId(List<goog_dclk_dsm__DC_User__c> dcUsers) {
    Map<Id, goog_dclk_dsm__DC_User__c> results = new Map<Id, goog_dclk_dsm__DC_User__c>();
    for (goog_dclk_dsm__DC_User__c dcuser : dcUsers) {
      if (dcUser.goog_dclk_dsm__DC_SalesforceUserRecord__c != null) {
        results.put(dcuser.goog_dclk_dsm__DC_SalesforceUserRecord__r.Id, dcUser);
      }
    }
    
    return results;
  }
  

  
  private void finishSFtoDC() {
    
    List<OpportunityTeamMember> otms = [SELECT id, userid, TeamMemberRole, goog_dclk_dsm__dc_splitPercent__c, OpportunityId FROM OpportunityTeamMember WHERE OpportunityId IN :targetIndexByOppId.keySet()];
    List<goog_dclk_dsm__DC_DoubleClickTeamMember__c> dctms = [Select id, goog_dclk_dsm__DC_Role__c, goog_dclk_dsm__dc_splitPercent__c, goog_dclk_dsm__DC_User__r.goog_dclk_dsm__DC_DoubleClickId__c, goog_dclk_dsm__DC_Opportunity__c from goog_dclk_dsm__DC_DoubleClickTeamMember__c where goog_dclk_dsm__DC_Opportunity__c IN :targetIndexByOppId.keySet()];
    if(otms.size() == 0 && dctms.size() == 0){
      return;
    }
    Map<Id, List<DC_UserRoleInfo>> userRolesByOppId = new Map<Id, List<DC_UserRoleInfo>>();
    
    List<Id> salesforceUserIds = new List<id>();
    for (OpportunityTeamMember otm : otms) {
      salesforceUserIds.add(otm.userid);
    }    

    Map<Id, goog_dclk_dsm__DC_User__c> dcUsersBySFUserId = getDCUsersBySFUserId([SELECT id, goog_dclk_dsm__dc_doubleclickid__c, goog_dclk_dsm__DC_SalesforceUserRecord__c,  goog_dclk_dsm__DC_SalesforceUserRecord__r.id FROM goog_dclk_dsm__DC_User__c WHERE goog_dclk_dsm__DC_SalesforceUserRecord__c IN :salesforceUserIds]);
    
    for (OpportunityTeamMember otm : otms) {
      if (!dcUsersBySFUserId.containsKey(otm.userid)) {
        continue;
      }
      
      string dcUserId = dcUsersBySFUserId.get(otm.userid).goog_dclk_dsm__dc_doubleclickid__c;
      
      if (!userRolesByOppId.containsKey(otm.opportunityId)) {
        userRolesByOppId.put(otm.opportunityId, new List<DC_UserRoleInfo>());
      }
      
      userRolesByOppId.get(otm.opportunityId).add(
        new DC_UserRoleInfo(dcUserId, otm.teammemberrole, otm.goog_dclk_dsm__DC_SplitPercent__c)       
      );
    }
    
    for (goog_dclk_dsm__DC_DoubleClickTeamMember__c dctm : dctms) {
      if (!userRolesByOppId.containsKey(dctm.goog_dclk_dsm__DC_Opportunity__c)) {
        userRolesByOppId.put(dctm.goog_dclk_dsm__DC_Opportunity__c, new List<DC_UserRoleInfo>());
      }
      userRolesByOppId.get(dctm.goog_dclk_dsm__DC_Opportunity__c).add(
        new DC_UserRoleInfo(dctm.goog_dclk_dsm__DC_User__r.goog_dclk_dsm__DC_DoubleClickId__c, dctm.goog_dclk_dsm__DC_Role__c, dctm.goog_dclk_dsm__DC_SplitPercent__c)
      );
    }
    
    Map<String, List<Long>> roleToUserIdMap = new Map<String, List<Long>>();
    for (Id oppId : targetIndexByOppId.keySet()) {
      
      roleToUserIdMap.clear();
      target.gotoObject(targetIndexByOppId.get(oppId));
      ((DC_DoubleClickProposalWrapper)target).clearTeamMembers();
      if(userRolesByOppId.containsKey(oppId)){
        for (DC_UserRoleInfo info : userRolesByOppId.get(oppId)) {
        	if(info.dcUserId != null){
	          if(info.role.equalsIgnoreCase('Sales Planner')){
	            if(!roleToUserIdMap.containsKey('salesPlannerIds')){
	              roleToUserIdMap.put('salesPlannerIds', new List<Long>());
	            }
	            roleToUserIdMap.get('salesPlannerIds').add(Long.valueOf(info.dcUserId));                
	          } else if(info.role.equalsIgnoreCase('Primary Trafficker')){
	            target.setField('primaryTraffickerId', info.dcUserId);
	          } else if(info.role.equalsIgnoreCase('Secondary Trafficker')){
	            if (!roleToUserIdMap.containsKey('secondaryTraffickerIds')){
	              roleToUserIdMap.put('secondaryTraffickerIds', new List<Long>());
	            }
	            roleToUserIdMap.get('secondaryTraffickerIds').add(Long.valueOf(info.dcUserId));
	          } else if(info.role.equalsIgnoreCase('Primary Sales Rep')){ 
	            ((DC_DoubleClickProposalWrapper)target).setPrimarySalesPerson(Long.valueOf(info.dcUserId), info.SplitPercent);
	          }else if(info.role.equalsIgnoreCase('Secondary Sales Rep')){
	            ((DC_DoubleClickProposalWrapper)target).addSecondarySalesPerson(Long.valueOf(info.dcUserId), info.SplitPercent);
	          }
        	}
        }
      }
      for(String k : roleToUserIdMap.keySet()){
        target.setField(k, roleToUserIdMap.get(k));
      }
    }
  }
  
  
  public class DC_UserRoleInfo {
    public DC_UserRoleInfo(String userId, String userRole) {
      dcUserId = userId;
      Role = userRole;
    }
    public DC_UserRoleInfo(String userId, String userRole, Decimal splitPct) {
      this(userId, userRole);
      SplitPercent = splitPct;
    }   
    public String dcUserId {get;set;}
    public goog_dclk_dsm__DC_User__c userRecord { get;set; }
    public String Role { get;set; }
    public Decimal SplitPercent {get;set;}
  }

}