// 
// (c) 2014 Appirio
//
// Test_DC_OpportunityProposalConnector : test class for DC_OpportunityProposalConnector 
//
// 14 March 2014   Karun Kumar(JDC) Original
//
@isTest
private class Test_DC_OpportunityProposalConnector {

  static testMethod void myUnitTest() {
  	
  	//updated  27-oct-2014
    goog_dclk_dsm__DC_Authentication__c objDC_Authentication = new goog_dclk_dsm__DC_Authentication__c(goog_dclk_dsm__DC_AccessToken__c ='1324324',goog_dclk_dsm__DC_ExpiresIn__c = 3600 ,goog_dclk_dsm__DC_RefreshToken__c = '454655656',goog_dclk_dsm__DC_TokenType__c = 'Bearer');
    insert objDC_Authentication;
    
    goog_dclk_dsm__DC_ObjectMapping__c dcObjMapping = DC_TestUtils.createObjectMapping('Proposal', 'Opportunity', true);
    
    goog_dclk_dsm__DC_FieldMapping__c fldMapping1 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping2 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'custom.SFDC_Opportunity_ID', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Id', Name = 'sfid');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping3 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'probabilityToClose', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Probability', Name = 'probability');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping4 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'status', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345,  Name = 'status');
    insert new List<goog_dclk_dsm__DC_FieldMapping__c>{fldMapping1,fldMapping2,fldMapping3,fldMapping4};
    
    system.debug('Setup success.');
    
    Account acc = DC_TestUtils.createAccount('testCompany', true);
    
    Opportunity opp1 = DC_TestUtils.createOpportunity('testOpp1',System.now().date(),acc.Id,'Prospecting',false);
    opp1.goog_dclk_dsm__DC_ProposalStatus__c = 'APPROVED';
    Opportunity opp2 = DC_TestUtils.createOpportunity('testOpp2',System.now().date(),acc.Id,'Prospecting',false);
    opp2.goog_dclk_dsm__DC_ProposalStatus__c = 'APPROVED';
    Opportunity opp3 = DC_TestUtils.createOpportunity('testOpp3',System.now().date(),acc.Id,'Prospecting',false);
    opp3.goog_dclk_dsm__DC_ProposalStatus__c = 'APPROVED';
    List<Opportunity> oppList = new List<Opportunity>{opp1,opp2,opp3};
    insert oppList;
    system.debug('opp success.');
    
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplProposal());
    Test.startTest();
    
    DC_OpportunityProposalConnector opcObj = DC_OpportunityProposalConnector.getInstance();
    opcObj.syncExistingOpportunities(oppList);
    opcObj.syncExistingOpportunities(oppList);
    try {
      opcObj.syncSFOpportunitiesToDCProposals(oppList);
    } catch(Exception  e) {
      system.debug('Exception :: '+e);
    }
    
    system.debug('Point 0');
    
    DC_GenericWrapper gwObj = opcObj.findMatchingProposals(opp1);
    system.assertEquals(gwObj <> null, true);
    system.debug('Point 1');
    try {
      opcObj.syncSFOppsToDCProposals(gwObj); //void
      opcObj.linkOpportunityAndProposal(opp1, gwObj); //void
    } catch(Exception  e) {
      system.debug('Exception :: '+e);
    }
    
    DC_Connector.DC_DoubleClickPageProcessingResult dcpprObj = opcObj.syncProposalsByPage(DateTime.now(), 10, 0);
    system.assertEquals(dcpprObj <> null, true);
    system.debug('Point 2');
    
    gwObj = opcObj.getApprovedProposals(gwObj);
    system.assertEquals(gwObj <> null, true);
    system.debug('Point 4');
    
    gwObj = opcObj.findMatchingApprovedProposals(opp1);
    system.assertEquals(gwObj <> null, true);
    system.debug('Point 5');
    
  }
}