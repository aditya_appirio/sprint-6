//
//
//  call the get methods on dc constants.
//
//
@isTest
private class Test_DC_Constants {

	static testMethod void callGetMethods() {
	  DC_Constants.getInvalid_Mapping_Company('test');
	  DC_Constants.getInvalid_Mapping_Proposal('test');
	  DC_Constants.getInvalid_Mapping_ProposalLI('test');
    DC_Constants.getInvalid_Mapping_Contact('test');    
    DC_Constants.getInvalid_Mapping_User('test');
    DC_Constants.getCustomFldMalformError('test');
	}
}