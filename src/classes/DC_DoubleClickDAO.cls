public abstract with sharing class DC_DoubleClickDAO {

	public interface Command {
		void execute();
	}

	protected DC_DoubleClickOAuthHandler authHandler;
	
	private void refreshToken() {
		authHandler.refreshAccessToken();
		this.tokenRefreshed();
	}
	
	protected abstract void tokenRefreshed();
	
	public DC_DoubleClickDAO(DC_DoubleClickOAuthHandler authHandler) {
		this.authHandler = authHandler;
	}

	public String formatDateForPQL(Datetime theDate) {
	    // TODO: Jaipur please put the timezone name below 'America/New_York' in the custom setting and have this class read from it. 
    	// this can be the same field used in DC_ServiceDateTime.getSalesforceDatetime
    if(theDate == null){
    	return null;
    }
    String publisherTimeZone;
		if(goog_dclk_dsm__DC_ConnectorApplicationSettings__c.getOrgDefaults().get('goog_dclk_dsm__DC_PublisherTimezoneInDSM__c') != null){
  		publisherTimeZone = String.valueOf(goog_dclk_dsm__DC_ConnectorApplicationSettings__c.getOrgDefaults().get('goog_dclk_dsm__DC_PublisherTimezoneInDSM__c'));
  		System.debug('@@ Formatted LMD: ' + theDate.format('yyyy-MM-dd\'T\'HH:mm:ss', publisherTimeZone));
  		return theDate.format('yyyy-MM-dd\'T\'HH:mm:ss', publisherTimeZone);
  	}
		return theDate.format();
	}

	protected void invokeAPI(Command command, boolean retry) {
		try {
      // TODO: check to see if token is expired before refreshing : @Anjali comment: if token is expired then it will go to catch part.
			command.execute();
		} catch (Exception ex) {
			if(retry) {
        // TODO: handle quota exception : @Anjali doubt: Can you please clarify more ? how you want it to handle ? 
        		//do We need to access DC_CompanyService.QuotaError's reason field ? and do we need to log it into DC_Connector_Log object ? 
				this.refreshToken();
				invokeAPI(command, false);
			} else {
				//throw ex;
				//Updated on 10-11-2014
				if(String.valueOf(ex).contains('ALL_TEAM_ASSOCIATION_NOT_ALLOWED')) {
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage()));
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You can not keep "All Companies" in Team associated to this Account. Please remove "All Companies" from DFP and add required companies and try again.'));
				}
				else {
					throw ex;
				}
			}
		}
	}
	
	//For Contact 
	public DC_ContactService.Statement createContactStatement(String query) {
		DC_ContactService.Statement result = new DC_ContactService.Statement();
		result.query = query;
		
		return result;
	}

	public DC_ContactService.Statement createContactStatement(String query, DC_ContactService.String_ValueMapEntry[] params) {
		DC_ContactService.Statement result = createContactStatement(query);
		result.values = params;
		
		return result;
	}
}