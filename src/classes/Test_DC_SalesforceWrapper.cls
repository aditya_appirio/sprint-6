// 
// (c) 2014 Appirio
//
// Test_DC_SalesforceWrapper : test class for DC_SalesforceWrapper
//
@isTest
private class Test_DC_SalesforceWrapper {

  static testMethod void testSFWrapper() {
    goog_dclk_dsm__DC_ObjectMapping__c dcObjMapping1 = DC_TestUtils.createObjectMapping('Company', 'Account', false);
    goog_dclk_dsm__DC_ObjectMapping__c dcObjMapping2 = DC_TestUtils.createObjectMapping('Contact', 'Contact', false);
    insert new List<goog_dclk_dsm__DC_ObjectMapping__c>{dcObjMapping1, dcObjMapping2};
    
    List<goog_dclk_dsm__DC_FieldMapping__c> fldMappingLst = new List<goog_dclk_dsm__DC_FieldMapping__c>();
    
    fldMappingLst.add(DC_TestUtils.createFieldMapping(dcObjMapping1.id, 'name', 'Name','name', false));
    fldMappingLst.add(DC_TestUtils.createFieldMapping(dcObjMapping1.id, 'id', 'goog_dclk_dsm__DC_DoubleClickId__c','DoubleClickId', false));
    fldMappingLst.add(DC_TestUtils.createFieldMapping(dcObjMapping1.id, 'thirdPartyCompanyId', 'NumberOfEmployees','numFld', false));
    
    fldMappingLst.add(DC_TestUtils.createFieldMapping(dcObjMapping2.id, 'email', 'email','email', false));
    
    insert fldMappingLst;
    
    Account acc = DC_TestUtils.createAccount('testCompany', true);
    Contact con = DC_TestUtils.createContact('fName', 'lName', acc.Id, true);
    
    DC_SalesforceWrapper sfWrapper = new DC_SalesforceWrapper(
    dcObjMapping1, new List<goog_dclk_dsm__DC_FieldMapping__c>{fldMappingLst[0], fldMappingLst[1], fldMappingLst[2]}, new List<Account>{acc});
    sfWrapper.next();
    sfWrapper.getNewRecords();
    sfWrapper.getExistingRecords();
    sfWrapper.getCurrentWrapped();
    sfWrapper.getField('name');
    sfWrapper.setField('name', 'test');
    sfWrapper.setField('DoubleClickId', 12345);
    try{
      sfWrapper.getField('test');
    }catch(exception e){}
    sfWrapper.isSalesforce();
    sfWrapper.isDoubleClick();
    
    sfWrapper = new DC_SalesforceWrapper(
    dcObjMapping2, new List<goog_dclk_dsm__DC_FieldMapping__c>{fldMappingLst[3]}, new List<Contact>{con});
    sfWrapper.next();
    sfWrapper.setField('email', 'test@test.com');
  }
}