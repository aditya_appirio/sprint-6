//
// Test_DC_QueueableBatchProposalLineSync : test class for DC_QueueableBatchProposalLineSync 
//
@isTest
private class Test_DC_QueueableBatchProposalLineSync {
 
  static List<goog_dclk_dsm__DC_ProcessingQueue__c> processQueues;
  static testMethod void myUnitTest() {
    createTestData();
    Account a = new Account(name='test');
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new DC_HttpCalloutMockImploAuth());
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplProductLineItem());
      
    Database.BatchableContext info;
    
    DC_QueueableBatchProcess mock = new DC_QueueableBatchProposalLineSync();
    mock.start(info);
    
    DC_QueueableProcessMock.DC_RetrieveUsersParams param = new DC_QueueableProcessMock.DC_RetrieveUsersParams();
    param.queueItem = processQueues[0];
    param.serialize();
    
    DC_QueueableProcessMock p = new DC_QueueableProcessMock();
    p.initialize(param);
    p.start(info);
    //p.start(info);
    try{
      p.execute(info, new List<Long>{1, 2});
    }catch(Exception e){}
    try{
      p.finish(info);
    }catch(Exception e){}
    
    DC_SalesforceDML instance = DC_SalesforceDML.getInstance();
    List<sObject> records = instance.objectListToSObjectList(new List<Account>{a});
    //System.assertEquals(fldMappingList.size(), records.size());
    
    
    System.assert(param.queueItem != null);
    //check all condition with insert command
    DC_SalesforceDML instancedml = DC_SalesforceDML.getInstance();
    DC_SalesforceDML.DC_SalesforceDMLStatus dmlStatus = instance.registerInsert(a);
    instance.flush();

    p.updateStats(dmlStatus);
    p.executeDML();
    p.getResultString();
    p.updateSyncDate(System.now());
    p.getParameterType();
    test.stopTest();
  }
  
  private static testMethod void myUnitTest2(){
    createTestData();
    goog_dclk_dsm__DC_User__c dcUser = DC_TestUtils.createDCUser();
    
    User u = [Select id from user where id = :Userinfo.getUserId()];
    System.runAs(u){
      Test.startTest();
      Test.setMock(WebServiceMock.class,new DC_WebServiceMockImplProductLineItem());
      Test.setMock(HttpCalloutMock.class,new DC_HttpCalloutMockImploAuth());
      DC_QueueableBatchProposalLineSync asb = new DC_QueueableBatchProposalLineSync();
      DC_QueueableProcess.DC_QueueableProcessParameter inparams;
      asb.initialize(inparams);
      asb.updatesyncDate(Datetime.now());
      asb.getParameterType();
      asb.getResultString();
      
      asb.getRecordsToProcessPerPage();
      asb.getBasisDate();
      asb.processRecords(1,1);
      Test.stopTest();
    }
  }
  
  private static void createTestData(){
    goog_dclk_dsm__DC_ConnectorApplicationSettings__c connAppSetting = DC_TestUtils.createConnAppSetting(1, true);
    processQueues = new List<goog_dclk_dsm__DC_ProcessingQueue__c>();
    processQueues.add(DC_TestUtils.createDCProcessingQueue(false, false, false, 'User Import', false));
    insert processQueues; 
    
    goog_dclk_dsm__DC_ObjectMapping__c objMapping1 = new goog_dclk_dsm__DC_ObjectMapping__c(goog_dclk_dsm__DC_DoubleClickObjectName__c='PROPOSAL_LINE_ITEM', goog_dclk_dsm__DC_SalesforceObjectName__c = 'OpportunityLineItem', name = 'OpportunityLineItem');
    insert objMapping1;
    
    goog_dclk_dsm__DC_FieldMapping__c fldMapping1 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping2 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'custom.SFDC_OpportunityLineItem_ID', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Id', Name = 'sfid');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping3 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'proposalId', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'OpportunityId', Name = 'ProposalId');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping4 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'probabilityToClose', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'probability', Name = 'Probability');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping5 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'name', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_ProposalLineName__c', Name = 'name');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping6 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'cost', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_CostBase__c', Name = 'cost');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping7 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'notes', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_AdNameComments__c', Name = 'notes');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping8 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'productId', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DSMProductId__c', Name = 'productId');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping9 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'baseRate', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_UnitPrice__c', Name = 'baseRate');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping10 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'dfpLineItemId', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_LineItemId__c', Name = 'dfpLineItemId');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping11 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'creativePlaceHolders', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_AdSize__c', Name = 'creativePlaceHolders');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping12 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'custom_test', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_AdSize__c', Name = 'custom_test');
    List<goog_dclk_dsm__DC_FieldMapping__c> fldMappingList = new List<goog_dclk_dsm__DC_FieldMapping__c>{fldMapping1, fldMapping2, fldMapping3, fldMapping4,fldMapping5,fldMapping6,fldMapping7,fldMapping8,fldMapping9,fldMapping10,fldMapping11,fldMapping12};
    insert fldMappingList;
  }
}