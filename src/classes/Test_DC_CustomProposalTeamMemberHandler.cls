// 
// (c) 2014 Appirio
//
// Test_DC_CustomProposalTeamMemberHandler: test class for DC_CustomProposalTeamMemberHandler 
//
@isTest
private class Test_DC_CustomProposalTeamMemberHandler {
  
  static Opportunity oppty, oppty1, oppty2, oppty3 ;
  static List<goog_dclk_dsm__DC_FieldMapping__c> oppFldMappingLst, companyFldMappingList;
  static goog_dclk_dsm__DC_ObjectMapping__c accObjMapping, oppObjMapping ;
  static Account a1;
  static Account acc3, acc4;
  
  @isTest
  static void myUnitTest() {
    createTestData();
    DC_ProposalMapper proposalMapper = DC_MapperFactory.getInstance().getOpportunityMapper();
          
    DC_ProposalService.Proposal proposal = new DC_ProposalService.Proposal();
    proposal.name = 'testProposal';
    proposal.status = 'open';
    proposal.id = 12345;
    List<goog_dclk_dsm__DC_ObjectMapping__c> objectMappingList = [SELECT Id, goog_dclk_dsm__DC_DoubleClickObjectName__c, Name, 
                                                            goog_dclk_dsm__DC_SalesforceObjectName__c
                                                     FROM goog_dclk_dsm__DC_ObjectMapping__c 
                                                     WHERE Name = 'Opportunity'
                                                     limit 1];
    DC_GenericWrapper proposalGWObj = proposalMapper.wrap(proposal);
    System.assert(proposalGWObj != null);
    
    DC_GenericWrapper opptyGWObj = proposalMapper.wrap(oppty);
    System.assert(opptyGWObj != null);
    
    DC_CustomProposalTeamMemberHandler handler  = new DC_CustomProposalTeamMemberHandler();
    handler.initialize(oppObjMapping, oppFldMappingLst, opptyGWObj, proposalGWObj);
    
    System.assert(opptyGWObj.hasNext());
    System.assert(proposalGWObj.hasNext());
    
    opptyGWObj.next();
    proposalGWObj.next();
    for(goog_dclk_dsm__DC_FieldMapping__c fieldMapping : oppFldMappingLst){
      handler.processField(fieldMapping, opptyGWObj, proposalGWObj);
    }
    
    handler.finish();
  }
  
  
  @isTest
  static void myUnitTest1() {
    createTestData();
    
    List<goog_dclk_dsm__DC_User__c> dcUserList = new List<goog_dclk_dsm__DC_User__c>();
    dcUserList.add(new goog_dclk_dsm__DC_User__c(Name = 'test DC User1', goog_dclk_dsm__DC_Email__c = 'test1@testmail.com', goog_dclk_dsm__DC_Role__c = 'Sales Planer', goog_dclk_dsm__DC_DoubleClickId__c = '12345'));
    dcUserList.add(new goog_dclk_dsm__DC_User__c(Name = 'test DC User2', goog_dclk_dsm__DC_Email__c = 'test2@testmail.com', goog_dclk_dsm__DC_Role__c = 'Primary Trafficker', goog_dclk_dsm__DC_SalesforceUserRecord__c = userinfo.getUserId(), goog_dclk_dsm__DC_DoubleClickId__c = '23456'));
    dcUserList.add(new goog_dclk_dsm__DC_User__c(Name = 'test DC User3', goog_dclk_dsm__DC_Email__c = 'test3@testmail.com', goog_dclk_dsm__DC_Role__c = 'Secondary Trafficker', goog_dclk_dsm__DC_DoubleClickId__c = '34567'));
    dcUserList.add(new goog_dclk_dsm__DC_User__c(Name = 'test DC User4', goog_dclk_dsm__DC_Email__c = 'test4@testmail.com', goog_dclk_dsm__DC_Role__c = 'Primary Sales Rep', goog_dclk_dsm__DC_DoubleClickId__c = '45678'));
    dcUserList.add(new goog_dclk_dsm__DC_User__c(Name = 'test DC User5', goog_dclk_dsm__DC_Email__c = 'test5@testmail.com', goog_dclk_dsm__DC_Role__c = 'Secondary Sales Rep', goog_dclk_dsm__DC_DoubleClickId__c = '56789'));
    insert dcUserList;
    
    List<goog_dclk_dsm__DC_DoubleClickTeamMember__c> dcTeamMemList = new List<goog_dclk_dsm__DC_DoubleClickTeamMember__c>();
    dcTeamMemList.add(DC_TestUtils.createDoubleClickTeamMember(oppty.id, 'Sales Planer', dcUserList[0].id, false));
    dcTeamMemList.add(DC_TestUtils.createDoubleClickTeamMember(oppty.id, 'Secondary Trafficker', dcUserList[2].id, false));
    dcTeamMemList.add(DC_TestUtils.createDoubleClickTeamMember(oppty.id, 'Primary Sales Rep', dcUserList[3].id, false));
    dcTeamMemList.add(DC_TestUtils.createDoubleClickTeamMember(oppty.id, 'Secondary Sales Rep', dcUserList[4].id, false));
    insert dcTeamMemList;
    
    DC_ProposalMapper proposalMapper = DC_MapperFactory.getInstance().getOpportunityMapper();
    
    DC_ProposalService.SalespersonSplit spSplit = new DC_ProposalService.SalespersonSplit();
    spSplit.userId = 45678;
    spSplit.split = 1;
    
    DC_ProposalService.SalespersonSplit spSplit1 = new DC_ProposalService.SalespersonSplit();
    spSplit1.userId = 56789;
    spSplit1.split = 1;
    
    DC_ProposalService.Proposal proposal = new DC_ProposalService.Proposal();
    proposal.name = 'testProposal';
    proposal.status = 'open';
    proposal.id = 12345;
    proposal.notes = oppty.id;
    proposal.salesPlannerIds = new List<Long>{12345};
    proposal.primaryTraffickerId = 23456;
    proposal.primarySalesperson = spSplit;
    proposal.secondarySalespeople = new List<DC_ProposalService.SalespersonSplit>{spSplit1};
    proposal.secondaryTraffickerIds = new List<Long>{34567};
    
    List<goog_dclk_dsm__DC_ObjectMapping__c> objectMappingList = [SELECT Id, goog_dclk_dsm__DC_DoubleClickObjectName__c, Name, 
                                                            goog_dclk_dsm__DC_SalesforceObjectName__c
                                                     FROM goog_dclk_dsm__DC_ObjectMapping__c 
                                                     WHERE Name = 'Opportunity'
                                                     limit 1];
    
    DC_GenericWrapper proposalGWObj = proposalMapper.wrap(proposal);
    DC_GenericWrapper opptyGWObj = proposalMapper.wrap(oppty);
    
    DC_CustomProposalTeamMemberHandler handler  = new DC_CustomProposalTeamMemberHandler();
    handler.initialize(oppObjMapping, oppFldMappingLst, proposalGWObj, opptyGWObj);
    
    System.assert(opptyGWObj.hasNext());
    System.assert(proposalGWObj.hasNext());
    
    opptyGWObj.next();
    proposalGWObj.next();
    for(goog_dclk_dsm__DC_FieldMapping__c fieldMapping : oppFldMappingLst){
      handler.processField(fieldMapping, proposalGWObj, opptyGWObj);
    }
    
    handler.finish();
    List<goog_dclk_dsm__DC_DoubleClickTeamMember__c> dtmList = [Select id from goog_dclk_dsm__DC_DoubleClickTeamMember__c where goog_dclk_dsm__DC_User__r.goog_dclk_dsm__DC_DoubleClickId__c = '34567'];
    System.assert(dtmList.size() > 0);
    
  }
  
  static void createTestData(){
    a1 = new Account(name = 'test1');
    a1.goog_dclk_dsm__DC_ReadyForDSM__c = true;
    
    Account a2 = new Account(name = 'test2');
    a2.goog_dclk_dsm__DC_DoubleClickId__c = '12345';
    
    acc3 = new Account(name = 'test2');
    
    acc4 = new Account(name = 'test4');
    acc4.goog_dclk_dsm__DC_ReadyForDSM__c = true;
    insert new List<Account>{a1, a2, acc3, acc4};
    
    oppty = new Opportunity(name = 'testProposal', AccountId = a1.id, StageName = 'Closed/Won', CloseDate = date.today());
    insert oppty;
    
    DC_TestUtils.createOpportunityTeamMember(oppty.id, 'Primary Trafficker', Userinfo.getUserId(), true);
        
    goog_dclk_dsm__DC_User__c dcUser = DC_TestUtils.createDCUser();
    
    DC_TestUtils.createDoubleClickTeamMember(oppty.id, 'Primary Sales Rep', dcUser.id, true);
    
    oppObjMapping = new goog_dclk_dsm__DC_ObjectMapping__c(goog_dclk_dsm__DC_DoubleClickObjectName__c='Proposal', goog_dclk_dsm__DC_SalesforceObjectName__c = 'Opportunity', name = 'Opportunity');
    accObjMapping = new goog_dclk_dsm__DC_ObjectMapping__c(goog_dclk_dsm__DC_DoubleClickObjectName__c='Company', goog_dclk_dsm__DC_SalesforceObjectName__c = 'Account', name = 'account');
    insert new List<goog_dclk_dsm__DC_ObjectMapping__c>{oppObjMapping, accObjMapping};
    
    oppFldMappingLst = new List<goog_dclk_dsm__DC_FieldMapping__c>();
    oppFldMappingLst.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = oppObjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId'));
    oppFldMappingLst.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = oppObjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'notes', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Id', Name = 'sfid'));
    oppFldMappingLst.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = oppObjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'probabilityToClose', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Probability', Name = 'probability'));
    oppFldMappingLst.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = oppObjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'status', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345,  Name = 'status'));
    oppFldMappingLst.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = oppObjMapping.id,   goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345,  Name = 'Team Members', goog_dclk_dsm__DC_HandlerClass__c = 'DC_CustomProposalTeamMemberHandler'));
    oppFldMappingLst.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = oppObjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'primarySalesperson', goog_dclk_dsm__DC_SalesforceFieldName__c = 'CreatedById',  Name = 'primarySalesperson'));
    oppFldMappingLst.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = oppObjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'primarySalesperson.userId', goog_dclk_dsm__DC_SalesforceFieldName__c = 'CreatedById',  Name = 'primarySalesperson.userId'));
    oppFldMappingLst.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = oppObjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'primarySalesperson.split', goog_dclk_dsm__DC_SalesforceFieldName__c = 'ExpectedRevenue',  Name = 'primarySalesperson.split'));
    oppFldMappingLst.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = oppObjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'salesPlannerIds', goog_dclk_dsm__DC_SalesforceFieldName__c = 'createdById',  Name = 'salesPlannerIds'));
    oppFldMappingLst.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = oppObjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'primaryTraffickerId', goog_dclk_dsm__DC_SalesforceFieldName__c = 'createdById',  Name = 'primaryTraffickerId'));
    oppFldMappingLst.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = oppObjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'secondaryTraffickerIds', goog_dclk_dsm__DC_SalesforceFieldName__c = 'createdById',  Name = 'secondaryTraffickerIds'));    
    oppFldMappingLst.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = oppObjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'secondarySalespeople', goog_dclk_dsm__DC_SalesforceFieldName__c = 'createdById',  Name = 'secondarySalespeople'));
    insert oppFldMappingLst;
    companyFldMappingList = new List<goog_dclk_dsm__DC_FieldMapping__c>();
  }
}