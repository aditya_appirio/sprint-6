// 
// (c) 2014 Appirio
//
// Called from DC_ConnectorAdminPageController
// DC Connector
// T-238071
// 17 Jan 2014   Anjali K (JDC)      Original
//
public with sharing class DC_ServicesFactory {
  
  //Establish & Get CompanyService
  public static DC_CompanyService.CompanyServiceInterfacePort getCompanyService(DC_DoubleClickOAuthHandler authHandler){
    DC_CompanyService.CompanyServiceInterfacePort companyService = new DC_CompanyService.CompanyServiceInterfacePort();
    companyService.RequestHeader = new DC_CompanyService.SoapRequestHeader();
    companyService.RequestHeader.applicationName = DC_Constants.APPLICATION_NAME; 

    companyService.inputHttpHeaders_x = new Map<String, String>();
    companyService.inputHttpHeaders_x.put(DC_Constants.AUTH_STR, authHandler.auth.goog_dclk_dsm__DC_TokenType__c + ' ' + authHandler.auth.goog_dclk_dsm__DC_AccessToken__c);

    companyService.RequestHeader.networkCode = authHandler.settings.goog_dclk_dsm__DC_NetworkCode__c;

    return companyService;
  }

  //Establish & Get WorkflowService
  public static DC_WorkflowService.WorkflowRequestServiceInterfacePort getWorkflowService(DC_DoubleClickOAuthHandler authHandler){
    DC_WorkflowService.WorkflowRequestServiceInterfacePort workflowService = new DC_WorkflowService.WorkflowRequestServiceInterfacePort();
    workflowService.RequestHeader = new DC_WorkflowService.SoapRequestHeader();
    workflowService.RequestHeader.applicationName = DC_Constants.APPLICATION_NAME; 

    workflowService.inputHttpHeaders_x = new Map<String, String>();
    workflowService.inputHttpHeaders_x.put(DC_Constants.AUTH_STR, authHandler.auth.goog_dclk_dsm__DC_TokenType__c + ' ' + authHandler.auth.goog_dclk_dsm__DC_AccessToken__c);

    workflowService.RequestHeader.networkCode = authHandler.settings.goog_dclk_dsm__DC_NetworkCode__c;

    return workflowService;
  }

  //Establish & Get NetworkService
  public static DC_NetworkService.NetworkServiceInterfacePort getNetworkService(DC_DoubleClickOAuthHandler authHandler){
    DC_NetworkService.NetworkServiceInterfacePort networkService = new DC_NetworkService.NetworkServiceInterfacePort();
    networkService.RequestHeader = new DC_NetworkService.SoapRequestHeader();
    networkService.RequestHeader.applicationName = DC_Constants.APPLICATION_NAME;

    networkService.inputHttpHeaders_x = new Map<String, String>();
    networkService.inputHttpHeaders_x.put(DC_Constants.AUTH_STR, authHandler.auth.goog_dclk_dsm__DC_TokenType__c + ' ' + authHandler.auth.goog_dclk_dsm__DC_AccessToken__c);

    networkService.RequestHeader.networkCode = authHandler.settings.goog_dclk_dsm__DC_NetworkCode__c;

    return networkService;
  }
	
  //Establish & Get CompanyService
  public static DC_ReportService.ReportServiceInterfacePort getReportService(DC_DoubleClickOAuthHandler authHandler){
    DC_ReportService.ReportServiceInterfacePort reportService = new DC_ReportService.ReportServiceInterfacePort();
    reportService.RequestHeader = new DC_ReportService.SoapRequestHeader();
    reportService.RequestHeader.applicationName = DC_Constants.APPLICATION_NAME; 

    reportService.inputHttpHeaders_x = new Map<String, String>();
    reportService.inputHttpHeaders_x.put(DC_Constants.AUTH_STR, authHandler.auth.goog_dclk_dsm__DC_TokenType__c + ' ' + authHandler.auth.goog_dclk_dsm__DC_AccessToken__c);

    reportService.timeout_x = 60000;

    reportService.RequestHeader.networkCode = authHandler.settings.goog_dclk_dsm__DC_NetworkCode__c;

    return reportService;
  }

  //Establish & Get Custom field service
  public static DC_CustomFieldService.CustomFieldServiceInterfacePort getCustomFieldService(DC_DoubleClickOAuthHandler authHandler){
    DC_CustomFieldService.CustomFieldServiceInterfacePort customFieldService = new DC_CustomFieldService.CustomFieldServiceInterfacePort();
    customFieldService.RequestHeader = new DC_CustomFieldService.SoapRequestHeader();
    customFieldService.RequestHeader.applicationName = DC_Constants.APPLICATION_NAME;

    customFieldService.inputHttpHeaders_x = new Map<String, String>();
    customFieldService.inputHttpHeaders_x.put(DC_Constants.AUTH_STR, authHandler.auth.goog_dclk_dsm__DC_TokenType__c + ' ' + authHandler.auth.goog_dclk_dsm__DC_AccessToken__c);

    customFieldService.RequestHeader.networkCode = authHandler.settings.goog_dclk_dsm__DC_NetworkCode__c;

    return customFieldService;
  }

	//Establish & Get ContactService : Contact 29 Jan : In Progress
  public static DC_ContactService.ContactServiceInterfacePort getContactService(DC_DoubleClickOAuthHandler authHandler){
    DC_ContactService.ContactServiceInterfacePort ContactService = new DC_ContactService.ContactServiceInterfacePort();
    ContactService.RequestHeader = new DC_ContactService.SoapRequestHeader();
    ContactService.RequestHeader.applicationName = DC_Constants.APPLICATION_NAME;

    ContactService.inputHttpHeaders_x = new Map<String, String>();
    ContactService.inputHttpHeaders_x.put(DC_Constants.AUTH_STR, authHandler.auth.goog_dclk_dsm__DC_TokenType__c + ' ' + authHandler.auth.goog_dclk_dsm__DC_AccessToken__c);

    ContactService.RequestHeader.networkCode = authHandler.settings.goog_dclk_dsm__DC_NetworkCode__c;

    return ContactService;
  }
  
  //Establish & Get proposalService : Proposal 29 Jan : In Progress
  public static DC_ProposalService.ProposalServiceInterfacePort getProposalService(DC_DoubleClickOAuthHandler authHandler){
    DC_ProposalService.ProposalServiceInterfacePort proposalService = new DC_ProposalService.ProposalServiceInterfacePort();
    proposalService.RequestHeader = new DC_ProposalService.SoapRequestHeader();
    proposalService.RequestHeader.applicationName = DC_Constants.APPLICATION_NAME;

    proposalService.inputHttpHeaders_x = new Map<String, String>();
    proposalService.inputHttpHeaders_x.put(DC_Constants.AUTH_STR, authHandler.auth.goog_dclk_dsm__DC_TokenType__c + ' ' + authHandler.auth.goog_dclk_dsm__DC_AccessToken__c);

    proposalService.RequestHeader.networkCode = authHandler.settings.goog_dclk_dsm__DC_NetworkCode__c;

    return proposalService;
  }
  
  //Establish & Get proposal Line Item Service : Proposal Line Item
  public static DC_ProposalLineItemService.ProposalLineItemServiceInterfacePort getProposalLineItemService(DC_DoubleClickOAuthHandler authHandler){
    DC_ProposalLineItemService.ProposalLineItemServiceInterfacePort proposalServiceLI = new DC_ProposalLineItemService.ProposalLineItemServiceInterfacePort();
    proposalServiceLI.RequestHeader = new DC_ProposalLineItemService.SoapRequestHeader();
    proposalServiceLI.RequestHeader.applicationName = DC_Constants.APPLICATION_NAME;

    proposalServiceLI.inputHttpHeaders_x = new Map<String, String>();
    proposalServiceLI.inputHttpHeaders_x.put(DC_Constants.AUTH_STR, authHandler.auth.goog_dclk_dsm__DC_TokenType__c + ' ' + authHandler.auth.goog_dclk_dsm__DC_AccessToken__c);

    proposalServiceLI.RequestHeader.networkCode = authHandler.settings.goog_dclk_dsm__DC_NetworkCode__c;

    return proposalServiceLI;
  }
  
  //Establish & Get proposalServiceLI : Proposal Line Item
  public static DC_UserService.UserServiceInterfacePort getUserService(DC_DoubleClickOAuthHandler authHandler){
    DC_UserService.UserServiceInterfacePort userService = new DC_UserService.UserServiceInterfacePort();
    userService.RequestHeader = new DC_UserService.SoapRequestHeader();
    userService.RequestHeader.applicationName = DC_Constants.APPLICATION_NAME;

    userService.inputHttpHeaders_x = new Map<String, String>();
    userService.inputHttpHeaders_x.put(DC_Constants.AUTH_STR, authHandler.auth.goog_dclk_dsm__DC_TokenType__c + ' ' + authHandler.auth.goog_dclk_dsm__DC_AccessToken__c);

    userService.RequestHeader.networkCode = authHandler.settings.goog_dclk_dsm__DC_NetworkCode__c;

    return userService;
  }
  
  //Establish & Get TeamService
  public static DC_TeamService.TeamServiceInterfacePort getTeamService(DC_DoubleClickOAuthHandler authHandler){
    DC_TeamService.TeamServiceInterfacePort teamService = new DC_TeamService.TeamServiceInterfacePort();
    teamService.RequestHeader = new DC_TeamService.SoapRequestHeader();
    teamService.RequestHeader.applicationName = DC_Constants.APPLICATION_NAME;

    teamService.inputHttpHeaders_x = new Map<String, String>();
    teamService.inputHttpHeaders_x.put(DC_Constants.AUTH_STR, authHandler.auth.goog_dclk_dsm__DC_TokenType__c + ' ' + authHandler.auth.goog_dclk_dsm__DC_AccessToken__c);

    teamService.RequestHeader.networkCode = authHandler.settings.goog_dclk_dsm__DC_NetworkCode__c;

    return teamService;
  }
  
  public static DC_UserTeamAssociationService.UserTeamAssociationServiceInterfacePort getUserTeamAssocationService(DC_DoubleClickOAuthHandler authHandler) {
    DC_UserTeamAssociationService.UserTeamAssociationServiceInterfacePort userTeamAssocService = new DC_UserTeamAssociationService.UserTeamAssociationServiceInterfacePort();
    userTeamAssocService.RequestHeader = new DC_UserTeamAssociationService.SoapRequestHeader();
    userTeamAssocService.RequestHeader.applicationName = DC_Constants.APPLICATION_NAME;

    userTeamAssocService.inputHttpHeaders_x = new Map<String, String>();
    userTeamAssocService.inputHttpHeaders_x.put(DC_Constants.AUTH_STR, authHandler.auth.goog_dclk_dsm__DC_TokenType__c + ' ' + authHandler.auth.goog_dclk_dsm__DC_AccessToken__c);

    userTeamAssocService.RequestHeader.networkCode = authHandler.settings.goog_dclk_dsm__DC_NetworkCode__c;

    return userTeamAssocService;
  }
}