// 
// (c) 2014 Appirio
//
// Test_DC_SyncDateManager : test class for DC_SyncDateManager
//
@isTest(seeAllData = false)
private class Test_DC_SyncDateManager {
  private static testMethod void TestDC_SyncDateManager(){
    Test.startTest();
    
    System.assertNotEquals(null, DC_SyncDateManager.opportunityLastSyncDate); 
    
    System.assertNotEquals(null, DC_SyncDateManager.opportunityLineitemLastSyncDate);
    
    DC_SyncDateManager.opportunitiesSynchronizedNow();
    DC_SyncDateManager.opportunityLineItemsSynchronizedNow();
    System.assertNotEquals(null, DC_SyncDateManager.opportunityLastSyncDate);
    
    DC_SyncDateManager.opportunityLineitemLastSyncDate = System.now();
    
    System.assertNotEquals(null, DC_SyncDateManager.deliveryDataLastSyncDate);
    System.assertEquals(null, DC_SyncDateManager.deliveryDataReportJobId);
    
    DC_SyncDateManager.opportunityLastSyncDate = System.now();
    DC_SyncDateManager.deliveryDataLastSyncDate = System.now();
    
    System.assertNotEquals(null, DC_SyncDateManager.deliveryDataLastSyncDate);
    Test.stopTest();
  }
}