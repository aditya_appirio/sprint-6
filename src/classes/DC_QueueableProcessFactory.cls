global class DC_QueueableProcessFactory {
  // construct the item and any parameters here
  // processName    string  process name that we are looking to get a class for
  // parameterJSON  string  json class parameter to restore
  public static DC_QueueableProcess getQueueableProcess(goog_dclk_dsm__DC_ProcessingQueue__c queueItem, String processName, String parameterJSON) {
  	// create a new instance for the type
    DC_QueueableProcess queueableProcess = getQueueableProcessInstance(processName); 
    // instantiate the parameters
    DC_QueueableProcess.DC_QueueableProcessParameter param = restoreParameters(queueItem, parameterJSON, queueableProcess.getParameterType());
    // initialize the queueableProcess
    queueableProcess.initialize(param); 
    return queueableProcess;
  }    
    
  // construct the item and any parameters here
  // processName    string  process name that we are looking to get a class for
  // parameterJSON  string  json class parameter to restore
  public static DC_QueueableProcess getQueueableProcess(String processName, DC_QueueableProcess.DC_QueueableProcessParameter parameters) {
    
    DC_QueueableProcess queueableProcess = getQueueableProcessInstance(processName);
    
    // initialize the queueableProcess
    queueableProcess.initialize(parameters);
    
    return queueableProcess;
  }  
  
  private static DC_QueueableProcess getQueueableProcessInstance(String processName) {
    // get the class name corresponding to this process
    string className = getClassName(processName);
    System.debug(String.format('Returned class name for {0}: {1}', new String[] { processName, className }));
    // get the type for this class
    Type newInstanceType = Type.forName(className);
    System.debug('Type retrieved successfully: ' + newInstanceType);
    // create a new instance for the type
    DC_QueueableProcess queueableProcess = (DC_QueueableProcess)newInstanceType.newInstance();
    
    return queueableProcess; 
  }
  
  private static DC_QueueableProcess.DC_QueueableProcessParameter restoreParameters(goog_dclk_dsm__DC_ProcessingQueue__c queueItem, string parameterJSON, System.Type parameterType) {
  	DC_QueueableProcess.DC_QueueableProcessParameter param;
  	if (parameterJSON == null || parameterJSON == '' || parameterType == null) {
      param = (DC_QueueableProcess.DC_QueueableProcessParameter) parameterType.newInstance();
  	} else {
      param = (DC_QueueableProcess.DC_QueueableProcessParameter)JSON.deserialize(parameterJSON, parameterType);
  	}
  	param.queueItem = queueItem;
  	
  	return param; 
  }
  
  // get the class name corresponding to the passed process name
  // processName  string  process name for which we'll get the class name
  private static string getClassName(string processName) {
    goog_dclk_dsm__DC_QueueItems__c item = goog_dclk_dsm__DC_QueueItems__c.getInstance(processName);
    
    // throw exception here
    if (item == null || item.goog_dclk_dsm__DC_ClassName__c == null || item.goog_dclk_dsm__DC_ClassName__c == '')
        return '';
        
    return item.goog_dclk_dsm__DC_ClassName__c;  
  }

  

  
}