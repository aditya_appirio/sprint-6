// 
// (c) 2014 Appirio
//
// Test_DC_DoubleClickProposalWrapper: test class for DC_DoubleClickProposalWrapper 
//
@isTest
private class Test_DC_DoubleClickProposalWrapper {

  static testMethod void myUnitTest() {
    
    Account acc = DC_TestUtils.createAccount('testCompany', true);
    
    Opportunity opp1 = DC_TestUtils.createOpportunity('testOpp1',System.now().date(),acc.Id,'Prospecting',true);
    
    goog_dclk_dsm__DC_ObjectMapping__c objMapping1 = new goog_dclk_dsm__DC_ObjectMapping__c(goog_dclk_dsm__DC_DoubleClickObjectName__c='Proposal', goog_dclk_dsm__DC_SalesforceObjectName__c = 'Opportunity', name = 'opportunity');
    insert objMapping1;
    List<goog_dclk_dsm__DC_FieldMapping__c> fldMappingList = new List<goog_dclk_dsm__DC_FieldMapping__c>();
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'doubleclickid'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'custom.SFDC_Opportunity_ID', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Id', Name = 'sfid'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'probabilityOfClose', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Probability', Name = 'probabilityOfClose'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'status', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345,  Name = 'status'));
    
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'name',  goog_dclk_dsm__DC_SalesforceFieldName__c = 'name', Name = 'name'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'startDateTime', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickLastModifiedDate__c', Name = 'startDateTime'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'endDateTime', goog_dclk_dsm__DC_SalesforceFieldName__c = 'CloseDate',  Name = 'endDateTime'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'budget', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_Budget__c',  Name = 'budget'));
    
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'agencyCommission',  goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_AgencyCommission__c', Name = 'agencyCommission'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'notes', goog_dclk_dsm__DC_SalesforceFieldName__c = 'Description', Name = 'notes'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'currencyCode', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_CurrencyCode__c',  Name = 'currencyCode'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'poNumber', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_PONumber__c',  Name = 'poNumber'));
    
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'approvalStatus', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_ProposalStatus__c',  Name = 'approvalStatus'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'lastModifiedDateTime', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickLastModifiedDate__c',  Name = 'lastModifiedDateTime'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'exchangeRate', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_ExchangeRate__c',  Name = 'exchangeRate'));
    
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'primarySalesperson', goog_dclk_dsm__DC_SalesforceFieldName__c = 'CreatedById',  Name = 'primarySalesperson'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'primarySalesperson.userId', goog_dclk_dsm__DC_SalesforceFieldName__c = 'CreatedById',  Name = 'primarySalesperson.userId'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'primarySalesperson.split', goog_dclk_dsm__DC_SalesforceFieldName__c = 'ExpectedRevenue',  Name = 'primarySalesperson.split'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'salesPlannerIds', goog_dclk_dsm__DC_SalesforceFieldName__c = 'createdById',  Name = 'salesPlannerIds'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'primaryTraffickerId', goog_dclk_dsm__DC_SalesforceFieldName__c = 'createdById',  Name = 'primaryTraffickerId'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'secondaryTraffickerIds', goog_dclk_dsm__DC_SalesforceFieldName__c = 'createdById',  Name = 'secondaryTraffickerIds'));    
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'secondarySalespeople', goog_dclk_dsm__DC_SalesforceFieldName__c = 'createdById',  Name = 'secondarySalespeople'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'pricingModel', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_PricingModel__c',  Name = 'pricingModel'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'custom.goog_dclk_dsm__SFDC_Opporunity_Id__c', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c= 12345,goog_dclk_dsm__DC_SalesforceFieldName__c = 'Id',  Name = 'custom.goog_dclk_dsm__SFDC_Opporunity_Id__c'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'customFieldValues', goog_dclk_dsm__DC_SalesforceFieldName__c = 'Id',  Name = 'customFieldValues'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'companyId', goog_dclk_dsm__DC_SalesforceFieldName__c = 'accountId',  Name = 'company'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'dfpOrderId', goog_dclk_dsm__DC_SalesforceFieldName__c = 'id',  Name = 'dfpOrderId'));
    
    
    insert fldMappingList;
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new DC_HttpCalloutMockImploAuth());
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplProposal());
    DC_DoubleClickOAuthHandler oAuthHandler = new DC_DoubleClickOAuthHandler(Userinfo.getUserId());
    
    DC_DoubleClickProposalDAO proposalDAO = new DC_DoubleClickProposalDAO(oAuthHandler);
    String query = null;
     
    DC_ProposalService.String_ValueMapEntry[] params = new List<DC_ProposalService.String_ValueMapEntry>();
    DC_ProposalService.Statement statement = proposalDAO.createProposalStatement(query, params);
    System.assert(statement != null);
     
    DC_ProposalService.Proposal proposal = new DC_ProposalService.Proposal();
    proposal.name = 'testProposal';
    proposal.status = 'open';
    proposal.id = 12345;
    
    DC_ProposalService.Proposal proposal1 = new DC_ProposalService.Proposal();
    proposal1.name = 'testProposal1';
    proposal1.status = 'open';
    
    DC_ProposalService.Proposal proposal2 = new DC_ProposalService.Proposal();
    proposal2.name = 'testProposal2';
    proposal2.status = 'open';
    
    DC_DoubleClickProposalWrapper wrapper = new DC_DoubleClickProposalWrapper(objMapping1, fldMappingList, new List<DC_ProposalService.Proposal>{proposal, proposal1});
    wrapper.next();
    wrapper.getField('doubleclickid');
    wrapper.getField('name');
    wrapper.getField('salesPlannerIds');
    wrapper.getField('primaryTraffickerId');
    wrapper.getField('secondaryTraffickerIds');
    wrapper.getField('pricingModel');
    wrapper.getField('custom.goog_dclk_dsm__sfdc_opporunity_id__c');
    wrapper.getField('company');
    wrapper.getField('secondarySalespeople');
    wrapper.getField('primarySalesperson');
    wrapper.getField('status');
    wrapper.getField('exchangeRate');
    wrapper.getField('lastModifiedDateTime');
    wrapper.getField('dfpOrderId');
    
    wrapper.getField('approvalStatus');
    wrapper.getField('poNumber');
    wrapper.getField('notes');
    wrapper.getField('customFieldValues');
    
    wrapper.getField('currencyCode');
    wrapper.getField('agencyCommission');
    wrapper.getField('endDateTime');
    wrapper.getField('startDateTime');
    
    wrapper.getField('probabilityOfClose');
    wrapper.getField('budget');
    
    
    //set
    wrapper.setField('doubleclickid', '12345');
    wrapper.setField('name', 'test');
    wrapper.setField('salesPlannerIds', new List<Long>{1, 2});
    wrapper.setField('primaryTraffickerId', 123);
    wrapper.setField('secondaryTraffickerIds', new List<Long>{1, 2});
    wrapper.setField('pricingModel', 'test');
    wrapper.setField('custom.goog_dclk_dsm__sfdc_opporunity_id__c', opp1.id);
    wrapper.setField('company', '12345');
    DC_ProposalService.SalespersonSplit split = new DC_ProposalService.SalespersonSplit();
    wrapper.setField('secondarySalespeople', new List<DC_ProposalService.SalespersonSplit>{split});
    wrapper.setField('primarySalesperson', split);
    wrapper.setField('status', 'Open');
    wrapper.setField('exchangeRate', 123);
    wrapper.setField('lastModifiedDateTime', date.today());
    wrapper.setField('lastModifiedDateTime', System.now());
    wrapper.setField('dfpOrderId', 12345);
    
    wrapper.setField('approvalStatus', 'test');
    wrapper.setField('poNumber', null);
    wrapper.setField('notes', 'test');
     
    DC_ProposalService.BaseCustomFieldValue v = new DC_ProposalService.BaseCustomFieldValue();
    v.customFieldId = 12345;
    wrapper.setField('customFieldValues', new List<DC_ProposalService.BaseCustomFieldValue>{v});
     
    wrapper.setField('currencyCode', 'INR');
    wrapper.setField('agencyCommission', 12345); 
    wrapper.setField('endDateTime', date.today());
    wrapper.setField('endDateTime', System.now());
    wrapper.setField('startDateTime', date.today());
    wrapper.setField('startDateTime', System.now());
    
    wrapper.setField('probabilityOfClose', '10'); 
    wrapper.setField('budget', new DC_ProposalService.Money());
    
    wrapper.getExistingRecords();
    wrapper.getCurrentWrapped();
    wrapper.hasPrevious();
    wrapper.previous();
    wrapper.gotoSfid('sfid');
    wrapper.gotoDCId('12345');
    System.debug('====' + wrapper.getObjects());
     
    
    wrapper.add(proposal2);
    wrapper.addAll(new List<DC_ProposalService.Proposal>{proposal2});
    wrapper.gotoLast();
    
    wrapper.getCurrentObject();
    wrapper.getCurrentIndex();
    wrapper.toString(1234);
    DC_CompanyService.Date_x dt = new DC_CompanyService.Date_x();
    dt.year = 2014; dt.month = 3; dt.day = 16;
    
    wrapper.toString(dt);
    
    DC_CompanyService.DateTime_x dtTime = new DC_CompanyService.DateTime_x();
    dtTime.date_x = dt;
    
    wrapper.toString(dtTime);
    
    DC_ProposalLineItemService.Size size = new DC_ProposalLineItemService.Size();
    size.height = 10;
    size.width = 10;
    List<DC_ProposalLineItemService.CreativePlaceholder> holderList = new List<DC_ProposalLineItemService.CreativePlaceholder>();
    DC_ProposalLineItemService.CreativePlaceholder holder = new DC_ProposalLineItemService.CreativePlaceholder();
    holder.id = 12345;
    holder.size = size;
    holderList.add(holder);
    
    wrapper.toString(holderList);
    
    wrapper.toLong(null);
    wrapper.toLong('12345');
    wrapper.toLong(true);
    Double dblVal = 12345;
    wrapper.toLong(dblVal);
    
    wrapper.toDecimal(null);
    
    Long longVal = 12345;
    wrapper.toDecimal(longVal);
    
    Decimal dcVal = 1234.23;
    wrapper.toDecimal(dcVal);
    wrapper.toDecimal('1234');
    wrapper.toDecimal(true);
    
    DC_ProposalService.Money money = new DC_ProposalService.Money();
    money.microAmount = 1234;
    wrapper.toDecimal(money);
    
    try{
      wrapper.toDecimal('test');
    }catch(exception e){}
    
    wrapper.toInteger(null);
    wrapper.toInteger('1234');
    wrapper.toInteger(true);
    wrapper.toInteger(longVal);
    wrapper.toInteger(dblVal);
    
    try{
      wrapper.toInteger('test');
    }catch(exception e){}
    
    wrapper.toBoolean(null);
    wrapper.toBoolean(true);
    
    longVal = 1;
    wrapper.toBoolean(longVal);
    wrapper.toBoolean('1');
    
    Integer i = 1;
    wrapper.toBoolean(i);
    
    try{
      wrapper.toBoolean('test');
    }catch(exception e){}
    
    wrapper.toLongArr(new List<Long>{1,2});
    try{
      wrapper.toLongArr('test');
    }catch(exception e){}
    wrapper.size();
    wrapper.getNewRecords();
    wrapper.clearTeamMembers();
    
    wrapper.getSingleWrappedRecords();
    
    wrapper.setPrimarySalesperson(12345, 25);
    wrapper.addSecondarySalesperson(12345, 25);
    
    wrapper.getUnmatchedRecords('poNumber');
    wrapper.getMatchedRecords('probabilityOfClose');
    wrapper.createWrapper(objMapping1, fldMappingList , new List<DC_ProposalService.Proposal>{proposal, proposal1});
    
    wrapper.toProposalBaseCustFldVal(null);
    wrapper.toProposalBaseCustFldVal(new List<DC_ProposalService.BaseCustomFieldValue>());
    wrapper.toSalesPersonSplitObj(null);
    wrapper.isSalesforce();
    wrapper.isDoubleClick();
    DC_DoubleClickProposalWrapper.DC_SalespersonSplit split1 = new DC_DoubleClickProposalWrapper.DC_SalespersonSplit(12345, 25);
    
    Test.stopTest();
  }
}