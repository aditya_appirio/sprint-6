// 
// (c) 2014 Appirio
//
// Controller to perform Workflow action on Opportunity
// 
// 31st March 2014    Anjali K (JDC)      Original
public with sharing class DC_WorkflowActionsPageController {
	
	//Variables
	public Opportunity currentOppty{get;set;}
	public DC_GenericWrapper pendingWFReqWrapper{get;set;}
	public DC_WorkflowManager wfManager{get;set;}
	public boolean isError {get;set;}
	private boolean isExisting;
  
  //Constructor
	public DC_WorkflowActionsPageController(ApexPages.standardController sc){
		
		try{
			currentOppty = (Opportunity)getSelectedRecord(sc.getId())[0];
			wfManager = DC_ManagerFactory.getInstance().getWorkflowManager('Admin');
			if(ApexPages.currentPage().getParameters().get('isExisting') != null){
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, Label.DC_WFActionSuccess));
			}
		}
		catch (Exception e) {
    		System.debug('e:::' + e);
	      if(!ApexPages.hasMessages()){
        	ApexPages.addMessages(e);
        	return;
      	}
  	  }
		
	}
	
  // query for the selected record
  public List<sObject> getSelectedRecord(Id recordId) {
    return [
      SELECT id, name, goog_dclk_dsm__DC_AccountDoubleClickId__c, 
             goog_dclk_dsm__DC_DoubleClickId__c, goog_dclk_dsm__DC_ReadyForDSM__c,
             AccountId, Account.goog_dclk_dsm__DC_ReadyForDSM__c,
             Account.Name, goog_dclk_dsm__DC_ProposalStatus__c
        FROM Opportunity 
       WHERE id = :recordId
    ];
  }
  
  //Action to get Pending WF request
	public void getPendingWorkflowRequests(){
		try{
		DC_GenericWrapper wrap = wfManager.wrapOppty(currentOppty);
		wrap.next();
		pendingWFReqWrapper = wfManager.getPendingWorkflowRequests(wrap);
		}
		catch (Exception e) {
    		System.debug('e:::' + e);
	      if(!ApexPages.hasMessages()){
        	ApexPages.addMessages(e);
        	return;
      	}
  	  }
	}
	
	public Pagereference cancelAction(){
		return new Pagereference('/'+currentOppty.id);
	}
	
	public Pagereference approveOppty(){
		if(currentOppty.goog_dclk_dsm__DC_DoubleClickId__c != null){
			DC_OpportunityProposalConnector connectorInstance = DC_OpportunityProposalConnector.getInstance();
			DC_GenericWrapper propsalWrapper = connectorInstance.getProposalFromOpportunity(currentOppty);
			DC_Connector.DC_DoubleClickPageProcessingResult res = connectorInstance.syncProposalsFromDCtoSF(propsalWrapper);
			connectorInstance.executeAllDml();
		}
		
		String pgURL = '/apex/DC_WorkflowActionsPage?id='+currentOppty.Id;
		if(ApexPages.hasMessages())
		{
			//String pgURL = '/apex/DC_WorkflowActionsPage?id='+currentOppty.Id;
			if(!isError){
					pgURL += '&isExisting='+false;
			}
		}
		else
		{
			//String pgURL = '/apex/DC_WorkflowActionsPage?id='+currentOppty.Id;
			if(isError){
					pgURL += '&isExisting='+true;
			}
		}
		Pagereference pg = new Pagereference(pgURL);
		pg.setRedirect(true);
        
		return pg;	
	}
}