// 
// (c) 2014 Appirio
//
// Controller to link Salesforce Accounts and DoubleClick Companies
// 
// 28 Jan 2014    Anjali K (JDC)      Original
//
public with sharing class DC_PushAccountToDSMController extends DC_LinkRecordController {
   
  // connector for integration actions
  private DC_AccountCompanyConnector connector1 { get; set; } 
  private static string accountLabel = Account.sObjectType.getDescribe().getLabel();
  private static string accountTeamLabel = goog_dclk_dsm__DC_DoubleClickAccountTeam__c.sObjectType.getDescribe().getLabel();
  // standard apex page controller  
  public DC_PushAccountToDSMController(ApexPages.Standardcontroller c) {
    super(c, accountLabel, Label.DC_DSM_Company);
    
    //updated try and catch 21-oct-2014
    try{
    connector1=DC_AccountCompanyConnector.getInstance();
    }
    catch (Exception e) {
    		System.debug('e:::' + e);
	      if(!ApexPages.hasMessages()){
        	ApexPages.addMessages(e);
        	return;
      	}
  	}
  } 
  
  // standard apex page controller  
  public DC_PushAccountToDSMController(ApexPages.Standardcontroller c, String sfLabel, String dcLabel) {
  	super(c, sfLabel, dcLabel);
  }
  // selected record returned as typed account record
  public Account selectedAccountRecord {
    get {
      return (Account)selectedRecord;
    }
  }
  
  // query for the selected records 
  // recordId   Id    record id for the record to be retrieved
  public override List<sObject> getSelectedRecord(Id recordId) {
    return [
      SELECT id, name, goog_dclk_dsm__DC_DoubleClickId__c, goog_dclk_dsm__DC_ReadyForDSM__c
        FROM Account 
       WHERE id = :recordId
    ];
  }
  
  public override string getContext() {
  	return 'Push Account to DSM';
  }
  
  public override void checkExistingDSMRecord(){
    // need to check Account status in DSM.  If non-existent, create.  No UPDATE
    //updated 21-oct-2014 try and catch 
    try{
	    if (!meetsAccountRequirements()) {
	      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, String.format(Label.DC_Account_Needs_Team, new String[]{accountLabel, accountTeamLabel})));
	      return;
	    }
	    super.checkExistingDSMRecord();
    }
    catch (Exception e) {
    		System.debug('e:::' + e);
	      if(!ApexPages.hasMessages()){
        	ApexPages.addMessages(e);
        	return;
      	}
  	}
  } 
  
  private boolean meetsAccountRequirements() {
  	// valid if doubleclick account team has at least one record.
  	return (([SELECT id FROM goog_dclk_dsm__DC_DoubleClickAccountTeam__c WHERE goog_dclk_dsm__DC_Account__c = :selectedAccountRecord.id]).size() > 0); 
  }
  
  // execute the connector1's find matching account methods
  public override DC_GenericWrapper executeConnectorGetMatchingRecords() {
  	//updated 21-oct-2014 try and catch
  	try{
    	return connector1.findMatchingCompanies(selectedAccountRecord);
  	}
  	catch (Exception e) {
    		System.debug('e:::' + e);
	      if(!ApexPages.hasMessages()){
        	ApexPages.addMessages(e);
        	return null;
      	}
  	}
  	return null;
  } 
  
  // execute the connector1's sync existing records actions
  public override void executeConnectorSyncExisting() {
  	//updated 21-oct-2014 try and catch
  	try{
    connector1.syncSFAccountsToDCCompanies(new Account[] { selectedAccountRecord });
  	}
  	catch (Exception e) {
    		System.debug('e:::' + e);
	      if(!ApexPages.hasMessages()){
        	ApexPages.addMessages(e);
        	return;
      	}
  	}
  }
  
  // execute the connector1's sync method to create records
  public override void executeConnectorSync() {
  	//updated 21-oct-2014 try and catch
  	try{
    connector1.syncSFAccountsToDCCompanies(new Account[] { selectedAccountRecord }); 
  	}
  	catch (Exception e) {
    		System.debug('e:::' + e);
	      if(!ApexPages.hasMessages()){
        	ApexPages.addMessages(e);
        	return;
      	}
  	}
  }
    
  //If SFDC Account exist in DSM but SFDC Account does not has value in goog_dclk_dsm__DC_DoubleClickId__c field then populate goog_dclk_dsm__DC_DoubleClickId__c with Company id
  public override void executeConnectorLink(DC_GenericWrapper wrapper){
    // create link between account record and wrapper
    //updated 21-oct-2014 try and catch
  	try{
    connector1.linkAccountsAndCompanies(new List<Account> { selectedAccountRecord }, wrapper);
    }
  	catch (Exception e) {
    		System.debug('e:::' + e);
	      if(!ApexPages.hasMessages()){
        	ApexPages.addMessages(e);
        	return;
      	}
  	}
  }     
  
  public override String getLinkMessage() {
    return Label.DC_SF_Account_has_Matching_DC_Company;
  }
  
  public override String getUniquenessViolationMessage() {
  	return Label.DC_Company_Uniqueness_Violation_Message;
  } 
  
}