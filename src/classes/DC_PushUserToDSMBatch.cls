public with sharing class DC_PushUserToDSMBatch extends DC_PushRecordToDSMProcess implements
																										Database.Batchable<sObject>,
																									  Database.AllowsCallouts,
																									  Database.Stateful {
																									  	
	public long totalNumberOfRecords; 
	
	public DC_PushUserToDSMBatch() {} 
 	public DC_PushUserToDSMBatch(string soql) {
 	  query = soql;
 	} 
 	
 	public DC_PushUserToDSMBatch(boolean useReadyForDSM) {
 		readyForDSM = useReadyForDSM;
 	}
 	 
  public override string getContext() {
  	return 'Push User to DSM';
  }
  
  public override void executeSync(List<SObject> sObjectList){
  	DC_DCUserUserConnector.getInstance().pushSalesforceUsersToNewDCUsers((List<User>)sObjectList);
  }
  
  public override String getDefaultQuery(){
    string queryColumnList = DC_ObjectMapper.getInstance().getCommaSeparatedSalesforceFieldList('User Batch');
    String defaultQry = 
           'SELECT ' + queryColumnList + ' ' + 
  	       'FROM User ' +
           'WHERE goog_dclk_dsm__DC_InitialBatchLoadRoleId__c != null ' + 
           ((readyForDSM) ? 'AND goog_dclk_dsm__DC_IncludeInBatchToDSM__c = true' : ''); 
    return defaultQry; 
  }																			  	
}