// 
// (c) 2014 Appirio
//
// DC_UserMapper
// T-251378
// 20 Feb 2014    Anjali K (JDC)      Original 
//
public with sharing class DC_UserMapper extends DC_ObjectMapper {
  
  private static DC_UserMapper instance;
  
  //constructor
  private DC_UserMapper(){
    super();
  }
  public static DC_UserMapper getInstance() {
    if(instance == null) {
      instance = new DC_UserMapper();
    } 
    return instance;
  }
  // Overrided method wrap : takes DC_UserService.User_x as param and call wrap(List<DC_UserService.User_x>)
  public DC_GenericWrapper wrap(DC_UserService.User_x user) {
    List<DC_UserService.User_x> userList = new List<DC_UserService.User_x>();
    userList.add(user);
    DC_GenericWrapper result = wrap(userList);
    return result;
  }
  
  // Overrided method wrap : takes List<DC_UserService.User_x> as param and convert into DC_DoubleClickUserWrapper instance
  public DC_GenericWrapper wrap(List<DC_UserService.User_x> users) {
    return new DC_DoubleClickUserWrapper(this.getObjectMappingByName('user'), this.getFieldMappingsByName('user'), users);
  }
  
  // Overrided method wrap : takes List<DC_UserService.User_x> as param and convert into DC_DoubleClickUserWrapper instance
  public DC_GenericWrapper wrap(List<DC_UserService.User_x> users, string objectMapping) {
    return new DC_DoubleClickUserWrapper(this.getObjectMappingByName(objectMapping), this.getFieldMappingsByName(objectMapping), users);
  }  
  
  // Overloaded method wrap : takes List<DC_ProposalService.Proposal> as param and convert into DC_DoubleClickProposalWrapper instance
  public DC_GenericWrapper wrap(DC_UserService.UserPage page) {
    DC_GenericWrapper wrapper = new DC_DoubleClickUserWrapper(this.getObjectMappingByName('user'), this.getFieldMappingsByName('user'), page.results);
    wrapper.totalResultSetSize = page.totalResultSetSize;
    return wrapper; 
  } 
  
  /*public DC_GenericWrapper wrap(List<User> salesforceUsers) {
  	return new DC_DoubleClickUserWrapper(this.getObjectMappingByName('User Batch'), this.getFieldMappingsByName('User Batch'), salesforceUsers);
  }*/
  
  //Create new DC contacts for SF Contacts
  public override DC_GenericWrapper createNewDCRecords(DC_GenericWrapper users) {
    DC_GenericWrapper dc_users = this.wrap(new List<DC_UserService.User_x>());
    return createNewDCRecords(users, dc_users);
  }
  
  //Create new DC contacts for SF Contacts
  public DC_GenericWrapper createNewDCRecords(DC_GenericWrapper users, string objectMapping) {
    DC_GenericWrapper dc_users = this.wrap(new List<DC_UserService.User_x>(), objectMapping);
    return createNewDCRecords(users, dc_users);
  }
  
  public DC_GenericWrapper createNewDCRecords(DC_GenericWrapper users, DC_GenericWrapper target) {
    users.gotoBeforeFirst();
    
    while(users.hasNext()) {
      users.next();
      target.add(new DC_UserService.User_x());
      target.gotoLast();
      updateFields(users, target);
    }
    return target;
  }  
  
  public void updateFieldsOnAllObjectsWithCreate(DC_GenericWrapper source, DC_GenericWrapper target) {
    source.gotoBeforeFirst();

    while(source.hasNext()) {
      source.next();
      if(target.gotoDCId(String.valueOf(source.toLong(source.getField('DoubleClickId'))))) {
        updateFields(source, target);
      } else {
        addNewRecordFromSource(source, target);
      }
    }
  }  
      
  public void addNewRecordFromSource(DC_GenericWrapper source, DC_GenericWrapper target) {
    object item;
    if (target instanceOf DC_SalesforceWrapper) {
      item = new goog_dclk_dsm__DC_User__c();
    } else {
      item = new DC_UserService.User_x();
    } 
    
    target.add(item);
    target.gotoLast();    
    
    updateFields(source, target);
  }  

}