/**
 */
@isTest
global class DC_WebServiceMockImplNetworkService implements WebServiceMock{
  global void doInvoke(Object stub, Object request, Map<String, Object> response,
                    String endpoint, String soapAction, String requestName,
                    String responseNS, String responseName, String responseType){
		DC_NetworkService.Network network = new DC_NetworkService.Network();
		network.id = 12345;
		network.displayName = 'test Network';
		network.networkCode = '12345';
		network.isTest = true;
		
		if(request instanceof DC_NetworkService.getAllNetworks_element){
	  	  DC_NetworkService.getAllNetworksResponse_element resp = new DC_NetworkService.getAllNetworksResponse_element();
	  	  DC_NetworkService.Network[] networkList = new List<DC_NetworkService.Network>{network};
    	  resp.rval = networkList;
    	  response.put('response_x', resp);
	  }else if(request instanceof DC_NetworkService.updateNetwork_element){
	  		DC_NetworkService.updateNetworkResponse_element resp = new DC_NetworkService.updateNetworkResponse_element();
	    	  resp.rval = network;
	    	  response.put('response_x', resp);
	  }else if(request instanceof DC_NetworkService.getCurrentNetwork_element){
	    DC_NetworkService.getCurrentNetworkResponse_element resp = new DC_NetworkService.getCurrentNetworkResponse_element();
	    resp.rval = network;
	    response.put('response_x', resp);
	      //response.put('response_x', new DC_ProposalService.getProposalsByStatementResponse_element());
	  }else if(request instanceof DC_NetworkService.makeTestNetwork_element){
	    DC_NetworkService.makeTestNetworkResponse_element resp = new DC_NetworkService.makeTestNetworkResponse_element();
	    resp.rval = network;
	    response.put('response_x', resp);
	      //response.put('response_x', new DC_ProposalService.getProposalsByStatementResponse_element());
	  }
	  return;
	}
}