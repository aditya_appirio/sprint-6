/**
 */
@isTest
private class Test_DC_CustomFieldConnector {

	static testMethod void testCustFieldConnector() {
		goog_dclk_dsm__DC_ObjectMapping__c objMapping = new goog_dclk_dsm__DC_ObjectMapping__c(goog_dclk_dsm__DC_DoubleClickObjectName__c='Proposal', goog_dclk_dsm__DC_SalesforceObjectName__c = 'Opportunity', name = 'Opportunity');
    insert objMapping;
    
    List<goog_dclk_dsm__DC_FieldMapping__c> fldMappingList = new List<goog_dclk_dsm__DC_FieldMapping__c>();
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'custom.SFDC_Opportunity_ID', goog_dclk_dsm__DC_SalesforceFieldName__c = 'Id', Name = 'sfid'));
    insert fldMappingList;
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new DC_HttpCalloutMockImploAuth());
		Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplCustomFieldService());
		Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplProposal());
    
		
    
    DC_CustomFieldConnector con = new DC_CustomFieldConnector();
    con.createOrGetCustomFieldId(fldMappingList);
    System.assertEquals(12345, [Select goog_dclk_dsm__DC_DoubleClickCustomFieldID__c from goog_dclk_dsm__DC_FieldMapping__c where id = :fldMappingList[1].id].goog_dclk_dsm__DC_DoubleClickCustomFieldID__c);
    Test.stopTest();
	}
}