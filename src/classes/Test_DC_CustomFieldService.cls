// 
// (c) 2014 Appirio
//
// Test_DC_CustomFieldService: test class for DC_CustomFieldService 
//
@isTest
private class Test_DC_CustomFieldService {

  static testMethod void myUnitTest() {
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplCustomFieldService());
    Test.startTest();
     
    //DC_CustomFieldService.updateCustomFieldOption_element cp = new DC_CustomFieldService.updateCustomFieldOption_element();
    DC_CustomFieldService.CommonError ce = new DC_CustomFieldService.CommonError(); 
    DC_CustomFieldService.DeactivateCustomFields dcf = new DC_CustomFieldService.DeactivateCustomFields();
    DC_CustomFieldService.updateCustomFieldOptions_element cte = new DC_CustomFieldService.updateCustomFieldOptions_element();
    DC_CustomFieldService.InternalApiError iae = new DC_CustomFieldService.InternalApiError();
    DC_CustomFieldService.getCustomFieldOptionResponse_element uce = new DC_CustomFieldService.getCustomFieldOptionResponse_element();
    DC_CustomFieldService.Authentication auth = new DC_CustomFieldService.Authentication();
    DC_CustomFieldService.SetValue sv = new DC_CustomFieldService.SetValue();
    System.assert(sv != null);
    
    DC_CustomFieldService.createCustomFieldsResponse_element ccre = new DC_CustomFieldService.createCustomFieldsResponse_element();
    //DC_CustomFieldService.createCustomField_element gce = new DC_CustomFieldService.createCustomField_element();
    DC_CustomFieldService.BooleanValue bv = new DC_CustomFieldService.BooleanValue();
    DC_CustomFieldService.CustomFieldPage cce = new DC_CustomFieldService.CustomFieldPage();
    DC_CustomFieldService.UniqueError ue = new DC_CustomFieldService.UniqueError();
    DC_CustomFieldService.CustomFieldError cfe = new DC_CustomFieldService.CustomFieldError();
    DC_CustomFieldService.RequiredError re = new DC_CustomFieldService.RequiredError();
    DC_CustomFieldService.FeatureError fe = new DC_CustomFieldService.FeatureError();
    System.assert(fe != null);
    
    DC_CustomFieldService.AuthenticationError ae = new DC_CustomFieldService.AuthenticationError();
    //DC_CustomFieldService.updateCustomField_element uce2 = new DC_CustomFieldService.updateCustomField_element();
    DC_CustomFieldService.PermissionError pe = new DC_CustomFieldService.PermissionError();
    DC_CustomFieldService.PublisherQueryLanguageSyntaxError pqlse = new DC_CustomFieldService.PublisherQueryLanguageSyntaxError();
    DC_CustomFieldService.String_ValueMapEntry svme = new DC_CustomFieldService.String_ValueMapEntry();
    DC_CustomFieldService.CustomField bc = new DC_CustomFieldService.CustomField();
    DC_CustomFieldService.Value val = new DC_CustomFieldService.Value();
    DC_CustomFieldService.OAuth oauth = new DC_CustomFieldService.OAuth();
    DC_CustomFieldService.createCustomFieldOptionsResponse_element cce2 = new DC_CustomFieldService.createCustomFieldOptionsResponse_element();
    
    DC_CustomFieldService.NumberValue nv = new DC_CustomFieldService.NumberValue();
    DC_CustomFieldService.SoapResponseHeader srh = new DC_CustomFieldService.SoapResponseHeader();
    DC_CustomFieldService.NotNullError nne = new DC_CustomFieldService.NotNullError();
    DC_CustomFieldService.NullError nullErr = new DC_CustomFieldService.NullError();
    DC_CustomFieldService.CustomFieldOption gcbs = new DC_CustomFieldService.CustomFieldOption();
    DC_CustomFieldService.CustomFieldAction ccre2 = new DC_CustomFieldService.CustomFieldAction();
    DC_CustomFieldService.updateCustomFields_element dtv = new DC_CustomFieldService.updateCustomFields_element();
    DC_CustomFieldService.EntityLimitReachedError entityLimitErr = new DC_CustomFieldService.EntityLimitReachedError();
    DC_CustomFieldService.Statement stat = new DC_CustomFieldService.Statement();
    DC_CustomFieldService.PublisherQueryLanguageContextError pqlc = new DC_CustomFieldService.PublisherQueryLanguageContextError();
    DC_CustomFieldService.ApiError apiErr = new DC_CustomFieldService.ApiError();
    DC_CustomFieldService.DateTime_x dtX = new DC_CustomFieldService.DateTime_x();
    DC_CustomFieldService.Date_x dateX = new DC_CustomFieldService.Date_x();
    DC_CustomFieldService.DateValue dtVal = new DC_CustomFieldService.DateValue();
    DC_CustomFieldService.SoapRequestHeader srh2 = new DC_CustomFieldService.SoapRequestHeader();
    DC_CustomFieldService.getCustomFieldsByStatement_element gcbs2 = new DC_CustomFieldService.getCustomFieldsByStatement_element();
    DC_CustomFieldService.getCustomFieldsByStatementResponse_element gcre = new DC_CustomFieldService.getCustomFieldsByStatementResponse_element();
    DC_CustomFieldService.ApiException apiEx = new DC_CustomFieldService.ApiException();
    DC_CustomFieldService.QuotaError qe = new DC_CustomFieldService.QuotaError();
    DC_CustomFieldService.ParseError parseErr = new DC_CustomFieldService.ParseError();
    DC_CustomFieldService.ActivateCustomFields dv = new DC_CustomFieldService.ActivateCustomFields();
    DC_CustomFieldService.ApplicationException ae2 = new DC_CustomFieldService.ApplicationException();
    //DC_CustomFieldService.updateCustomFieldResponse_element ucre = new DC_CustomFieldService.updateCustomFieldResponse_element();
    DC_CustomFieldService.ApiVersionError ave = new DC_CustomFieldService.ApiVersionError();
    DC_CustomFieldService.ServerError sErr = new DC_CustomFieldService.ServerError();
    DC_CustomFieldService.TypeError typeErr = new DC_CustomFieldService.TypeError();
    DC_CustomFieldService.CustomFieldServiceInterfacePort csiPort = new DC_CustomFieldService.CustomFieldServiceInterfacePort();
    
    DC_CustomFieldService.CustomFieldOption[] custFldOpts = csiPort.updateCustomFieldOptions(new List<DC_CustomFieldService.CustomFieldOption>());
    //DC_CustomFieldService.CustomFieldOption updateCustFldOpt = csiPort.updateCustomFieldOption(new DC_CustomFieldService.CustomFieldOption());
    //DC_CustomFieldService.CustomFieldOption createCustFld = csiPort.createCustomFieldOption(new DC_CustomFieldService.CustomFieldOption());
    DC_CustomFieldService.CustomFieldPage getCustFldStatement = csiPort.getCustomFieldsByStatement(new DC_CustomFieldService.Statement());
    //DC_CustomFieldService.CustomField getCustFld = csiPort.getCustomField(Long.valueOf('12345'));
    //DC_CustomFieldService.CustomField updateCustFld = csiPort.updateCustomField(new DC_CustomFieldService.CustomField());
    List<DC_CustomFieldService.CustomFieldOption> createCustFldOpts = csiPort.createCustomFieldOptions(new List<DC_CustomFieldService.CustomFieldOption>());
    DC_CustomFieldService.CustomFieldOption custFldOpt = csiPort.getCustomFieldOption(Long.valueOf('12345'));
    List<DC_CustomFieldService.CustomField> custFld = csiPort.createCustomFields(new List<DC_CustomFieldService.CustomField>());
    List<DC_CustomFieldService.CustomField> upDateCust_Fld = csiPort.updateCustomFields(new List<DC_CustomFieldService.CustomField>());
    
    DC_CustomFieldService.StatementError statErr = new DC_CustomFieldService.StatementError();
    //DC_CustomFieldService.ClientLogin cl = new DC_CustomFieldService.ClientLogin();
    DC_CustomFieldService.TextValue tv = new DC_CustomFieldService.TextValue();
    //DC_CustomFieldService.getCustomField_element iee = new DC_CustomFieldService.getCustomField_element();
    //DC_CustomFieldService.updateCustomFieldOptionResponse_element ucre2 = new DC_CustomFieldService.updateCustomFieldOptionResponse_element();
    //DC_CustomFieldService.getCustomFieldResponse_element custFldResp = new DC_CustomFieldService.getCustomFieldResponse_element();
    //DC_CustomFieldService.createCustomFieldOptionResponse_element custFldOptElem = new DC_CustomFieldService.createCustomFieldOptionResponse_element();
    DC_CustomFieldService.DateTimeValue dtTmVal = new DC_CustomFieldService.DateTimeValue();
    DC_CustomFieldService.updateCustomFieldOptionsResponse_element updateCustFldOpts = new DC_CustomFieldService.updateCustomFieldOptionsResponse_element();
    //DC_CustomFieldService.createCustomFieldOption_element createCustFldOpt = new DC_CustomFieldService.createCustomFieldOption_element();
    DC_CustomFieldService.performCustomFieldAction_element performCustFldActions = new DC_CustomFieldService.performCustomFieldAction_element();
    DC_CustomFieldService.performCustomFieldActionResponse_element performCustFldActionResp = new DC_CustomFieldService.performCustomFieldActionResponse_element();
    DC_CustomFieldService.createCustomFieldOptions_element createCustFldOpts1 = new DC_CustomFieldService.createCustomFieldOptions_element();
    DC_CustomFieldService.createCustomFields_element createCustFlds = new DC_CustomFieldService.createCustomFields_element();
    DC_CustomFieldService.getCustomFieldOption_element getCustFldOptEle = new DC_CustomFieldService.getCustomFieldOption_element();
    DC_CustomFieldService.updateCustomFieldsResponse_element updateCustFldRespEle = new DC_CustomFieldService.updateCustomFieldsResponse_element();
    DC_CustomFieldService.DropDownCustomField ddCustFld = new DC_CustomFieldService.DropDownCustomField();
    //DC_CustomFieldService.createCustomFieldResponse_element createCustfldRespEle = new DC_CustomFieldService.createCustomFieldResponse_element();
    DC_CustomFieldService.UpdateResult updateRes1 = new DC_CustomFieldService.UpdateResult();
    Test.stopTest();
  }
}