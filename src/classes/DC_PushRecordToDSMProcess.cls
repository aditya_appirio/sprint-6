public abstract with sharing class DC_PushRecordToDSMProcess {
  
  public string query;
  public boolean readyForDSM = true;
  Set<String> affectedRecordIds = new Set<String>();
  List<String> errorLog = new List<String>();
  List<Id> errRecIds = new List<Id>();

  private Integer successCount = 0;
  private Integer failureCount = 0;
  public Integer recordsProcessed = 0;

  // this will provide the default query that the opportunity batch currently uses if no query is provided
  public abstract string getDefaultQuery(); 
  public abstract string getContext() ;
  public abstract void executeSync(List<sObject> sObjectList);
  
  public Database.QueryLocator start( Database.BatchableContext bc ){
    string useQuery = (query == null || query == '') ? 
      getDefaultQuery() : query;
    
    System.debug('@@ using query: ' + useQuery);
    DC_Connector.DC_DoubleClickPageProcessingResult result;	
    try {
      return Database.getQueryLocator( useQuery );
    } catch (Exception e) {
    	System.debug('@@ Exception: ' + e);
      DC_ConnectorLogger.log(getContext(), e);  	
      DC_SalesforceDML.getInstance().flush();
    }
    return null;
  }
  
  public virtual void execute( Database.BatchableContext bc, List<SObject> sObjectList ) { 
    try {
      executeSync((List<sObject> )sObjectList);
      successCount += sObjectList.size();
      
    } catch (Exception e) {
    	DC_ConnectorLogger.log(getContext(), e);   
    	DC_SalesforceDML.getInstance().flush();
    	String err = '';
    	for(sObject obj : sObjectList){
    		errRecIds.add(obj.id);
    	}
    	errorLog.add('\nException : ' + e + '\non records :' + String.join(errRecIds, ', '));
    	failureCount += sObjectList.size();
    }
  }
  public void finish(Database.BatchableContext info) { 
    // log here	
    String res = 'Success records : ' + successCount + '\nError records : ' + failureCount;
    String status = failureCount > 0 ? ' With Errors' : 'Success';
    
		DC_ConnectorLogger.log(getContext(), res + errorLog, status, String.join(errRecIds, ', '));    
  }

}