// 
// (c) 2014 Appirio
//
// Test_DC_QueueableBatchUserSync : test class for DC_QueueableBatchUserSync 
//
@isTest
private class Test_DC_QueueableBatchUserSync {

	static testMethod void myUnitTest() {
		
		goog_dclk_dsm__DC_ObjectMapping__c dcObjMapping = DC_TestUtils.createObjectMapping('User', 'User', true);
    
    goog_dclk_dsm__DC_FieldMapping__c fldMapping1 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', 
      goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId');
    insert new List<goog_dclk_dsm__DC_FieldMapping__c>{fldMapping1};
    
		goog_dclk_dsm__DC_ConnectorApplicationSettings__c connAppSetting = DC_TestUtils.createConnAppSetting(1, true);
		List<goog_dclk_dsm__DC_ProcessingQueue__c> processQueues = new List<goog_dclk_dsm__DC_ProcessingQueue__c>();
		processQueues.add(DC_TestUtils.createDCProcessingQueue(false, false, false, 'User Import', false));
		insert processQueues;
		goog_dclk_dsm__DC_User__c dcUser = DC_TestUtils.createDCUser();
		
		User u = [Select id from user where id = :Userinfo.getUserId()];
		System.runAs(u){
		Test.startTest();
		Test.setMock(WebServiceMock.class,new DC_WebServiceMockImplUserService());
		Test.setMock(HttpCalloutMock.class,new DC_HttpCalloutMockImploAuth());
    DC_QueueableBatchUserSync asb = new DC_QueueableBatchUserSync();
    DC_QueueableProcess.DC_QueueableProcessParameter inparams;
    asb.initialize(inparams);
    asb.updatesyncDate(Datetime.now());
    asb.getParameterType();
    asb.getResultString();
    
    asb.getRecordsToProcessPerPage();
    asb.getBasisDate();
    asb.processRecords(1,1);

		Test.stopTest();
		}
	}
}