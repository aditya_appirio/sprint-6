// 
// (c) 2014 Appirio
//
// Test_DC_LinkRecordListController : test class for DC_LinkRecordListController 
//
//
@isTest
private class Test_DC_LinkRecordListController {

	static testMethod void myUnitTest() {
		List<goog_dclk_dsm__DC_LinkAccountFields__c> lstDCAccountFields = new List<goog_dclk_dsm__DC_LinkAccountFields__c>();
		lstDCAccountFields.add(DC_TestUtils.createDCAccountFields('doubleclickid', false,false));
		lstDCAccountFields.add(DC_TestUtils.createDCAccountFields('name', true,false));
		insert lstDCAccountFields;
		
		goog_dclk_dsm__DC_ObjectMapping__c objMapping = new goog_dclk_dsm__DC_ObjectMapping__c(goog_dclk_dsm__DC_DoubleClickObjectName__c='Company', goog_dclk_dsm__DC_SalesforceObjectName__c = 'Account', name = 'account');
    insert objMapping;
    goog_dclk_dsm__DC_FieldMapping__c fldMapping1 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping2 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'externalId', goog_dclk_dsm__DC_SalesforceFieldName__c = 'Id', Name = 'sfid');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping3 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'name', goog_dclk_dsm__DC_SalesforceFieldName__c = 'name', Name = 'name');
    insert new List<goog_dclk_dsm__DC_FieldMapping__c>{fldMapping1, fldMapping2,fldMapping3};
    
    Account a1 = new Account(name = 'test1');
    a1.goog_dclk_dsm__DC_ReadyForDSM__c = true;
    Account a2 = new Account(name = 'test2');
    insert new List<Account>{a1, a2};
    
    DC_LinkRecordListController cont = new DC_LinkRecordListController();
    
		DC_GenericWrapper genWrap = new DC_SalesforceWrapper(objMapping, new List<goog_dclk_dsm__DC_FieldMapping__c>{fldMapping1, fldMapping2, fldMapping3}, new List<Account>{a1, a2});
		genWrap.next();
		DC_LinkRecordListController.LinkActionWrapper wrapper ;
		try{
		 wrapper 
			= new DC_LinkRecordListController.LinkActionWrapper(null, genWrap, lstDCAccountFields);
		}catch(exception e){}
		cont.configContext = 'goog_dclk_dsm__DC_LinkAccountFields__c';
		cont.wrapper = genWrap;
		try{
			cont.updateLinkActionWrapperList();
			wrapper.link();
		}catch(exception e){}
	}
}