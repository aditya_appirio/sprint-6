public with sharing class DC_DoubleClickUserTeamAssociationDAO extends DC_DoubleClickDAO{
  protected override void tokenRefreshed() {
    this.userTeamAssociationService = null; 
  }
  
  //Construtor
  public DC_DoubleClickUserTeamAssociationDAO(DC_DoubleClickOAuthHandler authHandler) {
    super(authHandler);
  }
 
  // get a configured doubleclick opportunity interface
  private DC_UserTeamAssociationService.UserTeamAssociationServiceInterfacePort userTeamAssociationService {
    get {
      if(this.userTeamAssociationService == null) {
        this.userTeamAssociationService = DC_ServicesFactory.getUserTeamAssocationService(this.authHandler);
      } 

      return this.userTeamAssociationService;
    }
    set;
  }
  
  private class CreateUserTeamAssocationsCommand implements DC_DoubleClickDAO.Command {
    public DC_DoubleClickUserTeamAssociationDAO context;
    public DC_UserTeamAssociationService.UserTeamAssociation[] associations;
    public DC_UserTeamAssociationService.UserTeamAssociation[] result;
    
    public CreateUserTeamAssocationsCommand(DC_DoubleClickUserTeamAssociationDAO context, DC_UserTeamAssociationService.UserTeamAssociation[] assocs) {
      this.context = context;
      this.associations = assocs;
    }
    
    public void execute() {
      this.result = context.userTeamAssociationService.createUserTeamAssociations(this.associations);
    }
  }
  
  // retrieve a company using the passed in query string
  public DC_UserTeamAssociationService.UserTeamAssociation[] createUserTeamAssociations(DC_UserTeamAssociationService.UserTeamAssociation[] assocs) {
    CreateUserTeamAssocationsCommand cmd = new CreateUserTeamAssocationsCommand(this, assocs);
    
    this.invokeAPI(cmd, true);
    
    return cmd.result;
  }
}