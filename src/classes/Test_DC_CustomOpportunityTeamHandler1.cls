@isTest
private class Test_DC_CustomOpportunityTeamHandler1 {
  
  @isTest 
  static void testSFtoDC() {
    // need an opportunity
    // need an dc_team record
    // need an opp dc team record

    goog_dclk_dsm__DC_ObjectMapping__c dcObjMapping = DC_TestUtils.createObjectMapping('Proposal', 'Opportunity', true);
    List<goog_dclk_dsm__DC_FieldMapping__c> fldMappings = new List<goog_dclk_dsm__DC_FieldMapping__c>();
    fldMappings.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId'));
    fldMappings.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'custom.SFDC_Opportunity_ID', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Id', Name = 'sfid'));
    fldMappings.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'probabilityToClose', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Probability', Name = 'probability'));
    fldMappings.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'Team', goog_dclk_dsm__DC_HandlerClass__c = 'DC_CustomOpportunityTeamHandler'));
    insert fldMappings;

    Account a = DC_TestUtils.createAccount('test account', true);
    Opportunity o = DC_TestUtils.createOpportunity('test opp', Date.Today() + 1, a.id, 'Prospecting', true);
    
    goog_dclk_dsm__DC_Team__c team = new goog_dclk_dsm__DC_Team__c(name = 'Team 1', goog_dclk_dsm__DC_DoubleClickId__c = '32193392');
    insert team;
    insert new goog_dclk_dsm__DC_DoubleClickOpportunityTeam__c(goog_dclk_dsm__DC_Team__c = team.id, goog_dclk_dsm__DC_Opportunity__c = o.id);

    DC_ProposalService.Proposal p = new DC_ProposalService.Proposal();
    DC_CustomOpportunityTeamHandler handler = new DC_CustomOpportunityTeamHandler();

    DC_ProposalMapper mapper = DC_MapperFactory.getInstance().getOpportunityMapper();

    o = ((List<Opportunity>)database.query('SELECT id, '+ handler.getSalesforceQueryString() + ' FROM Opportunity where id = \'' + o.id + '\''))[0];

    DC_GenericWrapper sfwrapper = mapper.wrap(o);
    sfwrapper.next();

    DC_GenericWrapper dcwrapper = mapper.wrap(p);
    dcwrapper.next();

    handler.initialize(dcObjMapping, fldMappings, sfwrapper, dcwrapper);
    handler.processField(fldMappings[3], sfwrapper, dcwrapper);
    handler.finish();


    system.assertEquals(1, p.appliedTeamIds.size());

  }

  @isTest static void testDCtoSF() {
  	goog_dclk_dsm__DC_ObjectMapping__c dcObjMapping = DC_TestUtils.createObjectMapping('Proposal', 'Opportunity', true);
    List<goog_dclk_dsm__DC_FieldMapping__c> fldMappings = new List<goog_dclk_dsm__DC_FieldMapping__c>();
    fldMappings.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId'));
    fldMappings.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'custom.SFDC_Opportunity_ID', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Id', Name = 'sfid'));
    fldMappings.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'probabilityToClose', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Probability', Name = 'probability'));
    fldMappings.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'Team', goog_dclk_dsm__DC_HandlerClass__c = 'DC_CustomOpportunityTeamHandler'));
    insert fldMappings;

    Account a = DC_TestUtils.createAccount('test account', true);
    Opportunity o = DC_TestUtils.createOpportunity('test opp', Date.Today() + 1, a.id, 'Prospecting', true);
    
    goog_dclk_dsm__DC_Team__c team = new goog_dclk_dsm__DC_Team__c(name = 'Team 1', goog_dclk_dsm__DC_DoubleClickId__c = '32193392');
    insert team;
    insert new goog_dclk_dsm__DC_DoubleClickOpportunityTeam__c(goog_dclk_dsm__DC_Team__c = team.id, goog_dclk_dsm__DC_Opportunity__c = o.id);
		
		Test.startTest();
		Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplProposal());
		
    DC_ProposalService.Date_x dateX = new DC_ProposalService.Date_x();
    dateX.year = Date.today().year();
    dateX.month = Date.today().month();
    dateX.day = Date.today().addDays(10).day();
    
    DC_ProposalService.DateTime_x dt = new DC_ProposalService.DateTime_x();
    dt.date_x = dateX;
    
	  DC_ProposalService.Proposal proposal1 = new DC_ProposalService.Proposal();
	  proposal1.name = 'testProposal1';
	  proposal1.id = 12345;
	  proposal1.lastModifiedDateTime = dt;
    proposal1.startDateTime = dt;
    proposal1.endDateTime = dt;
  	proposal1.appliedTeamIds = new List<Long>{12345};
  
    DC_CustomOpportunityTeamHandler handler = new DC_CustomOpportunityTeamHandler();

    DC_ProposalMapper mapper = DC_MapperFactory.getInstance().getOpportunityMapper();

    o = ((List<Opportunity>)database.query('SELECT id, '+ handler.getSalesforceQueryString() + ' FROM Opportunity where id = \'' + o.id + '\''))[0];

    DC_GenericWrapper sfwrapper = mapper.wrap(o);
    sfwrapper.next();

    DC_GenericWrapper dcwrapper = mapper.wrap(proposal1);
    dcwrapper.next();

    handler.initialize(dcObjMapping, fldMappings, dcwrapper, sfwrapper);
    handler.processField(fldMappings[3], dcwrapper, sfwrapper);
    handler.finish();

		Test.stopTest();
  	
		List<goog_dclk_dsm__DC_DoubleClickOpportunityTeam__c> team1 = [Select id from goog_dclk_dsm__DC_DoubleClickOpportunityTeam__c where goog_dclk_dsm__DC_Opportunity__c = :o.id];
		System.assertEquals(1, team1.size()); 
  }  
}