// 
// (c) 2014 Appirio
//
// Test_DC_SalesforceAccountManager : test class for DC_SalesforceAccountManager 
//
// 14 feb 2014    Ankit Goyal (JDC)      Original
//
@isTest
private class Test_DC_SalesforceAccountManager {
  
  static testMethod void testDC_SalesforceAccountManager() {
    Account acc = DC_TestUtils.createAccount('test', true);
    goog_dclk_dsm__DC_ObjectMapping__c dcObjMapping = DC_TestUtils.createObjectMapping('Company', 'Account', true);
    goog_dclk_dsm__DC_FieldMapping__c dcFldMapping = DC_TestUtils.createFieldMapping(dcObjMapping.id,'Name', 'Name','name' , true);
    DC_SalesforceManager sm = new DC_SalesforceManager();
    DC_SalesforceAccountManager sam = new DC_SalesforceAccountManager();
    sam.getAccount(acc.id);
    
    
    sam.getAccounts(new List<Id>{acc.id});
    
    DC_GenericWrapper gw = sam.getAccounts(new List<Account>{acc});
    
    sam.updateSFRecords(gw);
  }
  
}