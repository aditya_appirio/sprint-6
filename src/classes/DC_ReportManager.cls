public with sharing class DC_ReportManager extends DC_DoubleClickManager {

  private DC_ReportDAO reportDAO {
    get {
      if(reportDAO == null) {
        reportDAO = new DC_ReportDAO(this.authHandler);
      }
        
      return reportDAO;
    }
    set;
  }

  private DC_DoubleClickProposalManager proposalManager {
    get {
      if(this.proposalManager == null) {
		//updated 15-oct-2014
        this.proposalManager = DC_ManagerFactory.getInstance().getProposalManager('Admin');
      }

      return this.proposalManager;
    } 
    set;
  }

  public DC_ReportManager(DC_DoubleClickOAuthHandler authHandler) {
    super(authHandler);
  }

  public DC_ProposalService.Date_x getEarliest(DC_ProposalService.Date_x date1, DC_ProposalService.Date_x date2) {
    if(date1 == null || date2 == null) {
      if(date1 == null) {
        return date2;
      } else {
        return date1;
      }
    }

    if(date1.year != date2.year) {
      if(date1.year < date2.year) {
        return date1;
      } else {
        return date2;
      }
    } else if(date1.month != date2.month) {
      if(date1.month < date2.month) {
        return date1;
      } else {
        return date2;
      }
    } else if (date1.day < date2.day) {
      return date1;
    } else {
      return date2;
    }
  }

  public DC_ProposalService.Date_x getLatest(DC_ProposalService.Date_x date1, DC_ProposalService.Date_x date2) {
    if(date1 == null || date2 == null) {
      if(date1 == null) {
        return date2;
      } else {
        return date1;
      }
    }

    if(date1.year != date2.year) {
      if(date1.year > date2.year) {
        return date1;
      } else {
        return date2;
      }
    } else if(date1.month != date2.month) {
      if(date1.month > date2.month) {
        return date1;
      } else {
        return date2;
      }
    } else if (date1.day > date2.day) {
      return date1;
    } else {
      return date2;
    }
  }

  public DC_ProposalService.Date_x getTodayDate() {
    Date d = System.today();
    DC_ProposalService.Date_x result = new DC_ProposalService.Date_x();

    result.day = d.day();
    result.month = d.month();
    result.year = d.year();

    return result;
  }

  private DC_ReportService.Date_x convertDate(DC_ProposalService.Date_x theDate) {
    DC_ReportService.Date_x result = new DC_ReportService.Date_x();

    result.year = theDate.year;
    result.month = theDate.month;
    result.day = theDate.day;

    return result;
  }

  public Long runLineItemDeliveryDataReport() {
    Set<Long> proposalIds = new Set<Long>();
    DC_ProposalService.Date_x startDate = getTodayDate();
    DC_ProposalService.Date_x endDate = startDate;

    // Find all proposals that have been modified since the last time this service ran
    // TODO: this date needs to be from a custom setting, and need to update the date when
    // the report is successfully returned
    DateTime lastSyncDate = DC_SyncDateManager.deliveryDataLastSyncDate;

    DC_GenericWrapper proposals = this.proposalManager.getProposalsUpdatedSince(lastSyncDate, 50000, 0);
		System.debug('proposals:::' + proposals);
    while(proposals.hasNext()) {
      proposals.next();

      proposalIds.add((Long)proposals.getField('DoubleClickId'));

      DC_ProposalService.Proposal proposal = ((DC_ProposalService.Proposal)proposals.getCurrentObject());

      if(proposal.startDateTime != null && proposal.startDateTime.date_x != null) {
        startDate = getEarliest(startDate, proposal.startDateTime.date_x);  
      }
      
      if(proposal.endDateTime != null && proposal.endDateTime.date_x != null) {
        endDate = getLatest(endDate, proposal.endDateTime.date_x);  
      }
    }

    // Find all proposals that are currently serving (startDate < now && endDate > now)
    proposals = this.proposalManager.getProposalsCurrentlyServing(50000, 0);

    while(proposals.hasNext()) {
      proposals.next();

      proposalIds.add((Long)proposals.getField('DoubleClickId'));

      DC_ProposalService.Proposal proposal = ((DC_ProposalService.Proposal)proposals.getCurrentObject());

      if(proposal.startDateTime != null && proposal.startDateTime.date_x != null) {
        startDate = getEarliest(startDate, proposal.startDateTime.date_x);  
      }
      
      if(proposal.endDateTime != null && proposal.endDateTime.date_x != null) {
        endDate = getLatest(endDate, proposal.endDateTime.date_x);  
      }
    }

    // Prepare report query filter using Dimension PROPOSAL_LINE_ITEM_ID
    // Fetching LINE_ITEM_LIFETIME_IMPRESSIONS and AD_SERVER_IMPRESSIONS columns
    DC_ReportService.ReportQuery query = new DC_ReportService.ReportQuery();
    String inStatement = '';
    String separator = '';

    if(proposalIds.size() == 0) {
      return null;
    }

    for(Long proposalId : proposalIds) {
    	if(proposalId != null){
	      inStatement += separator + proposalId;
	      separator = ', ';
    	}
    }

    System.debug('In statement: ' + inStatement);

    query.statement = new DC_ReportService.Statement();
    query.statement.query = 'WHERE PROPOSAL_ID in (' + inStatement + ')';
    query.dimensions = new String[]{'LINE_ITEM_ID','PROPOSAL_LINE_ITEM_ID'};
    query.dimensionAttributes = new String[]{'LINE_ITEM_LIFETIME_IMPRESSIONS', 'LINE_ITEM_GOAL_QUANTITY'};
    query.columns = new String[]{'AD_SERVER_IMPRESSIONS'};
    query.dateRangeType = 'CUSTOM_DATE';
    query.startDate = convertDate(startDate);
    query.endDate = convertDate(endDate);

    DC_SyncDateManager.deliveryDataLastSyncDate = System.now();

    return reportDAO.runReportJob(query);
  }

  public Boolean isReportJobComplete(Long reportJobId) {
    DC_ReportService.ReportJob reportJob = reportDAO.getReportJob(reportJobId);

    return reportJob.reportJobStatus.equalsIgnoreCase('completed');
  }

  public String getReport(Long reportJobId) {
    DC_ReportService.ReportDownloadOptions options = new DC_ReportService.ReportDownloadOptions();

    options.exportFormat = 'CSV_DUMP';
    options.includeReportProperties = false;
    options.includeTotalsRow = false;
    options.useGzipCompression = false;

    String reportDownloadUrl = reportDAO.getReportDownloadUrl(reportJobId, options);

    HttpRequest req = new HttpRequest();

    req.setEndpoint(reportDownloadUrl);
    req.setMethod('GET');

    Http http = new Http();

    HttpResponse resp = http.send(req);

    return resp.getBody();
  }
}