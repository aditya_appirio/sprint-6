/**
 */
@isTest
private class Test_DC_ProposalWorkflowActionController {
	
	static Opportunity opp1;
	static testMethod void testDC_ProposalWorkflowActionSubmit() {
		createTestData();
		Test.startTest();
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplProposal());
    ApexPages.currentPage().getParameters().put('action','submit');
    ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(opp1);
		DC_ProposalWorkflowActionController controller = new DC_ProposalWorkflowActionController(sc);
		controller.doActionOnProposal();
		Test.stopTest();
	}
	
	static testMethod void testDC_ProposalWorkflowActionRetract() {
		
    createTestData();
    Test.startTest();
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplProposal());
    ApexPages.currentPage().getParameters().put('action','retract');
    ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(opp1);
		DC_ProposalWorkflowActionController controller = new DC_ProposalWorkflowActionController(sc);
		controller.doActionOnProposal();
		Test.stopTest();
	}
	
	private static void createTestData(){
		goog_dclk_dsm__DC_ObjectMapping__c dcObjMapping = DC_TestUtils.createObjectMapping('Proposal', 'Opportunity', false);
    insert new List<goog_dclk_dsm__DC_ObjectMapping__c>{dcObjMapping};
    goog_dclk_dsm__DC_FieldMapping__c fldMapping1 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping2 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'custom.SFDC_Opportunity_ID', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Id', Name = 'sfid');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping3 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'probabilityToClose', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Probability', Name = 'probability');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping4 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'status', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345,  Name = 'status');
    
    
    insert new List<goog_dclk_dsm__DC_FieldMapping__c>{fldMapping1,fldMapping2,fldMapping3,fldMapping4};
    
    Account acc = DC_TestUtils.createAccount('testCompany', true);
    
    opp1 = DC_TestUtils.createOpportunity('testProposal1',System.now().date(),acc.Id,'Prospecting',false);
    opp1.goog_dclk_dsm__DC_ProposalStatus__c = 'DRAFT';
    opp1.goog_dclk_dsm__DC_DoubleClickId__c = '12345';
    insert opp1;
	}
}