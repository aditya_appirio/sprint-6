public with sharing class DC_ProposalWorkflowActionController {
	
	public Opportunity currentOppty {get;set;}
	private String actionToPerform;
	public DC_ProposalWorkflowActionController(ApexPages.standardController sc){
		currentOppty = (Opportunity)getSelectedRecord(sc.getId())[0];
		actionToPerform = ApexPages.currentPage().getParameters().get('action');
	}
	
	// query for the selected record
  public List<sObject> getSelectedRecord(Id recordId) {
    return [
      SELECT id, name, goog_dclk_dsm__DC_AccountDoubleClickId__c, 
             goog_dclk_dsm__DC_DoubleClickId__c, goog_dclk_dsm__DC_ReadyForDSM__c,
             AccountId, Account.goog_dclk_dsm__DC_ReadyForDSM__c,
             Account.Name, goog_dclk_dsm__DC_ProposalStatus__c
        FROM Opportunity 
       WHERE id = :recordId
    ];
  }
  
  public Pagereference doActionOnProposal(){
  	
  	//added 16-oct-2014
  	String strAction ='';
  	//updated 21-oct-2014 try and catch
  	try{ 
		  	if(actionToPerform.equalsIgnoreCase(Label.DC_Submit)){
		  		submitProposal();
		  		strAction = 'Submit Proposal';
		  	}else if(actionToPerform.equalsIgnoreCase(Label.DC_Retract)){
		  		retractProposals();
		  		strAction = 'Retract Proposal';
		  	}
		  	// Redirect the user back to the original page
		    DC_OpportunityProposalConnector connectorInstance = DC_OpportunityProposalConnector.getInstance();
		    
		    //updated 16-oct-2014
		    DC_GenericWrapper propsalWrapper = connectorInstance.getProposalFromOpportunity(currentOppty,strAction);
		    
		    //updated 16-oct-2014
		    DC_Connector.DC_DoubleClickPageProcessingResult res = connectorInstance.syncProposalsFromDCtoSF(propsalWrapper,strAction);
		    
		    connectorInstance.executeAllDml();
		
		  	
  		}
  		catch (Exception e) {
	    		System.debug('e:::' + e);
		      if(!ApexPages.hasMessages()){
	        	ApexPages.addMessages(e);
	      	}
	  	}
	  	
	  	PageReference pageRef = new PageReference('/' + currentOppty.Id);
	    pageRef.setRedirect(true);
	    return pageRef;
  	}
  
	private void submitProposal(){
		DC_OpportunityProposalConnector proposalConnector = DC_OpportunityProposalConnector.getInstance();
		proposalConnector.submitProposalsForApproval(currentOppty);
	}
	
	
	private void retractProposals(){
		DC_OpportunityProposalConnector proposalConnector = DC_OpportunityProposalConnector.getInstance();
		proposalConnector.retractProposals(currentOppty);
	}
	
}