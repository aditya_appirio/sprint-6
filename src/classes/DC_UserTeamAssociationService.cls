//Generated by wsdl2apex

public class DC_UserTeamAssociationService {
    public class CommonError {
        public String reason;
        private String[] reason_type_info = new String[]{'reason',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'reason'};
    }
    public class UserTeamAssociation extends DC_UserTeamAssociationService.UserRecordTeamAssociation {
        public Long userId;        
        public String UserRecordTeamAssociation_Type;
        public Long teamId;
        public String overriddenTeamAccessType;
        public String defaultTeamAccessType;
        public String type = 'UserTeamAssociation';

        private String[] type_att_info = new String[]{'xsi:type'};
        
        private String[] userId_type_info = new String[]{'userId',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] UserRecordTeamAssociation_Type_type_info = new String[]{'UserRecordTeamAssociation.Type',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] teamId_type_info = new String[]{'teamId',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] overriddenTeamAccessType_type_info = new String[]{'overriddenTeamAccessType',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] defaultTeamAccessType_type_info = new String[]{'defaultTeamAccessType',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'teamId','overriddenTeamAccessType', 'defaultTeamAccessType','UserRecordTeamAssociation_Type', 'userId'};
    }
    public class DateTimeValue {
        public DC_UserTeamAssociationService.DateTime_x value;
        private String[] value_type_info = new String[]{'value',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'value'};
    }
    public class InternalApiError {
        public String reason;
        private String[] reason_type_info = new String[]{'reason',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'reason'};
    }
    public class Statement {
        public String query;
        public DC_UserTeamAssociationService.String_ValueMapEntry[] values;
        private String[] query_type_info = new String[]{'query',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] values_type_info = new String[]{'values',DC_Constants.SERVICE_END_POINT,null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'query','values'};
    }
    public class PublisherQueryLanguageContextError {
        public String reason;
        private String[] reason_type_info = new String[]{'reason',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'reason'};
    }
    public class UserTeamAssociationServiceInterfacePort {
        //changed 30-oct-2014
        public String endpoint_x = DC_Constants.WSDL_SOAP_LOCATION +'/UserTeamAssociationService';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        public DC_UserTeamAssociationService.SoapResponseHeader ResponseHeader;
        public DC_UserTeamAssociationService.SoapRequestHeader RequestHeader;
        private String ResponseHeader_hns = 'ResponseHeader='+DC_Constants.SERVICE_END_POINT;
        private String RequestHeader_hns = 'RequestHeader='+DC_Constants.SERVICE_END_POINT;
        private String[] ns_map_type_info = new String[]{DC_Constants.SERVICE_END_POINT, 'DC_UserTeamAssociationService'};
        public DC_UserTeamAssociationService.UserTeamAssociation[] createUserTeamAssociations(DC_UserTeamAssociationService.UserTeamAssociation[] userTeamAssociations) {
            DC_UserTeamAssociationService.createUserTeamAssociations_element request_x = new DC_UserTeamAssociationService.createUserTeamAssociations_element();
            DC_UserTeamAssociationService.createUserTeamAssociationsResponse_element response_x;
            request_x.userTeamAssociations = userTeamAssociations;
            Map<String, DC_UserTeamAssociationService.createUserTeamAssociationsResponse_element> response_map_x = new Map<String, DC_UserTeamAssociationService.createUserTeamAssociationsResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              DC_Constants.SERVICE_END_POINT,
              'createUserTeamAssociations',
              DC_Constants.SERVICE_END_POINT,
              'createUserTeamAssociationsResponse',
              'DC_UserTeamAssociationService.createUserTeamAssociationsResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.rval;
        }
        public DC_UserTeamAssociationService.UserTeamAssociationPage getUserTeamAssociationsByStatement(DC_UserTeamAssociationService.Statement filterStatement) {
            DC_UserTeamAssociationService.getUserTeamAssociationsByStatement_element request_x = new DC_UserTeamAssociationService.getUserTeamAssociationsByStatement_element();
            DC_UserTeamAssociationService.getUserTeamAssociationsByStatementResponse_element response_x;
            request_x.filterStatement = filterStatement;
            Map<String, DC_UserTeamAssociationService.getUserTeamAssociationsByStatementResponse_element> response_map_x = new Map<String, DC_UserTeamAssociationService.getUserTeamAssociationsByStatementResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              DC_Constants.SERVICE_END_POINT,
              'getUserTeamAssociationsByStatement',
              DC_Constants.SERVICE_END_POINT,
              'getUserTeamAssociationsByStatementResponse',
              'DC_UserTeamAssociationService.getUserTeamAssociationsByStatementResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.rval;
        }
        public DC_UserTeamAssociationService.UserTeamAssociation[] updateUserTeamAssociations(DC_UserTeamAssociationService.UserTeamAssociation[] userTeamAssociations) {
            DC_UserTeamAssociationService.updateUserTeamAssociations_element request_x = new DC_UserTeamAssociationService.updateUserTeamAssociations_element();
            DC_UserTeamAssociationService.updateUserTeamAssociationsResponse_element response_x;
            request_x.userTeamAssociations = userTeamAssociations;
            Map<String, DC_UserTeamAssociationService.updateUserTeamAssociationsResponse_element> response_map_x = new Map<String, DC_UserTeamAssociationService.updateUserTeamAssociationsResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              DC_Constants.SERVICE_END_POINT,
              'updateUserTeamAssociations',
              DC_Constants.SERVICE_END_POINT,
              'updateUserTeamAssociationsResponse',
              'DC_UserTeamAssociationService.updateUserTeamAssociationsResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.rval;
        }
        public DC_UserTeamAssociationService.UpdateResult performUserTeamAssociationAction(DC_UserTeamAssociationService.UserTeamAssociationAction userTeamAssociationAction,DC_UserTeamAssociationService.Statement statement) {
            DC_UserTeamAssociationService.performUserTeamAssociationAction_element request_x = new DC_UserTeamAssociationService.performUserTeamAssociationAction_element();
            DC_UserTeamAssociationService.performUserTeamAssociationActionResponse_element response_x;
            request_x.userTeamAssociationAction = userTeamAssociationAction;
            request_x.statement = statement;
            Map<String, DC_UserTeamAssociationService.performUserTeamAssociationActionResponse_element> response_map_x = new Map<String, DC_UserTeamAssociationService.performUserTeamAssociationActionResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              DC_Constants.SERVICE_END_POINT,
              'performUserTeamAssociationAction',
              DC_Constants.SERVICE_END_POINT,
              'performUserTeamAssociationActionResponse',
              'DC_UserTeamAssociationService.performUserTeamAssociationActionResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.rval;
        }
    }
    public class ApiError {
        public String fieldPath;
        public String trigger_x;
        public String errorString;
        public String ApiError_Type;
        private String[] fieldPath_type_info = new String[]{'fieldPath',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] trigger_x_type_info = new String[]{'trigger',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] errorString_type_info = new String[]{'errorString',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] ApiError_Type_type_info = new String[]{'ApiError.Type',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'fieldPath','trigger_x','errorString','ApiError_Type'};
    }
    public class performUserTeamAssociationAction_element {
        public DC_UserTeamAssociationService.UserTeamAssociationAction userTeamAssociationAction;
        public DC_UserTeamAssociationService.Statement statement;
        private String[] userTeamAssociationAction_type_info = new String[]{'userTeamAssociationAction',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] statement_type_info = new String[]{'statement',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'userTeamAssociationAction','statement'};
    }
    public class Date_x {
        public Integer year;
        public Integer month;
        public Integer day;
        private String[] year_type_info = new String[]{'year',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] month_type_info = new String[]{'month',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] day_type_info = new String[]{'day',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'year','month','day'};
    }
    public class SoapRequestHeader {
        public String networkCode;
        public String applicationName;
        public DC_UserTeamAssociationService.Authentication authentication;
        private String[] networkCode_type_info = new String[]{'networkCode',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] applicationName_type_info = new String[]{'applicationName',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] authentication_type_info = new String[]{'authentication',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'networkCode','applicationName','authentication'};
    }
    public class Authentication {
        public String Authentication_Type;
        private String[] Authentication_Type_type_info = new String[]{'Authentication.Type',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'Authentication_Type'};
    }
    public class SetValue {
        public DC_UserTeamAssociationService.Value[] values;
        private String[] values_type_info = new String[]{'values',DC_Constants.SERVICE_END_POINT,null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'values'};
    }
    public class BooleanValue {
        public Boolean value;
        private String[] value_type_info = new String[]{'value',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'value'};
    }
    public class NullError {
        public String reason;
        private String[] reason_type_info = new String[]{'reason',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'reason'};
    }
    public class ApiException extends Exception {
        public DC_UserTeamAssociationService.ApiError[] errors;
        private String[] errors_type_info = new String[]{'errors',DC_Constants.SERVICE_END_POINT,null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'errors'};
    }
    public class createUserTeamAssociations_element {
        public DC_UserTeamAssociationService.UserTeamAssociation[] userTeamAssociations;
        private String[] userTeamAssociations_type_info = new String[]{'userTeamAssociations',DC_Constants.SERVICE_END_POINT,null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'userTeamAssociations'};
    }
    public class performUserTeamAssociationActionResponse_element {
        public DC_UserTeamAssociationService.UpdateResult rval;
        private String[] rval_type_info = new String[]{'rval',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'rval'};
    }
    public class createUserTeamAssociationsResponse_element {
        public DC_UserTeamAssociationService.UserTeamAssociation[] rval;
        private String[] rval_type_info = new String[]{'rval',DC_Constants.SERVICE_END_POINT,null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'rval'};
    }
    public class FeatureError {
        public String reason;
        private String[] reason_type_info = new String[]{'reason',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'reason'};
    }
    public class AuthenticationError {
        public String reason;
        private String[] reason_type_info = new String[]{'reason',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'reason'};
    }
    public class PermissionError {
        public String reason;
        private String[] reason_type_info = new String[]{'reason',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'reason'};
    }
    public class QuotaError {
        public String reason;
        private String[] reason_type_info = new String[]{'reason',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'reason'};
    }
    public class PublisherQueryLanguageSyntaxError {
        public String reason;
        private String[] reason_type_info = new String[]{'reason',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'reason'};
    }
    public class DateValue {
        public DC_UserTeamAssociationService.Date_x value;
        private String[] value_type_info = new String[]{'value',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'value'};
    }
    public class UserTeamAssociationAction {
        public String UserTeamAssociationAction_Type;
        private String[] UserTeamAssociationAction_Type_type_info = new String[]{'UserTeamAssociationAction.Type',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'UserTeamAssociationAction_Type'};
    }
    public class ApplicationException extends Exception {
        public String message;
        public String ApplicationException_Type;
        private String[] message_type_info = new String[]{'message',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] ApplicationException_Type_type_info = new String[]{'ApplicationException.Type',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'message','ApplicationException_Type'};
    }
    public class String_ValueMapEntry {
        public String key;
        public DC_UserTeamAssociationService.Value value;
        private String[] key_type_info = new String[]{'key',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] value_type_info = new String[]{'value',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'key','value'};
    }
    public class Value {
        public String Value_Type;
        private String[] Value_Type_type_info = new String[]{'Value.Type',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'Value_Type'};
    }
    public class updateUserTeamAssociationsResponse_element {
        public DC_UserTeamAssociationService.UserTeamAssociation[] rval;
        private String[] rval_type_info = new String[]{'rval',DC_Constants.SERVICE_END_POINT,null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'rval'};
    }
    public class ApiVersionError {
        public String reason;
        private String[] reason_type_info = new String[]{'reason',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'reason'};
    }
    public class OAuth {
        public String parameters;
        private String[] parameters_type_info = new String[]{'parameters',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'parameters'};
    }
    public class updateUserTeamAssociations_element {
        public DC_UserTeamAssociationService.UserTeamAssociation[] userTeamAssociations;
        private String[] userTeamAssociations_type_info = new String[]{'userTeamAssociations',DC_Constants.SERVICE_END_POINT,null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'userTeamAssociations'};
    }
    public class ServerError {
        public String reason;
        private String[] reason_type_info = new String[]{'reason',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'reason'};
    }
    public class DateTime_x {
        public DC_UserTeamAssociationService.Date_x date_x;
        public Integer hour;
        public Integer minute;
        public Integer second;
        public String timeZoneID;
        private String[] date_x_type_info = new String[]{'date',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] hour_type_info = new String[]{'hour',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] minute_type_info = new String[]{'minute',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] second_type_info = new String[]{'second',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] timeZoneID_type_info = new String[]{'timeZoneID',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'date_x','hour','minute','second','timeZoneID'};
    }
    public class StatementError {
        public String reason;
        private String[] reason_type_info = new String[]{'reason',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'reason'};
    }
    public class getUserTeamAssociationsByStatementResponse_element {
        public DC_UserTeamAssociationService.UserTeamAssociationPage rval;
        private String[] rval_type_info = new String[]{'rval',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'rval'};
    }
    public class NumberValue {
        public String value;
        private String[] value_type_info = new String[]{'value',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'value'};
    }
    public class DeleteUserTeamAssociations {
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{};
    }
    public class TextValue {
        public String value;
        private String[] value_type_info = new String[]{'value',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'value'};
    }
    public class SoapResponseHeader {
        public String requestId;
        public Long responseTime;
        private String[] requestId_type_info = new String[]{'requestId',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] responseTime_type_info = new String[]{'responseTime',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'requestId','responseTime'};
    }
    public class UserTeamAssociationPage {
        public Integer totalResultSetSize;
        public Integer startIndex;
        public DC_UserTeamAssociationService.UserTeamAssociation[] results;
        private String[] totalResultSetSize_type_info = new String[]{'totalResultSetSize',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] startIndex_type_info = new String[]{'startIndex',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] results_type_info = new String[]{'results',DC_Constants.SERVICE_END_POINT,null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'totalResultSetSize','startIndex','results'};
    }
    public class getUserTeamAssociationsByStatement_element {
        public DC_UserTeamAssociationService.Statement filterStatement;
        private String[] filterStatement_type_info = new String[]{'filterStatement',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'filterStatement'};
    }
    public class UpdateResult {
        public Integer numChanges;
        private String[] numChanges_type_info = new String[]{'numChanges',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'numChanges'};
    }
    public class NotNullError {
        public String reason;
        private String[] reason_type_info = new String[]{'reason',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'reason'};
    }
    public virtual class UserRecordTeamAssociation {
        public Long userId;
        public Long teamId;
        public String overriddenTeamAccessType;
        public String defaultTeamAccessType;      
        public String UserRecordTeamAssociation_Type;
        private String[] userId_type_info = new String[]{'userId',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] UserRecordTeamAssociation_Type_type_info = new String[]{'UserRecordTeamAssociation.Type',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] teamId_type_info = new String[]{'teamId',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] overriddenTeamAccessType_type_info = new String[]{'overriddenTeamAccessType',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};
        private String[] defaultTeamAccessType_type_info = new String[]{'defaultTeamAccessType',DC_Constants.SERVICE_END_POINT,null,'0','1','false'};        
        private String[] apex_schema_type_info = new String[]{DC_Constants.SERVICE_END_POINT,'true','false'};
        private String[] field_order_type_info = new String[]{'teamId','overriddenTeamAccessType','defaultTeamAccessType','UserRecordTeamAssociation_Type', 'userId'};
    }
}