public with sharing class DC_TeamMapper extends DC_ObjectMapper {
	
	private static DC_TeamMapper instance;
  
  //constructor
  private DC_TeamMapper(){
    super();
  }
  public static DC_TeamMapper getInstance() {
    if(instance == null) {
      instance = new DC_TeamMapper();
    } 
    return instance;
  } 
  // Overrided method wrap : takes DC_TeamService.Team as param and call wrap(List<DC_TeamService.Team>)
  public DC_GenericWrapper wrap(DC_TeamService.Team team) {
    List<DC_TeamService.Team> teamList = new List<DC_TeamService.Team>();
    teamList.add(team);
    DC_GenericWrapper result = wrap(teamList);
    return result;
  }
  
  // Overrided method wrap : takes List<DC_TeamService.Team> as param and convert into DC_DoubleClickUserWrapper instance
  public DC_GenericWrapper wrap(List<DC_TeamService.Team> teams) {
    return new DC_DoubleClickTeamWrapper(this.getObjectMappingByName('team'), this.getFieldMappingsByName('team'), teams);
  }
  
  // Overloaded method wrap : takes List<DC_ProposalService.Proposal> as param and convert into DC_DoubleClickProposalWrapper instance
  public DC_GenericWrapper wrap(DC_TeamService.TeamPage teamPage) {
    DC_GenericWrapper wrapper = new DC_DoubleClickTeamWrapper(this.getObjectMappingByName('team'), this.getFieldMappingsByName('team'), teamPage.results);
    wrapper.totalResultSetSize = teamPage.totalResultSetSize;
    return wrapper;
  } 
  
  //Create new DC contacts for SF Contacts
  public override DC_GenericWrapper createNewDCRecords(DC_GenericWrapper teams) {
    DC_GenericWrapper dc_teams = this.wrap(new List<DC_TeamService.Team>());
    teams.gotoBeforeFirst();
    
    while(teams.hasNext()) {
      teams.next();
      dc_teams.add(new DC_TeamService.Team());
      dc_teams.gotoLast();
      updateFields(teams, dc_teams);
    }
    return dc_teams;
  }
  
  public boolean updateFieldsOnAllObjectsWithCreate(DC_GenericWrapper source, DC_GenericWrapper target) {
    source.gotoBeforeFirst();

    while(source.hasNext()) {
      source.next();
      if(target.gotoDCId(String.valueOf(source.toLong(source.getField('DoubleClickId'))))) {
        updateFields(source, target);
      } else {
        addNewRecordFromSource(source, target);
      }
    }
    return getHandlerClassInMapping();
  }  
      
  public void addNewRecordFromSource(DC_GenericWrapper source, DC_GenericWrapper target) {
    object item;
    if (target instanceOf DC_SalesforceWrapper) {
      item = new goog_dclk_dsm__DC_Team__c();
    } else {
      item = new DC_TeamService.Team();
    } 
    
    target.add(item);
    target.gotoLast();    
    
    updateFields(source, target);
  }   
  
  /*public DateTime getLatestUpdateDate(DC_DoubleClickUserWrapper wrapper) {
  	if (wrapper.size() == 0)
      return null;
      
  	wrapper.gotoBeforeFirst();
  	 
  	DateTime curMax = DateTime.newInstance(1901, 1, 1);
  	DateTime cur;
  	while (wrapper.hasNext()) { 
      wrapper.next();
      if(((DC_ServiceDateTime)((DC_TeamService.Team)wrapper.getCurrentObject()).lastModifiedDateTime) != null)
      	cur = ((DC_ServiceDateTime)((DC_TeamService.Team)wrapper.getCurrentObject()).lastModifiedDateTime).getSalesforceDateTime();

      if (cur > curMax) {
        curMax = cur;
      }
  	}
  	
  	return curMax;
  }*/
}