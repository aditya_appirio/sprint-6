//
//  Batch syncing process for proposal lines
//
public with sharing class DC_QueueableBatchProposalLineSync extends DC_QueueableBatchProcess implements
  Database.batchable<long>, 
  Database.AllowsCallouts,
  Database.Stateful {
      
  public DC_RetrieveProposalsParams lineparams;
  
  // the lines to process for each page  
  public override long getRecordsToProcessPerPage() {
  	return (goog_dclk_dsm__DC_ConnectorApplicationSettings__c.getInstance().goog_dclk_dsm__DC_ProposalLinesToProcessPerPage__c == null) ? DC_Constants.PROPOSAL_PAGE_SIZE : 
    	         (long)goog_dclk_dsm__DC_ConnectorApplicationSettings__c.getInstance().goog_dclk_dsm__DC_ProposalLinesToProcessPerPage__c;
  }
  
  public override DateTime getBasisDate() {
  	return DC_SyncDateManager.opportunityLineitemLastSyncDate;
  }
    
  // DC_QueueableProcess Abstract Methods ===============
  
  // initialize the batch with the parameters
  public override void initialize(DC_QueueableProcess.DC_QueueableProcessParameter inparams) {
    params = lineparams = (DC_RetrieveProposalsParams)inparams;
  }

  // execute the batch process
  public override void execute() {
  	// using batch size one as each execution will operate on a full page of data
    Database.executeBatch(this, 1);
  } 

  // offer the parameter type for passing the parameter
  public override System.Type getParameterType() {
     return DC_RetrieveProposalsParams.class;
  }
  
  public override DC_Connector.DC_DoubleClickPageProcessingResult processRecords(long pageSize, long pageNo) {
  	return DC_OpportunityProposalLineItemConnector.getInstance().syncProposalLineItemsByPage(basisDate, pageSize, pageNo * pageSize);
  }

  public override void updateSyncDate(DateTime dt) {
    DC_SyncDateManager.opportunityLineItemLastSyncDate = dt;
  }
  public override String getResultString() {
  	return String.format('Total Records Processed: {0}\nInserted Records: {1}\nUpdated Records: {2}\nError Records: {3}', 
      new String[] { 
      	(recordsProcessed == null) ? '' : recordsProcessed.format() ,
      	String.valueOf(insertedRecordCount),
      	String.valueOf(updatedRecordCount),
      	String.valueOf(errorCount)     
      });
  }  

  public class DC_RetrieveProposalsParams extends DC_QueueableProcess.DC_QueueableProcessParameter { }
}