// 
//  Provides access to custom handler instances to support custom handler functionality in mappings
//
public with sharing class DC_FieldHandlerFactory {
  public static DC_FieldMappingCustomHandler getHandlerInstance(string className) {

	  // get the type for this class
    Type newInstanceType = Type.forName(className);

    if (newInstanceType == null) {
    	throw new DC_CustomFieldHandlerException('Class specified (' + className + ') does not correspond to an existing class.');
    }

	  // create a new instance for the type
    object handler = newInstanceType.newInstance();
    
    if (!(handler instanceof DC_FieldMappingCustomHandler)) {
    	throw new DC_CustomFieldHandlerException('Class specified (' + className + ') does not implmeent the DC_FieldHandlerFactory.DC_FieldMappingCustomHandler interface.');
    }      
    
	  return (DC_FieldMappingCustomHandler)handler;
  }
  
  public static String getHandlerQueryString(string className) {
  	 return getHandlerInstance(className).getSalesforceQueryString();
  }
	
	public class DC_CustomFieldHandlerException extends Exception { }
	
  public interface DC_FieldMappingCustomHandler {
  	// get a string to be added to the query that fetches the records
    string getSalesforceQueryString();
    void initialize(goog_dclk_dsm__DC_ObjectMapping__c objectMapping, List<goog_dclk_dsm__DC_FieldMapping__c> fieldMappings, DC_GenericWrapper source, DC_GenericWrapper target);
    void processField(goog_dclk_dsm__DC_FieldMapping__c currentField, DC_GenericWrapper source, DC_GenericWrapper target);
    void finish();
  }
}