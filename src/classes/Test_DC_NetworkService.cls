/**
 */
@isTest
private class Test_DC_NetworkService {

	static testMethod void myUnitTest() {
		Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplNetworkService());
    Test.startTest(); 
    DC_NetworkService.CommonError ce = new DC_NetworkService.CommonError();
    System.assert(ce != null);
    
    DC_NetworkService.InternalApiError iae = new DC_NetworkService.InternalApiError();
    DC_NetworkService.Authentication au = new DC_NetworkService.Authentication();
    DC_NetworkService.StringLengthError sle = new DC_NetworkService.StringLengthError();
    DC_NetworkService.NetworkServiceInterfacePort port = new DC_NetworkService.NetworkServiceInterfacePort();
    System.assert(port != null);
    
    DC_NetworkService.Network[] networkArr = port.getAllNetworks();
    DC_NetworkService.Network networkUpdate = port.updateNetwork(new DC_NetworkService.Network());
    DC_NetworkService.Network currNW = port.getCurrentNetwork();
    DC_NetworkService.Network testNW = port.makeTestNetwork();
    
    DC_NetworkService.makeTestNetworkResponse_element updateCompRes = new DC_NetworkService.makeTestNetworkResponse_element();
    DC_NetworkService.RequiredError re = new DC_NetworkService.RequiredError();
    DC_NetworkService.updateNetworkResponse_element updateNWResp = new DC_NetworkService.updateNetworkResponse_element();
    DC_NetworkService.FeatureError fe = new DC_NetworkService.FeatureError();
    DC_NetworkService.AuthenticationError ae = new DC_NetworkService.AuthenticationError();
    DC_NetworkService.PermissionError pe = new DC_NetworkService.PermissionError();
    DC_NetworkService.PublisherQueryLanguageSyntaxError PQLSyntaxErr = new DC_NetworkService.PublisherQueryLanguageSyntaxError();
    DC_NetworkService.makeTestNetwork_element testNwEle = new DC_NetworkService.makeTestNetwork_element();
    DC_NetworkService.getCurrentNetwork_element getCurrEle = new DC_NetworkService.getCurrentNetwork_element();
    DC_NetworkService.OAuth oAuth = new DC_NetworkService.OAuth();
    DC_NetworkService.getCurrentNetworkResponse_element currNWRespEle = new DC_NetworkService.getCurrentNetworkResponse_element();
    DC_NetworkService.Network nw = new DC_NetworkService.Network();
    DC_NetworkService.TypeError typeErr = new DC_NetworkService.TypeError();
    DC_NetworkService.SoapResponseHeader soapRespHeader = new DC_NetworkService.SoapResponseHeader();
    DC_NetworkService.NotNullError nne = new DC_NetworkService.NotNullError();
    DC_NetworkService.updateNetwork_element updateNWEle = new DC_NetworkService.updateNetwork_element();
    DC_NetworkService.ExchangeRateError exchangeRateErr = new DC_NetworkService.ExchangeRateError();
    DC_NetworkService.PublisherQueryLanguageContextError PQLContentErr = new DC_NetworkService.PublisherQueryLanguageContextError();
    DC_NetworkService.ApiError apiErr = new DC_NetworkService.ApiError();
    DC_NetworkService.getAllNetworks_element allNwEle = new DC_NetworkService.getAllNetworks_element();

    DC_NetworkService.SoapRequestHeader soapReqHeader = new DC_NetworkService.SoapRequestHeader();
    DC_NetworkService.ApiException apiExce = new DC_NetworkService.ApiException();
    DC_NetworkService.getAllNetworksResponse_element getAllNwResp = new DC_NetworkService.getAllNetworksResponse_element();
    DC_NetworkService.QuotaError quotaErr = new DC_NetworkService.QuotaError();
    DC_NetworkService.ApplicationException appExcep = new DC_NetworkService.ApplicationException();
    DC_NetworkService.ApiVersionError apiVersionErr = new DC_NetworkService.ApiVersionError();
    DC_NetworkService.ParseError parseErr = new DC_NetworkService.ParseError();
    DC_NetworkService.ServerError serverErr = new DC_NetworkService.ServerError();
    
    DC_NetworkService.StatementError statementErr = new DC_NetworkService.StatementError();
    DC_NetworkService.InvalidEmailError invalidEmailErr = new DC_NetworkService.InvalidEmailError();
    Test.stopTest();		
	}
}