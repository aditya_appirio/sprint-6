// 
// Abstract class for creating a common service date time
// This class is used to support the DateTime class in the different service 
// classes -- those are updated to extend from this object.
//
public abstract with sharing class DC_ServiceDateTime {
  public DC_ServiceDate date_x;
  public Integer hour;
  public Integer minute;
  public Integer second;
  static string publisherTimeZone;
  static {
  	if(goog_dclk_dsm__DC_ConnectorApplicationSettings__c.getOrgDefaults().get('goog_dclk_dsm__DC_PublisherTimezoneInDSM__c') != null){
  		publisherTimeZone = String.valueOf(goog_dclk_dsm__DC_ConnectorApplicationSettings__c.getOrgDefaults().get('goog_dclk_dsm__DC_PublisherTimezoneInDSM__c'));
  	}
  }

  // value setters and getters.  Note that these support the service sub-classes
  public void setDate(DC_ServiceDate value) {
    date_x = value;   
  }
  
  public DC_ServiceDate getDate() {
    return date_x;
  }
  
  public void setHour(integer value) {
    hour = value;
  }
  
  public integer getHour() {
    return hour;
  } 
  
  public void setMinute(integer value) {
    minute = value;
  }
  
  public integer getMinute() {
    return minute;
  }   
  
  public void setSecond(integer value) {
    second = value;
  }

  public integer getSecond() {
    return second;
  }

  // return the salesforce version of the date/time this object represents
  public Datetime getSalesforceDatetime() {
    // TODO: Jaipur please put the timezone name below 'America/New_York' in the custom setting and have this class read from it 
    //String publisherTimeZone = String.valueOf(goog_dclk_dsm__DC_ConnectorApplicationSettings__c.getOrgDefaults().get(goog_dclk_dsm__'DC_PublisherTimezoneInDSM__c'));
    Integer offset;
    if(publisherTimeZone != null){
    	offset = Timezone.getTimeZone(publisherTimeZone).getOffset(Datetime.now());
    }else{
    	offset = 0;
    }
    return Datetime.newInstanceGmt(date_x.year, date_x.month, date_x.day, hour, minute, second).addSeconds(offset / 1000);
  } 

  // set the date/time for this object based on the passed Salesforce date / time
  // theDatetime  DateTime  the date and time to set this instance to
  public void setSalesforceDatetime(Datetime theDatetime) {
    if (theDateTime == null)
      return;   
    // TODO: Jaipur please put the timezone name below 'America/New_York' in the custom setting and have this class read from it. 
    // it can be the same as the getSalesforceDatetime method
    System.debug('publisherTimeZone:' + publisherTimeZone);
    Integer offset;
    if(publisherTimeZone != null){
     offset = Timezone.getTimeZone(publisherTimeZone).getOffset(Datetime.now());
    }else{
    	offset = 0;
    }
    Datetime dt = theDatetime.addSeconds(offset / 1000 * -1);

    this.date_x.setSalesforceDate(dt.date());

    this.hour = dt.hour();
    this.minute = dt.minute();
    this.second = dt.second();
  }  
}