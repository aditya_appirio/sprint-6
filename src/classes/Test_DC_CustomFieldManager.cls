// 
// (c) 2014 Appirio
//
// Test_DC_CustomFieldManager: test class for DC_CustomFieldManager 
//
@isTest
private class Test_DC_CustomFieldManager {
	@isTest
  static void testCustFldManager() {
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplCustomFieldService());
    Test.startTest();
    DC_DoubleClickOAuthHandler oAuthHandler = new DC_DoubleClickOAuthHandler(Userinfo.getUserId());
    
    DC_CustomFieldManager custFldManager = new DC_CustomFieldManager(oAuthHandler);
    System.assertEquals(custFldManager.getCustomFieldIdByName('test'), 12345);
    
    Long custId = custFldManager.createCustomField('test1', 'testEntityType');
    
    Long custId1 = custFldManager.createOrGetCustomFieldId('test1', 'testEntityType');
    Long externalfldId = custFldManager.getProposalExternalIDFieldId();
    Test.stopTest();
    System.assert(custId1 != null);
    System.assert(externalfldId != null);
  }
  
  static testMethod void testProposalExternalFldId() {
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplCustomFieldService1());
    Test.startTest();
    DC_DoubleClickOAuthHandler oAuthHandler = new DC_DoubleClickOAuthHandler(Userinfo.getUserId());
    
    DC_CustomFieldManager custFldManager = new DC_CustomFieldManager(oAuthHandler);
	  Long externalfldId = custFldManager.getProposalExternalIDFieldId();
	  System.assertEquals(12345, externalfldId);
    Test.stopTest();
  }
  
  @isTest
  static void testCustFldIdByName() {
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplCustomFieldService());
    Test.startTest();
    DC_DoubleClickOAuthHandler oAuthHandler = new DC_DoubleClickOAuthHandler(Userinfo.getUserId());
    DC_CustomFieldManager custFldManager = new DC_CustomFieldManager(oAuthHandler);
    Long res;
	  res = custFldManager.getCustomFieldIdByName('blah');
	  System.assertEquals(12345, res);
    
    Test.stopTest();
  } 
}