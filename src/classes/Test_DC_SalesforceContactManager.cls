// 
// (c) 2014 Appirio
//
// Test_DC_SalesforceContactManager : test class for DC_SalesforceContactManager 
//
// 14 feb 2014    Ankit Goyal (JDC)      Original
//
@isTest
public with sharing class Test_DC_SalesforceContactManager {
  static testMethod void testDC_SalesforceContactManager() {
  	Account acc = DC_TestUtils.createAccount('test', true);
    Contact cnt = DC_TestUtils.createContact('test1','test2',acc.Id,true);
    goog_dclk_dsm__DC_ObjectMapping__c dcObjMapping = DC_TestUtils.createObjectMapping('Contact', 'Contact', true);
    goog_dclk_dsm__DC_FieldMapping__c dcFldMapping = DC_TestUtils.createFieldMapping(dcObjMapping.id,'Name', 'Name' , 'name', true);
    DC_SalesforceManager sm = new DC_SalesforceManager();
    DC_SalesforceContactManager scm = new DC_SalesforceContactManager();
    scm.getContact(cnt.id);

    scm.getContacts(new List<Id>{cnt.id});
    
    DC_GenericWrapper gw = scm.getContacts(new List<Contact>{cnt});
    
    scm.updateSFRecords(gw);
  }
}