public abstract with sharing class DC_QueueableBatchProcess extends DC_QueueableProcess {
// flag to indicate which direction the processing is using
  public long totalNumberOfRecords;
  public long recordsProcessed; 
  public long successCount;
  public long errorCount;
  public long insertedRecordCount;
  public long updatedRecordCount;
  public long recordsToProcessPerPage;
  public DateTime basisDate;
  public DateTime newBasis;
  
  boolean dirtyDataset = false;
  boolean hasErrors = false;  
  
  // abstract methods =====================================================  
  public abstract long getRecordsToProcessPerPage();
  public abstract DateTime getBasisDate();
  public abstract string getResultString();
  public abstract void updateSyncDate(DateTime dt);  
  public abstract DC_Connector.DC_DoubleClickPageProcessingResult processRecords(long pageSize, long pageNo);
  
  // common methods =====================================================
  
  public List<long> start(Database.BatchableContext info) {
    System.debug('starting...');
    successCount = 0;
    errorCount = 0;
    insertedRecordCount = 0;
    updatedRecordCount = 0;
    recordsProcessed = 0;
    // basis date is the date for records -- process all on or after this date
    basisDate = getBasisDate();
     
    // this will be the new basis that will replace the previous sync date 
    newBasis = DateTime.now();

    // batchRecords are the pages to process
    List<long> batchRecords = new List<long>();
    recordsToProcessPerPage = getRecordsToProcessPerPage();
    System.debug('@@ Records to process per page: ' + recordsToProcessPerPage);
    DC_Connector.DC_DoubleClickPageProcessingResult result;
    try {
      // get first iteration to populate the total number of records in set    
      System.debug('@@ Updating Salesforce (start)...');
      
      // process the first page, get the number of records to process
      result = processRecords(recordsToProcessPerPage, 0);

      System.debug('@@ total number of records returned: ' + result.totalResultSetSize);
      
      totalNumberOfRecords = result.totalResultSetSize;
      // calculate remaining records
      
      long remainingRecords = totalNumberOfRecords - recordsToProcessPerPage;

      // if there are more records to process, calculate the number of pages and construct our list to return
      if (remainingRecords > 0) {
      
        decimal numberOfRemainingCycles = (Math.ceil(Decimal.valueOf(remainingRecords) / Decimal.valueOf(recordsToProcessPerPage)));
        
        System.debug('@@ remaining records: ' + remainingRecords);
        System.debug('@@ remaining cycles: ' + numberOfRemainingCycles);
        
        // construct our list to return
        // starting at 1 as 0 was already processed
        for (long i = 1; i <= numberOfRemainingCycles; i++) { 
          batchRecords.add(i);
        }
      }
    } catch (Exception e) {
      System.debug('@@# execute-> logging exception: ' + e);
      DC_ConnectorLogger.log(params.queueItem.goog_dclk_dsm__DC_Process__c, e, params.queueItem.id);
      hasErrors = true;
    }
    executeDML();    
    if (result != null && result.dmlStatus != null) {
      updateStats(result.dmlStatus);
    }  

    return batchRecords;
  }
  

  // database.batchable execute method -- called each execution    
  public void execute(Database.BatchableContext info, List<long> pageNo) { 
    DC_Connector.DC_DoubleClickPageProcessingResult result;
    
    System.debug('@@ Updating Salesforce (execute)...');    
    try {
      
      // perform proposal sync
      result = processRecords(recordsToProcessPerPage, pageNo[0]);
      System.debug('@@ total number of records returned: ' + result.totalResultSetSize);      
      
      long newTotalRecords = result.totalResultSetSize; 
      System.debug('newTotalRecords::' + newTotalRecords);
      System.debug('totalNumberOfRecords::' + totalNumberOfRecords);
      // if we have a fluctuating recordset, bail out and reschedule for immediate execution
      if (newTotalRecords != totalNumberOfRecords || (Test.isRunningTest())) {
        System.debug('@@ dirty dataset -- rescheduling...');
        dirtyDataset = true;
        DC_QueueDispatch.queueItemCompleted(
              params.queueItem, 
              DC_QueueDispatch.DC_ExecutionStatus.Recordset_Updated,  
              getResultString()
            );          
        System.debug('@@ aborting current job...');
        System.abortJob(info.getJobId());
        System.debug('@@ scheduling new job...');
        DC_QueueDispatch.scheduleForImmediateExecution(params.queueItem.Id); 
      }
    } catch (Exception e) {
      System.debug('@@# execute-> logging exception: ' + e);
      DC_ConnectorLogger.log(params.queueItem.goog_dclk_dsm__DC_Process__c, e, params.queueItem.Id);
      hasErrors = true; 
    }
    executeDML();
    try {
      if (result != null && result.dmlStatus != null) {
        updateStats(result.dmlStatus);
      }    
    } catch (Exception e) {
      System.debug('@@# updating stats-> logging exception: ' + e);
      DC_ConnectorLogger.log(params.queueItem.goog_dclk_dsm__DC_Process__c, e, params.queueItem.Id);
    }

  }  
  
  public void executeDML() {
    try {
      DC_SalesforceDML.getInstance().flush();
    } catch (Exception e) {
      System.debug('@@# executeDML-> logging exception: ' + e);
      DC_ConnectorLogger.log(params.queueItem.goog_dclk_dsm__DC_Process__c, e, params.queueItem.Id);
      hasErrors = true;
    }       
  }  

  public void finish(Database.BatchableContext info) {
    System.debug('@@ finish'); 
    DC_QueueDispatch.queueItemCompleted(
      params.queueItem, 
      (hasErrors) ? DC_QueueDispatch.DC_ExecutionStatus.Error : DC_QueueDispatch.DC_ExecutionStatus.Success, 
      getResultString()
    );
    if (!hasErrors) {
      System.debug('@@ updating to new basis: ' + newBasis);
      updateSyncDate(newBasis);
    }
    
    DC_SalesforceDML.getInstance().flush();
  }    
    
  public void updateStats(DC_SalesforceDML.DC_SalesforceDMLStatus status) {
    System.debug('@@ updating stats: ' + status);
    System.debug('@@ previous stats-> recordsProcessed: ' + recordsProcessed + '; successCount: ' + successCount + '; errorCount: ' + 
      errorCount + '; insertedRecordCount: ' + insertedRecordCount + '; updatedRecordCount: ' + updatedRecordCount + '====' + status.getSuccessCount());
      
    recordsProcessed += status.getSuccessCount();
    successCount += status.getSuccessCount();
    errorCount += status.getErrorCount();
    insertedRecordCount += (status.getSuccessfulInsertRecords() != null) ? status.getSuccessfulInsertRecords().size() : 0;
    updatedRecordCount += (status.getSuccessfulUpdateRecords() != null) ? status.getSuccessfulUpdateRecords().size() : 0;
    
    System.debug('@@ current stats-> recordsProcessed: ' + recordsProcessed + '; successCount: ' + successCount + '; errorCount: ' + 
      errorCount + '; insertedRecordCount: ' + insertedRecordCount + '; updatedRecordCount: ' + updatedRecordCount);    
  }    
}