public with sharing class DC_ReportDAO extends DC_DoubleClickDAO {
  
  protected override void tokenRefreshed() {
    this.reportService = null;
  }

  public DC_ReportDAO(DC_DoubleClickOAuthHandler authHandler) {
    super(authHandler);
  }

  private DC_ReportService.ReportServiceInterfacePort reportService {
    get {
      if(this.reportService == null) {
        this.reportService = DC_ServicesFactory.getReportService(this.authHandler);
      } 

      return this.reportService;
    }
    set;
  }

  private class RunReportJobCommand implements DC_DoubleClickDAO.Command {
    public Long result;
    public DC_ReportDAO context;
    public DC_ReportService.ReportQuery query;
    
    public RunReportJobCommand(DC_ReportDAO context, DC_ReportService.ReportQuery query) {
      this.query = query;
      this.context = context;
    }
    
    public void execute() {
      DC_ReportService.ReportJob reportJob = new DC_ReportService.ReportJob();
      reportJob.reportQuery = this.query;
      this.result = context.reportService.runReportJob(reportJob).id;
    }
  }

  public Long runReportJob(DC_ReportService.ReportQuery reportQuery) {
    RunReportJobCommand cmd = new RunReportJobCommand(this, reportQuery);
    
    this.invokeAPI(cmd, true);
    
    return cmd.result;
  }

  private class GetReportJobCommand implements DC_DoubleClickDAO.Command {
    public DC_ReportService.ReportJob result;
    public Long reportJobId;
    public DC_ReportDAO context;
    
    public GetReportJobCommand(DC_ReportDAO context, Long reportJobId) {
      this.context = context;
      this.reportJobId = reportJobId; 
    }
    
    public void execute() {
      this.result = context.reportService.getReportJob(this.reportJobId);
    }
  }

  public DC_ReportService.ReportJob getReportJob(Long reportJobId) {
    GetReportJobCommand cmd = new GetReportJobCommand(this, reportJobId);
    
    this.invokeAPI(cmd, true);
    
    return cmd.result;
  }

  private class GetReportDownloaURLCommand implements DC_DoubleClickDAO.Command {
    public String result;
    public Long reportJobId;
    public DC_ReportService.ReportDownloadOptions options;
    public DC_ReportDAO context;
    
    public GetReportDownloaURLCommand(DC_ReportDAO context, Long reportJobId, 
      DC_ReportService.ReportDownloadOptions options) {
      this.context = context;
      this.reportJobId = reportJobId;
      this.options = options;
    }
    
    public void execute() {
      this.result = context.reportService.getReportDownloadUrlWithOptions(this.reportJobId, 
        this.options);
    }
  }

  public String getReportDownloadUrl(Long reportJobId, 
    DC_ReportService.ReportDownloadOptions options) {
    
    GetReportDownloaURLCommand cmd = new GetReportDownloaURLCommand(this, reportJobId, options);
    
    this.invokeAPI(cmd, true);
    
    return cmd.result;
  }

}