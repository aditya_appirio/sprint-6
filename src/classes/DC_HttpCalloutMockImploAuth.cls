// 
// (c) 2014 Appirio
//
// DC_HttpCalloutMockImpl_oAuth : mock callout class for testing
//
//
@isTest
global class DC_HttpCalloutMockImploAuth implements HttpCalloutMock{
  global HTTPResponse respond(HTTPRequest req) {
    HttpResponse res = new HttpResponse();
    res.setHeader('Content-Type', 'application/json');
    res.setBody('{"access_token":"test", "access_token":"test","token_type":"test", "refresh_token":"test", "expires_in":3600}');
    res.setStatusCode(200);
    // return response.
    return res;
  }
}