// 
// (c) 2014 Appirio
//
// DC_OpportunityProposalLineItemConnector
// T-249484 : Create Proposal Lines classes
// 
// 13 Feb 2014    Ankit Goyal (JDC)      Copied from DC_OpportunityProposalConnector
//
public with sharing class DC_OpportunityProposalLineItemConnector extends DC_Connector {
  private static DC_OpportunityProposalLineItemConnector instance;
  
  //constructor
  private DC_OpportunityProposalLineItemConnector(){
    super();
  }
  public static DC_OpportunityProposalLineItemConnector getInstance() {
    if(instance == null) {
      instance = new DC_OpportunityProposalLineItemConnector();
    } 
    return instance;
  }
  
   // opportunity mapper instance
  private DC_ProposalLineItemMapper opportunityLIMapper {
    get {
      if(opportunityLIMapper == null) {
        opportunityLIMapper = DC_MapperFactory.getInstance().getopportunityLIMapper();
      }
      return opportunityLIMapper;
    }
    set;
  }
  
  // doubleClick Manager Instance
  public DC_DoubleClickProposalLineItemManager proposalLIManager {
    get {
      if(proposalLIManager == null) {
        proposalLIManager = managerFactory.getproposalLIManager();
      }
      return proposalLIManager;  
    }
    set;
  }
  
  // salesforce opportunity manager instance
  private DC_SalesforceOpportunityLineItemManager opportunityLIManager {
    get {
      if(opportunityLIManager == null) {
        opportunityLIManager = managerFactory.getSalesforceOpportunityLIManager();
      }
      return opportunityLIManager;
    }
    set; 
  }
  /*
  public void syncSFOppLI_ToDCProposalLIs(List<Id> oliIds) {
  	List<OpportunityLineItem> opLines = new List<OpportunityLineItem>(); 
  	for (Id oliId : oliIds)
      opLines.add(new OpportunityLineItem(id=oliId));
  	 
  	syncSFOppLI_ToDCProposalLIs(opLines);
  }
  
  public void syncExistingOpportunityLineItems(List<OpportunityLineItem> oppList) { 
    DC_GenericWrapper oppsToSync = opportunityLIMapper.wrap(oppList);

    if(oppsToSync.size() > 0) {
      oppsToSync = oppsToSync.getExistingRecords();

      if(oppsToSync.size() > 0) {
        syncSFOppLI_ToDCProposalLIs(oppsToSync); 
      }
    }
  }
    
  
  public void syncSFOppLI_ToDCProposalLIs(List<OpportunityLineItem> oppList) {
    DC_GenericWrapper connectorQueriedOpportunities = opportunityLIManager.getOpportunityLineItems(oppList);

    syncSFOppLI_ToDCProposalLIs(connectorQueriedOpportunities);
  }
  
  public void syncSFOppLI_ToDCProposalLIs(DC_GenericWrapper opps) {
    DC_OpportunityProposalConnector.connectorSyncUpdate = true;
  	if (opps == null) {
      System.debug('No opportunity lines to update.  Exiting...');
      return;
  	}
  	
    // Find which contacts are new records to be created in DC    
    DC_GenericWrapper oppsToCreate = opps.getNewRecords();
    
    // Find which contacts are new records to be created in DC
    DC_GenericWrapper oppsToUpdate = opps.getExistingRecords();
        
    // Retrieve existing contacts from DC
    DC_GenericWrapper existingDCProposals = proposalLIManager.getProposalLineItems(oppsToUpdate);

    // Create prepare new contacts 
    DC_GenericWrapper newDCProposals = opportunityLIMapper.createNewDCProposalLIs(oppsToCreate);
    System.debug('oppsToUpdate::'+oppsToUpdate.size());
    System.debug('existingDCProposals::'+existingDCProposals.size());
    
    System.debug('oppsToCreate::'+oppsToCreate);
    System.debug('newDCProposals::'+newDCProposals);
    // Update existing contacts 
    existingDCProposals.gotoBeforeFirst();
    oppsToUpdate.gotoBeforeFirst();
    
    if(oppsToUpdate.size() == existingDCProposals.size()){ //Added Anjali
	    while(existingDCProposals.hasNext()) {
	    	System.debug('oppsToUpdate.currentIndex:b4 increment:' + oppsToUpdate);
	    	System.debug('existingDCProposals.currentIndex:b4 increment:' + existingDCProposals);
	      existingDCProposals.next();
	      System.debug('existingDCProposals.currentIndex:after increment oppsToUpdate :' + oppsToUpdate);
	      System.debug('existingDCProposals.currentIndex:after increment existingDCProposals:' + existingDCProposals);
	      oppsToUpdate.next();
	      
	      
	      System.debug('oppsToUpdate.currentIndex:after increment oppsToUpdate: ' + oppsToUpdate);
	      System.debug('oppsToUpdate.currentIndex:after increment existingDCProposals:' + existingDCProposals);
	      //existingDCProposals.updateFields(oppsToUpdate);
	      opportunityLIMapper.updateFields(oppsToUpdate, existingDCProposals);
	    } 
	    proposalLIManager.updateProposalLineItems(existingDCProposals); 
    }
       

    // Create new contacts in DoubleClick
    newDCProposals = proposalLIManager.createProposalLineItems(newDCProposals);
    System.debug('newDCProposals:11:'+newDCProposals);
    // Update contacts with company id
    newDCProposals.gotoBeforeFirst();
    oppsToCreate.gotoBeforeFirst();
    
    System.debug('newDCProposals:22:'+newDCProposals.size());
    System.debug('oppsToCreate:22:'+oppsToCreate.size());
    if(oppsToCreate.size() == newDCProposals.size()){//Added Anjali
	    while(newDCProposals.hasNext()) {
	      newDCProposals.next();
	      System.debug('newDCProposals:33:'+newDCProposals);
	    System.debug('oppsToCreate:33:'+oppsToCreate);
	      oppsToCreate.next();
	      
	      //oppsToCreate.updateFields(newDCProposals);
	      opportunityLIMapper.updateFields(newDCProposals, oppsToCreate);
	    }
    }
    oppsToCreate.gotoBeforeFirst();
    newDCProposals.gotoBeforeFirst();
    if(oppsToCreate.size() == newDCProposals.size()){//Added Anjali
	    while(oppsToCreate.hasNext()) {
	      oppsToCreate.next();
	      newDCProposals.next();
	
	      opportunityLIMapper.linkOpportunityLIAndProposalLI(oppsToCreate, newDCProposals);
	    }

    opportunityLIManager.updateOpportunityLineItems(oppsToCreate);
    }
    this.executeAllDml();
  }
  
  // link the Salesforce Account and the DoubleClick Company
  // account  Account           Salesforce account to link
  // company  DC_GenericWrapper wrapper containing DoubleClick company to link against
  public void linkOpportunityLIAndProposalLI(OpportunityLineItem opp, DC_GenericWrapper dcProposal) {
    DC_GenericWrapper wrappedOpportunity = opportunityLIMapper.wrap(opp);
    
    wrappedOpportunity.next();
    opportunityLIMapper.linkOpportunityLIAndProposalLI(wrappedOpportunity, dcProposal);

    proposalLIManager.updateProposalLineItems(opportunityLIMapper.wrap((DC_ProposalLineItemService.ProposalLineItem)dcProposal.getCurrentObject()));
    opportunityLIManager.updateCurrent(wrappedOpportunity);
  }
  
  */
  public DC_DoubleClickPageProcessingResult syncProposalLineItemsByPage(DateTime basis, long pageSize, long offset) {
  	DC_DoubleClickPageProcessingResult result = new DC_DoubleClickPageProcessingResult();
    DC_GenericWrapper updatedProposalLines = proposalLIManager.getProposalLinesUpdatedSince(basis, pageSize, offset); 
    List<DC_GenericWrapper> archivedProposalLines = new List<DC_GenericWrapper>();
   
    
    DC_GenericWrapper proposalLinesToProcess = getExistingOpportunitiesProposalLI(updatedProposalLines);
    
    //Anjali on 25th March
    if(!Boolean.valueOf(goog_dclk_dsm__DC_ConnectorApplicationSettings__c.getInstance().goog_dclk_dsm__DC_DoNotDeleteArchivedLineItems__c)){
    	System.debug('@@ removing archived lines.');
    	archivedProposalLines = removeArchivedProposalLines(proposalLinesToProcess);
    	if(archivedProposalLines.size() > 0 && archivedProposalLines[0].size() > 0){
    		System.debug('@@ archived lines found: ' + archivedProposalLines[0]);
    		DC_GenericWrapper archivedProposalLineItems = opportunityLIManager.getOpportunityLineItems(archivedProposalLines[0]);
    		System.debug('@@ archived lines retrieved: ' + archivedProposalLineItems);
    		opportunityLIManager.deleteOpportunityLineItems(archivedProposalLineItems);
    	}
   		proposalLinesToProcess = archivedProposalLines[1];
    }
    
    // the following retrieves existing opportunity lines
    DC_GenericWrapper opportunityLinesToUpdate = opportunityLIManager.generateUpsertableRecords(proposalLinesToProcess);
    
    
    
    // the following updates the records retrieved, and creates for those that don't exist
    opportunityLIMapper.updateFieldsOnAllObjectsWithCreate(proposalLinesToProcess, opportunityLinesToUpdate);

    result.dmlStatus = opportunityLIManager.upsertOpportunityLineItems(opportunityLinesToUpdate);
    result.totalResultSetSize = updatedProposalLines.totalResultSetSize;
    
    return result; 
  }  
  
  // get only those Proposal line items whose proposal ids are connected to SFDC Oppties
  public DC_GenericWrapper getExistingOpportunitiesProposalLI(DC_GenericWrapper wrapper) {
  	Map<String, List<DC_ProposalLineItemService.ProposalLineItem>> proposalIdLIMap 
  					= opportunityLIMapper.getExistingOpportunitiesProposalLI(wrapper);
  	//if(proposalIdLIMap.size() > 0){
  		
  		return opportunityLIManager.fetchOpptiesExistInSFDC(proposalIdLIMap);
  	//}
  	//return null;
  } 
  /*
  // find companies by matching on name of account that is provided
  // account  Account  account whose name will be used to find a matching companys
  public DC_GenericWrapper findMatchingProposals(OpportunityLineItem opp) {
    //return proposalLIManager.getMatchingProposalLineItems(opp.Opportunity.goog_dclk_dsm__DC_AccountDoubleClickId__c, opp.Opportunity.Name);
    return null;
  }*/
  
  // take a body of wrappers, identify those that are archived
  public List<DC_GenericWrapper> removeArchivedProposalLines(DC_GenericWrapper wrapper) {
  	return opportunityLIMapper.removeArchivedProposalLines(wrapper);
  }
}