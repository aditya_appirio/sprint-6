// 
// (c) 2014 Appirio
//
// Class used as wrapper for DC Contact
//
// 17 Jan 2014   Ankit Goyal (JDC)      Original
//
public with sharing class DC_DoubleClickContactWrapper extends DC_DoubleClickWrapper{
	
	public DC_DoubleClickContactWrapper(goog_dclk_dsm__DC_ObjectMapping__c objectMapping, List<goog_dclk_dsm__DC_FieldMapping__c> fieldMappings, List<Object> objects) {
    super(objectMapping, fieldMappings, objects);
  }
  
  // Method to get current Contact
  private DC_ContactService.Contact getCurrentContact() {
    if(this.currentObject == null){
      throw new DC_ConnectorException(DC_Constants.ARR_OUT_OF_BOUND_ERR);
    } else if(!(this.currentObject instanceOf DC_ContactService.Contact)){
        throw new DC_ConnectorException('Invalid Mapping for Contact');
    } else{
        return (DC_ContactService.Contact) this.currentObject;
    }
  }
  
  // Overridden getter for Contact fields 
  public override Object getField(String field) {
    String fieldName = getDoubleClickFieldName(field.toLowerCase());
    //Doubt: externalId field not present in DC_ContactService.Contact Class. Wouldn't it be needed?
    if(fieldName.toLowerCase().equals('id')) {
      return getCurrentContact().id;
    } else if(fieldName.toLowerCase().equals('name')) {
        return getCurrentContact().name;
    } else if(fieldName.toLowerCase().equals('companyid')) {
        return getCurrentContact().companyId;
    } else if(fieldName.toLowerCase().equals('status')) {
        return getCurrentContact().status;
    } else if(fieldName.toLowerCase().equals('address')) {
        return getCurrentContact().address;
    } else if(fieldName.toLowerCase().equals('cellphone')) {
        return getCurrentContact().cellPhone;
    } else if(fieldName.toLowerCase().equals('comment')) {
        return getCurrentContact().comment;
    } else if(fieldName.toLowerCase().equals('email')) {
        return getCurrentContact().email;
    } else if(fieldName.toLowerCase().equals('faxphone')) {
        return getCurrentContact().faxPhone;
    } else if(fieldName.toLowerCase().equals('title')) {
        return getCurrentContact().title;
    } else if(fieldName.toLowerCase().equals('workphone')) {
        return getCurrentContact().workPhone;
    }
    else {
      throw new DC_ConnectorException(DC_Constants.getInvalid_Mapping_Contact(field));
    }
  }
  
  // Overridden setter for Contact fields 
  public override void setField(String field, Object value) {
    String fieldName = getDoubleClickFieldName(field);
    try{
      if(fieldName.equalsIgnoreCase('id')) {
      		getCurrentContact().id = this.toInteger(value);  
      } else if(fieldName.equalsIgnoreCase('name')) {
          getCurrentContact().name = this.toString(value);
      } else if(fieldName.equalsIgnoreCase('companyid')) {
          getCurrentContact().companyId = this.toLong(value);  
      } else if(fieldName.equalsIgnoreCase('address')) {
          getCurrentContact().address = this.toString(value); 
      } else if(fieldName.equalsIgnoreCase('status')) {
          getCurrentContact().status = this.toString(value); 
      } else if(fieldName.equalsIgnoreCase('faxphone')) {
          getCurrentContact().faxPhone = this.toString(value); 
      } else if(fieldName.equalsIgnoreCase('cellphone')) {
          getCurrentContact().cellPhone = this.toString(value); 
      } else if(fieldName.equalsIgnoreCase('comment')) {
          getCurrentContact().comment = this.toString(value); 
      } else if(fieldName.equalsIgnoreCase('email')) {
          getCurrentContact().email = this.toString(value); 
      } else if(fieldName.equalsIgnoreCase('title')) {
          getCurrentContact().title = this.toString(value); 
      } else if(fieldName.equalsIgnoreCase('workphone')) {
          getCurrentContact().workPhone = this.toString(value); 
      }
      else {
        throw new DC_ConnectorException(DC_Constants.getInvalid_Mapping_Contact(field));
      }
    }
    catch(System.TypeException e){
      throw new DC_ConnectorException(e+DC_Constants.FOR_FLD_STR+field);
    }
  }
  
  public override DC_GenericWrapper getNewRecords() {
    this.gotoBeforeFirst();
    List<Object> result = new List<Object>();
    
    while(this.hasNext()) {
      this.next();
      
      if(this.getField('DoubleClickId') == null) {
        result.add(this.currentObject);
      }
    } 
    
    return new DC_DoubleClickContactWrapper(this.objectMapping, this.fieldMappings, result);
  }
  
  public override DC_GenericWrapper getExistingRecords() {
    this.gotoBeforeFirst();
    List<Object> result = new List<Object>();
    
    while(this.hasNext()) {
      this.next();
      
      if(this.getField('DoubleClickId') != null) {
        result.add(this.currentObject);
      }
    }
    
    return new DC_DoubleClickContactWrapper(this.objectMapping, this.fieldMappings, result);
  } 
  
  public override DC_GenericWrapper getCurrentWrapped() {
    return new DC_DoubleClickContactWrapper(this.objectMapping, this.fieldMappings, new object[] { this.currentObject });
  }
  
  public override DC_GenericWrapper createWrapper(goog_dclk_dsm__DC_ObjectMapping__c objectMapping, List<goog_dclk_dsm__DC_FieldMapping__c> fieldMappings, List<Object> objects) {
    return new DC_DoubleClickContactWrapper(objectMapping, fieldMappings, objects);
  }
  
}