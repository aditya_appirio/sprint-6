/**
 */
@isTest
private class Test_DC_ReportService {

	static testMethod void testDCReportService() {
		Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplReportService());
    Test.startTest(); 
    DC_ReportService.CommonError ce = new DC_ReportService.CommonError();
    System.assert(ce != null);
    
    DC_ReportService.getReportDownloadUrlWithOptionsResponse_element v1 = new DC_ReportService.getReportDownloadUrlWithOptionsResponse_element();
    DC_ReportService.getReportDownloadUrlWithOptions_element wrp = new DC_ReportService.getReportDownloadUrlWithOptions_element();
    DC_ReportService.runReportJobResponse_element wfReq = new DC_ReportService.runReportJobResponse_element();
    DC_ReportService.getReportDownloadURLResponse_element v2 = new DC_ReportService.getReportDownloadURLResponse_element();
    DC_ReportService.DateTimeValue dtTmVal = new DC_ReportService.DateTimeValue();
    DC_ReportService.Statement statement = new DC_ReportService.Statement();
    DC_ReportService.PublisherQueryLanguageContextError pqlErr = new DC_ReportService.PublisherQueryLanguageContextError();
    DC_ReportService.getReportDownloadURL_element wfApprovalReq = new DC_ReportService.getReportDownloadURL_element();
    DC_ReportService.ApiError apiErr = new DC_ReportService.ApiError();
    DC_ReportService.Date_x dateX = new DC_ReportService.Date_x();
    DC_ReportService.ReportQuery rptQry = new DC_ReportService.ReportQuery();
    
    DC_ReportService.BooleanValue boolVal = new DC_ReportService.BooleanValue();
    DC_ReportService.getReportJob_element usrEle = new DC_ReportService.getReportJob_element();
    DC_ReportService.QuotaError ue = new DC_ReportService.QuotaError();
    DC_ReportService.FeatureError fe = new DC_ReportService.FeatureError();
    DC_ReportService.AuthenticationError ae = new DC_ReportService.AuthenticationError();
    DC_ReportService.PermissionError pe = new DC_ReportService.PermissionError();
    DC_ReportService.PublisherQueryLanguageSyntaxError PQLSyntaxErr = new DC_ReportService.PublisherQueryLanguageSyntaxError();
    
    DC_ReportService.ReportError rptErr = new DC_ReportService.ReportError();
    DC_ReportService.DateValue dtVal = new DC_ReportService.DateValue();
    DC_ReportService.ApplicationException appExcep = new DC_ReportService.ApplicationException();
    DC_ReportService.String_ValueMapEntry strValMapEntry = new DC_ReportService.String_ValueMapEntry();
    DC_ReportService.ReportDownloadOptions rptDownloadOpts = new DC_ReportService.ReportDownloadOptions();
    
    DC_ReportService.InternalApiError iae = new DC_ReportService.InternalApiError();
    DC_ReportService.Authentication au = new DC_ReportService.Authentication();
    DC_ReportService.SetValue sv = new DC_ReportService.SetValue();
    DC_ReportService.ReportJob rptJob = new DC_ReportService.ReportJob();
    DC_ReportService.runReportJob_element req = new DC_ReportService.runReportJob_element();
    DC_ReportService.NotNullError nne = new DC_ReportService.NotNullError();
    DC_ReportService.getReportJobResponse_element uur = new DC_ReportService.getReportJobResponse_element();
    DC_ReportService.NumberValue numVal = new DC_ReportService.NumberValue();
    DC_ReportService.DateTime_x dt = new DC_ReportService.DateTime_x();
    DC_ReportService.StatementError se = new DC_ReportService.StatementError();
    DC_ReportService.ApiException apiExce = new DC_ReportService.ApiException();
    DC_ReportService.Value val = new DC_ReportService.Value();
    DC_ReportService.ApiVersionError v4= new DC_ReportService.ApiVersionError();
    DC_ReportService.OAuth oAuth = new DC_ReportService.OAuth();
    DC_ReportService.SoapResponseHeader srh1 = new DC_ReportService.SoapResponseHeader();
    DC_ReportService.ParseError parseErr = new DC_ReportService.ParseError();
    DC_ReportService.RequiredError reqErr = new DC_ReportService.RequiredError();
    DC_ReportService.ReportServiceInterfacePort port = new DC_ReportService.ReportServiceInterfacePort();
      System.assert(port != null);
      port.getReportDownloadURL(12345 , 'csv');
      port.getReportJob(12345);
      port.getReportDownloadUrlWithOptions(12345 ,  rptDownloadOpts);
      port.runReportJob(rptJob);
    DC_ReportService.SoapRequestHeader srh = new DC_ReportService.SoapRequestHeader();
    DC_ReportService.ServerError serverErr = new DC_ReportService.ServerError();
    DC_ReportService.TextValue txtVal = new DC_ReportService.TextValue();
    Test.stopTest();
	}
}