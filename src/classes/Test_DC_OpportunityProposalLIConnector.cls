// 
// (c) 2014 Appirio
//
// Test_DC_OpportunityProposalLIConnector : test class for DC_OpportunityProposalLIConnector 
//
@isTest(seeAllData = true)
private class Test_DC_OpportunityProposalLIConnector {
  static integer no = 100;
  static testMethod void myUnitTest() {
        
        
    List<goog_dclk_dsm__DC_ProcessingQueue__c> queueItems = [SELECT id, goog_dclk_dsm__DC_Process__c, goog_dclk_dsm__DC_Executing__c, goog_dclk_dsm__DC_Asynchronous__c 
                                              FROM goog_dclk_dsm__DC_ProcessingQueue__c 
                                              WHERE goog_dclk_dsm__DC_Active__c = true AND goog_dclk_dsm__DC_Status__c = 'Queued' AND goog_dclk_dsm__DC_Process__c = 'Proposal Line Item Service'];
    if(queueItems == null || queueItems.size() == 0){
      DC_TestUtils.createDCProcessingQueue(false, true, false, 'Proposal Line Item Service', true);
    }                                        
                                              
    Account a1 = new Account(name = 'test1');
    a1.goog_dclk_dsm__DC_ReadyForDSM__c = true;
    insert a1;
    
    Opportunity oppty = new Opportunity(name = 'testProposal1', AccountId = a1.id, StageName = 'Closed/Won', CloseDate = date.today());
    insert oppty;
    
    Pricebook2 standardPB = [select id, isActive from Pricebook2 where isStandard=true limit 1];
    if (!standardPB.isActive) {
      standardPB.isActive = true;
      update standardPB;
    }
    
    Product2 prod = new Product2(Name = 'testProd', IsActive = true);
    insert prod;
    PricebookEntry pbe = new PricebookEntry(Pricebook2Id = standardPB.Id, Product2Id = prod.Id, UnitPrice = 10000, IsActive = true, UseStandardPrice = false);
    insert pbe;
    
    OpportunityLineItem oppLI1 = DC_TestUtils.createOpportunityLI(23, 10, oppty.Id,pbe.Id, false);
    OpportunityLineItem oppLI2 = DC_TestUtils.createOpportunityLI(25, 10, oppty.Id,pbe.Id, false);
    oppLI2.goog_dclk_dsm__DC_doubleClickId__c = '12345';
    OpportunityLineItem oppLI3 = DC_TestUtils.createOpportunityLI(27, 10, oppty.Id,pbe.Id, false);
    oppLI3.goog_dclk_dsm__DC_doubleClickId__c = '123456';
    OpportunityLineItem oppLI4 = DC_TestUtils.createOpportunityLI(29, 10, oppty.Id,pbe.Id, false);
    oppLI4.goog_dclk_dsm__DC_doubleClickId__c = '1234567';
    List<OpportunityLineItem> oppLiList = new List<OpportunityLineItem>();
    oppLiList.add(oppLI1);
    oppLiList.add(oppLI2);
    oppLiList.add(oppLI3);
    oppLiList.add(oppLI4);

    insert oppLiList;
    
    DC_ProposalLineItemService.Money m = new DC_ProposalLineItemService.Money();
    m.currencyCode = 'USD';
    m.microAmount = 123;
        
    DC_ProposalLineItemService.ProposalLineItem proposalLI1 = new DC_ProposalLineItemService.ProposalLineItem();
    proposalLI1.name = 'testProposalLI1';
    proposalLI1.proposalId = 12345;
    proposalLI1.id = 12345;
    proposalLI1.productId = String.valueOf(12345);
    proposalLI1.cost = m;
    proposalLI1.baseRate = m;
    
    goog_dclk_dsm__DC_ObjectMapping__c dcObjMapping1 = DC_TestUtils.createObjectMapping('Proposal', 'Opportunity', false);
    goog_dclk_dsm__DC_ObjectMapping__c dcObjMapping2 = DC_TestUtils.createObjectMapping('ProposalLineItem', 'OpportunityLineItem', false);
    insert new List<goog_dclk_dsm__DC_ObjectMapping__c>{dcObjMapping1, dcObjMapping2};
    
    goog_dclk_dsm__DC_FieldMapping__c dcFldMapping1 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId');
    goog_dclk_dsm__DC_FieldMapping__c dcFldMapping2 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'externalId', goog_dclk_dsm__DC_SalesforceFieldName__c = 'Id', Name = 'sfid');
    
    goog_dclk_dsm__DC_FieldMapping__c dcFldMapping3 = DC_TestUtils.createFieldMapping(dcObjMapping2.id,'id', 'goog_dclk_dsm__DC_DoubleClickId__c','doubleClickId' , false);
    goog_dclk_dsm__DC_FieldMapping__c dcFldMapping4 = DC_TestUtils.createFieldMapping(dcObjMapping2.id,'custom.SFDC_OpportunityLineItem_ID', 'Id', 'sfid' , false);
    
    dcFldMapping4.goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345;
    insert new List<goog_dclk_dsm__DC_FieldMapping__c>{dcFldMapping1, dcFldMapping2, dcFldMapping3, dcFldMapping4};

    no++;
    User usr = new User();
    usr.Username= 'testUser' + no + '@someplacesomewhere.com.12345';
    usr.Email = 'testuser' + no + '@someplacesomewhere.com.12345';
    usr.Lastname = 'user';
    usr.Firstname = 'test';
    usr.Alias = 't' + no + '45';
    usr.CommunityNickname = '12346';
    usr.ProfileId = [ select id from profile where name = 'System Administrator' ].id;
    usr.TimeZoneSidKey = 'GMT';
    usr.LocaleSidKey = 'en_US';
    usr.EmailEncodingKey = 'ISO-8859-1';
    usr.LanguageLocaleKey = 'en_US';
    usr.UserPermissionsMobileUser = false;
    insert usr;

     
    test.starttest();
    Test.setMock(HttpCalloutMock.class, new DC_HttpCalloutMockImploAuth());
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplProposal());
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplProductLineItem());
    DC_ProposalLineItemMapper mapper = DC_MapperFactory.getInstance().getopportunityLIMapper();
    DC_GenericWrapper gwObj = mapper.wrap(proposalLI1);
    gwObj.next();
    DC_OpportunityProposalLineItemConnector connector = DC_OpportunityProposalLineItemConnector.getInstance();
   // connector.findMatchingProposals(oppLI1);
    connector.getExistingOpportunitiesProposalLI(gwObj);
    //try{
      //connector.syncExistingOpportunityLineItems(new List<OpportunityLineItem>{oppLI1, oppLI2, oppLI3, oppLI4});
    //}catch(Exception e){}
    
    //try{
      //connector.syncSFOppLI_ToDCProposalLIs(new List<OpportunityLineItem>{oppLI1});
    //}catch(Exception e){}
    System.runAs(usr){
      DC_QueueDispatch q = new DC_QueueDispatch();
      q.execute(null);
    }
    //connector.linkOpportunityLIAndProposalLI(oppLI1, gwObj);
    Test.stopTest();
        
  }
    
}