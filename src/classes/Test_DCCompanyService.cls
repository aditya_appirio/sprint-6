// 
// (c) 2014 Appirio
//
// Test_DC_CompanyService : test class for DC_CompanyService 
//
// 24 Jan 2014    Anjali K (JDC)      Original
//
@isTest
private class Test_DCCompanyService {
 
  static testMethod void testDCCompanyService() {
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplCompany());
    Test.startTest(); 
    DC_CompanyService.CommonError ce = new DC_CompanyService.CommonError();
    System.assert(ce != null);
    
    DC_CompanyService.InternalApiError iae = new DC_CompanyService.InternalApiError();
    DC_CompanyService.Authentication au = new DC_CompanyService.Authentication();
    DC_CompanyService.SetValue sv = new DC_CompanyService.SetValue();
    DC_CompanyService.StringLengthError sle = new DC_CompanyService.StringLengthError();
    DC_CompanyService.CompanyServiceInterfacePort port = new DC_CompanyService.CompanyServiceInterfacePort();
    System.assert(port != null);
    
    //Methods of CompanyServiceInterfacePort
    DC_CompanyService.Company[] companyArr = port.createCompanies(new List<DC_CompanyService.Company>{new DC_CompanyService.Company()});
    //DC_CompanyService.Company comp = port.createCompany(new DC_CompanyService.Company());
    DC_CompanyService.CompanyPage compPageVar = port.getCompaniesByStatement(new DC_CompanyService.Statement());
    //DC_CompanyService.Company getcomp = port.getCompany(Long.valueOf('12345'));
    DC_CompanyService.Company[] compArr1 = port.updateCompanies(new List<DC_CompanyService.Company>{new DC_CompanyService.Company()});
    //comp = port.updateCompany(new DC_CompanyService.Company());
    
    DC_CompanyService.BooleanValue boolVal = new DC_CompanyService.BooleanValue();
    DC_CompanyService.AppliedLabel al = new DC_CompanyService.AppliedLabel();
    DC_CompanyService.updateCompanyResponse_element updateCompRes = new DC_CompanyService.updateCompanyResponse_element();
    DC_CompanyService.UniqueError ue = new DC_CompanyService.UniqueError();
    DC_CompanyService.RequiredError re = new DC_CompanyService.RequiredError();
    DC_CompanyService.CompanyError compErr = new DC_CompanyService.CompanyError();
    DC_CompanyService.FeatureError fe = new DC_CompanyService.FeatureError();
    DC_CompanyService.AuthenticationError ae = new DC_CompanyService.AuthenticationError();
    DC_CompanyService.PermissionError pe = new DC_CompanyService.PermissionError();
    DC_CompanyService.PublisherQueryLanguageSyntaxError PQLSyntaxErr = new DC_CompanyService.PublisherQueryLanguageSyntaxError();
    DC_CompanyService.getCompany_element getcompEle = new DC_CompanyService.getCompany_element();
    DC_CompanyService.String_ValueMapEntry valMapEntry = new DC_CompanyService.String_ValueMapEntry();
    DC_CompanyService.Value val = new DC_CompanyService.Value(); 
    DC_CompanyService.getCompaniesByStatementResponse_element compStatementResp = new DC_CompanyService.getCompaniesByStatementResponse_element();
    DC_CompanyService.OAuth oAuth = new DC_CompanyService.OAuth();
    DC_CompanyService.createCompany_element compElement = new DC_CompanyService.createCompany_element();
    DC_CompanyService.CompanyPage compPage = new DC_CompanyService.CompanyPage();
    DC_CompanyService.updateCompaniesResponse_element compResEle = new DC_CompanyService.updateCompaniesResponse_element();
    DC_CompanyService.TypeError typeErr = new DC_CompanyService.TypeError();
    DC_CompanyService.NumberValue numVal = new DC_CompanyService.NumberValue();
    DC_CompanyService.SoapResponseHeader soapRespHeader = new DC_CompanyService.SoapResponseHeader();
    DC_CompanyService.NotNullError nne = new DC_CompanyService.NotNullError();
    DC_CompanyService.createCompanyResponse_element createCompResEle = new DC_CompanyService.createCompanyResponse_element();
    DC_CompanyService.DateTimeValue dtTimeVal = new DC_CompanyService.DateTimeValue();
    DC_CompanyService.CompanyCreditStatusError compCrediStatusErr = new DC_CompanyService.CompanyCreditStatusError();
    DC_CompanyService.Statement statement = new DC_CompanyService.Statement();
    DC_CompanyService.PublisherQueryLanguageContextError PQLContentErr = new DC_CompanyService.PublisherQueryLanguageContextError();
    DC_CompanyService.ApiError apiErr = new DC_CompanyService.ApiError();
    DC_CompanyService.Date_x serviceDate = new DC_CompanyService.Date_x();

    serviceDate.setYear(2014);
    serviceDate.setMonth(2);
    serviceDate.setDay(14);
    
    System.assertEquals(14, serviceDate.getDay());
    System.assertEquals(2, serviceDate.getMonth());
    System.assertEquals(2014, serviceDate.getYear());
    
    serviceDate.setSalesforceDate(Date.today());    
    System.assertEquals(Date.today(), serviceDate.getSalesforceDate());
      
    DC_CompanyService.SoapRequestHeader soapReqHeader = new DC_CompanyService.SoapRequestHeader();
    DC_CompanyService.TeamError teamErr = new DC_CompanyService.TeamError();
    DC_CompanyService.ApiException apiExce = new DC_CompanyService.ApiException();
    DC_CompanyService.Company compV = new DC_CompanyService.Company();
    DC_CompanyService.QuotaError quotaErr = new DC_CompanyService.QuotaError();
    DC_CompanyService.DateValue dtVal = new DC_CompanyService.DateValue();
    DC_CompanyService.ApplicationException appExcep = new DC_CompanyService.ApplicationException();
    DC_CompanyService.ApiVersionError apiVersionErr = new DC_CompanyService.ApiVersionError();
    DC_CompanyService.ParseError parseErr = new DC_CompanyService.ParseError();
    DC_CompanyService.LabelEntityAssociationError lblEntityAssErr = new DC_CompanyService.LabelEntityAssociationError();
    DC_CompanyService.ServerError serverErr = new DC_CompanyService.ServerError();
    DC_CompanyService.DateTime_x datetimeX = new DC_CompanyService.DateTime_x();
    
    datetimeX.setDate(serviceDate);
    datetimeX.getDate();
    datetimeX.setHour(10);
    datetimeX.setMinute(10);
    datetimeX.setSecond(10);
    System.assertEquals(10, datetimeX.getHour());
    System.assertEquals(10, datetimeX.getMinute());
    System.assertEquals(10, datetimeX.getSecond());
    datetimeX.setSalesforceDatetime(Datetime.now());
    datetimeX.getSalesforceDatetime();
      
    DC_CompanyService.StatementError statementErr = new DC_CompanyService.StatementError();
    DC_CompanyService.TextValue textVal = new DC_CompanyService.TextValue();
    DC_CompanyService.InvalidEmailError invalidEmailErr = new DC_CompanyService.InvalidEmailError();
    Test.stopTest();
  }
}