//
//
//  Test class for Test_DC_BatchRecordProcessorController
//  also tests the related batches
//
//
@isTest
private class Test_DC_BatchRecordProcessorController {
	
	@isTest static void testAccountBatchInstantiation() {
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplCompany());
    Test.startTest();    
    DC_BatchRecordProcessorController c = new DC_BatchRecordProcessorController();
    createMappings();
    c.startAccountBatch();
    system.assert(([SELECT Id FROM AsyncApexJob]).size() > 0);
    Test.stopTest();
	}
	
  @isTest static void testContactBatchInstantiation() {
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplContact());
    Test.startTest();
    DC_BatchRecordProcessorController c = new DC_BatchRecordProcessorController();
    createMappings();
    c.startContactBatch();
    system.assert(([SELECT Id FROM AsyncApexJob]).size() > 0);
    Test.stopTest();
	}
	
  @isTest static void testContactUserInstantiation() {
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplUserService());
    Test.startTest();
    DC_BatchRecordProcessorController c = new DC_BatchRecordProcessorController();

    createMappings();
    c.startUserBatch();
    system.assert(([SELECT Id FROM AsyncApexJob]).size() > 0);
    Test.stopTest();
    
  }  

  
  private static void createMappings() {
    goog_dclk_dsm__DC_ObjectMapping__c accountMapping = DC_TestUtils.createObjectMapping('Account', 'Account', 'Company', false);
    goog_dclk_dsm__DC_ObjectMapping__c contactMapping = DC_TestUtils.createObjectMapping('Contact', 'Contact', 'Contact', false);
    goog_dclk_dsm__DC_ObjectMapping__c userMapping = DC_TestUtils.createObjectMapping('User Batch', 'User', 'User', false);
    insert new List<goog_dclk_dsm__DC_ObjectMapping__c>{accountMapping, contactMapping, userMapping};

    goog_dclk_dsm__DC_FieldMapping__c fldMapping1 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = accountMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId', goog_dclk_dsm__DC_SyncDirection__c = 'Not Integrated', goog_dclk_dsm__DC_Enabled__c = true);
    goog_dclk_dsm__DC_FieldMapping__c fldMapping2 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = contactMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Id', Name = 'sfid', goog_dclk_dsm__DC_SyncDirection__c = 'Not Integrated', goog_dclk_dsm__DC_Enabled__c = true);
    goog_dclk_dsm__DC_FieldMapping__c fldMapping3 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = userMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'id', Name = 'Id', goog_dclk_dsm__DC_SyncDirection__c = 'Not Integrated', goog_dclk_dsm__DC_Enabled__c = true);

    Account a = new Account(name = 'Test Account', goog_dclk_dsm__DC_ReadyForDSM__c = true);
    insert a;
    goog_dclk_dsm__DC_Team__c team = new goog_dclk_dsm__DC_Team__c(name='Test Team', goog_dclk_dsm__DC_DoubleClickId__c='12345'); 
    insert team;
    
    goog_dclk_dsm__DC_DoubleClickAccountTeam__c dcat = new goog_dclk_dsm__DC_DoubleClickAccountTeam__c(goog_dclk_dsm__DC_Account__c = a.id, goog_dclk_dsm__DC_Team__c = team.id); 
    insert dcat;
    
    
    Contact c = new Contact(accountid = a.id, lastname = 'Test Contact', goog_dclk_dsm__DC_ReadyForDSM__c = true);
    insert c;
    Profile sysAdminProfile = [Select Id From Profile Where Name ='System Administrator'];
 
    insert new User(FirstName='test first', LastName='test last', Alias='Mr.', Email='test@gmail.com', 
                              UserName='testuser@gmail.com.testClass', ProfileId =sysAdminProfile.Id, 
                              TimeZoneSidKey='Asia/Kolkata', LocaleSidKey='en_US', LanguageLocaleKey='en_US', EmailEncodingKey='ISO-8859-1',
                              goog_dclk_dsm__DC_IncludeInBatchToDSM__c = true, goog_dclk_dsm__DC_InitialBatchLoadRoleId__c = '-1');

    insert new List<goog_dclk_dsm__DC_FieldMapping__c>{fldMapping1, fldMapping2, fldMapping3};
    
  }  
}