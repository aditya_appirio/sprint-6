// 
// (c) 2014 Appirio
//
// Class used for various utilities in goog_dclk_dsm__DC_ConnectorLog__c object (T-238924)
//
// 17 Jan 2014   Ankit Goyal (JDC)      Original
//
public with sharing class DC_ConnectorLogger {

  public static goog_dclk_dsm__DC_ConnectorLog__c log(String context, String entry, Status logStatus) {
    return log(context, entry, logStatus, '', null, new String[]{}, false);
  }
  
  public static goog_dclk_dsm__DC_ConnectorLog__c log(String context, String entry, Status logStatus, Id queueId) {
    return log(context, entry, logStatus, '', queueId, new String[]{}, false);
  }
  
  public static goog_dclk_dsm__DC_ConnectorLog__c log(String context, String entry, Status logStatus, String details, Id queueId) {
    return log(context, entry, logStatus, details, queueId, new String[]{}, false);
  }
  
  public static goog_dclk_dsm__DC_ConnectorLog__c log(String context, String entry, Status logStatus, String details, Id queueId, List<sObject> affectedRecords, boolean immediateDML) {
    return log(context, entry, logStatus, details, queueId, getListOfIds(affectedRecords), immediateDML);
  }
  
  public static goog_dclk_dsm__DC_ConnectorLog__c log(String context, String entry, Status logStatus, String details, Id queueId, List<id> affectedRecordIds, boolean immediateDML) {
    goog_dclk_dsm__DC_ConnectorLog__c logObj = new goog_dclk_dsm__DC_ConnectorLog__c();
    logObj.goog_dclk_dsm__DC_Context__c = context;
    logObj.goog_dclk_dsm__DC_Entry__c = truncate(entry, 255);
    logObj.goog_dclk_dsm__DC_Status__c = logStatus.name();
    logObj.goog_dclk_dsm__DC_Details__c = details;
    logObj.goog_dclk_dsm__DC_AffectedRecordIds__c = getCommaSeparatedIds(affectedRecordIds);
    logObj.goog_dclk_dsm__DC_ProcessingQueue__c = queueId;
    
    if (immediateDML) {
      try {
        insert logObj;
      } catch (Exception e) {} 
    } else {
      DC_SalesforceDML.getInstance().registerInsert(logObj);
    }
    
    return logObj;
  }  
  
  
  public static goog_dclk_dsm__DC_ConnectorLog__c log(String context, Exception e) {
    if (e instanceOf DC_SalesforceDML.DC_SalesforceDMLException && ((DC_SalesforceDML.DC_SalesforceDMLException)e).errorCommands != null) {
      return logDmlCommandErrors(context, null, (DC_SalesforceDML.DC_SalesforceDMLException)e);
    } else {
      return log(context, e, null, null, true);
    }
  }
  
  public static goog_dclk_dsm__DC_ConnectorLog__c logDmlCommandErrors(String context, Id queueId, DC_SalesforceDML.DC_SalesforceDMLException e) {
    List<DC_SalesforceDML.DC_SalesforceDMLCommand> cmds = e.errorCommands;
    System.debug('Count of error commands: ' + cmds.size());
    object result;

    for (DC_SalesforceDML.DC_SalesforceDMLCommand cmd : cmds) { 
      if (cmd.errorRecordIndex == null || cmd.errorRecordIndex.size() == 0){
        log( context, e, queueId, new List<sObject>(), true);
      } else {
        for (Integer i : cmd.errorRecordIndex) {
          result = cmd.getResults()[i];             
          log(context, 'DML Exception', Status.Error, getDMLErrorString(result), queueId, new Id[] {cmd.getRecords()[i].Id}, true);
        }
      }       
    }
 
    return null;
  }
  
  public static string getDMLErrorString(object dmlResult) {
    Database.Error[] errors = 
       (dmlResult instanceof Database.saveResult) ? ((Database.Saveresult)dmlResult).errors : 
       ((dmlResult instanceof Database.upsertResult) ? ((Database.upsertResult)dmlResult).errors : ((Database.Deleteresult)dmlResult).errors);
    String returnString = '';
    
    for (Database.Error er : errors) {
      returnString += (returnString != '') ? '\n' : '';
      returnString += String.format('{0} \nField(s): {1} \n {2} \n', new String[] { er.StatusCode.name(), String.join(er.fields, ', '), er.message });
    }
    
    return returnString;
  }

  public static goog_dclk_dsm__DC_ConnectorLog__c log(String context, Exception e, Id queueId) {
    if (e instanceOf DC_SalesforceDML.DC_SalesforceDMLException) {
      return logDmlCommandErrors(context, queueId, (DC_SalesforceDML.DC_SalesforceDMLException)e);
    } else {
      return log(context, e, queueId, null, true);
    }   

  }
  
  public static goog_dclk_dsm__DC_ConnectorLog__c log(String context, Exception e, List<sObject> affectedRecords) {
    return log(context, e, null, affectedRecords, true);
  }  
  
  public static goog_dclk_dsm__DC_ConnectorLog__c log(String context, String details, String status, String errRecIds) {
  	goog_dclk_dsm__DC_ConnectorLog__c logObj = new goog_dclk_dsm__DC_ConnectorLog__c();
    logObj.goog_dclk_dsm__DC_Context__c = context;
    logObj.goog_dclk_dsm__DC_Details__c = details;
    logObj.goog_dclk_dsm__DC_Status__c = status;
    logObj.goog_dclk_dsm__DC_AffectedRecordIds__c = errRecIds;
    insert logObj;
    return logObj;
  } 
  
  public static goog_dclk_dsm__DC_ConnectorLog__c log(String context, Exception e, Id queueId, List<sObject> affectedRecords, boolean immediateDML) {

    goog_dclk_dsm__DC_ConnectorLog__c logObj = new goog_dclk_dsm__DC_ConnectorLog__c();
    logObj.goog_dclk_dsm__DC_Context__c = context;
    logObj.goog_dclk_dsm__DC_Entry__c = e.getTypeName();
    logObj.goog_dclk_dsm__DC_Details__c = String.format('{0}: {1}\nStack Trace Follows:\n{2}', new String[] { e.getTypeName(), e.getMessage(), e.getStackTraceString() });
    logObj.goog_dclk_dsm__DC_ProcessingQueue__c = queueId;
    logObj.goog_dclk_dsm__DC_AffectedRecordIds__c = getCommaSeparatedIds(affectedRecords);
    
    if (immediateDML) {
      try {
        insert logObj;
      } catch (Exception e2) {}
    } else {
      DC_SalesforceDML.getInstance().registerInsert(logObj);
    }
    
    return logObj;
  }    
  
  private static String truncate(String s, integer l) {
    return s.substring(0, Math.min(s.length(), l));
  }
  
  private static String getCommaSeparatedIds(List<sObject> affectedRecords) {
    if (affectedRecords == null) {
      return '';
    }
      
    return getCommaSeparatedIds(getListOfIds(affectedRecords));
  }
  
  // Method for getting comma saprated Ids from an array of Id
  private static String getCommaSeparatedIds(List<id> affectedRecordIds){
    if (affectedRecordIds == null || affectedRecordIds.size() == 0)
      return '';
      
    return String.join(affectedRecordIds, ', ');
  }
  // Method for getting comma saprated Ids from an array of Id
  private static List<Id> getListOfIds(sObject[] affectedRecords){
    List<Id> recordIds=new List<Id>();
    if(affectedRecords !=null){
        for(sObject record : affectedRecords){
          recordIds.add(record.Id);
      }
    }
    return recordIds;
  }
 
  public enum Status {
    Success,
    Error,
    Conflict,
    Info
  }

}