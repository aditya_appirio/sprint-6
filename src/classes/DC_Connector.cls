// 
// (c) 2014 Appirio
//
// DC Connector
//
// 20 Jan 2014    Anjali K (JDC)      Original
//
public virtual with sharing class DC_Connector { 
  
  protected DC_ManagerFactory managerFactory;
  
  //contructor
  public DC_Connector(){
    managerFactory = DC_ManagerFactory.getInstance(); 
  }
 
  // execute all remaining DML
  public void executeAllDml() {
    DC_SalesforceDML.getInstance().flush();
  } 
  
  private DC_ObjectMapper dcMapper;
  private DC_SalesforceManager sfManager;
  private DC_DoubleClickManager dcManager;
  
  private DC_ObjectMapper get_dcMapper(String context){
    if (context == DC_Constants.ACCOUNT_CONTEXT){
      return DC_MapperFactory.getInstance().getAccountMapper();
    }
    else if (context == DC_Constants.CONTACT_CONTEXT){
      return DC_MapperFactory.getInstance().getContactMapper();
    }
    else if (context == DC_Constants.USER_CONTEXT){
      return DC_MapperFactory.getInstance().getUserMapper();
    }
    return null;
  } 
  
  private DC_DoubleClickManager get_dcManager(String context){
    if (context == DC_Constants.ACCOUNT_CONTEXT){
      return managerFactory.getCompanyManager();
    }
    else if (context == DC_Constants.CONTACT_CONTEXT){
      return managerFactory.getContactManager();
    }
    else if (context == DC_Constants.USER_CONTEXT){
      return managerFactory.getUserManager();
    }
    return null;
  }
  
  private DC_SalesforceManager get_sfManager(String context){
    if (context == DC_Constants.ACCOUNT_CONTEXT){
      return managerFactory.getSalesforceAccountManager();
    }
    else if (context == DC_Constants.CONTACT_CONTEXT){
      return managerFactory.getSalesforceContactManager();
    }else if (context == DC_Constants.USER_CONTEXT){
      return managerFactory.getSalesforceUserManager();
    }
    return null;
  }
  // Creates non-existent accounts as companies in DSM, otherwise updates existing
  // accounts DC_GenericWapper  wrapper with the Salesforce accounts to provide
  public DC_GenericWrapper syncSFRecordsToDCRecords(DC_GenericWrapper accounts, String context) {
    if (accounts == null)
      return null; 

    dcManager = get_dcManager(context);
    dcMapper  = get_dcMapper(context);
    sfManager = get_sfManager(context);
    
    // Find which accounts are new records to be created in DC
    DC_GenericWrapper recordsToCreate = accounts.getNewRecords();
    // Find which accounts already exist in DC
    DC_GenericWrapper recordsToUpdate = accounts.getExistingRecords();
    // Retrieve existing companies from DC
    System.debug('recordsToUpdate:::' + recordsToUpdate + 'recordsToCreate::' + recordsToCreate + 'dcMapper::' + dcMapper);
    DC_GenericWrapper existingRecords = dcManager.getDCRecords(recordsToUpdate);

    // Create prepare new companies
    DC_GenericWrapper newRecords;
    System.debug('recordsToCreate.size()::::' + recordsToCreate.size());
    if(recordsToCreate.size() > 0){
       newRecords = dcMapper.createNewDCRecords(recordsToCreate);
    }
    System.debug('newRecords::::' + newRecords);
    if (existingRecords.size() > 0) {
      // Update existing companies
      existingRecords.gotoBeforeFirst();
      recordsToUpdate.gotoBeforeFirst();
      while(existingRecords.hasNext()) {
        existingRecords.next();
        recordsToUpdate.next();
        
        dcMapper.updateFields(recordsToUpdate, existingRecords);
      } 
  
      dcManager.updateDCRecords(existingRecords);    
    }

    // Create new companies in DoubleClick
    if(newRecords != null && newRecords.size() > 0){
      newRecords = dcManager.createDCRecords(newRecords);
      // Update accounts with company id
      newRecords.gotoBeforeFirst();
      recordsToCreate.gotoBeforeFirst();
      while(newRecords.hasNext()) {
        newRecords.next();
        recordsToCreate.next();
        
        dcMapper.updateFields(newRecords, recordsToCreate);
      } 
      dcMapper.linkSFRecordsAndDCRecords(recordsToCreate, newRecords);
  
      sfManager.updateSFRecords(recordsToCreate);
    }
    //Added on 26th March by Anjali
    executeAllDml();
    return newRecords;
  }
  
  public class DC_DoubleClickPageProcessingResult {
  	public DC_SalesforceDML.DC_SalesforceDMLStatus dmlStatus {get;set;}
  	public long totalResultSetSize {get;set;}
  }
}