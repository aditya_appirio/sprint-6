// 
// (c) 2014 Appirio
//
// Test_DC_DoubleClickProposalLIWrapper: test class for DC_DoubleClickProposalLIWrapper 
//
@isTest
private class Test_DC_DoubleClickProposalLIWrapper {
  
  static testMethod void myUnitTest() {
    DC_ProposalLineItemService.Money m = new DC_ProposalLineItemService.Money();
    DC_ProposalLineItemService.Goal g = new DC_ProposalLineItemService.Goal();
    m.currencyCode = 'USD';
    m.microAmount = 123;
    g.units = 1;
    
    DC_ProposalLineItemService.ProposalLineItem proposalLI1 = new DC_ProposalLineItemService.ProposalLineItem();
    proposalLI1.name = 'testProposalLI1';
    proposalLI1.proposalId = 12345;
    proposalLI1.id = 12345;
    proposalLI1.productId = String.valueOf(12345);
    proposalLI1.cost = m;
    proposalLI1.baseRate = m;
    proposalLI1.goal = g;
    
    goog_dclk_dsm__DC_ObjectMapping__c objMapping1 = new goog_dclk_dsm__DC_ObjectMapping__c(goog_dclk_dsm__DC_DoubleClickObjectName__c='PROPOSAL_LINE_ITEM', goog_dclk_dsm__DC_SalesforceObjectName__c = 'OpportunityLineItem', name = 'OpportunityLineItem');
    insert objMapping1;
    
    goog_dclk_dsm__DC_FieldMapping__c fldMapping1 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping2 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'custom.SFDC_OpportunityLineItem_ID', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Id', Name = 'sfid');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping3 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'proposalId', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'OpportunityId', Name = 'ProposalId');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping4 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'probabilityToClose', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'probability', Name = 'Probability');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping5 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'name', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_ProposalLineName__c', Name = 'name');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping6 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'cost', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_CostBase__c', Name = 'cost');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping7 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'notes', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_AdNameComments__c', Name = 'notes');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping8 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'productId', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DSMProductId__c', Name = 'productId');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping9 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'baseRate', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_UnitPrice__c', Name = 'baseRate');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping10 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'dfpLineItemId', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_LineItemId__c', Name = 'dfpLineItemId');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping11 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'creativePlaceHolders', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_AdSize__c', Name = 'creativePlaceHolders');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping12 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'custom_test', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_AdSize__c', Name = 'custom_test');
    List<goog_dclk_dsm__DC_FieldMapping__c> fldMappingList = new List<goog_dclk_dsm__DC_FieldMapping__c>{fldMapping1, fldMapping2, fldMapping3, fldMapping4,fldMapping5,fldMapping6,fldMapping7,fldMapping8,fldMapping9,fldMapping10,fldMapping11,fldMapping12};
    insert fldMappingList;
    
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new DC_HttpCalloutMockImploAuth());
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplProductLineItem());
    DC_DoubleClickProposalLineItemWrapper wrapper = new DC_DoubleClickProposalLineItemWrapper(objMapping1, fldMappingList, new List<DC_ProposalLineItemService.ProposalLineItem>{proposalLI1});
    try{
        wrapper.getField('cost');
    }catch(Exception e){}
    wrapper.next();
    System.assertEquals(0, wrapper.getField('cost'));
    System.assertEquals(null, wrapper.getField('notes'));
    System.assertEquals('12345', wrapper.getField('productId'));
    System.assertEquals(0, wrapper.getField('baseRate'));
    System.assertEquals(null, wrapper.getField('dfpLineItemId'));
    System.assertEquals(null, wrapper.getField('creativePlaceHolders'));
    System.assertEquals(null, wrapper.getField('custom_test'));
    
    wrapper.setField('cost', 123);
    wrapper.setField('DoubleClickId', 12345);
    wrapper.setField('proposalId', 12345);
    wrapper.setField('name', 'test');
    wrapper.setField('notes', 'test');
    wrapper.setField('productId', '000000000000000');
    wrapper.setField('baseRate', 123 );
    wrapper.setField('dfpLineItemId', '000000000000000');
    wrapper.setField('creativePlaceHolders', new List<DC_ProposalLineItemService.CreativePlaceholder>());
    wrapper.setField('custom_test', 'customTest');
    try{
      wrapper.getField('exception');
    }catch(Exception e){}
    
    DC_GenericWrapper existingWrapper =  wrapper.getExistingRecords();
    DC_GenericWrapper newWrapper =  wrapper.getNewRecords();
    DC_GenericWrapper currWrapper =  wrapper.getCurrentWrapped();
    
    //System.assert(existingWrapper.size() > 0);
    
    //List<DC_ProposalLineItemService.ProposalLineItem> pliList = (List<DC_ProposalLineItemService.ProposalLineItem>)existingWrapper.objects;
    //System.assertEquals(pliList[0].name, 'test');
    
    wrapper.createWrapper(objMapping1, fldMappingList, new List<DC_ProposalLineItemService.ProposalLineItem>{proposalLI1});
    
    wrapper = new DC_DoubleClickProposalLineItemWrapper(objMapping1, fldMappingList, fldMappingList);
    try{
      wrapper.getField('cost');
    }catch(Exception e){}
        
    Test.stopTest();
  }
}