@isTest
private class Test_DC_SalesforceTeamManager {
	
  // test the upsert of team records
	@isTest static void testUpdateTeams() {
    goog_dclk_dsm__DC_ObjectMapping__c objMapping = DC_TestUtils.createObjectMapping('Team', 'Team', 'goog_dclk_dsm__DC_Team__c', false);
    insert objMapping;
    
    insert new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId', goog_dclk_dsm__DC_Enabled__c  = true);

    DC_TeamMapper teamMapper = DC_MapperFactory.getInstance().getTeamMapper();
    DC_SalesforceTeamManager teamManager = new DC_SalesforceTeamManager();

    goog_dclk_dsm__DC_Team__c team1 = new goog_dclk_dsm__DC_Team__c(Name = 'Test Team 1', goog_dclk_dsm__DC_DoubleClickId__c = '99744');
    insert team1;   
    goog_dclk_dsm__DC_Team__c team2 = new goog_dclk_dsm__DC_Team__c(Name = 'Test Team 2', goog_dclk_dsm__DC_DoubleClickId__c = '99745');

    team1.Name = 'Test Team 3';

    // expected behavior is update of outstanding update, as well as insertion of non-existing record
    teamManager.upsertTeams(
        teamMapper.wrap(new List<goog_dclk_dsm__DC_Team__c> {team1, team2})
      );

    DC_SalesforceDML.getInstance().flush();

    Map<Id, goog_dclk_dsm__DC_Team__c> teams = new Map<Id, goog_dclk_dsm__DC_Team__c>([SELECT id, name, goog_dclk_dsm__DC_DoubleClickId__c FROM goog_dclk_dsm__DC_Team__c]);
    System.assertEquals(2, teams.size());
    System.assertEquals('Test Team 3', teams.get(team1.Id).Name);
	}
	
  // test the matching of the salesforce record from a doubleclick record
	@isTest static void testGetMatching() {
    goog_dclk_dsm__DC_ObjectMapping__c objMapping = DC_TestUtils.createObjectMapping('Team', 'Team', 'goog_dclk_dsm__DC_Team__c', false);
    insert objMapping;
    
    insert new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId', goog_dclk_dsm__DC_Enabled__c  = true);
    insert new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'Name', goog_dclk_dsm__DC_SalesforceFieldName__c = 'Name', Name = 'Name', goog_dclk_dsm__DC_Enabled__c  = true, goog_dclk_dsm__DC_SyncDirection__c = 'To Salesforce Only');

    goog_dclk_dsm__DC_Team__c team1 = new goog_dclk_dsm__DC_Team__c(Name = 'Test Team 1', goog_dclk_dsm__DC_DoubleClickId__c = '99744');
    insert team1;   

    DC_TeamService.Team dcteam = new DC_TeamService.Team();
    dcteam.id = 99744;

    DC_TeamMapper teamMapper = DC_MapperFactory.getInstance().getTeamMapper();
    DC_SalesforceTeamManager teamManager = new DC_SalesforceTeamManager();

    DC_GenericWrapper wrapper = teamManager.getMatchingSalesforceRecords(teamMapper.wrap(dcteam));

    System.assertEquals(1, wrapper.size());
    wrapper.gotoBeforeFirst();
    wrapper.next();
    System.assertEquals('Test Team 1', (String)wrapper.getField('Name'));
	}
	
}