// 
// (c) 2014 Appirio
//
// Controller for link action list component
// 
// 20140206    Brandon Raines      Original
//
public with sharing class DC_LinkRecordListController {
	// api name of the configuration field that provides the field name 
	// to retrieve from the wrapper's record
  private final string CONFIG_ITEM_FIELD_FIELD_NAME = 'name';

  // generic wrapper with records to render
  private DC_GenericWrapper lWrapper;
  
  // local config context 
  private String lConfigContext;
  
  // records containing the field configuration
  public List<sObject> fieldConfig { get; set; }
  
  // property for the wrapper
  public DC_GenericWrapper wrapper { 
  	get {
  		return lWrapper;
  	}
  	set {
  		lWrapper = value;
  		
  		// if we have the config context as well, update the 
  		// list of value maps for rendering
      if (lConfigContext != null)
        updateLinkActionWrapperList();  		
  	}
  }
  
  // config context [custom setting] with the fields to 
  // be rendered
  public String configContext { 
  	get {
  		return lConfigContext;
  	} 
  	set {
  		lConfigContext = value;

      // if we have the wrapper, update the 
      // list of value maps for rendering
  		if (lWrapper != null)
  		  updateLinkActionWrapperList();
  	}
  }
  
  // url to use for deep linking
  public String dcUrlStub { get; set; }
  
  // link action handler provides the "createLink(integer)" we'll use to 
  // link the selected record 
  public DC_ListLinkActionHandler linkActionHandler { get; set; }
  
  // list of link action wrappers (inner class below) -- each item is a record
  // rendered on the page 
  public List<LinkActionWrapper> linkActionWrapperList { get; set; }
  
  // update the linkactionwrapperlist with new link action wrappers (defined below) 
  // based on the generic wrapper
  public void updateLinkActionWrapperList() {
  	linkActionWrapperList = new List<LinkActionWrapper>();
  	// retrieve field configuration from custom setting
  	fieldConfig = populateFieldConfig();

  	lwrapper.gotoBeforeFirst();
  	while (lwrapper.hasNext()) {
  		lwrapper.next();
      // create new link action wrapper list  		
  		linkActionWrapperList.add(new LinkActionWrapper(linkActionHandler, wrapper, fieldConfig));
  	}
  	system.debug('=============linkActionWrapperList==============='+linkActionWrapperList);
  }
  
  // retrieve field configuration from custom setting
  public List<sObject> populateFieldConfig() {
  	return Database.query(
  	  'SELECT Name, goog_dclk_dsm__DC_SortOrder__c, goog_dclk_dsm__DC_DeepLinkToDSMRecord__c FROM ' + configContext + ' ORDER BY goog_dclk_dsm__DC_SortOrder__c'
  	);
  }

  // inner class for a record coming from the generic wrapper
  // allows for the addition of a link action  
  public class LinkActionWrapper {
  	// contains the createLink(integer) action that will be called if the link method is called
  	// on the instance of this class
    private DC_ListLinkActionHandler linkActionHandler;
    
    // this records index in the source DC_GenericWrapper instance
    private Integer index;
    
    // map of values for the DC_GenericWrapper record provided
    public Map<String, Object> values { get; set; }
    
    // constructor
    // handler      DC_ListLinkActionHandler    provider of the createLink(integer) method used when the link is clicked    
    // wrapper      DC_GenericWrapper           wrapper containing the specific record for this instance
    // fieldConfig  List<sObject>               field configuration records used to define which fields appear
    public LinkActionWrapper(DC_ListLinkActionHandler handler, DC_GenericWrapper wrapper, List<sObject> fieldConfig) {
      linkActionHandler = handler;
      // identify the index of the selected record -- will be used for createLink call if selected
      index = wrapper.getCurrentIndex();
      
      // shift the values from the record to the value map (values)
      wrapperCurrentRecordValuesToMap(wrapper, fieldConfig);
    }

    // take the current record's values from the wrapper and put them in the values map
    // wrapper      DC_GenericWrapper     wrapper with record and values
    // fieldConfig  List<sObject>         records containing the field information for value selection
	  private void wrapperCurrentRecordValuesToMap(DC_GenericWrapper wrapper, List<sObject> fieldConfig) {
	    values = new Map<String, Object>(); 
	    system.debug('=============fieldConfig================'+fieldConfig);
	    for (sObject configItem : fieldConfig) {
	    	string configName = (string)configItem.get('name');
	      values.put(configName, wrapper.getField(configName));
	    }
	    system.debug('=============values================'+values);
	  }

    // called from the component display -- this is the result of clicking the "link" link
    public void link() {
      // pass this to the provided link action handler
      linkActionHandler.createLink(index);
      new DC_Connector().executeAllDml();
    }
  }
}