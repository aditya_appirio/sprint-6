/**
 */
@isTest
private class Test_DC_NetworkDAO {

	static testMethod void myUnitTest() {
		Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplNetworkService());
    Test.startTest();
    DC_DoubleClickOAuthHandler oAuthHandler = new DC_DoubleClickOAuthHandler(Userinfo.getUserId());
    
    DC_NetworkDAO networkDAO = new DC_NetworkDAO(oAuthHandler);
    networkDAO.getNetworks();
    networkDAO.getCurrentNetwork();
    
    DC_NetworkManager nwManager = new DC_NetworkManager(oAuthHandler);
    nwManager.getCurrentNetworkTimezone();
    Test.stopTest();
	}
}