//
// Test_DC_PushContactToDSMController : test class for DC_PushContactToDSMController
//
// 28 Oct 2014   updated
@isTest
private class Test_DC_PushContactToDSMController {
	
	static Contact cnct;
	static goog_dclk_dsm__DC_ObjectMapping__c objMapping1;
	static List<goog_dclk_dsm__DC_FieldMapping__c> fldMappingList ;
    static testMethod void testDC_PushContactToDSM() {
    
    createTestData();
    
    ApexPages.StandardController sc = new ApexPages.StandardController(cnct);
    DC_PushContactToDSMController controller = new DC_PushContactToDSMController(sc);
    
    
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplContact());
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplCompany());
    
    Test.setMock(HttpCalloutMock.class, new DC_HttpCalloutMockImploAuth());
    Test.startTest();
    controller.getSelectedRecord(cnct.id);
    controller.getContext();		//Added Date- 29-10-2014
    DC_GenericWrapper wrap = controller.executeConnectorGetMatchingRecords();
    System.assert(wrap != null);
    System.debug('wrap:::' + wrap);
    //wrap.next();
    controller.executeConnectorLink(wrap);
   
    controller.executeConnectorSync();
    
    DC_ContactMapper contactMapper = DC_MapperFactory.getInstance().getContactMapper(); 
    DC_ContactService.Contact contact1 = new DC_ContactService.Contact();
    contact1.Name = 'fname1 lname1';
    contact1.companyId = 12345;
    contact1.id = 123456;
    
    DC_DoubleClickOAuthHandler oAuthHandler = new DC_DoubleClickOAuthHandler(Userinfo.getUserId());
    
    
    
    List<DC_ContactService.Contact> contactList = new List<DC_ContactService.Contact>{new DC_ContactService.Contact()};
    DC_DoubleClickContactWrapper w = new DC_DoubleClickContactWrapper(objMapping1, fldMappingList, contactList);
    w.next();
    w.getField('DoubleClickId');
    w.getField('comment');
    w.getField('name');
    
    w.getField('status');
    w.getField('companyid');
    w.getField('address');
    
    w.getField('cellphone');
    w.getField('email');
    w.getField('faxphone');
    
    w.getField('title');
    w.getField('workphone');
    w.getNewRecords();
    w.setField('DoubleClickId', '12345');
    w.setField('comment', 'test');
    w.setField('faxphone', '1111111111');
    w.setField('email', 'test@test.com');
    
    w.setField('address', 'test');
    w.setField('cellphone', '1111111111');
    w.setField('status', 'working');
    w.setField('title', 'test');
    w.setField('workphone', '1111111111');
    w.getExistingRecords();
    w.getCurrentWrapped();
    w.createWrapper(objMapping1, fldMappingList, contactList);
    Test.stopTest();
  }
  
  static testMethod void testDC_PushContactToDSM1() {
    
    createTestData();
    
    ApexPages.StandardController sc = new ApexPages.StandardController(cnct);
    DC_PushContactToDSMController controller = new DC_PushContactToDSMController(sc);
    
    
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplContact());
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplCompany());
    
    Test.setMock(HttpCalloutMock.class, new DC_HttpCalloutMockImploAuth());
    Test.startTest();
    controller.getSelectedRecord(cnct.id);
    DC_GenericWrapper wrap = controller.executeConnectorGetMatchingRecords();
    System.assert(wrap != null);
    System.debug('wrap:::' + wrap);
    //wrap.next();
    controller.executeConnectorSyncExisting();
    controller.getLinkMessage();
    controller.getUniquenessViolationMessage();
    
    /* Start - Added by to cover Catch block , Date - 28-10-2014 */
    Account acct = new Account(Name = 'TestAcc');
    insert acct;
    controller.getSelectedRecord(acct.Id);
    controller.executeConnectorSyncExisting();
    controller.executeConnectorSync();
    controller.executeConnectorLink(wrap);
    /* Stop */
    
    Test.stopTest();
  }
  
  static testMethod void testDC_PushContactToDSM12() {
    
    createTestData();
    
    
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplContact());
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplCompany());
    
    Test.setMock(HttpCalloutMock.class, new DC_HttpCalloutMockImploAuth());
    Test.startTest();
    
    DC_ContactMapper contactMapper = DC_MapperFactory.getInstance().getContactMapper(); 
    DC_ContactService.Contact contact1 = new DC_ContactService.Contact();
    contact1.Name = 'fname1 lname1';
    contact1.companyId = 12345;
    contact1.id = 123456;
    
    DC_DoubleClickOAuthHandler oAuthHandler = new DC_DoubleClickOAuthHandler(Userinfo.getUserId());
    
    DC_DoubleClickContactManager cManager = new DC_DoubleClickContactManager(oAuthHandler);
    cManager.getContactsUpdatedSinceLastSync();
    cManager.getDCRecords(contactMapper.wrap(contact1));
    cManager.updateDCRecords(contactMapper.wrap(contact1));
    cManager.getDCRecordsIds(contactMapper.wrap(contact1));
    cManager.getMatchingContacts1(new List<String>{'12345'}, new List<String>{'testCompany'}, new List<String>());
    //cManager.getContact(12345);
    
    DC_DoubleClickManager dManager = new DC_DoubleClickContactManager(oAuthHandler);
    dManager.getDCRecords(contactMapper.wrap(contact1));
    dManager.updateDCRecords(contactMapper.wrap(contact1));
    
    DC_ContactContactConnector connector = DC_ContactContactConnector.getInstance();
    connector.syncExistingSF_Records(new List<Contact>{cnct});
    connector.findMatchingContacts(new List<String>{'12345'}, new List<String>{'fname1 lname1'}, new List<String>());
    Test.stopTest();
  }
  private static void createTestData(){
  	
  	//updated  27-oct-2014
  	goog_dclk_dsm__DC_Authentication__c objDC_Authentication = new goog_dclk_dsm__DC_Authentication__c(goog_dclk_dsm__DC_AccessToken__c ='1324324',goog_dclk_dsm__DC_ExpiresIn__c = 3600 ,goog_dclk_dsm__DC_RefreshToken__c = '454655656',goog_dclk_dsm__DC_TokenType__c = 'Bearer');
    insert objDC_Authentication;
    
  	Account a1 = new Account(name = 'testCompany');
    a1.goog_dclk_dsm__DC_DoubleClickId__c = '12345';
    a1.goog_dclk_dsm__DC_ReadyForDSM__c = true;
    insert a1;
         
    cnct = new Contact(AccountId = a1.id, FirstName = 'fname1', LastName = 'lname1');
    insert cnct; 
    
    objMapping1 = new goog_dclk_dsm__DC_ObjectMapping__c(goog_dclk_dsm__DC_DoubleClickObjectName__c='Company', goog_dclk_dsm__DC_SalesforceObjectName__c = 'Account', name = 'account');
    goog_dclk_dsm__DC_ObjectMapping__c objMapping2 = new goog_dclk_dsm__DC_ObjectMapping__c(goog_dclk_dsm__DC_DoubleClickObjectName__c='Contact', goog_dclk_dsm__DC_SalesforceObjectName__c = 'Contact', name = 'contact');
    insert new List<goog_dclk_dsm__DC_ObjectMapping__c>{objMapping1, objMapping2};
    
    fldMappingList = new List<goog_dclk_dsm__DC_FieldMapping__c>();
    goog_dclk_dsm__DC_FieldMapping__c fldMapping1 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping2 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping2.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping3 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping2.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'name', goog_dclk_dsm__DC_SalesforceFieldName__c = 'FirstName', Name = 'name');
    
    
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping2.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'status', goog_dclk_dsm__DC_SalesforceFieldName__c = 'LeadSource', Name = 'status'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping2.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'address', goog_dclk_dsm__DC_SalesforceFieldName__c = 'MailingStreet', Name = 'address'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping2.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'cellphone', goog_dclk_dsm__DC_SalesforceFieldName__c = 'MobilePhone', Name = 'cellphone'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping2.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'comment', goog_dclk_dsm__DC_SalesforceFieldName__c = 'Description', Name = 'comment'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping2.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'email', goog_dclk_dsm__DC_SalesforceFieldName__c = 'email', Name = 'email'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping2.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'faxphone', goog_dclk_dsm__DC_SalesforceFieldName__c = 'Fax', Name = 'faxphone'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping2.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'title', goog_dclk_dsm__DC_SalesforceFieldName__c = 'title', Name = 'title'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping2.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'workphone', goog_dclk_dsm__DC_SalesforceFieldName__c = 'OtherPhone', Name = 'workphone'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping2.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'companyid', goog_dclk_dsm__DC_SalesforceFieldName__c = 'AccountId', Name = 'companyid'));
    
    fldMappingList.add(fldMapping1); 
    fldMappingList.add(fldMapping2);
    fldMappingList.add(fldMapping3);
    insert fldMappingList;
  }
}