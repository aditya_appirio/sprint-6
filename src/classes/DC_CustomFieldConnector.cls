public with sharing class DC_CustomFieldConnector {

  private DC_CustomFieldManager manager {
    get {
      if(manager == null) {
        manager = DC_ManagerFactory.getInstance().getCustomFieldManager();
      }

      return manager;
    }
    set;
  }

  public void createOrGetCustomFieldId(goog_dclk_dsm__DC_FieldMapping__c fieldMapping) {
    List<goog_dclk_dsm__DC_FieldMapping__c> mappings = [select Id, goog_dclk_dsm__DC_Object_Mapping__r.goog_dclk_dsm__DC_DoubleClickObjectName__c, goog_dclk_dsm__DC_DoubleClickFieldName__c, goog_dclk_dsm__DC_DoubleClickCustomFieldID__c from goog_dclk_dsm__DC_FieldMapping__c where Id = :fieldMapping.Id];

    if(mappings.size() > 0) {
      goog_dclk_dsm__DC_FieldMapping__c mapping = mappings[0];

      mapping.goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = manager.createOrGetCustomFieldId(mapping.goog_dclk_dsm__DC_DoubleClickFieldName__c, mapping.goog_dclk_dsm__DC_Object_Mapping__r.goog_dclk_dsm__DC_DoubleClickObjectName__c);

      DC_SalesforceDML.getInstance().registerUpdate(mapping);
    }
  }

  public void createOrGetCustomFieldId(List<goog_dclk_dsm__DC_FieldMapping__c> fieldMappings) {
    Integer i;
    for(i = 0; i < fieldMappings.size() && i < 5; i++) {
      createOrGetCustomFieldId(fieldMappings.get(i));
    }

    DC_SalesforceDML.getInstance().flush();
  }
}