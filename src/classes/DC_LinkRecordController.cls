public abstract class DC_LinkRecordController implements DC_ListLinkActionHandler {
  protected final string READY_FOR_DSM_FIELD_API = 'goog_dclk_dsm__DC_ReadyForDSM__c';
  protected final string DOUBLECLICK_ID_FIELD_API = 'goog_dclk_dsm__DC_DoubleClickId__c';

  public boolean escapeMessages { get; set; }

  // if linking is required, shift the values to the result wrapper
  public DC_GenericWrapper resultWrapper { get; set; } 

  // record id for the selected salesforce object
  private Id selectedRecordId;
  
  // provide the selected record
  public sObject selectedRecord { get; set; }
  
  // connector for integration actions
  protected DC_Connector connector { get; set; } 
    
  // label of the record in Salesforce
  public String sfRecordLabel { get; set; }
  
  // label of the record in DSM
  public String dcRecordLabel { get; set; }
  
  // show the result panel
  public boolean showResultPanel { 
    get {
      return isLink && !linkComplete;   
    }
  }
  
  public boolean isTestExce;
  // return an instance of this to be provided as the link action handler to the component
  public DC_ListLinkActionHandler linkActionHandler {
    get {
      return this;
    }
  }
  
  // 3 possible avenues for the record
  private boolean isCreate = false;
  public boolean isUpdate = false;
  public boolean isLink = false; 
  
  // when linking, the subsequent completion of the activity
  private boolean linkComplete = false;
  
  // exception path
  protected boolean isException = false;
  
  ApexPages.Standardcontroller controller { get; private set; }
  
  public DC_LinkRecordController(ApexPages.Standardcontroller c, String sfLabel, String dcLabel) {
    controller = c;
    selectedRecordId = c.getId();
    sfRecordLabel = sfLabel;
    dcRecordLabel = dcLabel; 
    escapeMessages = true;
    isTestExce = false;
   
    List<sObject> returnedRecords = getSelectedRecord(selectedRecordId);
    
    // if we have anything other than 1 record, raise an exception
    if (returnedRecords.size() != 1){
      if( ApexPages.currentPage() != null){
        ApexPages.addMessages(
          new DC_ConnectorException(String.format(Label.DC_Invalid_Id, new String[] { sfRecordLabel } ))
        );
      }else {
        DC_ConnectorLogger.log(getContext(), new DC_ConnectorException(String.format(Label.DC_Invalid_Id, new String[] { sfRecordLabel } )));
      }
      return;
    }
    connector = new DC_Connector();		
    selectedRecord = returnedRecords[0];
  }
  
  // label for the button that returns the user to the selected record
  public string backButtonLabel {
    get {
      // if we are giving the opportunity to link, provide a cancel button, otherwise back button
      return (showResultPanel && !isException) ? Label.DC_Cancel : String.format(Label.DC_Back_to_Record, new String[] { sfRecordLabel });
      
    }
  }
      
  //Check if record already exists in DSM.  NOTE: this is called as a page action in the VF page as DML cannot occur from the constructor  
  // 3 possible scenarios:
  // - record already linked (determined by examining double click id on record) -- perform update
  // - record not linked, but one matches in DSM -- offer ability to link
  // - record not linked, and no matches -- create record
  public virtual void checkExistingDSMRecord(){
  	try{
    if (!isSelectedRecordReadyForDSM()) {
      if(!ApexPages.hasMessages()){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, String.format(Label.DC_Record_Not_Ready_for_DSM, new String[] { sfRecordLabel } )));
      }
      return;     
    } 
    System.debug(' yes execute ');
    //if record  already exist in DSM, update the record
    if(selectedRecord.get(DOUBLECLICK_ID_FIELD_API) != null){
      try {
        System.debug('double click id exists in SFDC====');
        executeConnectorSyncExisting();
        isUpdate = true;
      } catch (Exception e) {
      	
        if(!ApexPages.hasMessages()){
          ApexPages.addMessages(e);
          DC_ConnectorLogger.log(getContext(), e);
        }else{
          DC_ConnectorLogger.log(getContext(), e);
        }
        isException = true;
        DC_SalesforceDML.getInstance().flush();
        return;
      }
      connector.executeAllDml();
      
      if(!ApexPages.hasMessages()){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, String.format(Label.DC_DSM_Record_Update_Successful, new String[]{ dcRecordLabel, sfRecordLabel })));
      }
      return;
      
    } else { //else find existing records that match the selected record
      System.debug('duble click id not exis in SFDC=====');
      resultWrapper = executeConnectorGetMatchingRecords();
      // if records were returned above, then wrap them for display
      if(resultWrapper.objects != null && resultWrapper.objects.size() > 0){ 
        System.debug('Record already exist in Double click=====');
        
        if(!ApexPages.hasMessages()){
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, String.format(getLinkMessage(), new String[]{ sfRecordLabel, dcRecordLabel})));
        }
        isLink = true;
        connector.executeAllDml();
        return;
      } else {
        // otherwise, create the record
        System.debug('Record not exist in Double click, Create=====');
        createDCRecordFromSFRecord();
      }
      connector.executeAllDml();
     }
  	}
  	catch (Exception e) {
    		System.debug('e:::' + e);
	      if(!ApexPages.hasMessages()){
        	ApexPages.addMessages(e);
        	return; 
      	}
  	}
  }  
  
  // Create the record in DSM from the Salesforce Record
  public void createDCRecordFromSFRecord(){
    isCreate = true;
    try {
    	if(Test.isRunningTest() && isTestExce){
    		Http http = new Http();
				HTTPResponse res = http.send(new HttpRequest());
    		
    	}
      executeConnectorSync();
      // create the account record
    } catch (System.CalloutException ce) {
      DC_ConnectorLogger.log(getContext(), ce);
      string msg = ce.getMessage();
      
      // if this is a uniqueness exception, provide additional information on how to resolve
      if (msg.indexOf(DC_Constants.UNIQUE_ERROR) != -1) {
        if(!ApexPages.hasMessages()){
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, String.format(getUniquenessViolationMessage(), new String[]{ sfRecordLabel, dcRecordLabel})));
        }
        escapeMessages = false;
      } else {
        // otherwise display the exception
        if(!ApexPages.hasMessages()){
          ApexPages.addMessages(ce);
        }
      }
      isException = true;
      DC_SalesforceDML.getInstance().flush();
      return;
    } catch (Exception e) {
    	
     if(!ApexPages.hasMessages()){
        ApexPages.addMessages(e);
      }
      DC_ConnectorLogger.log(getContext(), e);
      DC_SalesforceDML.getInstance().flush();
      isException = true;
      return;
    }
    
    String msgStr = String.format(Label.DC_Record_Creation_Successful, new String[]{ sfRecordLabel });
    if((sfRecordLabel == DC_Constants.ACCOUNT_CONTEXT && goog_dclk_dsm__DC_ConnectorApplicationSettings__c.getInstance().goog_dclk_dsm__DC_AllowAccountTriggerPushToDSM__c) || (sfRecordLabel == DC_Constants.Contact_CONTEXT && goog_dclk_dsm__DC_ConnectorApplicationSettings__c.getInstance().goog_dclk_dsm__DC_AllowContactTriggerPushToDSM__c)){
      msgStr += Label.RECORD_FUTURE_UPDATE_STR;
    }

    if(!ApexPages.hasMessages()){
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, msgStr));
    }         

    if (selectedRecord.getSObjectType() == Opportunity.sObjectType && ((string)selectedRecord.get('goog_dclk_dsm__DC_ProposalStatus__c') == 'PENDING_APPROVAL' || (string)selectedRecord.get('goog_dclk_dsm__DC_ProposalStatus__c') == 'APPROVED')) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 
        String.format(Label.DC_Approval_Retracted, new String[]{ sfRecordLabel, dcRecordLabel})));
    }
  }
   
  // page subtitle to display the action that is taking place
  public String subtitle {
    get {
      if (isLink) {
        return String.format(Label.DC_Link_SF_Record_with_DSM_Record, new String[] {sfRecordLabel, dcRecordLabel});
      } else if (isUpdate) {
        return String.format(Label.DC_Update_Existing_DSM_Record, new String[] {dcRecordLabel});
      } else if (isCreate) {
        return String.format(Label.DC_Create_New_DSM_Record,  new String[] {dcRecordLabel, sfRecordLabel});
      } else {
        return String.format(Label.DC_Push_Salesforce_Record_to_DSM,  new String[] {sfRecordLabel});
      }
    }
  }  
   
  //Return to selected record
  public PageReference back() {
    PageReference pageRef = new PageReference('/'+ selectedRecord.id);
    return pageRef;
  } 
  
  // create a link between the salesforce record and the doubleclick record
  // selectedWrapper  DC_GenericWrapper   wrapper to use for the link creation
  public void createLinkSFRecordAndDCRecord(DC_GenericWrapper selectedWrapper) {
    try {
      executeConnectorLink(selectedWrapper);
    } catch (Exception e) {
      if(!ApexPages.hasMessages()){
        ApexPages.addMessages(e);
      }
      DC_ConnectorLogger.log(getContext(), e);
      DC_SalesforceDML.getInstance().flush();
      isException = true;
      return;
    } 
    isLink = true;
    if(!ApexPages.hasMessages()){
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, String.format(Label.DC_Record_Linking_Successful, new String[]{ sfRecordLabel, dcRecordLabel })));   
    }
  }
      
  // action called by "link" action on list 
  public virtual void createLink(integer selectedRecordIndex) {
  	System.debug('resultWrapper:::' + resultWrapper);
  	System.debug('selectedRecordIndex:::' + selectedRecordIndex);
    resultWrapper.gotoObject(selectedRecordIndex);
    createLinkSFRecordAndDCRecord(resultWrapper); 
    linkComplete = true;
  }    
  
  public virtual boolean isSelectedRecordReadyForDSM() {
    // check if ready for dsm.  If not, stop
    return ((boolean)selectedRecord.get(READY_FOR_DSM_FIELD_API));
  }  
  
  public abstract string getContext();
  
  // get the selected salesforce record
  public abstract List<sObject> getSelectedRecord(Id recordId);

  // sync a record we know exists in DC
  public abstract void executeConnectorSyncExisting();
  
  // execute the sync with the connector
  public abstract void executeConnectorSync();
  
  // create a link between the two records
  public abstract void executeConnectorLink(DC_GenericWrapper wrapper);

  // get the records that match the selected record
  public abstract DC_GenericWrapper executeConnectorGetMatchingRecords();
  
  // get the message indicating that linking is available
  public abstract String getLinkMessage();

  // get the message to be displayed in the case that uniqueness is being violated.
  // this is being treated specially to offer mitigation steps.
  public abstract String getUniquenessViolationMessage();
}