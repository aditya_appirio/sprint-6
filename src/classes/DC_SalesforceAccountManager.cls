// 
// (c) 2014 Appirio
//
// contains method to get and update Account
// 
//
public with sharing class DC_SalesforceAccountManager extends DC_SalesforceManager {
	
	// enforce singleton 
	private DC_CompanyMapper mapper {
    get {
      if(mapper == null) {
        mapper = DC_CompanyMapper.getInstance();
      }
      return mapper;
    }
    set;
  }

  // get the account related to the passed in id
  // id   string    account id for the account to retrieve
  public DC_GenericWrapper getAccount(String id) {
    String fieldNames = this.mapper.getCommaSeparatedSalesforceFieldList('Account');
    String query = 'SELECT ' + fieldNames + ' FROM Account where id = :id';
     
    return mapper.wrap((List<Account>)Database.query(query));
  }
  
  // get the accounts for the ids provided and return in a generic wrapper
  // ids  List<Id>  list of ids for which to retrieve accounts
  public DC_GenericWrapper getAccounts(List<Id> ids) {
    String fieldNames = this.mapper.getCommaSeparatedSalesforceFieldList('Account');
    // BR: replaced above with bind variable
    String query = 'SELECT ' + fieldNames + ' FROM Account where id in :ids';

    return mapper.wrap((List<Account>)Database.query(query));
  }

  // get the account records from the passed accounts 
  public DC_GenericWrapper getAccounts(List<Account> accounts) {
    if(accounts == null || accounts.size() == 0) {
      return null;
    }

    List<Id> ids = new List<Id>(new Map<Id, Account>(accounts).keySet());

    return getAccounts(ids);
  }
  
}