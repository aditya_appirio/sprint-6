public with sharing class DC_CustomAccountTeamHandler extends DC_CustomTeamHandler {
  public override string getSalesforceQueryString() {
  	return '(SELECT id, goog_dclk_dsm__DC_Team__r.goog_dclk_dsm__DC_DoubleClickId__c FROM goog_dclk_dsm__DoubleClick_Account_Teams__r)'; 
  }
  
  public override void processField(goog_dclk_dsm__DC_FieldMapping__c currentField, DC_GenericWrapper source, DC_GenericWrapper target) {
  	Account a = (Account)source.getCurrentObject();
  	
    List<Long> teamIds = new List<Long>();
    for (goog_dclk_dsm__DC_DoubleClickAccountTeam__c team : a.goog_dclk_dsm__DoubleClick_Account_Teams__r) { 
      teamids.add(long.valueOf(team.goog_dclk_dsm__dc_team__r.goog_dclk_dsm__DC_DoubleClickId__c));    	
    }
    ((DC_CompanyService.Company) target.getCurrentObject()).appliedTeamIds = teamIds;
  }
}