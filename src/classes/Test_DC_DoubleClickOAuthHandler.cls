// 
// (c) 2014 Appirio
//
// Test_DC_DoubleClickOAuthHandler: test class for DC_DoubleClickOAuthHandler 
//
@isTest
private class Test_DC_DoubleClickOAuthHandler {

  static testMethod void testoAuthHandler() {
    goog_dclk_dsm__DC_ConnectorAuthenticationSettings__c connSetting = DC_TestUtils.createConnectorSetting(
      '802187318127-fvdeo6krkkt0tbrmqd0u378sudior617.apps.googleusercontent.com',
        'sfdc-dc-connector', 'iGwU4g1MYxG6DNHnYeKlFYPE', '38192739', '6931', 
        'https://c.na11.visual.force.com/apex/DC_ConnectorAdminPage', 
        'https://www.google.com/apis/ads/publisher', true,'https://ads.google.com/apis/ads/publisher/v201408');
        
    DC_TestUtils.createDCAuthentication('ya29.1.AADtN_WfW_S3p2Raw6ZCsveGTL0goy-T4lXQnjlnjDr97bue0MmKtBXHT-2cTac', 
      '1/VbGziuloIkBV7SZS7rP5wlRDzkVMX-UIf_l3wpVZm9Q', 3600, 'Bearer', true);
    
    Profile p = [SELECT id from profile where name =: 'System Administrator']; 
    User user1 = new User(alias = 'testUser', email='standarduser' + Math.random()  + '@testorg.com',
                 emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                 localesidkey='en_US', profileid = p.Id, timezonesidkey='America/Los_Angeles', 
                 username='teststandarduser' + Math.random() + '@testorg.com', IsActive=true,
                 CompanyName = 'test Company');
                 
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new     DC_HttpCalloutMockImploAuth());
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplNetworkService());
    DC_DoubleClickOAuthHandler handler = new DC_DoubleClickOAuthHandler(user1.Id);
    handler.exchangeCode('123', true);
    handler.refreshAccessToken();
    handler.saveSettings();
    Test.stopTest();
  }
}