// 
// (c) 2014 Appirio
//
// Test_DC_AccountTriggerController : test class for DC_AccountTriggerController 
//
// 13 March 2014   Karun Kumar(JDC) Original
//
@isTest
private class Test_DC_AccountTriggerController {

  static testMethod void myUnitTest() {
    goog_dclk_dsm__DC_ObjectMapping__c objMapping = new goog_dclk_dsm__DC_ObjectMapping__c(goog_dclk_dsm__DC_DoubleClickObjectName__c='Company', goog_dclk_dsm__DC_SalesforceObjectName__c = 'Account', name = 'account');
    insert objMapping;
    goog_dclk_dsm__DC_FieldMapping__c fldMapping1 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping2 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'externalId', goog_dclk_dsm__DC_SalesforceFieldName__c = 'Id', Name = 'sfid');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping3 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'name', goog_dclk_dsm__DC_SalesforceFieldName__c = 'name', Name = 'name');
    insert new List<goog_dclk_dsm__DC_FieldMapping__c>{fldMapping1, fldMapping2,fldMapping3};
    
    goog_dclk_dsm__DC_ConnectorApplicationSettings__c connAppSetting = DC_TestUtils.createConnAppSetting(1, false);
    connAppSetting.goog_dclk_dsm__DC_AllowAccountTriggerPushToDSM__c = true;
    insert connAppSetting;
    
    //updated  27-oct-2014
  	goog_dclk_dsm__DC_Authentication__c objDC_Authentication = new goog_dclk_dsm__DC_Authentication__c(goog_dclk_dsm__DC_AccessToken__c ='1324324',goog_dclk_dsm__DC_ExpiresIn__c = 3600 ,goog_dclk_dsm__DC_RefreshToken__c = '454655656',goog_dclk_dsm__DC_TokenType__c = 'Bearer');
    insert objDC_Authentication;
    
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplCompany());
    Test.startTest();
    Account acc = DC_TestUtils.createAccount('test acc', true);
    acc.goog_dclk_dsm__DC_ReadyForDSM__c = true;
    acc.BillingCity = 'Jaipur';
    acc.BillingCountry = 'India';
    acc.BillingState = 'Rajasthan';
    acc.goog_dclk_dsm__dc_doubleclickid__c = '1';    
    update acc;
    
    acc = [Select goog_dclk_dsm__DC_DoubleClickId__c, goog_dclk_dsm__DC_DSMRecord__c, goog_dclk_dsm__DC_ReadyForDSM__c, BillingCity, BillingCountry From Account Where Id = :acc.Id];
    system.assertEquals(acc.goog_dclk_dsm__DC_ReadyForDSM__c, true);
    system.assertEquals(acc.BillingCountry, 'India');
    system.assertEquals(acc.goog_dclk_dsm__DC_DoubleClickId__c, '1');
    system.assert(acc.goog_dclk_dsm__DC_DSMRecord__c.contains('Exists in DSM'));
    
  }
}