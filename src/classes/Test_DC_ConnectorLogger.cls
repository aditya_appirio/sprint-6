// 
// (c) 2014 Appirio
//
// Test Class for DC_ConnectorLogger (T-238924)
//
// 17 Jan 2014   Ankit Goyal (JDC)      Original
//
@isTest
public with sharing class Test_DC_ConnectorLogger {

  static testMethod void myUnitTest() {
        
    Account a = DC_TestUtils.createAccount('test', true);
    DC_ConnectorLogger.Status status = DC_ConnectorLogger.Status.Success;
    goog_dclk_dsm__DC_ProcessingQueue__c queue = DC_TestUtils.createDCProcessingQueue(false, true, false, 'Proposal Line Item Service', true);
    
    // Prepare Test Data
    goog_dclk_dsm__DC_ConnectorLog__c objLog1 = DC_ConnectorLogger.log('Success1','Some Error Description-1',status);
    goog_dclk_dsm__DC_ConnectorLog__c objLog2 = DC_ConnectorLogger.log('Success2','Some Error Description-2',status, queue.id);
    goog_dclk_dsm__DC_ConnectorLog__c objLog3 = DC_ConnectorLogger.log('Success3','Some Error Description-3',status, 'Details3', queue.id);
    goog_dclk_dsm__DC_ConnectorLog__c objLog4 = DC_ConnectorLogger.log('Success4','Some Error Description-4',status, 'Details4', queue.id, new List<Account>{a} ,false);
    goog_dclk_dsm__DC_ConnectorLog__c objLog5 = DC_ConnectorLogger.log('Success4','Some Error Description-4',status, 'Details4', queue.id, new List<Account>{a} ,true); 
    goog_dclk_dsm__DC_ConnectorLog__c objLogError;
    try{ 
      objLogError.goog_dclk_dsm__DC_Status__c = objLog3.goog_dclk_dsm__DC_Status__c + 'testErr';
    }catch(exception e){
      DC_ConnectorLogger.log('testErr', e);
    }
    
    try{
      objLogError.goog_dclk_dsm__DC_Status__c = objLog3.goog_dclk_dsm__DC_Status__c + 'testErr';
    }catch(exception e){ 
      DC_ConnectorLogger.log('testErr', e, queue.id);
    }
    
    try{
      objLogError.goog_dclk_dsm__DC_Status__c = objLog3.goog_dclk_dsm__DC_Status__c + 'testErr';
    }catch(exception e){
      DC_ConnectorLogger.log('testErr', e, new List<Account>{a} );
    }
     
    DC_SalesforceDML.DC_SalesforceDMLException dmle = new DC_SalesforceDML.DC_SalesforceDMLException();
    Exception e = new DC_ConnectorException('test!');
    
    DC_ConnectorLogger.log('test', dmle);
    DC_ConnectorLogger.log('test', e);
    
    DC_SalesforceDML.DC_SalesforceDMLException excep = new DC_SalesforceDML.DC_SalesforceDMLException();
    excep.errorCommands = new List<DC_SalesforceDML.DC_SalesforceDMLInsert>();
    
    Account acc = new Account(name = 'test');
    DC_SalesforceDML.DC_SalesforceDMLInsert c = new DC_SalesforceDML.DC_SalesforceDMLInsert(new List<Account>{acc});
    c.errorRecordIndex = new List<Integer>{1,2};
    excep.errorCommands.add(c);
    
    Contact con = DC_TestUtils.createContact('fName', 'lName', acc.Id, true);
    Database.DeleteResult sr = Database.delete(con);

    DC_ConnectorLogger.getDMLErrorString(sr);
    try{
      DC_ConnectorLogger.logDmlCommandErrors('testContext',queue.id, excep);
    }catch(Exception ex){} 
  }
}