// 
// (c) 2014 Appirio
//
// WebServiceMockImpl_Contact : implements WebServiceMock for webServicecallout test classes 
//
// 24 Jan 2014    Ankit Goyal (JDC)      Original
//
@IsTest
global class DC_WebServiceMockImplContact implements WebServiceMock {
  global void doInvoke(Object stub, Object request, Map<String, Object> response,
                      String endpoint, String soapAction, String requestName, 
                      String responseNS, String responseName, String responseType){
    DC_ContactService.Contact contact = new DC_ContactService.Contact();
    contact.Name = 'fname1 lname1';
    contact.companyId = 12345;
    
    /*DC_ContactService.Contact contact1 = new DC_ContactService.Contact();
    contact1.Name = 'fname1 lname1';
    contact1.companyId = 12345;
    contact1.id = 123456;*/
    
    DC_ContactService.ContactPage contactPage = new DC_ContactService.ContactPage();
    contactPage.results = new List<DC_ContactService.Contact>{contact};
    contactPage.totalResultSetSize = 1;
    contactPage.startIndex = 0; 
      
    /*if(request instanceof DC_ContactService.getContact_element){
        DC_ContactService.getContactResponse_element resp = new DC_ContactService.getContactResponse_element();
        resp.rval = contact1;
        response.put('response_x', resp);
        //response.put('response_x', new DC_ContactService.getContactResponse_element());
    }else if(request instanceof DC_ContactService.createContact_element){
        DC_ContactService.createContactResponse_element resp = new DC_ContactService.createContactResponse_element();
        resp.rval = contact;
        response.put('response_x', resp);
        //response.put('response_x', new DC_ContactService.createContactResponse_element()); 
    }else*/
     if(request instanceof DC_ContactService.updateContacts_element){
        DC_ContactService.updateContactsResponse_element resp = new DC_ContactService.updateContactsResponse_element();
        resp.rval = new List<DC_ContactService.Contact>{contact};
        response.put('response_x', resp);
        
        //response.put('response_x', new DC_ContactService.updateContactsResponse_element()); 
    }else if(request instanceof DC_ContactService.createContacts_element){
        DC_ContactService.createContactsResponse_element resp = new DC_ContactService.createContactsResponse_element();
        resp.rval = new List<DC_ContactService.Contact>{contact};
        response.put('response_x', resp);
        
        //response.put('response_x', new DC_ContactService.createContactsResponse_element()); 
    }else if(request instanceof DC_ContactService.getContactsByStatement_element){
        DC_ContactService.getContactsByStatementResponse_element resp = new DC_ContactService.getContactsByStatementResponse_element();
        resp.rval = contactPage;
        response.put('response_x', resp);
        //response.put('response_x', new DC_ContactService.getContactsByStatementResponse_element());
    }/*else if(request instanceof DC_ContactService.updateContact_element){
        DC_ContactService.updateContactResponse_element resp = new DC_ContactService.updateContactResponse_element();
        resp.rval = contact1;
        response.put('response_x', resp);
        //response.put('response_x', new DC_ContactService.updateContactResponse_element());
    }*/
    return;
  }
}