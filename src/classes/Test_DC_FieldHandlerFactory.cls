// 
// (c) 2014 Appirio
//
// Test_DC_FieldHandlerFactory : test class for DC_FieldHandlerFactory 
//
//
@isTest
private class Test_DC_FieldHandlerFactory {

  static testMethod void myUnitTest() {
  	try {
      DC_FieldHandlerFactory.DC_FieldMappingCustomHandler handler = DC_FieldHandlerFactory.getHandlerInstance('DC_CustomProposalTeamMemberHandler');
      
      System.assert(handler != null);
  	} catch (Exception e) {
  		System.assert(false); 
  	}    
  }
}