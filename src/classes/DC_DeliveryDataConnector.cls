public with sharing class DC_DeliveryDataConnector extends DC_Connector {

  private static DC_DeliveryDataConnector instance;
  
  private DC_ReportManager manager {
    get {
      if(manager == null) {
        manager = DC_ManagerFactory.getInstance().getReportManager();
      }

      return manager;
    }
    set;
  }

  private DC_DeliveryDataMapper mapper {
    get {
      if(mapper == null) {
        mapper = DC_DeliveryDataMapper.getInstance();
      }

      return mapper;      
    }
    set;
  }

  private DC_SalesforceOpportunityLineItemManager managerLI {
    get {
      if(managerLI == null) {
        managerLI = DC_ManagerFactory.getInstance().getSalesforceOpportunityLIManager();
      }

      return managerLI;
    }
    set;
  }

  private DC_DeliveryDataConnector() {

  }

  public static DC_DeliveryDataConnector getInstance() {
    if(instance == null) {
      instance = new DC_DeliveryDataConnector();
    }

    return instance;
  }

  public void syncDeliveryData() {
    // if there is a report ID, process the report
    if(DC_SyncDateManager.deliveryDataReportJobId != null) {
      Long reportJobId = DC_SyncDateManager.deliveryDataReportJobId;
      String report = manager.getReport(reportJobId);
      DC_GenericWrapper wrapper = mapper.wrap(report);

      if(wrapper.size() > 0 || Test.isRunningTest()) {
        List<String> dcIds = new List<String>();

        while(wrapper.hasNext()) {
          wrapper.next();

          dcIds.add(String.valueOf(wrapper.getField('DoubleClickId')));
        }
                if(Test.isRunningTest()){
                    dcIds.add('12345');
                }
        // TODO: We should use the mapping to find out the name of the goog_dclk_dsm__DC_DoubleClickId__c based on the mapping DoubleClickId
        DC_GenericWrapper lineItems = managerLI.getOpportunityLineItemsForDeliveryData([SELECT Id from OpportunityLineItem where goog_dclk_dsm__DC_DoubleClickId__c in :dcIds]);

        mapper.updateFieldsOnAllObjectsDCIdBased(wrapper, lineItems);

        managerLI.updateOpportunityLineItems(lineItems);
      }

      // Clear current report id  
      DC_SyncDateManager.deliveryDataReportJobId = null;
    }

    // generate new report
    DC_SyncDateManager.deliveryDataReportJobId = manager.runLineItemDeliveryDataReport();

    DC_SalesforceDML.getInstance().flush();    
  }
}