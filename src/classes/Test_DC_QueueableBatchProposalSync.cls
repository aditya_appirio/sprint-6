// 
// (c) 2014 Appirio
//
// Test_DC_QueueableBatchProposalSync : test class for DC_QueueableBatchProposalSync 
//
@isTest
private class Test_DC_QueueableBatchProposalSync {

  static testMethod void myUnitTest() {
    
    goog_dclk_dsm__DC_ConnectorApplicationSettings__c connAppSetting = DC_TestUtils.createConnAppSetting(1, true);
    List<goog_dclk_dsm__DC_ProcessingQueue__c> processQueues = new List<goog_dclk_dsm__DC_ProcessingQueue__c>();
    processQueues.add(DC_TestUtils.createDCProcessingQueue(false, false, false, 'User Import', false));
    insert processQueues; 
    
    //updated  27-oct-2014
  	goog_dclk_dsm__DC_Authentication__c objDC_Authentication = new goog_dclk_dsm__DC_Authentication__c(goog_dclk_dsm__DC_AccessToken__c ='1324324',goog_dclk_dsm__DC_ExpiresIn__c = 3600 ,goog_dclk_dsm__DC_RefreshToken__c = '454655656',goog_dclk_dsm__DC_TokenType__c = 'Bearer');
    insert objDC_Authentication;
    
    goog_dclk_dsm__DC_ObjectMapping__c objMapping1 = new goog_dclk_dsm__DC_ObjectMapping__c(goog_dclk_dsm__DC_DoubleClickObjectName__c='Proposal', goog_dclk_dsm__DC_SalesforceObjectName__c = 'Opportunity', name = 'Opportunity');
    insert objMapping1;
    
    goog_dclk_dsm__DC_FieldMapping__c fldMapping1 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping2 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'custom.SFDC_Opportunity_ID', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Id', Name = 'sfid');
    insert new List<goog_dclk_dsm__DC_FieldMapping__c>{fldMapping1, fldMapping2};
    
    
    Test.startTest();
    Test.setMock(WebServiceMock.class,new DC_WebServiceMockImplProposal());
    Test.setMock(HttpCalloutMock.class,new DC_HttpCalloutMockImploAuth());
    DC_QueueableBatchProposalSync asb = new DC_QueueableBatchProposalSync();
    DC_QueueableProcess.DC_QueueableProcessParameter inparams;
    asb.initialize(inparams);
    asb.updatesyncDate(Datetime.now());
    asb.getParameterType();
    asb.getResultString();
    
    asb.getRecordsToProcessPerPage();
    asb.getBasisDate();
    asb.processRecords(1,1);
    Test.stopTest();
  
  }
}