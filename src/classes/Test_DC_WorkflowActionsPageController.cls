/**
 */
@isTest
private class Test_DC_WorkflowActionsPageController {
	
	static List<Opportunity> oppList;
	static goog_dclk_dsm__DC_ObjectMapping__c dcObjMapping1;
	static List<goog_dclk_dsm__DC_FieldMapping__c> wfFldMappings = new List<goog_dclk_dsm__DC_FieldMapping__c>();
	@isTest
	static void myUnitTest() {
		createTestData();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplWorkflowService());
		Test.setMock(HttpCalloutMock.class, new DC_HttpCalloutMockImploAuth());
		ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(oppList[0]);
		DC_WorkflowActionsPageController controller = new DC_WorkflowActionsPageController(sc);
		controller.getPendingWorkflowRequests();
		controller.cancelAction();
		
		DC_WorkflowActionListController componentController = new DC_WorkflowActionListController();
		componentController.configContext = 'goog_dclk_dsm__DC_LinkWorkflowFields__c';
		componentController.wfManager = controller.wfManager;
		componentController.wrapper = controller.pendingWFReqWrapper;
		componentController.updateWFActionWrapperList();
		componentController.WFActionWrapperList[0].selectedAction = 'Approve';
		componentController.opportunity = oppList[0];
		componentController.performWFActions();
		Test.stopTest();
	}
	
	@isTest
	static void myUnitTestReject() {
		createTestData();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplWorkflowService());
		Test.setMock(HttpCalloutMock.class, new DC_HttpCalloutMockImploAuth());
		ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(oppList[0]);
		DC_WorkflowActionsPageController controller = new DC_WorkflowActionsPageController(sc);
		controller.getPendingWorkflowRequests();
		controller.cancelAction();
		
		DC_WorkflowActionListController componentController = new DC_WorkflowActionListController();
		componentController.configContext = 'goog_dclk_dsm__DC_LinkWorkflowFields__c';
		componentController.wfManager = controller.wfManager;
		componentController.wrapper = controller.pendingWFReqWrapper;
		componentController.updateWFActionWrapperList();
		componentController.WFActionWrapperList[0].selectedAction = 'Reject';
		componentController.opportunity = oppList[0];
		componentController.performWFActions();
		Test.stopTest();
	}
	
	@isTest
	static void approveOpptyTest() {
		createTestData();
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new DC_HttpCalloutMockImploAuth());
		Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplWorkflowService());
		
		Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplProposal());
		ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(oppList[0]);
		DC_WorkflowActionsPageController controller = new DC_WorkflowActionsPageController(sc);
		controller.getSelectedRecord(controller.currentOppty.id);
		controller.isError = false;
		controller.approveOppty();
		Test.stopTest();
	}
	
	@isTest
	static void testDCWorkflowMapper() {
		createTestData();
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new DC_HttpCalloutMockImploAuth());
		Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplWorkflowService());
		
		Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplProposal());
		
		DC_WorkflowService.WorkflowRequest wfRequest = new DC_WorkflowService.WorkflowRequest();
	  wfRequest.entityId = 12345;
	  wfRequest.id = 12345;
	  wfRequest.status = 'PENDING_APPROVAL';
	  wfRequest.workflowRuleName = 'test rule';
	  wfRequest.entityType = 'PROPOSAL';
	  
		DC_DoubleClickWorkflowMapper mapper = DC_DoubleClickWorkflowMapper.getInstance();
		DC_GenericWrapper wrap = mapper.wrap(wfRequest);
		
		try {
		  mapper.createNewDCRecords(wrap);
		  system.assert(false);
		} catch (Exception e) {
			system.assert(true);
		}

    try {
      mapper.linkSFRecordAndDCRecord(wrap, wrap);
      system.assert(false);
    } catch (Exception e) {
      system.assert(true);
    }
		
		Test.stopTest();
	}
	
	@isTest
	static void testDCWorkflowWrapper() {
		createTestData();
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new DC_HttpCalloutMockImploAuth());
		Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplWorkflowService());
		
		Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplProposal());
		
		DC_WorkflowService.WorkflowRequest wfRequest = new DC_WorkflowService.WorkflowRequest();
	  wfRequest.entityId = 12345;
	  wfRequest.id = 12345;
	  wfRequest.status = 'PENDING_APPROVAL';
	  wfRequest.workflowRuleName = 'test rule';
	  wfRequest.entityType = 'PROPOSAL';
	  
		DC_DoubleClickWorkflowWrapper wfWrapper = new DC_DoubleClickWorkflowWrapper(dcObjMapping1, wfFldMappings, new List<Object>{wfRequest});
		wfWrapper.next();
		wfWrapper.getField('doubleClickId');
		wfWrapper.getField('entityType');
		wfWrapper.getField('workflowRuleName');
		wfWrapper.getField('status');
		wfWrapper.getField('name');
		wfWrapper.getField('entityId');
		
		wfWrapper.setField('doubleClickId', 12345);
		wfWrapper.setField('entityType', 'PROPOSAL');
		wfWrapper.setField('workflowRuleName', 'test rule');
		wfWrapper.setField('status', 'APPROVE');
		wfWrapper.setField('name', 'test rule');
		wfWrapper.setField('entityId', 12345);
		wfWrapper.getNewRecords();
		wfWrapper.getExistingRecords();
		wfWrapper.getCurrentWrapped();
		wfWrapper.createWrapper(dcObjMapping1, wfFldMappings, new List<Object>{wfRequest});
		Test.stopTest();
	}
	private static void createTestData(){
	goog_dclk_dsm__DC_Authentication__c objDC_Authentication = new goog_dclk_dsm__DC_Authentication__c(goog_dclk_dsm__DC_AccessToken__c ='1324324',goog_dclk_dsm__DC_ExpiresIn__c = 3600 ,goog_dclk_dsm__DC_RefreshToken__c = '454655656',goog_dclk_dsm__DC_TokenType__c = 'Bearer');
    insert objDC_Authentication;
    	
	goog_dclk_dsm__DC_ObjectMapping__c dcObjMapping = DC_TestUtils.createObjectMapping('Proposal', 'Opportunity', false);
	dcObjMapping1 = DC_TestUtils.createObjectMapping('WorkflowRequest', 'WorkflowRequest', null, false);
    insert new List<goog_dclk_dsm__DC_ObjectMapping__c>{dcObjMapping, dcObjMapping1};
    goog_dclk_dsm__DC_FieldMapping__c fldMapping1 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping2 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'custom.SFDC_Opportunity_ID', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Id', Name = 'sfid');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping3 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'probabilityToClose', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Probability', Name = 'probability');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping4 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'status', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345,  Name = 'status');
    
    goog_dclk_dsm__DC_FieldMapping__c fldMapping5 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'entityType',  Name = 'entityType');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping6 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id',  Name = 'DoubleClickId');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping7 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'workflowRuleName',  Name = 'workflowRuleName');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping8 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'status', Name = 'status');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping9 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'workflowRuleName',  Name = 'name');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping10 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'entityId',  Name = 'entityId');
    
    insert new List<goog_dclk_dsm__DC_FieldMapping__c>{fldMapping1,fldMapping2,fldMapping3,fldMapping4,fldMapping5, fldMapping6, fldMapping7, fldMapping8, fldMapping9};
    wfFldMappings.add(fldMapping5);wfFldMappings.add(fldMapping6);wfFldMappings.add(fldMapping7);wfFldMappings.add(fldMapping8);wfFldMappings.add(fldMapping9);wfFldMappings.add(fldMapping10);
    system.debug('Setup success.');
    
    Account acc = DC_TestUtils.createAccount('testCompany', true);
    
    Opportunity opp1 = DC_TestUtils.createOpportunity('testProposal1',System.now().date(),acc.Id,'Prospecting',false);
    opp1.goog_dclk_dsm__DC_ProposalStatus__c = 'DRAFT';
    opp1.goog_dclk_dsm__DC_DoubleClickId__c = '12345';
    Opportunity opp2 = DC_TestUtils.createOpportunity('testOpp2',System.now().date(),acc.Id,'Prospecting',false);
    opp2.goog_dclk_dsm__DC_ProposalStatus__c = 'DRAFT';
    oppList = new List<Opportunity>{opp1,opp2};
    insert oppList;
    
    goog_dclk_dsm__DC_LinkWorkflowFields__c linkWFFields = new goog_dclk_dsm__DC_LinkWorkflowFields__c();
    linkWFFields.name = 'name';
    linkWFFields.goog_dclk_dsm__DC_SortOrder__c = 10;
    linkWFFields.goog_dclk_dsm__DC_DeepLinkToDSMRecord__c = true;
    insert linkWFFields;
	}
}