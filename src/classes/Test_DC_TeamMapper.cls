/**
 */
@isTest
private class Test_DC_TeamMapper {

	static testMethod void myUnitTest() {
		goog_dclk_dsm__DC_ObjectMapping__c dcobjMapping1 = DC_TestUtils.createObjectMapping('Team', 'Team', 'goog_dclk_dsm__DC_Team__c', true);
		List<goog_dclk_dsm__DC_FieldMapping__c> fldMappings = new List<goog_dclk_dsm__DC_FieldMapping__c>();
    
    fldMappings.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId'));
    fldMappings.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'name', goog_dclk_dsm__DC_SalesforceFieldName__c = 'name', Name = 'name'));
    fldMappings.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'description', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_description__c', Name = 'description'));
    
    fldMappings.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'hasAllInventory',  Name = 'hasAllInventory'));
    fldMappings.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'hasAllCompanies',  Name = 'hasAllCompanies'));
    fldMappings.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'teamAccessType',  Name = 'teamAccessType'));
    
    fldMappings.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'companyIds',  Name = 'companyIds'));
    fldMappings.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'adUnitIds',  Name = 'adUnitIds'));
    insert fldMappings;
    
		goog_dclk_dsm__DC_Team__c sfTeam = new goog_dclk_dsm__DC_Team__c(name = 'Team 1', goog_dclk_dsm__DC_DoubleClickId__c = '32193392');
    insert sfTeam;
		Test.startTest();
		Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplTeamService());
		DC_TeamMapper tMapper = DC_TeamMapper.getInstance();
		DC_TeamService.Team team = new DC_TeamService.Team();
		tMapper.wrap(team);
		tMapper.wrap(new List<DC_TeamService.Team>{team});
		
		DC_TeamService.TeamPage teamPage = new DC_TeamService.TeamPage();
	  teamPage.results = new List<DC_TeamService.Team>{team};
	  teamPage.totalResultSetSize = 1;
	  teamPage.startIndex = 0;  
	  
	  tMapper.wrap(teamPage);
	  
	  tMapper.createNewDCRecords(tMapper.wrap(team));
	  
	  boolean isHandlerClass = tMapper.updateFieldsOnAllObjectsWithCreate(tMapper.wrap(team), tMapper.wrap(sfTeam));
	  System.assertEquals(false, isHandlerClass);
	  
	  DC_DoubleClickTeamWrapper wrapper = new DC_DoubleClickTeamWrapper(dcobjMapping1, fldMappings, new List<DC_TeamService.Team>{team});
	  wrapper.next();
	  wrapper.getField('DoubleClickId');
	  wrapper.getField('name');
	  wrapper.getField('description');
	  wrapper.getField('companyIds');
	  wrapper.getField('adUnitIds');
	  wrapper.getField('teamAccessType');
	  wrapper.getField('hasAllCompanies');
	  wrapper.getField('hasAllInventory');
	  
	  wrapper.setField('DoubleClickId', 12345);
	  wrapper.setField('name', 'test');
	  wrapper.setField('description', 'test');
	  wrapper.setField('companyIds', new List<Long>{12345});
	  wrapper.setField('adUnitIds', new List<String>{'test'});
	  wrapper.setField('teamAccessType', 'test');
	  wrapper.setField('hasAllCompanies', true);
	  wrapper.setField('hasAllInventory', true);
	  wrapper.createWrapper(dcobjMapping1, fldMappings, new List<DC_TeamService.Team>{team});
	  DC_GenericWrapper gw = wrapper.getCurrentWrapped();
	  System.assert(gw != null);
	  try{
	  	wrapper.getExistingRecords();
	  }catch(exception e){}
	  try{
	  	wrapper.getNewRecords();
	  }catch(exception e){}
		Test.stopTest();
	}
}