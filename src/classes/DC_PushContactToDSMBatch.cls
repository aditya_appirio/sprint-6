public with sharing class DC_PushContactToDSMBatch extends DC_PushRecordToDSMProcess implements
																										Database.Batchable<sObject>,
																									  Database.AllowsCallouts,
																									  Database.Stateful {
	public DC_PushContactToDSMBatch() {}
 	
 	public DC_PushContactToDSMBatch(string soql) {
 	  query = soql;
 	}
 	
 	public DC_PushContactToDSMBatch(boolean useReadyForDSM) {
 		readyForDSM = useReadyForDSM;
 	}
 	
  public override string getContext() {
  	return 'Push Contact to DSM';
  }
  
  // DC_PushRecordToDSMProcess ==========================
  public override void executeSync(List<SObject> sObjectList){
  	DC_ContactContactConnector.getInstance().syncSFContactsToDCContacts((List<Contact>)sObjectList);
  }
  public override String getDefaultQuery(){
  	string queryColumnList = DC_ObjectMapper.getInstance().getCommaSeparatedSalesforceFieldList('Contact');
  	
  	String defaultQry = 
  	       'SELECT ' + queryColumnList + ' ' + 
  	       'FROM Contact ' + 
  	       'WHERE goog_dclk_dsm__DC_DoubleClickId__c = null AND accountid != null ' +
  	       ((readyForDSM) ? 'AND goog_dclk_dsm__DC_ReadyForDSM__c = :readyForDSM' : '');
           
    return defaultQry;
  }
  // END DC_PushRecordToDSMProcess ==========================
}