// 
// (c) 2014 Appirio
//
// DC_SalesforceUserManager : Manager class for SFDC's DC User object 
// T-251378
// 20 Feb 2014   Anjali K (JDC)      Original
//
public with sharing class DC_SalesforceUserManager extends DC_SalesforceManager {
    
    // enforce singleton 
    private DC_UserMapper mapper {
    get {
      if(mapper == null) {
        mapper = DC_UserMapper.getInstance();
      }
      return mapper;
    }
    set;
  }
  
  public DC_GenericWrapper getSalesforceUsers(Set<Id> userIds) {
    String fieldNames = this.mapper.getCommaSeparatedSalesforceFieldList('User Batch');
    String query = 'SELECT ' + fieldNames + ' FROM User WHERE id = :userIds';
     
    return mapper.wrap((List<User>)Database.query(query)); 
  }

  // get the sfdc dc_user related to the passed in id
  // id   string    user id for the DC_User to retrieve
  public DC_GenericWrapper getUser(String id) {
    String fieldNames = this.mapper.getCommaSeparatedSalesforceFieldList('User');
    System.debug('fieldNames::' + fieldNames);
    String query = 'SELECT ' + fieldNames + ' FROM goog_dclk_dsm__DC_User__c where id = :id';
     
    return mapper.wrap((List<goog_dclk_dsm__DC_User__c>)Database.query(query));
  }
 
  // get the sfdc dc users for the ids provided and return in a generic wrapper
  // ids  List<Id>  list of ids for which to retrieve sfdc dc users
  public DC_GenericWrapper getUsers(List<Id> ids) {
    String fieldNames = this.mapper.getCommaSeparatedSalesforceFieldList('User');
    String query = 'SELECT ' + fieldNames + ' FROM goog_dclk_dsm__DC_User__c where id in :ids';

    return mapper.wrap((List<goog_dclk_dsm__DC_User__c>)Database.query(query));
  }

  // get the sfdc dc user records from the passed sfdc dc users 
  public DC_GenericWrapper getUsers(List<goog_dclk_dsm__DC_User__c> users) {
    if(users == null || users.size() == 0) {
      return null;
    }
    List<Id> ids = new List<Id>(new Map<Id, goog_dclk_dsm__DC_User__c>(users).keySet());
    return getUsers(ids);
  }
  
  //update User
  public void updateUsers(DC_GenericWrapper users) {
    List<goog_dclk_dsm__DC_User__c> userList = new List<goog_dclk_dsm__DC_User__c>();
    for(Object o : users.getObjects()) {
      userList.add((goog_dclk_dsm__DC_User__c)o);
    }
    if(userList.size() > 0) {
      DC_SalesforceDML.getInstance().registerUpdate(userList);
    }
  }
  
  //get User
  public DC_GenericWrapper getUsers(DC_GenericWrapper wrapper) {
    wrapper.gotoBeforeFirst();
    List<Id> userIds = new List<Id>();
    while(wrapper.hasNext()) {
      wrapper.next();
      userIds.add((String)wrapper.getField('sfid'));
    }
    return getUsers(userIds);
  }
  
  public DC_GenericWrapper getMatchingSalesforceRecords(DC_GenericWrapper wrapper) {
    
    wrapper.gotoBeforeFirst();
    List<String> dcIds = new List<String>();
    
    string opLineId;
    while(wrapper.hasNext()) {
      wrapper.next();
      dcIds.add(String.valueOf(wrapper.toLong(wrapper.getField('doubleclickid'))));
    }
    return mapper.wrap([SELECT id, goog_dclk_dsm__dc_doubleclickid__c FROM goog_dclk_dsm__DC_User__c WHERE goog_dclk_dsm__dc_doubleclickid__c IN :dcIds]); 
  }  
  
  //update OpportunityLineItem
  public DC_SalesforceDML.DC_SalesforceDMLStatus upsertUsers(DC_GenericWrapper users) {
    List<goog_dclk_dsm__DC_User__c> userList = new List<goog_dclk_dsm__DC_User__c>();
    
    for(Object u : users.getObjects()) {
      userList.add((goog_dclk_dsm__DC_User__c)u);
    }

    DC_SalesforceDML.DC_SalesforceDMLStatus status;
    if(userList.size() > 0) {
      status = DC_SalesforceDML.getInstance().registerUpsert(userList);
    }
    
    return status;
  }  
    
    

}