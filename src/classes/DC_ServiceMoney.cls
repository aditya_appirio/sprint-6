public virtual with sharing class DC_ServiceMoney {
  public String currencyCode;
  public Long microAmount;

  public String getCurrencyCode() {
    return this.currencyCode;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public Long getMicroAmount() {
    return this.microAmount;
  }

  public void setMicroAmount(Long microAmount) {
    this.microAmount = microAmount;
  }
}