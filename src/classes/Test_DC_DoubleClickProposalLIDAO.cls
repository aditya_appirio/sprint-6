// 
// (c) 2014 Appirio
//
// Test_DC_DoubleClickProposalLIDAO: test class for DC_DoubleClickProposalLIDAO 
//
@isTest
private class Test_DC_DoubleClickProposalLIDAO {

  static testMethod void myUnitTest() {
    Account a1 = new Account(name = 'test1');
    a1.goog_dclk_dsm__DC_ReadyForDSM__c = true;
    insert a1;
     
    Opportunity oppty = new Opportunity(name = 'testProposal1', AccountId = a1.id, StageName = 'Closed/Won', CloseDate = date.today());
    insert oppty;
    
    goog_dclk_dsm__DC_ObjectMapping__c objMapping1 = new goog_dclk_dsm__DC_ObjectMapping__c(goog_dclk_dsm__DC_DoubleClickObjectName__c='PROPOSAL_LINE_ITEM', goog_dclk_dsm__DC_SalesforceObjectName__c = 'OpportunityLineItem', name = 'OpportunityLineItem');
    insert objMapping1;
    
    goog_dclk_dsm__DC_FieldMapping__c fldMapping1 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping2 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'custom.SFDC_OpportunityLineItem_ID', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Id', Name = 'sfid');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping3 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'proposalId', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'OpportunityId', Name = 'ProposalId');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping4 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'isArchived',  goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_isArchived__c', Name = 'isArchived');
    insert new List<goog_dclk_dsm__DC_FieldMapping__c>{fldMapping1, fldMapping2, fldMapping3, fldMapping4};
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplProductLineItem());
    Test.startTest();
    DC_DoubleClickOAuthHandler oAuthHandler = new DC_DoubleClickOAuthHandler(Userinfo.getUserId());
    
    DC_DoubleClickProposalLineItemDAO proposalLIDAO = new DC_DoubleClickProposalLineItemDAO(oAuthHandler);
    String query = null;
    
    DC_ProposalLineItemService.String_ValueMapEntry[] params = new List<DC_ProposalLineItemService.String_ValueMapEntry>();
    DC_ProposalLineItemService.Statement statement = proposalLIDAO.createProposalLIStatement(query, params);
    System.assert(statement != null);
    System.assertEquals(statement.query, query);
     
    DC_ProposalLineItemService.ProposalLineItem pdctLnItem = new DC_ProposalLineItemService.ProposalLineItem();
    pdctLnItem.name = 'testPDCTLnItem';
    pdctLnItem.proposalId = 123456;
    pdctLnItem.isArchived = true;
    
    DC_ProposalLineItemService.ProposalLineItem[] pdctLnItems = 
    proposalLIDAO.createProposalLineItems(new List<DC_ProposalLineItemService.ProposalLineItem>{pdctLnItem});
    System.assert(pdctLnItems.size() > 0);
    System.assertEquals(pdctLnItems[0].name, 'testProposalLI');
    
    DC_ProposalLineItemService.ProposalLineItemPage proposalLIPage = new DC_ProposalLineItemService.ProposalLineItemPage();
    proposalLIPage.results = new List<DC_ProposalLineItemService.ProposalLineItem>{pdctLnItem};     
    proposalLIPage.totalResultSetSize = 2;
    proposalLIPage.startIndex = 0;
    
    //proposalLIDAO.updateProposalLineItem(pdctLnItem);
    
    proposalLIDAO.updateProposalLineItems(new List<DC_ProposalLineItemService.ProposalLineItem>{pdctLnItem});
    
    query = 'WHERE Name = \'testPDCTLnItem\'';
    proposalLIPage = proposalLIDAO.getProposalLineItemsByStatement(query);
    System.assert(proposalLIPage != null);
    System.assertEquals(proposalLIPage.totalResultSetSize, 3);
    System.assertEquals(pdctLnItems[0].name, 'testProposalLI');
    
    //DC_ProposalLineItemService.ProposalLineItem proposalLI1 = proposalLIDAO.getProposalLineItem(123456, true);
    //System.assertEquals(proposalLI1.Name, 'testProposalLI1');
    
    DC_ProposalLineItemMapper mapper = DC_ProposalLineItemMapper.getInstance();
    DC_GenericWrapper wrapper = mapper.wrap(pdctLnItems);
    mapper.wrap(proposalLIPage);
    
    DC_GenericWrapper gw = mapper.createNewDCProposalLIs(mapper.wrap(pdctLnItem));
    System.assert(gw.size() > 0);
    
    try{
      mapper.linkOpportunityLIAndProposalLI(mapper.wrap(pdctLnItem), wrapper);
    }catch(Exception e){}
    
    mapper.updateFieldsOnAllObjectsWithCreate(mapper.wrap(pdctLnItem), wrapper);
    mapper.addNewRecordFromSource(mapper.wrap(pdctLnItem), wrapper);
    mapper.getExistingOpportunitiesProposalLI(wrapper);
    
    mapper.removeArchivedProposalLines(wrapper);
    try{
      mapper.updateField(wrapper,fldMapping3 , 123);
    }catch(Exception e){}
    Test.stopTest();
    
  }
}