// 
// (c) 2014 Appirio
//
// DC_DoubleClickProposalLineItemWrapper 
// T-249484
// 13 Feb 2014   Ankit Goyal (JDC)      Copied From DC_DoubleClickProposalWrapper
//
public with sharing class DC_DoubleClickProposalLineItemWrapper extends DC_DoubleClickWrapper{
  
  private DC_CustomFieldManager customFieldManager; 

  //Constructor : pass values to DC_DoubleClickWrapper
  public DC_DoubleClickProposalLineItemWrapper(goog_dclk_dsm__DC_ObjectMapping__c objectMapping, List<goog_dclk_dsm__DC_FieldMapping__c> fieldMappings, List<Object> objects) {
    super(objectMapping, fieldMappings, objects); //pass values to DC_DoubleClickWrapper

    this.customFieldManager = DC_ManagerFactory.getInstance().getCustomFieldManager();
  }

  //Get Current Proposal
  private DC_ProposalLineItemService.ProposalLineItem getCurrentProposalLI() {
    if(this.currentObject == null){
      throw new DC_ConnectorException(DC_Constants.ARR_OUT_OF_BOUND_ERR);
    } else if(!(this.currentObject instanceOf DC_ProposalLineItemService.ProposalLineItem)){
        throw new DC_ConnectorException(DC_Constants.INVALID_PROPOSAL_MAPPING);
    } else{
        return (DC_ProposalLineItemService.ProposalLineItem) this.currentObject;
    }
  } 
  
	//Modified on 19th march
  private String getTextCustomFieldValue(Long fieldId) {
    if(getCurrentProposalLI().customFieldValues != null) {

      for(DC_ProposalLineItemService.BaseCustomFieldValue fieldValue : getCurrentProposalLI().customFieldValues) {
        
        if (fieldValue instanceOf DC_ProposalLineItemService.CustomFieldValue) {
          DC_ProposalLineItemService.CustomFieldValue curfield = (DC_ProposalLineItemService.CustomFieldValue)fieldValue;
          if(curfield.customFieldId == fieldId) {    
            if (curfield.value instanceOf DC_ProposalLineItemService.TextValue) {
              return ((DC_ProposalLineItemService.TextValue)curfield.value).value;
            }
          }
        }
      } 
    }

    return null;
  }
  //Get DC Field of Proposal respective to Opportunity Field (Param)
  //@param : field => SFDC Field
  public override Object getField(String field) {
  	
    String fieldName = getDoubleClickFieldName(field.toLowerCase());
    System.debug('fieldName:::' + fieldName);
    if(fieldName.equalsIgnoreCase('id')) {
      return getCurrentProposalLI().id;
    } else if(fieldName.equalsIgnoreCase('name')) {
      return getCurrentProposalLI().name;
    } else if(fieldName.equalsIgnoreCase('proposalId')) {
      return getCurrentProposalLI().proposalId;      
    } else if(fieldName.equalsIgnoreCase('cost')) {
      return getCurrentProposalLI().cost.microAmount / 1000000;   
    //Added for T-256464   : START   
    } else if(fieldName.equalsIgnoreCase('notes')) {
      return getCurrentProposalLI().notes;      
    } else if(fieldName.equalsIgnoreCase('baseRate')) {
      return getCurrentProposalLI().baseRate.microAmount / 1000000;          
    } else if(fieldName.equalsIgnoreCase('productId')) {
      return getCurrentProposalLI().productId;      
    } else if(fieldName.equalsIgnoreCase('dfpLineItemId')) {
      return getCurrentProposalLI().dfpLineItemId;          
    } else if(fieldName.equalsIgnoreCase('creativePlaceholders')) {
      return getCurrentProposalLI().creativePlaceholders;          
    } else if(fieldName.equalsIgnoreCase('isArchived')) {
      return getCurrentProposalLI().isArchived;          
    }
    else if(fieldName.equalsIgnoreCase('goalUnit')) {
      return getCurrentProposalLI().goal.units;          
    }
    //Added for T-256464 : END
    else if(fieldName.startsWithIgnoreCase('custom')) {
      Long customFieldId = getCustomFieldId(fieldName);

      if(customFieldId != null) {
        return getTextCustomFieldValue(customFieldId);
      } else {
        throw new DC_ConnectorException(DC_Constants.CUSTOM_FIELD_ERROR_STR);
      }
    }
    else {
      throw new DC_ConnectorException(DC_Constants.getInvalid_Mapping_ProposalLI(field));
    }
  }
  
  private void setTextCustomFieldValue(Long fieldId, String value) {
    DC_ProposalLineItemService.BaseCustomFieldValue field = new DC_ProposalLineItemService.BaseCustomFieldValue();
    field.value = new DC_ProposalLineItemService.Value();
    field.customFieldId = fieldId;
    ((DC_ProposalLineItemService.Value)field.value).value = this.toString(value);

    if(getCurrentProposalLI().customFieldValues == null) {
      getCurrentProposalLI().customFieldValues = new List<DC_ProposalLineItemService.BaseCustomFieldValue>();
    } else {
      Integer idx = 0;
      for(DC_ProposalLineItemService.BaseCustomFieldValue fieldValue : getCurrentProposalLI().customFieldValues) {
        if(fieldValue.customFieldId == fieldId) {
          getCurrentProposalLI().customFieldValues.remove(idx);
          break;
        } 

        idx++;
      }
    }

    getCurrentProposalLI().customFieldValues.add(field);
  }

  //Set value into DC Field
  //@param : Field (SFDC field), value : sfdc field value
  public override void setField(String field, Object value) {
    String fieldName = getDoubleClickFieldName(field);
    System.debug('field::' + field);
    System.debug('fieldName::' + fieldName);
    try{
      if(fieldName.equalsIgnoreCase('id')) {
        getCurrentProposalLI().id = this.toInteger(value);
      } else if(fieldName.equalsIgnoreCase('name')) {
          getCurrentProposalLI().name = this.toString(value); 
      } 
      //Added for T-256464   : START   
      else if(fieldName.equalsIgnoreCase('proposalId')) {
          getCurrentProposalLI().proposalId = this.toLong(value); 
      } else if(fieldName.equalsIgnoreCase('notes')) {
          getCurrentProposalLI().notes = this.toString(value); 
      } else if(fieldName.equalsIgnoreCase('productId')) {
          getCurrentProposalLI().productId = this.toString(value); 
          //added by debug 30-oct-2014 to remove error 
          getCurrentProposalLI().goal.units = 1; 
      } else if(fieldName.equalsIgnoreCase('dfpLineItemId')) {
          getCurrentProposalLI().dfpLineItemId = this.toLong(value); 
      } 
      
      else if(fieldName.equalsIgnoreCase('cost')) {
      		getCurrentProposalLI().cost = new DC_ProposalLineItemService.Money();
      		System.debug('this.toLong(value):::' + this.toLong(value));
          getCurrentProposalLI().cost.microAmount = this.toLong(value); 
          System.debug('getCurrentProposalLI().cost.microAmount:::' + getCurrentProposalLI().cost.microAmount);
      } else if(fieldName.equalsIgnoreCase('baseRate')) {
          getCurrentProposalLI().baseRate = new DC_ProposalLineItemService.Money();
          getCurrentProposalLI().baseRate.microAmount = this.toLong(value); 
      } else if(fieldName.equalsIgnoreCase('creativePlaceholders')) {
          getCurrentProposalLI().creativePlaceholders = (DC_ProposalLineItemService.CreativePlaceholder[]) value;
      } else if(fieldName.equalsIgnoreCase('isArchived')) {
          getCurrentProposalLI().isArchived = this.toBoolean(value); 
      } 
      else if(fieldName.startsWithIgnoreCase('custom')) {
          System.debug('########################### ' + fieldName);
          Long customFieldId = getCustomFieldId(fieldName);

          if(customFieldId != null) {
            setTextCustomFieldValue(customFieldId, (String)value);
          } else {
            throw new DC_ConnectorException(DC_Constants.CUSTOM_FIELD_ERROR_STR);
          }
      }
      //Added for T-256464   : END   
      else {
        throw new DC_ConnectorException(DC_Constants.getInvalid_Mapping_Proposal(field));
      }
    }
    catch(System.TypeException e){
      throw new DC_ConnectorException(e+DC_Constants.FOR_FLD_STR+field);
    }
  }
  
  //create new records
  public override DC_GenericWrapper getNewRecords() {
    this.gotoBeforeFirst();
    List<Object> result = new List<Object>();
    System.debug('this:::' + this);
    while(this.hasNext()) {
      this.next();
      System.debug('this::in:' + this);
      if(this.getField('DoubleClickId') == null) {
        result.add(this.currentObject);
      }
    }
    
    return new DC_DoubleClickProposalLineItemWrapper(this.objectMapping, this.fieldMappings, result);
  }
  
  public override DC_GenericWrapper getExistingRecords() {
    this.gotoBeforeFirst();
    List<Object> result = new List<Object>();
    
    while(this.hasNext()) {
      this.next();
      
      if(this.getField('DoubleClickId') != null) {
        result.add(this.currentObject);
      }
    }
    
    return new DC_DoubleClickProposalLineItemWrapper(this.objectMapping, this.fieldMappings, result);
  }
  
  public override DC_GenericWrapper getCurrentWrapped() {
    return new DC_DoubleClickProposalLineItemWrapper(this.objectMapping, this.fieldMappings, new object[] { this.currentObject });
  }
  
  public override DC_GenericWrapper createWrapper(goog_dclk_dsm__DC_ObjectMapping__c objectMapping, List<goog_dclk_dsm__DC_FieldMapping__c> fieldMappings, List<Object> objects) {
    return new DC_DoubleClickProposalLineItemWrapper(objectMapping, fieldMappings, objects);
  }

  
}