// 
// (c) 2014 Appirio
//
// implements WebServiceMock for webServicecallout test classes 
//
// 24 Jan 2014    Anjali K (JDC)      Original
@isTest
global class DC_WebServiceMockImplWorkflowService implements WebServiceMock{
  global void doInvoke(Object stub, Object request, Map<String, Object> response,
                    String endpoint, String soapAction, String requestName,
                    String responseNS, String responseName, String responseType){
      DC_WorkflowService.WorkflowRequest wfRequest = new DC_WorkflowService.WorkflowRequest();
      wfRequest.entityId = 12345;
      wfRequest.id = 12345;
      wfRequest.status = 'PENDING_APPROVAL';
      wfRequest.workflowRuleName = 'test rule';
      wfRequest.entityType = 'PROPOSAL';
      
      DC_WorkflowService.WorkflowRequestPage page = new DC_WorkflowService.WorkflowRequestPage();
      page.results = new List<DC_WorkflowService.WorkflowRequest>{wfRequest};
			page.totalResultSetSize = 1;
			page.startIndex = 0;	
			
			
			DC_ProposalService.Date_x dateX = new DC_ProposalService.Date_x();
	    dateX.year = Date.today().year();
	    dateX.month = Date.today().month();
	    dateX.day = Date.today().addDays(10).day();
	    
	    DC_ProposalService.DateTime_x dt = new DC_ProposalService.DateTime_x();
	    dt.date_x = dateX;
       
      DC_ProposalService.Proposal proposal1 = new DC_ProposalService.Proposal();
      proposal1.name = 'testProposal1';
      proposal1.id = 12345;
      proposal1.lastModifiedDateTime = dt;
	    proposal1.startDateTime = dt;
	    proposal1.endDateTime = dt;
      
      DC_ProposalService.ProposalPage proposalPage = new DC_ProposalService.ProposalPage();
      proposalPage.results = new List<DC_ProposalService.Proposal>{proposal1};    
      proposalPage.totalResultSetSize = 1;
      proposalPage.startIndex = 0;
      
			DC_WorkflowService.UpdateResult updateRes = new DC_WorkflowService.UpdateResult();
			updateRes.numChanges = 1;
			System.debug('request::test:: ' + request);
      if(request instanceof DC_WorkflowService.performWorkflowRequestAction_element){
      	  DC_WorkflowService.performWorkflowRequestActionResponse_element resp = new DC_WorkflowService.performWorkflowRequestActionResponse_element();
	    	  resp.rval = updateRes;
	    	  response.put('response_x', resp);
      }else if(request instanceof DC_WorkflowService.getWorkflowRequestsByStatement_element){
      		DC_WorkflowService.getWorkflowRequestsByStatementResponse_element resp = new DC_WorkflowService.getWorkflowRequestsByStatementResponse_element();
	    	  resp.rval = page;
	    	  response.put('response_x', resp);
      }else if(request instanceof DC_ProposalService.getProposalsByStatement_element){
        DC_ProposalService.getProposalsByStatementResponse_element resp = new DC_ProposalService.getProposalsByStatementResponse_element();
        resp.rval = proposalPage;
        response.put('response_x', resp);
          //response.put('response_x', new DC_ProposalService.getProposalsByStatementResponse_element());
      }
      return;
  }
}