// 
// (c) 2014 Appirio
//
// DC_CompanyMapper
// T-246423
// 06 Feb 2014    Ankit Goyal (JDC)      Original 
//
public with sharing class DC_CompanyMapper extends DC_ObjectMapper {
  
  private static DC_CompanyMapper instance;
  //constructor
  private DC_CompanyMapper(){
    super();
  }
  public static DC_CompanyMapper getInstance() {
    if(instance == null) {
      instance = new DC_CompanyMapper();
    } 
    return instance;
  }

  // Overrided method wrap : takes DC_CompanyService.Company as param and call wrap(List<DC_CompanyService.Company> companies)
  public DC_GenericWrapper wrap(DC_CompanyService.Company company) {
    List<DC_CompanyService.Company> companyList = new List<DC_CompanyService.Company>();
    companyList.add(company);
    DC_GenericWrapper result = wrap(companyList);

    return result;
  }
  
  // Overrided method wrap : takes List<DC_CompanyService.Company> as param and convert into DC_DoubleClickCompanyWrapper
  public DC_GenericWrapper wrap(List<DC_CompanyService.Company> companies) {
    return new DC_DoubleClickCompanyWrapper(this.getObjectMappingByName('account'), this.getFieldMappingsByName('account'), companies);
  }
  
  // Create New company in DSM for Accounts that are not in DSM
  // accounts   DC_GenericWrapper   accounts to create as companies in DSM
  public override DC_GenericWrapper createNewDCRecords(DC_GenericWrapper accounts) {
    DC_GenericWrapper companies = this.wrap(new List<DC_CompanyService.Company>());
    
    accounts.gotoBeforeFirst();
    
    while(accounts.hasNext()) {
      accounts.next();
      companies.add(new DC_CompanyService.Company());
      companies.gotoLast();
      updateFields(accounts, companies);
    }
    
    return companies; 
  }

  // Links the current company with the current account in the wrappers
  public override void linkSFRecordAndDCRecord(DC_GenericWrapper account, DC_GenericWrapper company) {
    System.debug('>>>linkSFRecordAndDCRecord: CompMapper');
    account.setField('DoubleClickId', company.getField('DoubleClickId'));
    company.setField('sfid', account.getField('sfid'));
  }
}