/**
 */
@isTest
private class Test_DC_TeamService {

	static testMethod void testDCTeamService() {
		 
    Test.startTest(); 
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplTeamService());
    DC_TeamService.CommonError ce = new DC_TeamService.CommonError();
    System.assert(ce != null);
    
    DC_TeamService.getCurrentTeamResponse_element v1 = new DC_TeamService.getCurrentTeamResponse_element();
    DC_TeamService.getTeamsByStatementResponse_element wfReq = new DC_TeamService.getTeamsByStatementResponse_element();
    DC_TeamService.updateTeam_element v2 = new DC_TeamService.updateTeam_element();
    DC_TeamService.DateTimeValue dtTmVal = new DC_TeamService.DateTimeValue();
    DC_TeamService.Statement statement = new DC_TeamService.Statement();
    DC_TeamService.createTeams_element wra = new DC_TeamService.createTeams_element();
    DC_TeamService.PublisherQueryLanguageContextError pqlErr = new DC_TeamService.PublisherQueryLanguageContextError();
    DC_TeamService.ApiError apiErr = new DC_TeamService.ApiError();
    DC_TeamService.Date_x dateX = new DC_TeamService.Date_x();
    
    DC_TeamService.BooleanValue boolVal = new DC_TeamService.BooleanValue();
    DC_TeamService.getTeam_element TeamEle = new DC_TeamService.getTeam_element();
    DC_TeamService.QuotaError ue = new DC_TeamService.QuotaError();
    DC_TeamService.FeatureError fe = new DC_TeamService.FeatureError();
    DC_TeamService.AuthenticationError ae = new DC_TeamService.AuthenticationError();
    DC_TeamService.PermissionError pe = new DC_TeamService.PermissionError();
    DC_TeamService.PublisherQueryLanguageSyntaxError PQLSyntaxErr = new DC_TeamService.PublisherQueryLanguageSyntaxError();
    
    DC_TeamService.TeamPage quotaErr = new DC_TeamService.TeamPage();
    DC_TeamService.DateValue dtVal = new DC_TeamService.DateValue();
    DC_TeamService.ApplicationException appExcep = new DC_TeamService.ApplicationException();
    DC_TeamService.String_ValueMapEntry strValMapEntry = new DC_TeamService.String_ValueMapEntry();
    DC_TeamService.createTeam_element liOpErr = new DC_TeamService.createTeam_element();
    
    DC_TeamService.InternalApiError iae = new DC_TeamService.InternalApiError();
    DC_TeamService.Authentication au = new DC_TeamService.Authentication();
    DC_TeamService.SetValue sv = new DC_TeamService.SetValue();
    DC_TeamService.createTeamsResponse_element req = new DC_TeamService.createTeamsResponse_element();
    DC_TeamService.NotNullError nne = new DC_TeamService.NotNullError();
    DC_TeamService.updateTeamsResponse_element uur = new DC_TeamService.updateTeamsResponse_element();
    DC_TeamService.NumberValue numVal = new DC_TeamService.NumberValue();
    DC_TeamService.TypeError cl = new DC_TeamService.TypeError();
    DC_TeamService.DateTime_x dt = new DC_TeamService.DateTime_x();
    DC_TeamService.StatementError se = new DC_TeamService.StatementError();
    DC_TeamService.ApiException apiExce = new DC_TeamService.ApiException();
    DC_TeamService.Value val = new DC_TeamService.Value();
    DC_TeamService.ApiVersionError v4= new DC_TeamService.ApiVersionError();
    DC_TeamService.OAuth oAuth = new DC_TeamService.OAuth();
    DC_TeamService.updateTeams_element v5 = new DC_TeamService.updateTeams_element();
    DC_TeamService.createTeamResponse_element v6 = new DC_TeamService.createTeamResponse_element();
    DC_TeamService.SoapResponseHeader srh1 = new DC_TeamService.SoapResponseHeader();
    DC_TeamService.getTeamResponse_element TeamResEle  = new DC_TeamService.getTeamResponse_element();
    DC_TeamService.Team team = new DC_TeamService.Team();
    DC_TeamService.updateTeamResponse_element TeamRes = new DC_TeamService.updateTeamResponse_element();
    DC_TeamService.getCurrentTeam_element getCurrTeamEle = new DC_TeamService.getCurrentTeam_element();
    DC_TeamService.RequiredError reqErr = new DC_TeamService.RequiredError();
    DC_TeamService.UniqueError uniqueErr = new DC_TeamService.UniqueError();
    DC_TeamService.TeamServiceInterfacePort port = new DC_TeamService.TeamServiceInterfacePort();
      System.assert(port != null);
      port.getTeam(12345);
      port.createTeam(team);
      try{
        port.getTeamsByStatement(statement);
      }catch(exception e){}
      port.getCurrentTeam();
      port.createTeams(new List<DC_TeamService.Team>{team});
      port.updateTeams(new List<DC_TeamService.Team>{team});
      port.updateTeam(team);
    DC_TeamService.SoapRequestHeader srh = new DC_TeamService.SoapRequestHeader();
    DC_TeamService.TeamError teamErr = new DC_TeamService.TeamError();
    DC_TeamService.getTeamsByStatement_element getTeamStatement = new DC_TeamService.getTeamsByStatement_element();
    DC_TeamService.ServerError serverErr = new DC_TeamService.ServerError();
    DC_TeamService.TextValue txtVal = new DC_TeamService.TextValue();
    Test.stopTest();
	}
}