// 
// (c) 2014 Appirio
//
// DC_DoubleClickProposalDAO
// T-245174 : Create Push to DSM functionality for Opportunities
// 
// 04 Feb 2014    Ankit Goyal (JDC)      Original
//
public with sharing class DC_DoubleClickProposalDAO extends DC_DoubleClickDAO{
  protected override void tokenRefreshed() {
    this.proposalService = null; 
  }
  
  //Construtor
  public DC_DoubleClickProposalDAO(DC_DoubleClickOAuthHandler authHandler) {
    super(authHandler);
  }
 
  // get a configured doubleclick opportunity interface
  private DC_ProposalService.ProposalServiceInterfacePort proposalService {
    get {
      if(this.proposalService == null) {
        this.proposalService = DC_ServicesFactory.getProposalService(this.authHandler);
      } 

      return this.proposalService;
    }
    set;
  }

  
  //Inner class for getting Proposal by statement
  private class GetProposalByStatementCommand implements DC_DoubleClickDAO.Command {
    public DC_ProposalService.ProposalPage result;
    public DC_DoubleClickProposalDAO context;
    public DC_ProposalService.Statement statement;
    
    public GetProposalByStatementCommand(DC_DoubleClickProposalDAO context, DC_ProposalService.Statement statement) {
      this.context = context;
      this.statement = statement;
    }
    
    public void execute() {
      this.result = context.proposalService.getProposalsByStatement(statement);
      System.debug('@@ Service: Total Result Set Size: ' + this.result.totalResultSetSize);
      
      if (this.result == null || this.result.results == null || this.result.results.size() == 0) {
        return;        
      }
      for (DC_ProposalService.Proposal proposal : this.result.results) {
        if ((proposal.customFieldValues == null || proposal.customFieldValues.size() == 0) && (proposal.dropDownCustomFieldValues == null || proposal.dropDownCustomFieldValues.size() == 0)) {
          continue;
        }
        if(proposal.customFieldValues != null){
	        for (integer i = 0; i < proposal.customFieldValues.size(); i++) {
	          System.debug('@@ Custom Field Text: ' + proposal.customFieldValues[i]);
	          System.debug('@@ Custom Field Type: ' + proposal.customFieldValues[i].BaseCustomFieldValue_Type);
	
	          proposal.customFieldValues[i] = copyCustomField(proposal.customFieldValues[i]);
	        }
        }
				System.debug('::proposal.dropDownCustomFieldValues::' + proposal.dropDownCustomFieldValues);
				if(proposal.dropDownCustomFieldValues != null){
	        for (integer i = 0; i < proposal.dropDownCustomFieldValues.size(); i++) {
	          System.debug('@@ Custom Field Text: ' + proposal.dropDownCustomFieldValues[i]);
	          System.debug('@@ Custom Field Type: ' + proposal.dropDownCustomFieldValues[i].BaseCustomFieldValue_Type);
	
	          proposal.dropDownCustomFieldValues[i] = copyCustomField(proposal.dropDownCustomFieldValues[i]);
	        }
				}
        System.debug('@@ Copied Custom Fields: ' + proposal.customFieldValues);
      }
    }
    public DC_ProposalService.BaseCustomFieldValue copyCustomField(DC_ProposalService.BaseCustomFieldValue inval) {
      DC_ProposalService.BaseCustomFieldValue ret;
      if (inval.BaseCustomFieldValue_Type == 'CustomFieldValue') {
        DC_ProposalService.CustomFieldValue cfv = new DC_ProposalService.CustomFieldValue();
        cfv.customFieldId = inval.customFieldId;
        cfv.value = copyCustomFieldValue(inval.value); // copy value
        System.debug('@@ Value after copy: ' + cfv.value);
        ret = cfv;
      } else if (inval.BaseCustomFieldValue_Type == 'DropDownCustomFieldValue') {
        DC_ProposalService.DropDownCustomFieldValue ddcfv = new DC_ProposalService.DropDownCustomFieldValue();
        ddcfv.customFieldId = inval.customFieldId;
        ddcfv.customFieldOptionId = inval.customFieldOptionId; // copy value
        ret = (DC_ProposalService.BaseCustomFieldValue)ddcfv;
      } 
      //ret.customFieldId = inval.customFieldId;
      //ret.value = inval.value; 
      return ret;
    }

    public DC_ProposalService.Value copyCustomFieldValue(DC_ProposalService.Value inval) {
      DC_ProposalService.Value ret;
      System.debug('@@ Service value: ' + inval.value);

      if (inval.value_type == 'BooleanValue') {
        DC_ProposalService.BooleanValue bv = new DC_ProposalService.BooleanValue();
        bv.value = boolean.valueOf(inval.value);
        ret = bv;
      /*} else if (inval.value_type == 'DateTimeValue') {
        DC_ProposalService.DateTimeValue dtv = new DC_ProposalService.DateTimeValue();
        dtv.value = new DC_ProposalService.DateTime_x();
        dtv.value.date_x = new DC_ProposalService.Date_x();
        dtv.value.date_x.year = 
        ret = dtv;
      } else if (inval.value_type == 'DateValue') {
        DC_ProposalService.DateValue dv = new DC_ProposalService.DateValue();
        ret = dv; */
      } else if (inval.value_type == 'NumberValue') {
        DC_ProposalService.NumberValue nv = new DC_ProposalService.NumberValue();
        nv.value = inval.value;
        ret = nv;
      /*} else if (inval.value_type == 'SetValue') {
        DC_ProposalService.SetValue sv = new DC_ProposalService.SetValue();
        ret = sv;*/
      } else if (inval.value_type == 'TextValue') {
        DC_ProposalService.TextValue tv = new DC_ProposalService.TextValue();
        tv.value = inval.value;
        System.debug('@@ TextValue: ' + tv.value);
        ret = tv;
      }

      return ret;
    }    
  }


  
  // retrieve a company using the passed in query string
  public DC_ProposalService.ProposalPage getProposalsByStatement(String query) {
  	System.debug('query:::' + query);
    DC_ProposalService.Statement statement = this.createProposalStatement(query);
    System.debug('statement:::' + statement);
    GetProposalByStatementCommand cmd = new GetProposalByStatementCommand(this, statement);
    
    this.invokeAPI(cmd, true);
    return cmd.result;
  }
  
  //Inner class for creating Proposals
  private class CreateProposalsCommand implements DC_DoubleClickDAO.Command {
    public DC_DoubleClickProposalDAO context;
    public DC_ProposalService.Proposal[] proposals;
    public DC_ProposalService.Proposal[] result;
    
    public CreateProposalsCommand(DC_DoubleClickProposalDAO context, DC_ProposalService.Proposal[] proposals) {
      this.context = context;
      this.proposals = proposals;
    }
    
    public void execute() {
      this.result = context.proposalService.createProposals(this.proposals);
    }
  }
  
  // retrieve a company using the passed in query string
  public DC_ProposalService.Proposal[] createProposals(DC_ProposalService.Proposal[] proposals) {
    CreateProposalsCommand cmd = new CreateProposalsCommand(this, proposals);
    
    this.invokeAPI(cmd, true);
    
    return cmd.result;
  }
  
  //Inner class for updating Proposals
  private class UpdateProposalsCommand implements DC_DoubleClickDAO.Command {
    public DC_DoubleClickProposalDAO context;
    public DC_ProposalService.Proposal[] proposals;
    public DC_ProposalService.Proposal[] result; 
    
    public UpdateProposalsCommand(DC_DoubleClickProposalDAO context, DC_ProposalService.Proposal[] proposals) {
      this.context = context;
      this.proposals = proposals;
    }
    
    public void execute() {
      this.result = context.proposalService.updateProposals(this.proposals);
    }
  }
  
  //Command for updating Proposals
  public DC_ProposalService.Proposal[] updateProposals(DC_ProposalService.Proposal[] proposals) {
    UpdateProposalsCommand cmd = new UpdateProposalsCommand(this, proposals);
    
    this.invokeAPI(cmd, true);
    return cmd.result;
  }
  
  public DC_ProposalService.Statement createProposalStatement(String query) {
    DC_ProposalService.Statement result = new DC_ProposalService.Statement();
    result.query = query;
    
    return result;
  }

  public DC_ProposalService.Statement createProposalStatement(String query, DC_ProposalService.String_ValueMapEntry[] params) {
    DC_ProposalService.Statement result = createProposalStatement(query);
    result.values = params;
    
    return result;
  }
  
  //Inner class for retracting Proposals
  private class RetractProposalsCommand implements DC_DoubleClickDAO.Command {
    public DC_DoubleClickProposalDAO context;
    public DC_ProposalService.Proposal[] proposals;
    
    public RetractProposalsCommand(DC_DoubleClickProposalDAO context, DC_ProposalService.Proposal[] proposals) {
      this.context = context;
      this.proposals = proposals;
    }
    
    public void execute() {
      // Prepare filter statement
      DC_ProposalService.Statement statement = new DC_ProposalService.Statement();
      
      statement.query = 'WHERE id in (';
      String separator = '';
      
      for(DC_ProposalService.Proposal proposal : proposals) {
      	statement.query += separator + proposal.id;
      	separator = ',';
      }
      
      statement.query += ')';
      
      // Prepare proposal action
      DC_ProposalService.RetractProposals retractAction = new DC_ProposalService.RetractProposals();
      
      // Execute proposal action
      context.proposalService.performProposalAction(retractAction, statement);
    }
  }
  
  //Command for retracting Proposals
  public void retractProposals(DC_ProposalService.Proposal[] proposals) {
    RetractProposalsCommand cmd = new RetractProposalsCommand(this, proposals);
    
    this.invokeAPI(cmd, true);
  }
  
  //Inner class for submitting Proposals for approval
  private class SubmitProposalsForApprovalCommand implements DC_DoubleClickDAO.Command {
    public DC_DoubleClickProposalDAO context;
    public DC_ProposalService.Proposal[] proposals;
    
    public SubmitProposalsForApprovalCommand(DC_DoubleClickProposalDAO context, DC_ProposalService.Proposal[] proposals) {
      this.context = context;
      this.proposals = proposals;
    }
    
    public void execute() {
      // Prepare filter statement
      DC_ProposalService.Statement statement = new DC_ProposalService.Statement();
      
      statement.query = 'WHERE id in (';
      String separator = '';
      
      for(DC_ProposalService.Proposal proposal : proposals) {
        statement.query += separator + proposal.id;
        separator = ',';
      }
      
      statement.query += ')';
      
      // Prepare proposal action
      DC_ProposalService.SubmitProposalsForApproval submitAction = new DC_ProposalService.SubmitProposalsForApproval();
      
      // Execute proposal action
      context.proposalService.performProposalAction(submitAction, statement);
    }
  }
  
  //Command for submitting Proposals for approval
  public void submitProposalsForApproval(DC_ProposalService.Proposal[] proposals) {
    SubmitProposalsForApprovalCommand cmd = new SubmitProposalsForApprovalCommand(this, proposals);
    
    this.invokeAPI(cmd, true);
  }  
  // ** DEPRECATED for 201403 API ====================================
  /*
  
  //Inner class for getting Proposal
  private class GetProposalCommand implements DC_DoubleClickDAO.Command {
    public DC_ProposalService.Proposal result;
    public DC_DoubleClickProposalDAO context;
    public Integer id;
    
    public GetProposalCommand(DC_DoubleClickProposalDAO context, Integer id) {
      this.context = context;
      this.id = id;
    }
    
    public void execute() {
      this.result = context.proposalService.getProposal(id);
    }
  }

  // retrieve a DC company by the passed in integer id
  public DC_ProposalService.Proposal getProposal(Integer id, boolean retry) {
    GetProposalCommand cmd = new GetProposalCommand(this, id);
    
    this.invokeAPI(cmd, true);
    
    return cmd.result;
  }  


  
  //Command for updating Proposal
  public void updateProposal(DC_ProposalService.Proposal proposal) {
    UpdateProposalCommand cmd = new UpdateProposalCommand(this, proposal);
    
    this.invokeAPI(cmd, true);
  }
  //Inner class for updating Proposal
  private class UpdateProposalCommand implements DC_DoubleClickDAO.Command {
    public DC_DoubleClickProposalDAO context;
    public DC_ProposalService.Proposal proposal;
    
    public UpdateProposalCommand(DC_DoubleClickProposalDAO context, DC_ProposalService.Proposal proposal) {
      this.context = context;
      this.proposal = proposal;
    }
    
    public void execute() {
      context.proposalService.updateProposal(this.proposal);
    }
  }

  */
  // END DEPRECATED ====================================

}