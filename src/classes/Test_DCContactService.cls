// 
// (c) 2014 Appirio
//
// Test_DC_ContactService : test class for DC_ContactService 
//
// 24 Jan 2014    Ankit Goyal (JDC)      Original
//
@isTest
public with sharing class Test_DCContactService {

  static testMethod void testDCContactService() {
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplContact());
    Test.startTest();
    
    DC_ContactService.ContactPage cp = new DC_ContactService.ContactPage();
    DC_ContactService.CommonError ce = new DC_ContactService.CommonError();
    DC_ContactService.ContactError cte = new DC_ContactService.ContactError();
    DC_ContactService.InternalApiError iae = new DC_ContactService.InternalApiError();
    DC_ContactService.updateContacts_element uce = new DC_ContactService.updateContacts_element();
    DC_ContactService.Authentication auth = new DC_ContactService.Authentication();
    DC_ContactService.SetValue sv = new DC_ContactService.SetValue();
    System.assert(cp != null);
    
    DC_ContactService.StringLengthError sle = new DC_ContactService.StringLengthError();
    //DC_ContactService.createContactResponse_element ccre = new DC_ContactService.createContactResponse_element();
    //DC_ContactService.getContact_element gce = new DC_ContactService.getContact_element();
    DC_ContactService.BooleanValue bv = new DC_ContactService.BooleanValue();
    //DC_ContactService.createContact_element cce = new DC_ContactService.createContact_element();
    DC_ContactService.UniqueError ue = new DC_ContactService.UniqueError();
    DC_ContactService.RequiredError re = new DC_ContactService.RequiredError();
    DC_ContactService.FeatureError fe = new DC_ContactService.FeatureError();
    System.assert(fe != null);
    
    DC_ContactService.AuthenticationError ae = new DC_ContactService.AuthenticationError();
   // DC_ContactService.updateContact_element uce2 = new DC_ContactService.updateContact_element();
    DC_ContactService.PermissionError pe = new DC_ContactService.PermissionError();
    DC_ContactService.PublisherQueryLanguageSyntaxError pqlse = new DC_ContactService.PublisherQueryLanguageSyntaxError();
    DC_ContactService.String_ValueMapEntry svme = new DC_ContactService.String_ValueMapEntry();
    DC_ContactService.BaseContact bc = new DC_ContactService.BaseContact();
    DC_ContactService.Value val = new DC_ContactService.Value();
    DC_ContactService.OAuth oauth = new DC_ContactService.OAuth();
    DC_ContactService.createContacts_element cce2 = new DC_ContactService.createContacts_element();
    DC_ContactService.NumberValue nv = new DC_ContactService.NumberValue();
    DC_ContactService.SoapResponseHeader srh = new DC_ContactService.SoapResponseHeader();
    DC_ContactService.NotNullError nne = new DC_ContactService.NotNullError();
    DC_ContactService.getContactsByStatementResponse_element gcbs = new DC_ContactService.getContactsByStatementResponse_element();
    DC_ContactService.createContactsResponse_element ccre2 = new DC_ContactService.createContactsResponse_element();
    DC_ContactService.DateTimeValue dtv = new DC_ContactService.DateTimeValue();
    DC_ContactService.Statement stat = new DC_ContactService.Statement();
    DC_ContactService.PublisherQueryLanguageContextError pqlc = new DC_ContactService.PublisherQueryLanguageContextError();
    DC_ContactService.ApiError apiErr = new DC_ContactService.ApiError();
    DC_ContactService.Date_x dateX = new DC_ContactService.Date_x();
    DC_ContactService.SoapRequestHeader srh2 = new DC_ContactService.SoapRequestHeader();
    DC_ContactService.getContactsByStatement_element gcbs2 = new DC_ContactService.getContactsByStatement_element();
    //DC_ContactService.getContactResponse_element gcre = new DC_ContactService.getContactResponse_element();
    DC_ContactService.ApiException apiEx = new DC_ContactService.ApiException();
    DC_ContactService.QuotaError qe = new DC_ContactService.QuotaError();
    DC_ContactService.DateValue dv = new DC_ContactService.DateValue();
    DC_ContactService.ApplicationException ae2 = new DC_ContactService.ApplicationException();
    DC_ContactService.updateContactsResponse_element ucre = new DC_ContactService.updateContactsResponse_element();
    DC_ContactService.ApiVersionError ave = new DC_ContactService.ApiVersionError();
    DC_ContactService.Contact cont = new DC_ContactService.Contact();
    DC_ContactService.ServerError sErr = new DC_ContactService.ServerError();
    DC_ContactService.DateTime_x dtX = new DC_ContactService.DateTime_x();
    DC_ContactService.ContactServiceInterfacePort csiPort = new DC_ContactService.ContactServiceInterfacePort();
    
    // Contact Service Interface Port Methods 
    //DC_ContactService.Contact contact1 = csiPort.createContact(new DC_ContactService.Contact());
    //DC_ContactService.Contact contact2 = csiPort.updateContact(new DC_ContactService.Contact());
    DC_ContactService.Contact[] companyArr = csiPort.createContacts(new List<DC_ContactService.Contact>{new DC_ContactService.Contact()});
    DC_ContactService.Contact[] companyArr2 = csiPort.updateContacts(new List<DC_ContactService.Contact>{new DC_ContactService.Contact()});
    //DC_ContactService.Contact getcomp = csiPort.getContact(Long.valueOf('12345'));
    DC_ContactService.ContactPage contPageVar = csiPort.getContactsByStatement(new DC_ContactService.Statement());
    
    
    
    DC_ContactService.StatementError statErr = new DC_ContactService.StatementError();
    DC_ContactService.TextValue tv = new DC_ContactService.TextValue();
    DC_ContactService.InvalidEmailError iee = new DC_ContactService.InvalidEmailError();
    //DC_ContactService.updateContactResponse_element ucre2 = new DC_ContactService.updateContactResponse_element();
    System.assert(ucre != null);
    Test.stopTest();
  }
}