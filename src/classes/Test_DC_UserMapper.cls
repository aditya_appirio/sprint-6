// 
// (c) 2014 Appirio
//
// Test_DC_UserMapper : test class for DC_UserMapper 
//
// 14 March 2014   Karun Kumar(JDC) Original
// @Anjali K : Increased coverage
@isTest 
private class Test_DC_UserMapper { 

  static testMethod void myUnitTest() {   
    goog_dclk_dsm__DC_ObjectMapping__c dcObjMapping = DC_TestUtils.createObjectMapping('User', 'User', 'goog_dclk_dsm__DC_User__c', false);
    goog_dclk_dsm__DC_ObjectMapping__c dcobjMapping2 = DC_TestUtils.createObjectMapping('Contact', 'Contact', false);
    goog_dclk_dsm__DC_ObjectMapping__c dcobjMapping3 = DC_TestUtils.createObjectMapping('Proposal', 'Opportunity', false);
    goog_dclk_dsm__DC_ObjectMapping__c dcobjMapping4 = DC_TestUtils.createObjectMapping('PROPOSAL_LINE_ITEM', 'OpportunityLineItem', false); 
    goog_dclk_dsm__DC_ObjectMapping__c dcObjMapping5 = DC_TestUtils.createObjectMapping('Company', 'Account', false);
    
    insert new List<goog_dclk_dsm__DC_ObjectMapping__c>{dcObjMapping, dcObjMapping2, dcobjMapping3, dcobjMapping4, dcObjMapping5};
    
    goog_dclk_dsm__DC_FieldMapping__c fldMapping1 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', 
      goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId');
    
    goog_dclk_dsm__DC_FieldMapping__c fldMapping2 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', 
      goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'sfid');
    
    goog_dclk_dsm__DC_FieldMapping__c fldMapping3 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'custom.email', 
      goog_dclk_dsm__DC_SalesforceFieldName__c = 'email', Name = 'email');
    
    goog_dclk_dsm__DC_FieldMapping__c fldMapping4 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping2.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId');
    
    goog_dclk_dsm__DC_FieldMapping__c fldMapping5 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping4.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId');
    
    goog_dclk_dsm__DC_FieldMapping__c fldMapping6 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping3.id, goog_dclk_dsm__DC_HandlerClass__c = 'DC_CustomProposalTeamMemberHandler' , goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'doubleclickid');
    
    goog_dclk_dsm__DC_FieldMapping__c fldMapping7 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping5.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId');
    
    insert new List<goog_dclk_dsm__DC_FieldMapping__c>{fldMapping1, fldMapping2,fldMapping3, fldMapping4, fldMapping5, fldMapping6, fldMapping7};
    goog_dclk_dsm__DC_User__c dcUser = DC_TestUtils.createDCUser();
    Profile sysAdminProfile = [Select Id From Profile Where Name ='System Administrator'];
    
    User userObj = new User(FirstName='test first', LastName='test last', Alias='Mr.', Email='test@gmail.com', 
                            UserName='testuser@gmail.com.testClass', ProfileId =sysAdminProfile.Id, 
                            TimeZoneSidKey='Asia/Kolkata', LocaleSidKey='en_US', LanguageLocaleKey='en_US', EmailEncodingKey='ISO-8859-1');
    insert userObj;
    
    DC_UserService.User_x userX = new DC_UserService.User_x();    
    userX.id = 12345;
    userX.name = 'testUser';
    
    toDC_DateTime(System.now());
    //userX.lastModifiedDateTime = toDC_DateTime(System.now());//;
    
    system.runAs(userObj) {
      Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplUserService());
      Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplCustomFieldService());
      Test.startTest();
       
      DC_UserMapper umObj = DC_UserMapper.getInstance();
      DC_GenericWrapper sfObj = umObj.wrap(new List<goog_dclk_dsm__DC_User__c> { dcUser } );
      DC_GenericWrapper gwObj = umObj.wrap(userX);
      DC_GenericWrapper gwObj2 = umObj.wrap(new list<DC_UserService.User_x>{userX});
      DC_UserService.UserPage tempPage = new DC_UserService.UserPage();
      gwObj = umObj.wrap(tempPage);
      gwObj = umObj.createNewDCRecords(sfObj);
      DC_GenericWrapper dcUserWrap = umObj.wrap(dcUser);
      dcUserWrap.gotoBeforeFirst();
      try{
        umObj.createNewDCRecords(dcUserWrap);
      }catch(exception e){}
      dcUserWrap.gotoBeforeFirst();
      dcUserWrap.next();
      umObj.createNewDCRecords(dcUserWrap);
      //umObj.linkSFRecordAndDCRecord(gwObj, gwObj2);
      
      
      
      // source object needs to be doubleclick, target saalesforce
      umObj.updateFieldsOnAllObjectsWithCreate(gwObj, sfObj);
      umObj.addNewRecordFromSource(gwObj, gwObj2);
      umObj.addNewRecordFromSource(gwObj, sfObj);
      //DateTime dt = umObj.getLatestUpdateDate((DC_DoubleClickUserWrapper)gwObj2);
      System.debug('gwObj:::' + gwObj);
      umObj.linkSFRecordAndDCRecord(sfObj, gwObj);
      umObj.updateFieldsOnAllObjectsWithCreate(dcUserWrap, gwObj);
      umObj.wrap(new list<DC_UserService.User_x>{userX}, 'User');
      umObj.createNewDCRecords(sfObj, 'User');
      
      DC_ObjectMapper mapper = DC_ObjectMapper.getInstance();
      
      String jsonData = '{"a":"ab","b":"cd","c":"cd","d":"de","e":"ef","f":"fg"}';
      JSONParser parser = JSON.createParser(jsonData);
      mapper.populateMap(new Map<String, String>(), parser);
     // mapper.updateField(dcObjMapping, fldMapping1, userX);
      
      try{
        mapper.getSfToDcFieldConversions('User', 'DoubleClickId'); 
      }catch(exception e){}
      try{
        mapper.getDCToSfFieldConversions('User', 'DoubleClickId'); 
      }catch(exception e){}
      
      mapper.getCommaSeparatedSalesforceFieldList('User');
      mapper.updateFieldsOnAllObjects(sfObj, gwObj);
      
      try{
        mapper.createCustomFields('User');
      }catch(Exception e){}
      mapper.createNewDCRecords(sfObj);
      
      Account acc = DC_TestUtils.createAccount('test', true);
      Contact con = DC_TestUtils.createContact('test','test',acc.id, true);
      Opportunity opp = DC_TestUtils.createOpportunity('test', Date.today(), acc.Id, 'Closed Won', true);
      mapper.wrap(new List<Contact>{con});
      mapper.wrap(con);
      
      mapper.wrap(opp);
      mapper.wrap(new List<Opportunity>{opp});
      
      mapper.wrap(new OpportunityLineItem());
      mapper.wrap(new List<OpportunityLineItem>{new OpportunityLineItem()});
      
      DC_CompanyService.Company company = new DC_CompanyService.Company();     
      company.id = 12345;
      company.name = 'testCompany';
      
      DC_CompanyMapper compMapper = DC_CompanyMapper.getInstance();
      mapper.linkSFRecordsAndDCRecords(compMapper.wrap(acc), compMapper.wrap(company));
      
      
      Test.stopTest();
    }
    
  }
  
  
  private static testMethod void myUnitTest1(){
        
    goog_dclk_dsm__DC_ObjectMapping__c objMapping1 = DC_TestUtils.createObjectMapping('Proposal', 'Opportunity', false);
    insert objMapping1;
    
    List<goog_dclk_dsm__DC_FieldMapping__c> fldMappingList = new List<goog_dclk_dsm__DC_FieldMapping__c>();
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'custom.SFDC_Opportunity_ID', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345,goog_dclk_dsm__DC_SyncDirection__c = 'Bi-Directional', goog_dclk_dsm__DC_SalesforceFieldName__c = 'Id', Name = 'sfid'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'probabilityToClose', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Probability', Name = 'probability'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'status', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345,  Name = 'status'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345,  Name = 'Team Members', goog_dclk_dsm__DC_SyncDirection__c = 'Bi-Directional',  goog_dclk_dsm__DC_HandlerClass__c = 'DC_CustomProposalTeamMemberHandler'));
    
    insert fldMappingList;
    
    Account acc = DC_TestUtils.createAccount('test', true);
    Opportunity opp = DC_TestUtils.createOpportunity('test', Date.today(), acc.Id, 'Closed Won', true);
    
    DC_TestUtils.createOpportunityTeamMember(opp.id, 'Primary Trafficker', Userinfo.getUserId(), true);
    
    
    goog_dclk_dsm__DC_User__c dcUser = DC_TestUtils.createDCUser();
    
    DC_TestUtils.createDoubleClickTeamMember(opp.id, 'Primary Sales Rep', dcUser.id, true);
    
    DC_ProposalService.Proposal proposal = new DC_ProposalService.Proposal();
    proposal.name = 'testProposal';
    proposal.status = 'open';
    proposal.id = 12345;
    
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplProposal());
    Test.startTest();
     
    DC_DoubleClickProposalWrapper sfObj = new DC_DoubleClickProposalWrapper(objMapping1, fldMappingList, new List<DC_ProposalService.Proposal>{proposal});
     
    DC_ObjectMapper pmObj = DC_ProposalMapper.getInstance();
    
    DC_ProposalMapper pMapper = DC_ProposalMapper.getInstance();
    DC_GenericWrapper gwObj = pMapper.wrap(proposal);
    DC_GenericWrapper gwObj2 = pMapper.wrap(new list<DC_ProposalService.Proposal>{proposal});
    sfObj.next();
    gwObj.next();
    pmObj.updateFields(gwObj, sfObj);
    
    pmObj.updateFields(sfObj, gwObj);
    
    Test.stopTest();
  
  }
  private static DC_UserService.DateTime_x toDC_DateTime(Object obj){
    if(obj == null) {
      return null;
    }
    if(obj instanceof DateTime) {
      DateTime dt= (Datetime)obj;
      DC_UserService.DateTime_x dc_DateTime = new DC_UserService.DateTime_x();
      DC_UserService.Date_x dc_Date = new DC_UserService.Date_x();
      dc_Date.day =dt.day();
      dc_Date.month=dt.month();
      dc_Date.year=dt.year();
      
      dc_DateTime.date_x = dc_Date;
      dc_DateTime.hour = dt.hour();
      dc_DateTime.minute = dt.minute();
      dc_DateTime.second = dt.second();
      
      return (DC_UserService.DateTime_x) dc_DateTime;
    } 
    throw new DC_ConnectorException(DC_Constants.UNSUPPORTED_DATA_CONVERT_ERR);
  }
  
}