// 
// (c) 2014 Appirio
//
// Test_DC_QueueDispatch : test class for DC_QueueDispatch 
//
// 14 feb 2014    Ankit Goyal (JDC)      Original
//
@isTest
private class Test_DC_QueueDispatch {

  static testMethod void myUnitTest() {
    List<goog_dclk_dsm__DC_ProcessingQueue__c> processQueues = new List<goog_dclk_dsm__DC_ProcessingQueue__c>();
    processQueues.add(DC_TestUtils.createDCProcessingQueue(true, true, true, 'testProcess1', false));
    processQueues.add(DC_TestUtils.createDCProcessingQueue(false, true, true, 'testProcess2', false));
    insert processQueues;
    
    goog_dclk_dsm__DC_QueueItems__c d = DC_TestUtils.createDCQueueItem('testProcess2', 'DC_QueueableBatchProposalSync', true);
    
    Test.StartTest();
      DC_QueueDispatch sh1 = new DC_QueueDispatch();
      String sch = '0 0 0 * * ?'; system.schedule('Test Query Dispatch', sch, sh1); 
      //DC_QueueDispatch.queueItemCompleted(processQueues[1].id, DC_QueueDispatch.DC_ExecutionStatus.Success, 'testDetails');
    Test.stopTest(); 

  }
  
  static testMethod void myUnitTest1() {
    List<goog_dclk_dsm__DC_ProcessingQueue__c> processQueues = new List<goog_dclk_dsm__DC_ProcessingQueue__c>();
    processQueues.add(DC_TestUtils.createDCProcessingQueue(false, false, false, 'testProcess2', false));
    insert processQueues;
    goog_dclk_dsm__DC_QueueItems__c d = DC_TestUtils.createDCQueueItem('testProcess2','DC_QueueableBatchProposalSync', true);
    try{
      Test.StartTest();
      
      DC_QueueDispatch sh1 = new DC_QueueDispatch(); 
      String sch = '0 0 0 * * ?'; system.schedule('Test Query Dispatch', sch, sh1); 
      
      Test.stopTest(); 
    }catch(Exception e){}
  }
  
  static testMethod void myUnitTest2() {
    List<goog_dclk_dsm__DC_ProcessingQueue__c> processQueues = new List<goog_dclk_dsm__DC_ProcessingQueue__c>();
    processQueues.add(DC_TestUtils.createDCProcessingQueue(false, true, false, 'testProcess3', false));
    insert processQueues;
    goog_dclk_dsm__DC_QueueItems__c d = DC_TestUtils.createDCQueueItem('testProcess3','DC_QueueableBatchProposalSync', true);
    
    Test.setMock(HttpCalloutMock.class, new DC_HttpCalloutMockImploAuth());
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplProposal());
    Test.StartTest();
    DC_QueueDispatch sh1 = new DC_QueueDispatch();
    String sch = '0 0 0 * * ?'; system.schedule('Test Query Dispatch', sch, sh1); 
      
    Test.stopTest(); 
  }
  
  static testMethod void myUnitTest3() {
    List<goog_dclk_dsm__DC_ProcessingQueue__c> processQueues = new List<goog_dclk_dsm__DC_ProcessingQueue__c>();
    processQueues.add(DC_TestUtils.createDCProcessingQueue(true, true, true, 'testProcess1', false));
    processQueues.add(DC_TestUtils.createDCProcessingQueue(false, true, true, 'testProcess2', false));
    insert processQueues;
    
    goog_dclk_dsm__DC_QueueItems__c d = DC_TestUtils.createDCQueueItem('testProcess2', 'DC_QueueableBatchProposalSync', true);
    
    Test.StartTest();
    DC_QueueDispatch sh1 = new DC_QueueDispatch(processQueues[0].Id);
    String sch = '0 0 0 * * ?'; 
    system.schedule('Test Query Dispatch', sch, sh1); 
    //DC_QueueDispatch.queueItemCompleted(processQueues[1].id, DC_QueueDispatch.DC_ExecutionStatus.Success, 'testDetails');
    DC_QueueDispatch.scheduleForImmediateExecution(processQueues[0].Id);
    DC_QueueDispatch.scheduleQueue(10);
    
    try {
      DC_QueueDispatch.scheduleQueue(70);
    } catch(Exception e) {
      system.debug('Error checked successfully'+e);
    }
    
    DC_QueueDispatch.remoteStartProcess(processQueues[0].Id);
    
    DC_QueueDispatch sh2 = new DC_QueueDispatch(null);
    String sch2 = '0 0 0 * * ?'; 
    system.schedule('Test Query Dispatch2', sch2, sh2); 
      
    Test.stopTest(); 

  }
}