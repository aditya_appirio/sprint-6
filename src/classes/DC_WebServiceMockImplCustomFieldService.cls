// 
// (c) 2014 Appirio
//
// WebServiceMockImpl_CustomFieldService : implements WebServiceMock for webServicecallout test classes 
//
// 17 feb 2014    Anjali K (JDC)      Original
//
@IsTest
global class DC_WebServiceMockImplCustomFieldService implements WebServiceMock{
  global void doInvoke(Object stub, Object request, Map<String, Object> response,
                    String endpoint, String soapAction, String requestName,
                    String responseNS, String responseName, String responseType){
      
      
      DC_CustomFieldService.CustomField custFld = new DC_CustomFieldService.CustomField();
      custFld.name = 'custFld1';
      custFld.id = 12345;
      custFld.CustomField_Type = 'Integer';
      
      DC_CustomFieldService.CustomField custFld1 = new DC_CustomFieldService.CustomField();
      custFld1.name = 'custFld2';
      custFld1.CustomField_Type = 'Integer';
      
      DC_CustomFieldService.CustomFieldPage custFldPage = new DC_CustomFieldService.CustomFieldPage();
      custFldPage.startIndex = 0;
      custFldPage.results = new List<DC_CustomFieldService.CustomField>{custFld, custFld1};
      custFldPage.totalResultSetSize = 2;
       
      DC_CustomFieldService.CustomFieldOption custFldOption = new DC_CustomFieldService.CustomFieldOption();
      custFldOption.displayName = 'custFldOpt1';
      custFldOption.id = 12345;
      custFldOption.customFieldId = 12345;
      
      DC_CustomFieldService.CustomFieldOption custFldOption1 = new DC_CustomFieldService.CustomFieldOption();
      custFldOption1.displayName = 'custFldOpt2';
      custFldOption1.customFieldId = 12345;
                      
      if(request instanceof DC_CustomFieldService.updateCustomFieldOptions_element){
          DC_CustomFieldService.updateCustomFieldOptionsResponse_element resp = new DC_CustomFieldService.updateCustomFieldOptionsResponse_element();
          resp.rval = new List<DC_CustomFieldService.CustomFieldOption>{custFldOption};
          response.put('response_x', resp);
          //response.put('response_x', new DC_CustomFieldService.updateCustomFieldOptionsResponse_element());
      }/*else if(request instanceof DC_CustomFieldService.getCustomField_element){
          DC_CustomFieldService.getCustomFieldResponse_element resp = new DC_CustomFieldService.getCustomFieldResponse_element();
          resp.rval = custFld;
          response.put('response_x', resp);
          
          //.put('response_x', new DC_CustomFieldService.getCustomFieldResponse_element()); 
      }else if(request instanceof DC_CustomFieldService.updateCustomFieldOption_element){
          DC_CustomFieldService.updateCustomFieldOptionResponse_element resp = new DC_CustomFieldService.updateCustomFieldOptionResponse_element();
          resp.rval = custFldOption;
          response.put('response_x', resp);
          
          //response.put('response_x', new DC_CustomFieldService.updateCustomFieldOptionResponse_element()); 
      }else if(request instanceof DC_CustomFieldService.createCustomFieldOption_element){
          DC_CustomFieldService.createCustomFieldOptionResponse_element resp = new DC_CustomFieldService.createCustomFieldOptionResponse_element();
          resp.rval = custFldOption1;
          response.put('response_x', resp);
          
          //response.put('response_x', new DC_CustomFieldService.createCustomFieldOptionResponse_element()); 
      }*/else if(request instanceof DC_CustomFieldService.getCustomFieldsByStatement_element){
          DC_CustomFieldService.getCustomFieldsByStatementResponse_element resp = new DC_CustomFieldService.getCustomFieldsByStatementResponse_element();
          resp.rval = custFldPage;
          response.put('response_x', resp);
          
          //response.put('response_x', new DC_CustomFieldService.getCustomFieldsByStatementResponse_element());
      }/*else if(request instanceof DC_CustomFieldService.updateCustomField_element){
          DC_CustomFieldService.updateCustomFieldResponse_element resp = new DC_CustomFieldService.updateCustomFieldResponse_element();
          resp.rval = custFld;
          response.put('response_x', resp);
          
          //response.put('response_x', new DC_CustomFieldService.updateCustomFieldResponse_element());
      }*/else if(request instanceof DC_CustomFieldService.createCustomFieldOptions_element){
          DC_CustomFieldService.createCustomFieldOptionsResponse_element resp = new DC_CustomFieldService.createCustomFieldOptionsResponse_element();
          resp.rval = new List<DC_CustomFieldService.CustomFieldOption>{custFldOption1};
          response.put('response_x', resp);
          
          //response.put('response_x', new DC_CustomFieldService.createCustomFieldOptionsResponse_element());
      }else if(request instanceof DC_CustomFieldService.getCustomFieldOption_element){
          DC_CustomFieldService.getCustomFieldOptionResponse_element resp = new DC_CustomFieldService.getCustomFieldOptionResponse_element();
          resp.rval = custFldOption;
          response.put('response_x', resp);
          
          //response.put('response_x', new DC_CustomFieldService.getCustomFieldOptionResponse_element());    
      
      }else if(request instanceof DC_CustomFieldService.createCustomFields_element){
          DC_CustomFieldService.createCustomFieldsResponse_element resp = new DC_CustomFieldService.createCustomFieldsResponse_element();
          resp.rval = new List<DC_CustomFieldService.CustomField>{custFld1};
          response.put('response_x', resp);
          
          //response.put('response_x', new DC_CustomFieldService.createCustomFieldsResponse_element());
      }else if(request instanceof DC_CustomFieldService.updateCustomFields_element){
          DC_CustomFieldService.updateCustomFieldsResponse_element resp = new DC_CustomFieldService.updateCustomFieldsResponse_element();
          resp.rval = new List<DC_CustomFieldService.CustomField>{custFld};
          response.put('response_x', resp);
          
          //response.put('response_x', new DC_CustomFieldService.updateCustomFieldsResponse_element());
      }/*else if(request instanceof DC_CustomFieldService.createCustomField_element){
          DC_CustomFieldService.createCustomFieldResponse_element resp = new DC_CustomFieldService.createCustomFieldResponse_element();
          resp.rval = custFld1;
          response.put('response_x', resp);
          //response.put('response_x', new DC_CustomFieldService.createCustomFieldResponse_element()); 
      }*/else if(request instanceof DC_CustomFieldService.performCustomFieldAction_element)
          response.put('response_x', new DC_CustomFieldService.performCustomFieldActionResponse_element());     
          
          
      return;
  }
}