//
// (c) 2014 Appirio, Inc.
//
// To provide test coverage to DC_SyncDateManager class
//
// 13 Feb 2014     Jitendra Kothari       Original
//

@isTest(seeAllData = true)
private class Test_DC_SalesforceDML {
  
  static Opportunity oppty;
  static List<Account> accList;
  static Account singleNullAccount;
  static final Integer ACCOUNT_COUNT = 2; 
  static goog_dclk_dsm__DC_DoubleClickTeamMember__c dcTeamMem;
  static OpportunityTeamMember oppTeamMem;
  static OpportunityLineItem oppLI1 ;
  private static testMethod void TestDC_SalesforceDML(){
    createData();   
    
    Test.startTest();
    
    DC_SalesforceDML instance = DC_SalesforceDML.getInstance();
    List<sObject> records = instance.objectListToSObjectList(accList);
    System.assertEquals(accList.size(), records.size());
    
    //check all condition with insert command
    DC_SalesforceDML.DC_SalesforceDMLStatus result = instance.registerInsert(accList.get(0));
    System.assertEquals(false, result.getExecuted());
    System.assertEquals(null, result.getSuccessful());
    System.assertEquals(null, result.getLastException());
    System.assertEquals(false, result.getRecords().isEmpty());
    System.assertEquals(null, result.getRecords().get(0).get('Id'));
    System.assertEquals(null, result.getResults());
    
    instance.flush();   
    System.assertEquals(true, result.getExecuted());
    System.assertEquals(true, result.getSuccessful());
    System.assertEquals(null, result.getLastException());
    System.assertEquals(false, result.getRecords().isEmpty());
    System.assertNotEquals(null, result.getRecords().get(0).get('Id'));
    System.assertNotEquals(null, result.getResults());
    System.assertEquals(false, result.getResults().isEmpty());
    System.assertEquals(1, result.getResults().size());   
    
    //These for coverage only
    result = instance.registerInsert(singleNullAccount);
    result = instance.registerUpdate(singleNullAccount);
    result = instance.registerDelete(singleNullAccount);    
    result = instance.registerUpsert(singleNullAccount);
    
    result = instance.registerUpsert(accList.get(1));
    result = instance.registerUpsert(accList.get(1));   
    result = instance.registerUpdate(accList.get(1));
    result = instance.registerDelete(accList.get(1));
    
    instance.flush();
    
    instance.registerUpsert(null, null);
    
    Account a = new Account(name = 'test');
    
    SObjectType accountType = Schema.getGlobalDescribe().get('Account');
    Map<String,Schema.SObjectField> mfields = accountType.getDescribe().fields.getMap();
    
    instance.registerUpsert(new List<Account>{a}, mfields.get('name'));
    
    DC_SalesforceDML.DC_SalesforceDMLInsert cmdInstance = new DC_SalesforceDML.DC_SalesforceDMLInsert(new List<Account>{a});
    
    cmdInstance.getSuccessCount();
    
    
    
    cmdInstance.getInsertRecords();
    
    cmdInstance.getUpdateRecords();
    
    cmdInstance.getSuccessfulInsertRecords();
    
    cmdInstance.getSuccessfulUpdateRecords();
    
    cmdInstance.getSuccessfulDeleteRecords();
    
    cmdInstance.getInsertResults();
    
    cmdInstance.getUpdateResults();
    
    Contact c = new Contact(firstname = 'a');
    cmdInstance = new DC_SalesforceDML.DC_SalesforceDMLInsert(new List<Contact>{c});
    try{
      cmdInstance.getErrorCount();
    }catch(Exception e){}
    //DC_SalesforceDML.DC_SalesforceDMLUpsert upsertRec = new DC_SalesforceDML.DC_SalesforceDMLUpsert(new List<Account>{a}, Schema.SObjectType.get('Account').fields[0]);
    
    DC_SalesforceDML.DC_SalesforceDMLUpsert cmdUpsert = new DC_SalesforceDML.DC_SalesforceDMLUpsert(new List<OpportunityTeamMember>{oppTeamMem});
    cmdUpsert.executeCommandDML();
    cmdUpsert = new DC_SalesforceDML.DC_SalesforceDMLUpsert(new List<goog_dclk_dsm__DC_DoubleClickTeamMember__c>{dcTeamMem});
    cmdUpsert.executeCommandDML();
    
    cmdUpsert = new DC_SalesforceDML.DC_SalesforceDMLUpsert(new List<OpportunityLineItem>{oppLI1});
    cmdUpsert.executeCommandDML();
    
    try{
      Opportunity opp = new Opportunity(name = 'testException');
      insert opp;
    }catch(Exception e){
      DC_SalesforceDML.DC_SalesforceDMLInsert insertCmd = new DC_SalesforceDML.DC_SalesforceDMLInsert(new List<Account>{new Account(name = 'test')});
      DC_SalesforceDML.DC_SalesforceDMLException dmlExcep = new DC_SalesforceDML.DC_SalesforceDMLException('msg', e, new List<DC_SalesforceDML.DC_SalesforceDMLInsert>{insertCmd});
    }
    
    Test.stopTest(); 
  }
  private static void createData(){
    goog_dclk_dsm__DC_ObjectMapping__c objMapping = new goog_dclk_dsm__DC_ObjectMapping__c(goog_dclk_dsm__DC_DoubleClickObjectName__c='Company', goog_dclk_dsm__DC_SalesforceObjectName__c = 'Account', name = 'account');
    insert objMapping;
    goog_dclk_dsm__DC_FieldMapping__c fldMapping = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId');
    insert fldMapping;
        
    accList = new List<Account>();
    for(Integer i = 1; i < ACCOUNT_COUNT + 1; i++){
      accList.add(DC_TestUtils.createAccount('TestAccount' + i, false));
    }   
    Account acc = DC_TestUtils.createAccount('testAcc1', true);
    oppty = new Opportunity(name = 'testProposal1', AccountId = acc.id, StageName = 'Closed/Won', CloseDate = date.today());
    insert new List<Opportunity>{oppty};
    
    oppTeamMem = DC_TestUtils.createOpportunityTeamMember(oppty.id, 'Primary Trafficker', Userinfo.getUserId(), true);
    
    goog_dclk_dsm__DC_User__c dcUser = DC_TestUtils.createDCUser();
    
    dcTeamMem = DC_TestUtils.createDoubleClickTeamMember(oppty.id, 'Primary Sales Rep', dcUser.id, true);
    
    Pricebook2 standardPB = [select id, isActive from Pricebook2 where isStandard=true limit 1];
    if (!standardPB.isActive) {
        standardPB.isActive = true;
        update standardPB;
    }
    
    Product2 prod = new Product2(Name = 'testProd', IsActive = true);
    insert prod;
    PricebookEntry pbe = new PricebookEntry(Pricebook2Id = standardPB.Id, Product2Id = prod.Id, UnitPrice = 10000, IsActive = true, UseStandardPrice = false);
    insert pbe;
    
    oppLI1 = DC_TestUtils.createOpportunityLI(23, 10, oppty.Id,pbe.Id, true);
    
  }
}