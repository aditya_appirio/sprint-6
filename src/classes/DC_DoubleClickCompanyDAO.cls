// 
// (c) 2014 Appirio
//
// Class used for DoubleClick data access object, company specific
//
// 21 Jan 2014  Mauricio Desiderio  Original
//
public with sharing class DC_DoubleClickCompanyDAO extends DC_DoubleClickDAO {

  protected override void tokenRefreshed() {
    this.companyService = null;
  }
  
  public DC_CompanyService.Statement createStatement(String query) {
    DC_CompanyService.Statement result = new DC_CompanyService.Statement();

    result.query = query;
    
    return result;
  }

  public DC_CompanyService.Statement createStatement(String query, DC_CompanyService.String_ValueMapEntry[] params) {
    DC_CompanyService.Statement result = createStatement(query);
    result.values = params;
    
    return result;
  }

  public DC_DoubleClickCompanyDAO(DC_DoubleClickOAuthHandler authHandler) {
    super(authHandler);
  }

  // get a configured doubleclick company interface
  private DC_CompanyService.CompanyServiceInterfacePort companyService {
    get {
      if(this.companyService == null) {
        this.companyService = DC_ServicesFactory.getCompanyService(this.authHandler);
      } 

      return this.companyService;
    }
    set;
  }
  
  private class GetCompanyByStatementCommand implements DC_DoubleClickDAO.Command {
    public DC_CompanyService.CompanyPage result;
    public DC_DoubleClickCompanyDAO context;
    public DC_CompanyService.Statement statement;
    
    public GetCompanyByStatementCommand(DC_DoubleClickCompanyDAO context, DC_CompanyService.Statement statement) {
      this.context = context;
      this.statement = statement;
    }
    
    public void execute() {
      this.result = context.companyService.getCompaniesByStatement(statement);
    }
  }
  
  // retrieve a company using the passed in query string
  public DC_CompanyService.CompanyPage getCompaniesByStatement(String query) {
  	System.debug('query:::' + query);
    DC_CompanyService.Statement statement = this.createStatement(query);
    System.debug('statement:::' + statement);
    GetCompanyByStatementCommand cmd = new GetCompanyByStatementCommand(this, statement);
    
    this.invokeAPI(cmd, true);
    
    return cmd.result;
  }
  
  private class CreateCompaniesCommand implements DC_DoubleClickDAO.Command {
    public DC_DoubleClickCompanyDAO context;
    public DC_CompanyService.Company[] companies;
    public DC_CompanyService.Company[] result;
    
    public CreateCompaniesCommand(DC_DoubleClickCompanyDAO context, DC_CompanyService.Company[] companies) {
      this.context = context;
      this.companies = companies;
    }
    
    public void execute() {
      this.result = context.companyService.createCompanies(this.companies);
    }
  }
  
  // retrieve a company using the passed in query string
  public DC_CompanyService.Company[] createCompanies(DC_CompanyService.Company[] companies) {
    CreateCompaniesCommand cmd = new CreateCompaniesCommand(this, companies);
    
    this.invokeAPI(cmd, true);
    
    return cmd.result;
  }
  
  private class UpdateCompaniesCommand implements DC_DoubleClickDAO.Command {
    public DC_DoubleClickCompanyDAO context;
    public DC_CompanyService.Company[] companies;
    
    public UpdateCompaniesCommand(DC_DoubleClickCompanyDAO context, DC_CompanyService.Company[] companies) {
      this.context = context;
      this.companies = companies;
    }
    
    public void execute() {
      context.companyService.updateCompanies(this.companies);
    }
  }
  
  public void updateCompanies(DC_CompanyService.Company[] companies) {
    UpdateCompaniesCommand cmd = new UpdateCompaniesCommand(this, companies);
    
    this.invokeAPI(cmd, true);
  }

  // ** DEPRECATED for 201403 API ====================================
  /*

  private class UpdateCompanyCommand implements DC_DoubleClickDAO.Command {
    public DC_DoubleClickCompanyDAO context;
    public DC_CompanyService.Company company;
    
    public UpdateCompanyCommand(DC_DoubleClickCompanyDAO context, DC_CompanyService.Company company) {
      this.context = context;
      this.company = company;
    }
    
    public void execute() {
      context.companyService.updateCompany(this.company);
    }
  }

  public void updateCompany(DC_CompanyService.Company company) {
    UpdateCompanyCommand cmd = new UpdateCompanyCommand(this, company);
    
    this.invokeAPI(cmd, true);
  }
  */
}