/**
 */
@isTest
private class Test_DC_CustomAccountTeamHandler {

	static testMethod void testAccountTeamProcessField() {
		goog_dclk_dsm__DC_ObjectMapping__c dcObjMapping = DC_TestUtils.createObjectMapping('Company', 'Account', true);
    List<goog_dclk_dsm__DC_FieldMapping__c> fldMappings = new List<goog_dclk_dsm__DC_FieldMapping__c>();
    fldMappings.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId'));
    fldMappings.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcobjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'Team', goog_dclk_dsm__DC_HandlerClass__c = 'DC_CustomAccountTeamHandler'));
    insert fldMappings;

    Account a = DC_TestUtils.createAccount('test account', true);
    
    goog_dclk_dsm__DC_Team__c team = new goog_dclk_dsm__DC_Team__c(name = 'Team 1', goog_dclk_dsm__DC_DoubleClickId__c = '32193392');
    insert team;
    insert new goog_dclk_dsm__DC_DoubleClickAccountTeam__c(goog_dclk_dsm__DC_Team__c = team.id, goog_dclk_dsm__DC_Account__c = a.id);

    DC_CompanyService.Company company = new DC_CompanyService.Company();
    DC_CustomAccountTeamHandler handler = new DC_CustomAccountTeamHandler();

    DC_CompanyMapper mapper = DC_MapperFactory.getInstance().getAccountMapper();

    a = ((List<Account>)database.query('SELECT id, '+ handler.getSalesforceQueryString() + ' FROM Account where id = \'' + a.id + '\''))[0];

    DC_GenericWrapper sfwrapper = mapper.wrap(a);
    sfwrapper.next();

    DC_GenericWrapper dcwrapper = mapper.wrap(company);
    dcwrapper.next();

    handler.initialize(dcObjMapping, fldMappings, sfwrapper, dcwrapper);
    handler.processField(fldMappings[1], sfwrapper, dcwrapper);
    handler.finish();


    system.assertEquals(1, company.appliedTeamIds.size());
	}
}