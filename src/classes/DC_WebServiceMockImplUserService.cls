// 
// (c) 2014 Appirio
//
// implements WebServiceMock for webServicecallout test classes 
//
// 24 Jan 2014    Anjali K (JDC)      Original
@isTest
global class DC_WebServiceMockImplUserService implements WebServiceMock{
  global void doInvoke(Object stub, Object request, Map<String, Object> response,
                    String endpoint, String soapAction, String requestName,
                    String responseNS, String responseName, String responseType){
      
    DC_CustomFieldService.CustomField custFld = new DC_CustomFieldService.CustomField();
	  custFld.name = 'custFld1';
	  custFld.id = 12345;
	  custFld.CustomField_Type = 'Integer';
	  
	  DC_CustomFieldService.CustomField custFld1 = new DC_CustomFieldService.CustomField();
	  custFld1.name = 'custFld2';
	  custFld1.CustomField_Type = 'Integer';
	  custFld1.id = 123456;
	  
	  DC_CustomFieldService.CustomFieldPage custFldPage = new DC_CustomFieldService.CustomFieldPage();
	  custFldPage.startIndex = 0;
	  custFldPage.results = new List<DC_CustomFieldService.CustomField>{custFld, custFld1};
	  custFldPage.totalResultSetSize = 2;
    
    DC_CustomFieldService.CustomFieldOption custFldOption = new DC_CustomFieldService.CustomFieldOption();
	  custFldOption.displayName = 'custFldOpt1';
	  custFldOption.id = 12345;
	  custFldOption.customFieldId = 12345;
	  
	  DC_CustomFieldService.CustomFieldOption custFldOption1 = new DC_CustomFieldService.CustomFieldOption();
	  custFldOption1.displayName = 'custFldOpt2';
	  custFldOption1.customFieldId = 12345;
    
    DC_UserService.Value bVal = new DC_UserService.BooleanValue();
    bVal.value = 'true';
    bVal.value_type = 'BooleanValue';
    
    DC_UserService.Value textVal = new DC_UserService.TextValue();
    textVal.value = 'test';
    textVal.value_type = 'TextValue';
    DC_UserService.BaseCustomFieldValue custFldValue = new DC_UserService.CustomFieldValue();
    custFldValue.customFieldId = 12345;
    custFldValue.BaseCustomFieldValue_Type = 'CustomFieldValue';
    custFldValue.value = bVal;
    
    DC_UserService.BaseCustomFieldValue dropDownCustFldValue = new DC_UserService.DropDownCustomFieldValue();
    dropDownCustFldValue.customFieldId = 123456;
    dropDownCustFldValue.BaseCustomFieldValue_Type = 'DropDownCustomFieldValue';
    dropDownCustFldValue.value = textVal;
      
      DC_UserService.User_x userX = new DC_UserService.User_x();     
      userX.id = 12345; 
      userX.name = 'testUser';
      userX.customFieldValues = new List<DC_UserService.BaseCustomFieldValue>{custFldValue};
      
      DC_UserService.User_x userX1 = new DC_UserService.User_x();     
      userX1.name = 'testUser1';
      userX1.dropDownCustomFieldValues = new List<DC_UserService.BaseCustomFieldValue>{dropDownCustFldValue};
      
      DC_UserService.UserPage userPage = new DC_UserService.UserPage();
      userPage.results = new List<DC_UserService.User_x>{userX, userX1};
      userPage.totalResultSetSize = 2;
      userPage.startIndex = 0;  
                      
      if(request instanceof DC_UserService.performUserAction_element)
          response.put('response_x', new DC_UserService.performUserActionResponse_element()); 
      else if(request instanceof DC_UserService.getUsersByStatement_element){
          DC_UserService.getUsersByStatementResponse_element resp = new DC_UserService.getUsersByStatementResponse_element();
          resp.rval = userPage;
          response.put('response_x', resp);
      }else if(request instanceof DC_UserService.getCurrentUser_element) {
      	  DC_UserService.getCurrentUserResponse_element resp = new DC_UserService.getCurrentUserResponse_element();    	
          resp.rval = userX1;
          response.put('response_x', resp);
      }else if(request instanceof DC_UserService.createUsers_element){
          DC_UserService.createUsersResponse_element resp = new DC_UserService.createUsersResponse_element();
          resp.rval = new List<DC_UserService.User_x>{userX};
          response.put('response_x', resp);
      }else if(request instanceof DC_UserService.getAllRoles_element){
          response.put('response_x', new DC_UserService.getAllRolesResponse_element());
      }else if(request instanceof DC_UserService.updateUsers_element) {
          response.put('response_x', new DC_UserService.updateUsersResponse_element());
      } else if (request instanceof DC_UserTeamAssociationService.CreateUserTeamAssociations_element) {
      	response.put('response_x', new DC_UserTeamAssociationService.CreateUserTeamAssociationsResponse_element());
      } else {    
        throw new DC_TestUtils.DC_MethodNotSupportedInMockException();
      } 
          
          
      
      return;
  }
}