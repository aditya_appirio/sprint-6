public with sharing class DC_UserTeamAssociationConnector extends DC_Connector {
  private static DC_UserTeamAssociationConnector instance;
    
  public static DC_UserTeamAssociationConnector getInstance() {
    if(instance == null) {
      instance = new DC_UserTeamAssociationConnector();
    } 
    return instance;
  }
  
  public void createUserTeamAssociation(long dcUserId, long dcTeamId) {
  	DC_DoubleClickUserTeamAssociationManager manager = DC_ManagerFactory.getInstance().getUserTeamAssociationManager();
  	manager.createUserTeamAssociation(dcUserId, dcTeamId, DC_DoubleClickUserTeamAssociationManager.DC_TeamAccessType.READ_WRITE);
  }
  
  public void associateUserWithAllEntitiesTeam(long dcUserId) {
  	createUserTeamAssociation(dcUserId, (long)-1);
  }
}