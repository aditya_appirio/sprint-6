// 
// (c) 2014 Appirio
//
// DC_WebServiceMockImplReportService : implements WebServiceMock for webServicecallout test classes 
//
// 24 Jan 2014    Anjali K (JDC)      Original
//
@IsTest
global class DC_WebServiceMockImplReportService implements WebServiceMock{
  global void doInvoke(Object stub, Object request, Map<String, Object> response,
                    String endpoint, String soapAction, String requestName,
                    String responseNS, String responseName, String responseType){
    
    DC_ReportService.ReportQuery rQuery = new DC_ReportService.ReportQuery();
    
    DC_ReportService.ReportJob report = new DC_ReportService.ReportJob();     
    report.id = 12345;
    report.reportJobStatus = 'New';
    report.reportQuery = rQuery;
    
                    
    if(request instanceof DC_ReportService.getReportDownloadURL_element){
        DC_ReportService.getReportDownloadURLResponse_element resp = new DC_ReportService.getReportDownloadURLResponse_element();
        resp.rval = 'sfid,DoubleClickId\nsfid,DoubleClickId\nsfid,DoubleClickId';
        response.put('response_x', resp);
        //response.put('response_x', new DC_CompanyService.getCompanyResponse_element());
    }else if(request instanceof DC_ReportService.getReportJob_element){
        DC_ReportService.getReportJobResponse_element resp = new DC_ReportService.getReportJobResponse_element();
        resp.rval = report;
        response.put('response_x', resp);
        //response.put('response_x', new DC_CompanyService.updateCompanyResponse_element()); 
    }
    else if(request instanceof DC_ReportService.getReportDownloadUrlWithOptions_element){
        DC_ReportService.getReportDownloadUrlWithOptionsResponse_element resp = new DC_ReportService.getReportDownloadUrlWithOptionsResponse_element();
        resp.rval = 'sfid,DoubleClickId\nsfid,DoubleClickId\nsfid,DoubleClickId';
        response.put('response_x', resp);
        //response.put('response_x', new DC_CompanyService.updateCompaniesResponse_element()); 
    }else if(request instanceof DC_ReportService.runReportJob_element){
        DC_ReportService.runReportJobResponse_element resp = new DC_ReportService.runReportJobResponse_element();
        resp.rval = report;
        response.put('response_x', resp);
        //response.put('response_x', new DC_CompanyService.createCompanyResponse_element()); 
    }
    return;
  }
}