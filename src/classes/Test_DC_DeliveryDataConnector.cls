// 
// (c) 2014 Appirio
//
// Test_DC_SalesforceOpportunityManager : test class for DC_SalesforceOpportunityManager 
//
// 14 feb 2014    Ankit Goyal (JDC)      Original
//
@isTest(seeAllData = true)
public without sharing class Test_DC_DeliveryDataConnector {
	
	 static testMethod void testDeliveryDataConnector(){
		
		Account acc = DC_TestUtils.createAccount('test', true);
    Opportunity opp = DC_TestUtils.createOpportunity('testOpp',System.today(),acc.Id,'Prospecting', false);
    opp.goog_dclk_dsm__DC_DoubleClickId__c = '12345';
    insert opp;
    //PricebookEntry PBE=[select id from PricebookEntry limit 1 ];
    Pricebook2 standardPB = [select id, isActive from Pricebook2 where isStandard=true limit 1];
    if (!standardPB.isActive) {
      standardPB.isActive = true;
      update standardPB;
    }
    
    Product2 prod = new Product2(Name = 'testProd', IsActive = true);
    insert prod;
    PricebookEntry pbe = new PricebookEntry(Pricebook2Id = standardPB.Id, Product2Id = prod.Id, UnitPrice = 10000, IsActive = true, UseStandardPrice = false);
    insert pbe;
    OpportunityLineItem oppLI = DC_TestUtils.createOpportunityLI(23, 10, opp.Id,pbe.Id, false);
    
    goog_dclk_dsm__DC_ObjectMapping__c dcDelMapping = DC_TestUtils.createObjectMapping('DeliveryData', 'DeliveryDataReport', 'OpportunityLineItem', true);
    goog_dclk_dsm__DC_FieldMapping__c dcDelFldMapping1 = DC_TestUtils.createFieldMapping(dcDelMapping.id,'id', 'goog_dclk_dsm__DC_DoubleClickId__c','doubleClickId' , false);
    
    insert new List<goog_dclk_dsm__DC_FieldMapping__c>{dcDelFldMapping1};
        
    oppLI.goog_dclk_dsm__DC_DoubleClickId__c = '12345';
    insert oppLI;
    
		DC_DeliveryDataConnector conn = DC_DeliveryDataConnector.getInstance();
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new DC_HttpCalloutMockImploAuth());
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplProposal());
    //Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplReportService());
    
    
    DC_ProposalService.Proposal proposal = new DC_ProposalService.Proposal();
    proposal.name = 'testProposal';
    proposal.status = 'open';
    
    DC_DoubleClickOAuthHandler oAuthHandler = new DC_DoubleClickOAuthHandler(Userinfo.getUserId());
		
		DC_DeliveryDataMapper mapper = DC_DeliveryDataMapper.getInstance();
		mapper.wrap('test');
		
		DC_DoubleClickProposalDAO proposalDAO = new DC_DoubleClickProposalDAO(oAuthHandler);
    DC_ProposalService.Proposal[] proposals = proposalDAO.createProposals(new List<DC_ProposalService.Proposal>{proposal});
    System.assert(proposals != null);
    System.assert(proposals.size() > 0);
    try{
			conn.syncDeliveryData();
			System.assert(DC_SyncDateManager.deliveryDataReportJobId != null);
    }catch(Exception e){}
    Test.stopTest();
	}
}