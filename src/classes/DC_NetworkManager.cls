public with sharing class DC_NetworkManager extends DC_DoubleClickManager {
  private DC_NetworkDAO networkDAO {
    get {
        if(networkDAO == null) {
            networkDAO = new DC_NetworkDAO(this.authHandler);
        }
        
        return networkDAO;
    }
    set;
  }

  public DC_NetworkManager(DC_DoubleClickOAuthHandler authHandler){
    super(authHandler);
  }

  public String getCurrentNetworkTimezone() {
    DC_NetworkService.Network network = networkDAO.getCurrentNetwork();

    return network.timeZone;
  }
  
}