// 
// (c) 2014 Appirio
//
// contains methods to get and update Contact
// 
//
public with sharing class DC_SalesforceContactManager extends DC_SalesforceManager {
	//get Contact : In Progress
	
	private DC_ContactMapper mapper {
    get {
      if(mapper == null) {
        mapper = DC_ContactMapper.getInstance();
      }
      return mapper;
    }
    set;
  }

  // get the Contact related to the passed in id
  // id   string    contact id for the contact to retrieve
  public DC_GenericWrapper getContact(String id) {
     String fieldNames = this.mapper.getCommaSeparatedSalesforceFieldList('Contact');
     
     String query = 'SELECT ' + fieldNames + ' FROM Contact where id = :id';
     
     return mapper.wrap((List<Contact>)Database.query(query));
  }
  
  // get the contact records from the passed contacts 
  public DC_GenericWrapper getContacts(List<Contact> contacts) {
    if(contacts == null || contacts.size() == 0) {
      return null;
    }

    List<Id> ids = new List<Id>(new Map<Id, Contact>(contacts).keySet());

    return getContacts(ids);
  }  
  
  // get the Contacts for the ids provided and return in a generic wrapper
  // ids  List<Id>  list of ids for which to retrieve Contacts
  public DC_GenericWrapper getContacts(List<Id> ids) {
    String fieldNames = this.mapper.getCommaSeparatedSalesforceFieldList('Contact');

    String query = 'SELECT ' + fieldNames + ' FROM Contact where id in :ids';

    return mapper.wrap((List<Contact>)Database.query(query));
  }  
  
}