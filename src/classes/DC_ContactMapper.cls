// 
// (c) 2014 Appirio
//
// DC_CompanyMapper
// T-246423
// 06 Feb 2014    Ankit Goyal (JDC)      Original 
//
public with sharing class DC_ContactMapper extends DC_ObjectMapper {
  
  private static DC_ContactMapper instance;
  
  //constructor
  private DC_ContactMapper(){
    super();
  }
  public static DC_ContactMapper getInstance() {
    if(instance == null) {
      instance = new DC_ContactMapper();
    } 
    return instance;
  }
  // Overrided method wrap : takes DC_ContactService.Contact as param and call wrap(List<DC_ContactService.Contact>)
  public DC_GenericWrapper wrap(DC_ContactService.Contact contact) {
    List<DC_ContactService.Contact> contactList = new List<DC_ContactService.Contact>();
    contactList.add(contact);
    DC_GenericWrapper result = wrap(contactList);
    
    return result;
  }
  
  // Overrided method wrap : takes List<DC_ContactService.Contact> as param and convert into DC_DoubleClickContactWrapper instance
  public DC_GenericWrapper wrap(List<DC_ContactService.Contact> contacts) {
    return new DC_DoubleClickContactWrapper(this.getObjectMappingByName('contact'), this.getFieldMappingsByName('contact'), contacts);
  }
  
  //Create new DC contacts for SF Contacts
  public override DC_GenericWrapper createNewDCRecords(DC_GenericWrapper contacts) {
    DC_GenericWrapper dc_contacts = this.wrap(new List<DC_ContactService.Contact>());
    contacts.gotoBeforeFirst();
    
    while(contacts.hasNext()) {
      contacts.next();
      dc_contacts.add(new DC_ContactService.Contact());
      dc_contacts.gotoLast();
      updateFields(contacts, dc_contacts);
    }
    return dc_contacts;
  }
}