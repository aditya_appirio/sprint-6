//
//  Class for generating test data for test classes
//
public with sharing class DC_TestUtils {
  public static Account createAccount(String name, boolean isInsert){
    Account acc = new Account(Name = name);
    if(isInsert){
      insert acc;
    }
    return acc;
  }
  
  public static Contact createContact(String fName, String lName, String accId, boolean isInsert){
    Contact cnct = new Contact(FirstName = fName, LastName = lName, AccountId = accId);
    if(isInsert){
      insert cnct;
    }
    return cnct;
  }
  
  public static Opportunity createOpportunity(String name, Date closeDate, String accId, String stage, boolean isInsert){
    Opportunity opp = new Opportunity(Name = name, StageName = stage, CloseDate=closeDate , AccountId = accId);
    if(isInsert){
      insert opp;
    }
    return opp;
  }
  public static OpportunityLineItem createOpportunityLI(Integer qty, Integer price, String oppId,String pbeId, boolean isInsert){
    OpportunityLineItem opp = new OpportunityLineItem(OpportunityId = oppId, Quantity=qty, UnitPrice=price,pricebookEntryId=pbeId);
    if(isInsert){
      insert opp;
    }
    return opp;
  }
  
  public static OpportunityTeamMember createOpportunityTeamMember(String oppId, String role, String userId, boolean isInsert){
    OpportunityTeamMember oppTeamMem = new OpportunityTeamMember(OpportunityId = oppId, TeamMemberRole = role, 
        UserId = userId);
    if(isInsert){
      insert oppTeamMem;
    }
    return oppTeamMem;
  }
  
  public static goog_dclk_dsm__DC_DoubleClickTeamMember__c createDoubleClickTeamMember(String oppId, String role, String userId, boolean isInsert){
    goog_dclk_dsm__DC_DoubleClickTeamMember__c dcTeamMem = new goog_dclk_dsm__DC_DoubleClickTeamMember__c(goog_dclk_dsm__DC_Opportunity__c = oppId, goog_dclk_dsm__DC_Role__c = role, 
        goog_dclk_dsm__DC_User__c = userId);
    if(isInsert){
      insert dcTeamMem;
    }
    return dcTeamMem;
  }
  
  public static goog_dclk_dsm__DC_ObjectMapping__c createObjectMapping(String name, String dcObjName, String sfObjName, boolean isInsert){
    goog_dclk_dsm__DC_ObjectMapping__c objMapping = new goog_dclk_dsm__DC_ObjectMapping__c();
    objMapping.goog_dclk_dsm__DC_SalesforceObjectName__c = sfObjName;
    objMapping.goog_dclk_dsm__DC_DoubleClickObjectName__c = dcObjName;
    objMapping.Name = name;
    if(isInsert){
      insert objMapping;
    }
    return objMapping;
  }  
  
  
  public static goog_dclk_dsm__DC_ObjectMapping__c createObjectMapping(String dcObjName, String sfObjName, boolean isInsert){
    goog_dclk_dsm__DC_ObjectMapping__c objMapping = new goog_dclk_dsm__DC_ObjectMapping__c();
    objMapping.goog_dclk_dsm__DC_SalesforceObjectName__c = sfObjName;
    objMapping.goog_dclk_dsm__DC_DoubleClickObjectName__c = dcObjName;
    objMapping.Name = sfObjName;
    if(isInsert){
      insert objMapping;
    }
    return objMapping;
  }
  
  public static goog_dclk_dsm__DC_FieldMapping__c createFieldMapping(String objMapId, String dcFldName, String dcObjName, String name, boolean isInsert){
    goog_dclk_dsm__DC_FieldMapping__c fldMapping = new goog_dclk_dsm__DC_FieldMapping__c();
    fldMapping.goog_dclk_dsm__DC_Object_Mapping__c = objMapId;
    fldMapping.goog_dclk_dsm__DC_DoubleClickFieldName__c = dcFldName;
    fldMapping.goog_dclk_dsm__DC_SalesforceFieldName__c = dcObjName;
    fldMapping.goog_dclk_dsm__DC_Enabled__c = true;
    fldMapping.name = name;
    if(isInsert){
      insert fldMapping;
    }
    return fldMapping;
  }
  
  public static goog_dclk_dsm__DC_LinkAccountFields__c createDCAccountFields(String name, boolean isDeepLink, boolean isInsert){
    goog_dclk_dsm__DC_LinkAccountFields__c dcLinkAccFlds = new goog_dclk_dsm__DC_LinkAccountFields__c();
    dcLinkAccFlds.name = name;
    dcLinkAccFlds.goog_dclk_dsm__DC_DeepLinktoDSMRecord__c = isDeepLink;
    if(isInsert){
      insert dcLinkAccFlds;
    }
    return dcLinkAccFlds;
  }
  
  public static goog_dclk_dsm__DC_ProcessingQueue__c createDCProcessingQueue(boolean isExecuting, boolean isActive, boolean isAsync, String processX, boolean isInsert){
    goog_dclk_dsm__DC_ProcessingQueue__c processQueue = new goog_dclk_dsm__DC_ProcessingQueue__c();
    processQueue.goog_dclk_dsm__DC_Executing__c = isExecuting;
    processQueue.goog_dclk_dsm__DC_Process__c = processX;
    processQueue.goog_dclk_dsm__DC_Active__c = isActive;
    processQueue.goog_dclk_dsm__DC_Asynchronous__c = isAsync;
    if(isInsert){
      insert processQueue;
    }
    return processQueue;
  }
  
  public static goog_dclk_dsm__DC_QueueItems__c createDCQueueItem(String name, String className, boolean isInsert){
    goog_dclk_dsm__DC_QueueItems__c queueItem = new goog_dclk_dsm__DC_QueueItems__c();
    queueItem.Name = name;
    queueItem.goog_dclk_dsm__dc_ClassName__c = className; 
    if(isInsert){
      insert queueItem;
    }
    return queueItem;
  }
  
  public static goog_dclk_dsm__DC_ConnectorAuthenticationSettings__c createConnectorSetting(String clientId, 
      String appName, String clientSecret, String compCode, String netCode,String redirectURI, String scope, boolean isInsert,String soapURL){
    goog_dclk_dsm__DC_ConnectorAuthenticationSettings__c connSetting = new goog_dclk_dsm__DC_ConnectorAuthenticationSettings__c();
    connSetting.goog_dclk_dsm__DC_ClientID__c = clientId;
    //connSetting.goog_dclk_dsm__Application_Name__c = appName;
    connSetting.goog_dclk_dsm__DC_ClientSecret__c = clientSecret;
    //connSetting.goog_dclk_dsm__Company_Code__c = compCode;
    connSetting.goog_dclk_dsm__DC_NetworkCode__c = netCode;
    connSetting.goog_dclk_dsm__DC_RedirectURI__c = redirectURI;
    connSetting.goog_dclk_dsm__DC_Scope__c = scope;
    connSetting.goog_dclk_dsm__WSDL_Soap_Location__c = soapURL;
    if(isInsert){
      insert connSetting;
    }
    return connSetting;
  }
  
  public static goog_dclk_dsm__DC_Authentication__c createDCAuthentication(String accessToken, String refreshToken, Integer expiresIn, String tokenType, boolean isInsert){
    goog_dclk_dsm__DC_Authentication__c dcAuth = new goog_dclk_dsm__DC_Authentication__c();
    dcAuth.goog_dclk_dsm__DC_AccessToken__c = accessToken;
    dcAuth.goog_dclk_dsm__DC_RefreshToken__c = refreshToken;
    dcAuth.goog_dclk_dsm__DC_ExpiresIn__c = 3600;
    dcAuth.goog_dclk_dsm__DC_TokenType__c = tokenType;
    if(isInsert){
      insert dcAuth;
    }
    return dcAuth;
  }
  
  public static goog_dclk_dsm__DC_ConnectorApplicationSettings__c createConnAppSetting(Integer usersToProcess, boolean isInsert){
    goog_dclk_dsm__DC_ConnectorApplicationSettings__c dcConAppSetting = new goog_dclk_dsm__DC_ConnectorApplicationSettings__c();
    dcConAppSetting.goog_dclk_dsm__DC_UsersToProcessPerPage__c = usersToProcess;
    
    if(isInsert){
      insert dcConAppSetting;
    }
    return dcConAppSetting;
  }
  
  public static goog_dclk_dsm__DC_User__c createDCUser(){
    goog_dclk_dsm__DC_User__c dcUser = new goog_dclk_dsm__DC_User__c();
    dcUser.Name = 'test DC User';
    dcUser.goog_dclk_dsm__DC_Email__c = 'test@testmail.com';
    dcUser.goog_dclk_dsm__DC_Role__c = 'Sales Planer';
    dcUser.goog_dclk_dsm__DC_SalesforceUserRecord__c = userinfo.getUserId();
    dcUser.goog_dclk_dsm__DC_DoubleClickId__c = '12345';
    insert dcUser;
    return dcUser;
  }
  
  public class DC_MethodNotSupportedInMockException extends Exception {}
}