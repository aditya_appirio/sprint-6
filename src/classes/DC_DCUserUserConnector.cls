// 
// (c) 2014 Appirio
//
// DC_DCUserUserConnector : connector of Double click user and SFDC's DC User 
// T-251378
// 20 Feb 2014   Anjali K (JDC)      Original
//
public with sharing class DC_DCUserUserConnector extends DC_Connector {
  
  private static DC_DCUserUserConnector instance;
  private DC_UserMapper userMapper;
  
  public static DC_DCUserUserConnector getInstance() {
    if(instance == null) {
      instance = new DC_DCUserUserConnector();
    } 
    return instance;
  } 
   
  //constructor
  private DC_DCUserUserConnector(){
    super();
    userMapper = DC_MapperFactory.getInstance().getUserMapper();
  }
  
  // Salesforce user manager instance  
  private DC_SalesforceUserManager userManager {
    get {
      if(userManager == null) {
        userManager = managerFactory.getSalesforceUserManager();
      }
      return userManager;
    }
    set;
  }
  
    // DC user manager instance 
  public DC_DoubleClickUserManager dcUserManager {
    get {
      if(dcUserManager == null) {
        dcUserManager = managerFactory.getUserManager();
      }
      return dcUserManager;
    }
    set;
  }
  
  // find doubleclick users by matching on name of dc user that is provided
  // user  goog_dclk_dsm__DC_Users__c  user whose name will be used to find a matching users
  public DC_GenericWrapper findMatchingUsers(goog_dclk_dsm__DC_User__c user) {
    return dcUserManager.getUnmatchedUsersByName(user.Name);
  }

  // link the Salesforce dc user and the DoubleClick User
  // user    goog_dclk_dsm__DC_Users__c           Salesforce dc user to link
  // dcUser  DC_GenericWrapper wrapper containing DoubleClick user to link against
  /*public void linkSFUserAndDCUser(goog_dclk_dsm__DC_User__c user, DC_GenericWrapper dcUser) {
    DC_GenericWrapper wrappedUser = userMapper.wrap(user);
    wrappedUser.next();
    userMapper.linkSFRecordAndDCRecord(wrappedUser, dcUser);

    dcUserManager.updateUser(dcUser);
  }*/ 
  
  public DC_Connector.DC_DoubleClickPageProcessingResult syncUsersByPage(DateTime basis, long pageSize, long offset) {
    DC_Connector.DC_DoubleClickPageProcessingResult result = new DC_Connector.DC_DoubleClickPageProcessingResult();
    DC_GenericWrapper updatedUsers = dcUserManager.getUsersUpdatedSinceLastSync(basis, pageSize, offset); 
    //not needed for user as for user we need to just pull the users from dsm and upsert them into the DC Users object
        
    DC_GenericWrapper matchingUsers = userManager.getMatchingSalesforceRecords(updatedUsers);
    userMapper.updateFieldsOnAllObjectsWithCreate(updatedUsers, matchingUsers);
    
    result.dmlStatus = userManager.upsertUsers(matchingUsers); 
    result.totalResultSetSize = updatedUsers.totalResultSetSize;
    return result;
  }  
  
  // on 26th April Anjali : sync the salesforce contacts to the DoubleClick users
  /*public DC_GenericWrapper syncSFUsersToDCUsers(List<User> userList) {
    DC_GenericWrapper wrap = syncSFRecordsToDCRecords(userMapper.wrap(userList), DC_Constants.USER_BATCH_CONTEXT); 
    return wrap; 
  }*/
  
  public DC_GenericWrapper pushSalesforceUsersToNewDCUsers(List<User> userList) {
  	DC_GenericWrapper wrap = userMapper.wrap(userList);
  	DC_GenericWrapper newRecords = userMapper.createNewDCRecords(wrap, 'User Batch');
  	return dcUserManager.createDCRecords(newRecords);  	
  }

  public void registerCurrentUserWithAllEntitiesTeam() {
    DC_GenericWrapper wrapper = dcUserManager.getCurrentUser();
    wrapper.gotoBeforeFirst();
    wrapper.next();
    long dcUserId = (long)wrapper.getField('DoubleClickId');
    DC_UserTeamAssociationConnector.getInstance().associateUserWithAllEntitiesTeam(dcUserId);
  }
}