/**
 */
@isTest
private class Test_DC_ReportManager {

	static testMethod void testDCReportManager() {
		goog_dclk_dsm__DC_ObjectMapping__c objMapping1 = new goog_dclk_dsm__DC_ObjectMapping__c(goog_dclk_dsm__DC_DoubleClickObjectName__c='Proposal', goog_dclk_dsm__DC_SalesforceObjectName__c = 'Opportunity', name = 'opportunity');
		goog_dclk_dsm__DC_ObjectMapping__c dcDelDataObjMapping = new goog_dclk_dsm__DC_ObjectMapping__c(goog_dclk_dsm__DC_DoubleClickObjectName__c='DeliveryDataReport', goog_dclk_dsm__DC_SalesforceObjectName__c = 'OpportunityLineItem', name = 'DeliveryData');
    insert new List<goog_dclk_dsm__DC_ObjectMapping__c>{objMapping1, dcDelDataObjMapping};
    goog_dclk_dsm__DC_FieldMapping__c fldMapping3 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping4 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'custom.SFDC_Opportunity_ID', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Id', Name = 'sfid');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping5 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'probabilityToClose', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Probability', Name = 'probability');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping6 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'status', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345,  Name = 'status');
    
    goog_dclk_dsm__DC_FieldMapping__c ddFldMapping1 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcDelDataObjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'Dimension.PROPOSAL_LINE_ITEM_ID', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId');
    goog_dclk_dsm__DC_FieldMapping__c ddFldMapping2 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcDelDataObjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'DimensionAttribute.LINE_ITEM_LIFETIME_IMPRESSIONS', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_LifetimeImpressions__c', Name = 'LifetimeImpressions');
    goog_dclk_dsm__DC_FieldMapping__c ddFldMapping3 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcDelDataObjMapping.id, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Id', Name = 'sfid');
    goog_dclk_dsm__DC_FieldMapping__c ddFldMapping4 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcDelDataObjMapping.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'DimensionAttribute.LINE_ITEM_GOAL_QUANTITY', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_GoalQuantity__c', Name = 'GoalQuantity');
    
    insert new List<goog_dclk_dsm__DC_FieldMapping__c>{fldMapping3, fldMapping4, fldMapping5, fldMapping6,ddFldMapping1, ddFldMapping2, ddFldMapping3, ddFldMapping4};
    
    List<goog_dclk_dsm__DC_FieldMapping__c> ddFldMappingList = new List<goog_dclk_dsm__DC_FieldMapping__c>{ddFldMapping1, ddFldMapping2, ddFldMapping3, ddFldMapping4};
    DC_ProposalService.Date_x dateX = new DC_ProposalService.Date_x();
    dateX.year = Date.today().year();
    dateX.month = Date.today().month();
    dateX.day = Date.today().addDays(10).day();
    
    DC_ProposalService.DateTime_x dt = new DC_ProposalService.DateTime_x();
    dt.date_x = dateX;
    
    DC_ProposalService.Proposal proposal = new DC_ProposalService.Proposal();
    proposal.name = 'testProposal';
    proposal.status = 'open';
    proposal.lastModifiedDateTime = dt;
    
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new DC_HttpCalloutMockImploAuth());
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplProposal());
    //Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplReportService());
    
		DC_DoubleClickOAuthHandler oAuthHandler = new DC_DoubleClickOAuthHandler(Userinfo.getUserId());
		
		DC_DeliveryDataMapper mapper = DC_DeliveryDataMapper.getInstance();
		mapper.wrap('test');
		
		DC_DoubleClickProposalDAO proposalDAO = new DC_DoubleClickProposalDAO(oAuthHandler);
    
		DC_ReportManager rptManager = new DC_ReportManager(oAuthHandler);
		System.assert(rptManager.getTodayDate() != null);
		try{
			rptManager.runLineItemDeliveryDataReport();
		}catch(Exception e){}
		try{
			rptManager.isReportJobComplete(12345);
		}catch(Exception e){}
		//rptManager.getReport(12345);
		
		String report = 'sfid,DoubleClickId\nsfid,DoubleClickId\nsfid,DoubleClickId';
		DC_ReportLineWrapper wrapper = new DC_ReportLineWrapper(dcDelDataObjMapping, ddFldMappingList, report);
		wrapper.next();
		wrapper.getField('DoubleClickId');
		wrapper.setField('DoubleClickId', '12345');
		try{
			wrapper.getNewRecords();
		}catch(Exception e){}
		try{
			wrapper.getExistingRecords();
		}catch(Exception e){}
		try{
			wrapper.createWrapper(dcDelDataObjMapping, ddFldMappingList, new List<Object>{report});
		}catch(Exception e){}
		Test.stopTest();
	}
	
	static testMethod void testDCReportManager1() {
    
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new DC_HttpCalloutMockImploAuth());
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplProposal());
    //Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplReportService());
    
		DC_DoubleClickOAuthHandler oAuthHandler = new DC_DoubleClickOAuthHandler(Userinfo.getUserId());
		
		DC_DoubleClickProposalDAO proposalDAO = new DC_DoubleClickProposalDAO(oAuthHandler);
    
		DC_ReportManager rptManager = new DC_ReportManager(oAuthHandler);
		try{
			rptManager.getReport(12345);
		}catch(Exception e){}
		
		DC_ProposalService.Date_x dtX1 = new DC_ProposalService.Date_x();
		dtX1.year = 2013;
		dtX1.month = 10;
		
		DC_ProposalService.Date_x dtX2 = new DC_ProposalService.Date_x();
		dtX2.year = 2014;
		dtX2.month = 11;
		rptManager.getEarliest(null, null);
		rptManager.getLatest(null, null);
		
		System.assertEquals(dtX1, rptManager.getEarliest(dtX1, dtX2));
		System.assertEquals(dtX2, rptManager.getLatest(dtX2, dtX1));
		
		dtX1.year = 2015;
		System.assertEquals(dtX2, rptManager.getEarliest(dtX1, dtX2));
		System.assertEquals(dtX1, rptManager.getLatest(dtX2, dtX1));
		
		dtX1.year = 2014;
		rptManager.getEarliest(dtX1, dtX2);
		rptManager.getLatest(dtX2, dtX1);
		
		dtX1.month = 12;
		rptManager.getEarliest(dtX1, dtX2);
		rptManager.getLatest(dtX2, dtX1);
		System.assertEquals(dtX2, rptManager.getEarliest(dtX1, dtX2));
		System.assertEquals(dtX1, rptManager.getLatest(dtX2, dtX1));
		
		dtX1.month = 11;
		dtX1.day = 1;
		dtX2.day = 2;
		rptManager.getEarliest(dtX1, dtX2);
		rptManager.getLatest(dtX2, dtX1);
		
		dtX1.day = 3;
		dtX2.day = 2;
		
		System.assertEquals(dtX2, rptManager.getEarliest(dtX1, dtX2));
		System.assertEquals(dtX1, rptManager.getLatest(dtX2, dtX1));
		
		rptManager.getEarliest(dtX1, null);
		rptManager.getLatest(dtX2, null);
		Test.stopTest();
	}
}