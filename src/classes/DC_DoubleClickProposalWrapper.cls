// 
// (c) 2014 Appirio
//
// DC_DoubleClickProposalWrappertjhvc
// T-241982
// 24 Jan 2014   Anjali K (JDC)      Original
//
public with sharing class DC_DoubleClickProposalWrapper extends DC_DoubleClickWrapper{
	
  private DC_CustomFieldManager customFieldManager; 

	//Constructor : pass values to DC_DoubleClickWrapper 
	public DC_DoubleClickProposalWrapper(goog_dclk_dsm__DC_ObjectMapping__c objectMapping, List<goog_dclk_dsm__DC_FieldMapping__c> fieldMappings, List<Object> objects) {
    super(objectMapping, fieldMappings, objects); //pass values to DC_DoubleClickWrapper

    this.customFieldManager = DC_ManagerFactory.getInstance().getCustomFieldManager();
  }

	//Get Current Proposal
  private DC_ProposalService.Proposal getCurrentProposal() {
  	System.debug('this.currentObject:::' + this.currentObject);
    if(this.currentObject == null){
      throw new DC_ConnectorException(DC_Constants.ARR_OUT_OF_BOUND_ERR);
    } else if(!(this.currentObject instanceOf DC_ProposalService.Proposal)){
        throw new DC_ConnectorException(DC_Constants.INVALID_PROPOSAL_MAPPING);
    } else{
        return (DC_ProposalService.Proposal) this.currentObject;
    }
  }
	
	//Get DC Field of Proposal respective to Opportunity Field (Param)
	//@param : field => SFDC Field
  public override Object getField(String field) {
    String fieldName = getDoubleClickFieldName(field.toLowerCase());
    System.debug('fieldName::' + fieldName);
    System.debug('custom::' + fieldName.startsWithIgnoreCase('custom'));

    if(fieldName.equalsIgnoreCase('id')) {
      return getCurrentProposal().id;
    } else if(fieldName.equalsIgnoreCase('name')) {
      return getCurrentProposal().name;
    } else if(fieldName.equalsIgnoreCase('startDateTime')) {
      return getCurrentProposal().startDateTime;
    } else if(fieldName.equalsIgnoreCase('endDateTime')) {
      return getCurrentProposal().endDateTime;
    } else if(fieldName.equalsIgnoreCase('budget')) {
      return getCurrentProposal().budget;
    } else if(fieldName.equalsIgnoreCase('probabilityOfClose')) {
      return getCurrentProposal().probabilityOfClose;
    } else if(fieldName.equalsIgnoreCase('agencyCommission')) {
      return getCurrentProposal().agencyCommission;
    } else if(fieldName.equalsIgnoreCase('notes')) {
      return getCurrentProposal().notes;
    } else if(fieldName.equalsIgnoreCase('currencyCode')) {
      return getCurrentProposal().currencyCode;
    }
    else if(fieldName.equalsIgnoreCase('customFieldValues')) {
      return getCurrentProposal().customFieldValues;
    }
    else if(fieldName.equalsIgnoreCase('poNumber')) {
      return getCurrentProposal().poNumber;
    }
    else if(fieldName.equalsIgnoreCase('approvalStatus')) {
      return getCurrentProposal().approvalStatus;
    }
    else if(fieldName.equalsIgnoreCase('dfpOrderId')) {
      return getCurrentProposal().dfpOrderId;
    }
    else if(fieldName.equalsIgnoreCase('lastModifiedDateTime')) {
      return getCurrentProposal().lastModifiedDateTime;
    }
    else if(fieldName.equalsIgnoreCase('exchangeRate')) {
      return getCurrentProposal().exchangeRate;
    }
    else if(fieldName.equalsIgnoreCase('status')) {
      return getCurrentProposal().status;
    }
    else if(fieldName.equalsIgnoreCase('primarySalesperson.split')) {
      return getCurrentProposal().primarySalesperson.split;
    }
    else if(fieldName.equalsIgnoreCase('primarySalesperson')) {
      return getCurrentProposal().primarySalesperson;
    }
    else if(fieldName.equalsIgnoreCase('pricingModel')) {
      return getCurrentProposal().pricingModel;
    }
    else if(fieldName.equalsIgnoreCase('salesPlannerIds')) {
      return getCurrentProposal().salesPlannerIds;
    }
    else if(fieldName.equalsIgnoreCase('primaryTraffickerId')) {
      return getCurrentProposal().primaryTraffickerId;
    } 
    else if(fieldName.equalsIgnoreCase('secondaryTraffickerIds')) {
      return getCurrentProposal().secondaryTraffickerIds;
    }
    else if(fieldName.equalsIgnoreCase('secondarySalespeople')) {
      return getCurrentProposal().secondarySalespeople;
    }
    else if(fieldName.equalsIgnoreCase('issold')) {
      return getCurrentProposal().isSold;
    }     
    else if(fieldName.equalsIgnoreCase('totalresultsetsize')) {
    	return this.totalResultSetSize;
    } 
    else if(fieldName.startsWithIgnoreCase('custom')) {
      Long customFieldId = getCustomFieldId(fieldName);
      
      //updated to handle custom field value of type boolean,number
      Schema.DisplayType customfldType = getCustomFieldType(fieldName,'Opportunity');
      System.debug('------customfldType----------' + customfldType);
      if(customFieldId != null && (Schema.DisplayType.STRING == customfldType)||(Schema.DisplayType.ID == customfldType)) {
        return getTextCustomFieldValue(customFieldId);
      } 
      else if(customFieldId != null && Schema.DisplayType.BOOLEAN == customfldType){
      	return getBoolCustomFieldValue(customFieldId);
      }
      else {
        throw new DC_ConnectorException(DC_Constants.CUSTOM_FIELD_ERROR_STR);
      }
    } else if(fieldName.startsWithIgnoreCase('company')) {
      if(fieldName.endsWithIgnoreCase('id')) {
        if(getCurrentProposal().advertiser != null) {
          return getCurrentProposal().advertiser.companyId;
        } else {
          return null;
        }
      } else {
        return null;
      }
    } else {
      throw new DC_ConnectorException(DC_Constants.getInvalid_Mapping_Proposal(field));
    }
  }

  private String getTextCustomFieldValue(Long fieldId) {
    if(getCurrentProposal().customFieldValues != null) {

      for(DC_ProposalService.BaseCustomFieldValue fieldValue : getCurrentProposal().customFieldValues) {
        
        if (fieldValue instanceOf DC_ProposalService.CustomFieldValue) {
          DC_ProposalService.CustomFieldValue curfield = (DC_ProposalService.CustomFieldValue)fieldValue;
          if(curfield.customFieldId == fieldId) {    
            if (curfield.value instanceOf DC_ProposalService.TextValue) {
              return ((DC_ProposalService.TextValue)curfield.value).value;
            }
          }
        }
      } 
    }

    return null;
  }

  private void setTextCustomFieldValue(Long fieldId, String value) {
    DC_ProposalService.CustomFieldValue field = new DC_ProposalService.CustomFieldValue();
    field.value = new DC_ProposalService.TextValue();
    field.customFieldId = fieldId;
    ((DC_ProposalService.TextValue)field.value).value = this.toString(value);

    if(getCurrentProposal().customFieldValues == null) {
      getCurrentProposal().customFieldValues = new List<DC_ProposalService.BaseCustomFieldValue>();
    } else {
      Integer idx = 0;
      for(DC_ProposalService.BaseCustomFieldValue fieldValue : getCurrentProposal().customFieldValues) {
        if(fieldValue.customFieldId == fieldId) {
          getCurrentProposal().customFieldValues.remove(idx);
          break;
        } 

        idx++;
      }
    }
    if (value != null && value != '') {
      getCurrentProposal().customFieldValues.add(field);
    }
  }
  
  //added to handle custom field value of type boolean
  private Boolean getBoolCustomFieldValue(Long fieldId) {
    if(getCurrentProposal().customFieldValues != null) {

      for(DC_ProposalService.BaseCustomFieldValue fieldValue : getCurrentProposal().customFieldValues) {
        
        if (fieldValue instanceOf DC_ProposalService.CustomFieldValue) {
          DC_ProposalService.CustomFieldValue curfield = (DC_ProposalService.CustomFieldValue)fieldValue;
          if(curfield.customFieldId == fieldId) {    
            if (curfield.value instanceOf DC_ProposalService.BooleanValue) {
              return ((DC_ProposalService.BooleanValue)curfield.value).value;
            }
          }
        }
      } 
    }

    return null;
  }

  //added to handle custom field value of type boolean
  private void setBoolCustomFieldValue(Long fieldId, Boolean value) {
    DC_ProposalService.CustomFieldValue field = new DC_ProposalService.CustomFieldValue();
    field.value = new DC_ProposalService.BooleanValue();
    field.customFieldId = fieldId;
    ((DC_ProposalService.BooleanValue)field.value).value = (Boolean)value;

    if(getCurrentProposal().customFieldValues == null) {
      getCurrentProposal().customFieldValues = new List<DC_ProposalService.BaseCustomFieldValue>();
    } else {
      Integer idx = 0;
      for(DC_ProposalService.BaseCustomFieldValue fieldValue : getCurrentProposal().customFieldValues) {
        if(fieldValue.customFieldId == fieldId) {
          getCurrentProposal().customFieldValues.remove(idx);
          break;
        } 

        idx++;
      }
    }
    //if (value != null && value != '') {
      getCurrentProposal().customFieldValues.add(field);
    //}
  }
  
  //Set value into DC Field
  //@param : Field (SFDC field), value : sfdc field value
  public override void setField(String field, Object value) {
    String fieldName = getDoubleClickFieldName(field);
    
    try{
	    if(fieldName.equalsIgnoreCase('id')) {
	    	getCurrentProposal().id = this.toInteger(value);
	    } else if(fieldName.equalsIgnoreCase('name')) {
	        getCurrentProposal().name = this.toString(value); 
      } else if(fieldName.equalsIgnoreCase('budget')) {
          getCurrentProposal().budget = (DC_ProposalService.Money) value;
      } else if(fieldName.equalsIgnoreCase('probabilityOfClose')) {
          getCurrentProposal().probabilityOfClose = toInteger(value);
      } else if(fieldName.equalsIgnoreCase('startDateTime')) {
          getCurrentProposal().startDateTime = new DC_ProposalService.Datetime_x();
          getCurrentProposal().startDateTime.date_x = new DC_ProposalService.Date_x();

          if(value instanceof Date) {
            getCurrentProposal().startDateTime.date_x.setSalesforceDate((Date) value);
          } else if(value instanceof Datetime) {
            getCurrentProposal().startDateTime.setSalesforceDatetime((Datetime) value);
          } else {
            // TO DO-Done: Jaipur externalize this string
            throw new DC_ConnectorException(DC_Constants.INVALID_TYPE_OF_FIELD + field);
          }
      } else if(fieldName.equalsIgnoreCase('endDateTime')) {
          System.debug('setting enddatetime ' + value);

          getCurrentProposal().endDateTime = new DC_ProposalService.Datetime_x();
          getCurrentProposal().endDateTime.date_x = new DC_ProposalService.Date_x();

          if(value instanceof Date) {
            getCurrentProposal().endDateTime.date_x.setSalesforceDate((Date) value);
          } else if(value instanceof Datetime) {
            getCurrentProposal().endDateTime.setSalesforceDatetime((Datetime) value);
          } else {
            // TO DO-Done: Jaipur externalize this string
            throw new DC_ConnectorException(DC_Constants.INVALID_TYPE_OF_FIELD + field);
          }
      } else if(fieldName.equalsIgnoreCase('agencyCommission')) {
          getCurrentProposal().agencyCommission = this.toLong(value); 
	    } else if(fieldName.equalsIgnoreCase('currencyCode')) {
	        getCurrentProposal().currencyCode = this.toString(value); 
	    } else if(fieldName.equalsIgnoreCase('notes')) {
	        getCurrentProposal().notes = this.toString(value); 
	    } else if(fieldName.equalsIgnoreCase('customFieldValues')) {
	        //getCurrentProposal().customFieldValues = toProposalBaseCustFldVal(value);
	    } else if(fieldName.equalsIgnoreCase('poNumber')) {
	        getCurrentProposal().poNumber = this.toString(value); 
	    } else if(fieldName.equalsIgnoreCase('approvalStatus')) {
	        getCurrentProposal().approvalStatus = this.toString(value); 
	    } else if(fieldName.equalsIgnoreCase('dfpOrderId')) {
	        getCurrentProposal().dfpOrderId = this.toLong(value); 
	    } else if(fieldName.equalsIgnoreCase('lastModifiedDateTime')) {
	        //getCurrentProposal().lastModifiedDateTime = toProposalDateTime(value);
	        getCurrentProposal().lastModifiedDateTime = new DC_ProposalService.Datetime_x();
          getCurrentProposal().lastModifiedDateTime.date_x = new DC_ProposalService.Date_x();

          if(value instanceof Date) {
            getCurrentProposal().lastModifiedDateTime.date_x.setSalesforceDate((Date) value);
          } else if(value instanceof Datetime) {
            getCurrentProposal().lastModifiedDateTime.setSalesforceDatetime((Datetime) value);
          } else {
            throw new DC_ConnectorException(DC_Constants.INVALID_TYPE_OF_FIELD + field);
          }
	    } else if(fieldName.equalsIgnoreCase('exchangeRate')) {
	        getCurrentProposal().exchangeRate = this.toLong(value); 
	    } else if(fieldName.equalsIgnoreCase('status')) {
	        getCurrentProposal().status = this.toString(value); 
	    } else if(fieldName.equalsIgnoreCase('primarySalesperson.split')) {
	        getCurrentProposal().primarySalesperson.split = this.toInteger(value);
      } else if(fieldName.equalsIgnoreCase('primarySalesperson.userId')) {
	        getCurrentProposal().primarySalesperson.userId = this.toLong(value);
      } else if(fieldName.equalsIgnoreCase('primarySalesperson')) {
	        getCurrentProposal().primarySalesperson = toSalesPersonSplitObj(value);
      } else if(fieldName.equalsIgnoreCase('secondarySalespeople')) {
      		DC_ProposalService.SalespersonSplit[] spSplit = toSalesPersonSplit(value);
	        getCurrentProposal().secondarySalespeople = spSplit;
      }
      else if(fieldName.equalsIgnoreCase('salesPlannerIds')) {
          getCurrentProposal().salesPlannerIds = this.toLongArr(value);
      } else if(fieldName.equalsIgnoreCase('primaryTraffickerId')) {
          getCurrentProposal().primaryTraffickerId = this.toLong(value);
      } else if(fieldName.equalsIgnoreCase('secondaryTraffickerIds')) {
          getCurrentProposal().secondaryTraffickerIds = this.toLongArr(value);
      } else if(fieldName.equalsIgnoreCase('refreshExchangeRate')) {
          getCurrentProposal().refreshExchangeRate = (boolean)value;
      } else if(fieldName.equalsIgnoreCase('isSold')) {
          getCurrentProposal().isSold = (boolean)value;          
      } else if(fieldName.equalsIgnoreCase('pricingModel')) {
          getCurrentProposal().pricingModel = this.toString(value);
	    } else if(fieldName.startsWithIgnoreCase('custom')) {
          System.debug('########################### ' + fieldName);
          Long customFieldId = getCustomFieldId(fieldName);
          System.debug('------fieldName----------' + fieldName);
          //updated to solve spotify issue
		  Schema.DisplayType customfldType = getCustomFieldType(fieldName,'Opportunity');
		  System.debug('------customfldType----------' + customfldType);
      	  if(customFieldId != null && (Schema.DisplayType.STRING == customfldType)||(Schema.DisplayType.ID == customfldType)) {
            setTextCustomFieldValue(customFieldId, (String)value);
          } 
          else if(customFieldId != null && Schema.DisplayType.BOOLEAN == customfldType) {
          	setBoolCustomFieldValue(customFieldId, (Boolean)value);
          }
          else {
            throw new DC_ConnectorException(DC_Constants.CUSTOM_FIELD_ERROR_STR);
          }
      } else if(fieldName.startsWithIgnoreCase('company')) {
	        if(fieldName.endsWithIgnoreCase('id')) {
	          getCurrentProposal().advertiser = new DC_ProposalService.ProposalCompanyAssociation();
	
	          getCurrentProposal().advertiser.companyId = this.toInteger(value);
	          // TODO: we will have to persist this in the Account record so that we can map it here
	          getCurrentProposal().advertiser.type_x = 'ADVERTISER';
	        }
      }
	    else {
	      throw new DC_ConnectorException(DC_Constants.getInvalid_Mapping_Proposal(field));
	    }
    }
    catch(System.TypeException e){
    	throw new DC_ConnectorException(e+DC_Constants.FOR_FLD_STR+field);
    }
  }
  
  public void clearTeamMembers() {
  	DC_ProposalService.Proposal proposal = getCurrentProposal();
  	proposal.primarySalesperson = null;
  	proposal.secondarySalespeople = new List<DC_ProposalService.SalesPersonSplit>();
  	proposal.primaryTraffickerId = null;
  	proposal.secondaryTraffickerIds = new Long[]{};
  	proposal.salesPlannerIds = new Long[]{};
  }
  
  public void setPrimarySalesperson(long dcUserId, decimal split) {
  	getCurrentProposal().primarySalesPerson = toSalesPersonSplitObj(dcUserId, split);
  }
  
  public void addSecondarySalesperson(long dcUserId, decimal split) {
    getCurrentProposal().secondarySalespeople.add(toSalesPersonSplitObj(dcUserId, split));
  }
  
  // Method to convert salesforce datetime to Proposal date time
	/*private DC_ProposalService.DateTime_x toProposalDateTime(Object obj){
    if(obj == null) {
      return null;
    }
    if(obj instanceof DateTime) {
      DateTime dt= (Datetime)obj;
      DC_ProposalService.DateTime_x dc_DateTime = new DC_ProposalService.DateTime_x();
      DC_ProposalService.Date_x dc_Date = new DC_ProposalService.Date_x();
      dc_Date.day =dt.day();
      dc_Date.month=dt.month();
      dc_Date.year=dt.year();
      
      dc_DateTime.date_x = dc_Date;
      dc_DateTime.hour = dt.hour();
      dc_DateTime.minute = dt.minute();
      dc_DateTime.second = dt.second();
      
      return (DC_ProposalService.DateTime_x) dc_DateTime;
    } 
    throw new DC_ConnectorException(DC_Constants.UNSUPPORTED_DATA_CONVERT_ERR);
  }*/
  
	//create new records
  public override DC_GenericWrapper getNewRecords() {
    this.gotoBeforeFirst();
    List<Object> result = new List<Object>();
    
    while(this.hasNext()) {
      this.next();
      
      if(this.getField('DoubleClickId') == null) {
      	result.add(this.currentObject);
      }
    }
    
    return new DC_DoubleClickProposalWrapper(this.objectMapping, this.fieldMappings, result);
  }
   
  public override DC_GenericWrapper getExistingRecords() {
    this.gotoBeforeFirst();
    List<Object> result = new List<Object>();
    
    while(this.hasNext()) { 
      this.next();
       
      if(this.getField('DoubleClickId') != null) {
        result.add(this.currentObject);
      }
    }
    
    return new DC_DoubleClickProposalWrapper(this.objectMapping, this.fieldMappings, result);
  }
  
  public override DC_GenericWrapper getCurrentWrapped() {
    return new DC_DoubleClickProposalWrapper(this.objectMapping, this.fieldMappings, new object[] { this.currentObject });
  }
  
  public override DC_GenericWrapper createWrapper(goog_dclk_dsm__DC_ObjectMapping__c objectMapping, List<goog_dclk_dsm__DC_FieldMapping__c> fieldMappings, List<Object> objects) {
    return new DC_DoubleClickProposalWrapper(objectMapping, fieldMappings, objects);
  }
  
  public DC_ProposalService.BaseCustomFieldValue[] toProposalBaseCustFldVal(Object obj){
    if(obj == null) {
      return null;
    }
    if(obj instanceof DC_ProposalService.BaseCustomFieldValue[]) {
      return (DC_ProposalService.BaseCustomFieldValue[]) obj;
    } 
    throw new DC_ConnectorException(DC_Constants.UNSUPPORTED_DATA_CONVERT_ERR);
  }
  
  public DC_ProposalService.SalespersonSplit[] toSalesPersonSplit(Object obj){
    if(obj == null) {
      return null;
    }
    if(obj instanceof DC_ProposalService.SalespersonSplit[]) {
      return (DC_ProposalService.SalespersonSplit[]) obj;
    } 
    throw new DC_ConnectorException(DC_Constants.UNSUPPORTED_DATA_CONVERT_ERR);
  }
  
  public DC_ProposalService.SalespersonSplit toSalesPersonSplitObj(Object obj){
    if(obj == null) {
      return null;
    }
    if(obj instanceof DC_ProposalService.SalespersonSplit) {
      return (DC_ProposalService.SalespersonSplit) obj;
    } 
    throw new DC_ConnectorException(DC_Constants.UNSUPPORTED_DATA_CONVERT_ERR);
  }
  
  public DC_ProposalService.SalespersonSplit toSalesPersonSplitObj(long dcUserId, decimal split){
    DC_ProposalService.SalesPersonSplit splitObj = new DC_ProposalService.SalespersonSplit();
    splitObj.userId = dcUserId;
    splitObj.split = (split != null) ? ((integer)split / 10) * 10000 : 0;        
    return splitObj;
  }  
  
  public class DC_SalespersonSplit {
  	public long userId {get;set;}
  	public decimal split {get;set;}
  	
  	public DC_SalespersonSplit(long dcUserId, decimal splitPercent) {
  		userId = dcUserId;
  		split = (splitPercent / 10) * 10000;
  	}
  }
}