// 
// (c) 2014 Appirio
//
// Custom field data access object
//
// 12 Feb 2014    Ankit Goyal (JDC)      Original
//
public class DC_CustomFieldDAO extends DC_DoubleClickDAO {

  public DC_CustomFieldDAO(DC_DoubleClickOAuthHandler authHandler) {
    super(authHandler);
  }

  public DC_CustomFieldService.Statement createStatement(String query) {
    DC_CustomFieldService.Statement result = new DC_CustomFieldService.Statement();

    result.query = query;
    
    return result;
  }

  public DC_CustomFieldService.Statement createStatement(String query, DC_CustomFieldService.String_ValueMapEntry[] params) {
    DC_CustomFieldService.Statement result = createStatement(query);
    result.values = params;
    
    return result;
  }

  // get a configured doubleclick company interface
  private DC_CustomFieldService.CustomFieldServiceInterfacePort customFieldService {
    get {
      if(this.customFieldService == null) {
        this.customFieldService = DC_ServicesFactory.getCustomFieldService(this.authHandler);
      } 

      return this.customFieldService;
    }
    set;
  }

  protected override void tokenRefreshed() {
    this.customFieldService = null;
  }

  private class CreateCustomFieldCommand implements DC_DoubleClickDAO.Command {
    public DC_CustomFieldDAO context;
    public DC_CustomFieldService.CustomField[] customFields;
    public DC_CustomFieldService.CustomField[] result;
    
    public CreateCustomFieldCommand(DC_CustomFieldDAO context, DC_CustomFieldService.CustomField[] customFields) {
      this.context = context;
      this.customFields = customFields;
    }
    
    public void execute() {
      /*if(Test.isRunningTest()) {
        Integer i = 1;

        for(DC_CustomFieldService.CustomField field : customFields) {
          field.id = i;
        }

        this.result = customFields;
      } else {
        this.result = context.customFieldService.createCustomFields(this.customFields);
      }*/
      this.result = context.customFieldService.createCustomFields(this.customFields);
    }
  }
  
  // retrieve a company using the passed in query string
  public DC_CustomFieldService.CustomField[] createCustomFields(DC_CustomFieldService.CustomField[] customFields) {
    CreateCustomFieldCommand cmd = new CreateCustomFieldCommand(this, customFields);
    
    this.invokeAPI(cmd, true);
    
    return cmd.result;
  }

  private class GetCustomFieldByStatementCommand implements DC_DoubleClickDAO.Command {
    public DC_CustomFieldService.CustomFieldPage result;
    public DC_CustomFieldDAO context;
    public DC_CustomFieldService.Statement statement;
    
    public GetCustomFieldByStatementCommand(DC_CustomFieldDAO context, DC_CustomFieldService.Statement statement) {
      this.context = context;
      this.statement = statement;
    }
    
    public void execute() {
      this.result = context.customFieldService.getCustomFieldsByStatement(statement);
    }
  }
  
  // retrieve a company using the passed in query string
  public DC_CustomFieldService.CustomFieldPage getCustomFieldsByStatement(String query) {
  	System.debug('query:::' + query);
    DC_CustomFieldService.Statement statement = this.createStatement(query);
    System.debug('statement:::' + statement);
    GetCustomFieldByStatementCommand cmd = new GetCustomFieldByStatementCommand(this, statement);
    
    this.invokeAPI(cmd, true);
    
    return cmd.result;
  }


}