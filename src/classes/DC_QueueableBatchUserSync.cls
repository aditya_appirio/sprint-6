//
// queueable batch user sync process --
// 
public with sharing class DC_QueueableBatchUserSync extends DC_QueueableBatchProcess implements
  Database.batchable<long>, 
  Database.AllowsCallouts,
  Database.Stateful {
      
  public DC_RetrieveUsersParams uparams;

  public override long getRecordsToProcessPerPage() {
    return (long)goog_dclk_dsm__DC_ConnectorApplicationSettings__c.getInstance().goog_dclk_dsm__DC_UsersToProcessPerPage__c;
  }
  
  public override datetime getBasisDate() {
  	return DateTime.now();
  }
    
  // DC_QueueableProcess ===============
  public override void initialize(DC_QueueableProcess.DC_QueueableProcessParameter inparams) {
    params = uparams = (DC_RetrieveUsersParams)inparams;
  }

  public override void execute() {
    Database.executeBatch(this, 1);
  } 

  public override System.Type getParameterType() {
     return DC_RetrieveUsersParams.class;
  }
  
  public override DC_Connector.DC_DoubleClickPageProcessingResult processRecords(long pageSize, long pageNo) {
    return DC_DCUserUserConnector.getInstance().syncUsersByPage(basisDate, pageSize, pageNo * pageSize);
  }
  
  public override void updateSyncDate(DateTime dt) {
    return;

    // users do not have sync dates-
  }
  public override String getResultString() {
    return String.format('Total Records Processed: {0}\nInserted Records: {1}\nUpdated Records: {2}\nError Records: {3}', 
      new String[] { 
        (recordsProcessed == null) ? '' : recordsProcessed.format(),
        String.valueOf(insertedRecordCount),
        String.valueOf(updatedRecordCount),
        String.valueOf(errorCount)     
      });
  }   
  

  public class DC_RetrieveUsersParams extends DC_QueueableProcess.DC_QueueableProcessParameter { }
     
}