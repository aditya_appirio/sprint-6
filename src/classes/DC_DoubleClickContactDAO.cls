// 
// (c) 2014 Appirio
//
// Class used for DoubleClick data access object, Contact specific
//
// 29 Jan 2014  Anjali K  Original
//
public with sharing class DC_DoubleClickContactDAO extends DC_DoubleClickDAO{
  
  protected override void tokenRefreshed() {
    this.contactService = null; 
  }
  
  //Construtor
  public DC_DoubleClickContactDAO(DC_DoubleClickOAuthHandler authHandler) {
    super(authHandler);
  }
 
  // get a configured doubleclick company interface
  private DC_ContactService.ContactServiceInterfacePort contactService {
    get {
      if(this.contactService == null) {
        this.contactService = DC_ServicesFactory.getContactService(this.authHandler);
      } 

      return this.contactService;
    }
    set;
  }
  
  //Inner class for getting Contact by statement
  private class GetContactByStatementCommand implements DC_DoubleClickDAO.Command {
    public DC_ContactService.ContactPage result;
    public DC_DoubleClickContactDAO context;
    public DC_ContactService.Statement statement;
    
    public GetContactByStatementCommand(DC_DoubleClickContactDAO context, DC_ContactService.Statement statement) {
      this.context = context;
      this.statement = statement;
    }
    
    public void execute() {
      this.result = context.contactService.getContactsByStatement(statement);
    }
  }
  
  // retrieve a company using the passed in query string
  public DC_ContactService.ContactPage getContactsByStatement(String query) {
    DC_ContactService.Statement statement = this.createContactStatement(query);
    
    GetContactByStatementCommand cmd = new GetContactByStatementCommand(this, statement);
    
    this.invokeAPI(cmd, true);
    
    return cmd.result;
  }
  
  //Inner class for creating Contacts
  private class CreateContactsCommand implements DC_DoubleClickDAO.Command {
    public DC_DoubleClickContactDAO context;
    public DC_ContactService.Contact[] contacts;
    public DC_ContactService.Contact[] result;
    
    public CreateContactsCommand(DC_DoubleClickContactDAO context, DC_ContactService.Contact[] contacts) {
      this.context = context;
      this.contacts = contacts;
    }
    
    public void execute() {
      this.result = context.contactService.createContacts(this.contacts);
    }
  }
  
  // retrieve a company using the passed in query string
  public DC_ContactService.Contact[] createContacts(DC_ContactService.Contact[] contacts) {
    CreateContactsCommand cmd = new CreateContactsCommand(this, contacts);
    
    this.invokeAPI(cmd, true);
    
    return cmd.result;
  }
  
  //Inner class for updating Contacts
  private class UpdateContactsCommand implements DC_DoubleClickDAO.Command {
    public DC_DoubleClickContactDAO context;
    public DC_ContactService.Contact[] contacts;
    
    public UpdateContactsCommand(DC_DoubleClickContactDAO context, DC_ContactService.Contact[] contacts) {
      this.context = context;
      this.contacts = contacts;
    }
    
    public void execute() {
      context.contactService.updateContacts(this.contacts);
    }
  }
  
  //Command for updating Contacts
  public void updateContacts(DC_ContactService.Contact[] contacts) {
    UpdateContactsCommand cmd = new UpdateContactsCommand(this, contacts); 
    
    this.invokeAPI(cmd, true);
  } 
   
  // ** DEPRECATED for 201403 API ====================================
  /*
  //Inner class for getting Contact 
  private class GetContactCommand implements DC_DoubleClickDAO.Command {
    public DC_ContactService.Contact result;
    public DC_DoubleClickContactDAO context;
    public Integer id;
    
    public GetContactCommand(DC_DoubleClickContactDAO context, Integer id) {
      this.context = context;
      this.id = id;
    }
    
    public void execute() {
      this.result = context.contactService.getContact(id);
    }
  }

  // retrieve a DC company by the passed in integer id
  public DC_ContactService.Contact getContact(Integer id, boolean retry) {
    GetContactCommand cmd = new GetContactCommand(this, id);
    
    this.invokeAPI(cmd, true);
    
    return cmd.result;
  }
  
  //Inner class for updating Contact
  private class UpdateContactCommand implements DC_DoubleClickDAO.Command {
    public DC_DoubleClickContactDAO context;
    public DC_ContactService.Contact contact;
    
    public UpdateContactCommand(DC_DoubleClickContactDAO context, DC_ContactService.Contact contact) {
      this.context = context;
      this.contact = contact;
    }
    
    public void execute() {
      context.contactService.updateContact(this.contact);
    }
  }
  //Command for updating Contact
  public void updateContact(DC_ContactService.Contact contact) {
    UpdateContactCommand cmd = new UpdateContactCommand(this, contact);
    
    this.invokeAPI(cmd, true);
  }
  */
  // END DEPRECATED ====================================

}