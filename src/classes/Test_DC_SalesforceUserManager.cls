// 
// (c) 2014 Appirio
//
// Test_DC_SalesforceUserManager : test class for DC_SalesforceUserManager
//
@isTest
private class Test_DC_SalesforceUserManager {

  static testMethod void testDCSFUserManager() {
    goog_dclk_dsm__DC_User__c dcUser = DC_TestUtils.createDCUser();
    goog_dclk_dsm__DC_ObjectMapping__c dcObjMapping1 = new goog_dclk_dsm__DC_ObjectMapping__c();
    dcObjMapping1.goog_dclk_dsm__DC_DoubleClickObjectName__c = 'User';
    dcObjMapping1.Name = 'User';
    dcObjMapping1.goog_dclk_dsm__DC_SalesforceObjectName__c = 'goog_dclk_dsm__DC_User__c';
    insert dcObjMapping1;
    
    goog_dclk_dsm__DC_FieldMapping__c dcFldMapping1 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId', goog_dclk_dsm__DC_Enabled__c  = true);
    goog_dclk_dsm__DC_FieldMapping__c dcFldMapping2 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'externalId', goog_dclk_dsm__DC_SalesforceFieldName__c = 'Id', Name = 'sfid', goog_dclk_dsm__DC_Enabled__c  = true);
    
    goog_dclk_dsm__DC_FieldMapping__c dcFldMapping3 = DC_TestUtils.createFieldMapping(dcObjMapping1.id,'id', 'goog_dclk_dsm__DC_DoubleClickId__c','doubleClickId' , false);
    goog_dclk_dsm__DC_FieldMapping__c dcFldMapping4 = DC_TestUtils.createFieldMapping(dcObjMapping1.id,'id', 'Id', 'sfid' , false);
    insert new List<goog_dclk_dsm__DC_FieldMapping__c>{dcFldMapping1, dcFldMapping2, dcFldMapping3,dcFldMapping4};
    
    DC_SalesforceManager sm = new DC_SalesforceManager();
    DC_SalesforceUserManager sfUserManager = new DC_SalesforceUserManager();
    sfUserManager.getUser(dcUser.id);
    
    
    sfUserManager.getUsers(new List<Id>{dcUser.id});
    
    DC_GenericWrapper gw = sfUserManager.getUsers(new List<goog_dclk_dsm__DC_User__c>{dcUser});
    
    sfUserManager.updateUsers(gw);
    sfUserManager.getUsers(gw);
    sfUserManager.upsertUsers(gw);
    sfUserManager.getMatchingSalesforceRecords(gw);
  }
}