public with sharing class DC_DoubleClickUserTeamAssociationManager extends DC_DoubleClickManager {
	public DC_DoubleClickUserTeamAssociationManager(DC_DoubleClickOAuthHandler authHandler){
    super(authHandler);
  }
  private DC_DoubleClickUserTeamAssociationDAO userTeamAssociationDAO {
    get {
      if(userTeamAssociationDAO == null) {
        userTeamAssociationDAO = new DC_DoubleClickUserTeamAssociationDAO(this.authHandler);
      }
        
      return userTeamAssociationDAO;
    }
    set;
  }
  public void createUserTeamAssociation(long userId, long teamId, DC_TeamAccessType accessType) {
  	DC_UserTeamAssociationService.UserTeamAssociation assoc = new DC_UserTeamAssociationService.UserTeamAssociation();
  	assoc.teamId = teamId;
  	assoc.userId = userId;
  	//assoc.overriddenTeamAccessType = '';//accessType.name();
  	//assoc.defaultTeamAccessType = ''; 
  	
  	userTeamAssociationDAO.createUserTeamAssociations(new DC_UserTeamAssociationService.UserTeamAssociation[] {assoc});
  }
  
  public enum DC_TeamAccessType {
    NONE, READ_ONLY, READ_WRITE  	
  }
  
}