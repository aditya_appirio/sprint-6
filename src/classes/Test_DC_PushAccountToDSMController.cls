// 
// (c) 2014 Appirio
//
// Test_DC_PushAccountToDSMController : test class for DC_PushAccountToDSMController 
//
// 28 Jan 2014    Anjali K (JDC)      Original
// 
@isTest
private class Test_DC_PushAccountToDSMController {
  
  static List<DC_FieldMapping__c> fldMappingList ;
  static Account a2;
  static Account a1;
  static DC_ObjectMapping__c objMapping;
  static DC_Authentication__c objDC_Authentication;
  
  @isTest
  static  void testDC_PushToDSM() {
    createTestData();
    
    ApexPages.StandardController sc = new ApexPages.StandardController(a1);
    DC_PushAccountToDSMController controller = new DC_PushAccountToDSMController(sc);
    
    
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplCompany());
    Test.setMock(HttpCalloutMock.class, new DC_HttpCalloutMockImploAuth());
    Test.startTest();
    controller.getSelectedRecord(a1.id);
    DC_GenericWrapper wrap = controller.executeConnectorGetMatchingRecords();
    System.assert(wrap != null);
    
    wrap.next();
    controller.executeConnectorLink(wrap);
   
    controller.executeConnectorSync();
    controller.getLinkMessage();
    controller.getUniquenessViolationMessage();
    controller.isSelectedRecordReadyForDSM();
    controller.checkExistingDSMRecord();
    System.debug('=== ' + controller.backButtonLabel);
    
    sc = new ApexPages.StandardController(a2);
    controller = new DC_PushAccountToDSMController(sc);
    controller.getSelectedRecord(a2.id);
    controller.isSelectedRecordReadyForDSM();
    controller.checkExistingDSMRecord();
    
    //a2.DC_ReadyForDSM__c = true;
    //update a2;
    sc = new ApexPages.StandardController(a1);
    controller = new DC_PushAccountToDSMController(sc);
    controller.checkExistingDSMRecord();
    
    PageReference thePage = new PageReference('/apex/goog_dclk_dsm__dc_pushtodsmCompanypage'); 
    Test.setCurrentPage(thePage); 
    sc = new ApexPages.StandardController(a1);
    controller = new DC_PushAccountToDSMController(sc);
    controller.checkExistingDSMRecord();
      
      
    Test.stopTest();
  }
  
  @isTest
  static  void testDC_PushToDSM2() {
    createTestData();
    
    ApexPages.StandardController sc = new ApexPages.StandardController(a1);
    DC_PushAccountToDSMController controller = new DC_PushAccountToDSMController(sc);
    
    
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplCompany());
    Test.setMock(HttpCalloutMock.class, new DC_HttpCalloutMockImploAuth());
    Test.startTest();
    controller.getSelectedRecord(a1.id);
    DC_GenericWrapper wrap = controller.executeConnectorGetMatchingRecords();
    System.assert(wrap != null);
    
    //wrap.next();
    controller.executeConnectorSyncExisting();
      
      
    Test.stopTest();
  }
  
  @isTest
  static void testDC_PushToDSM1() {
    createTestData();    
  
    update a2;
    
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplCompany());
    Test.setMock(HttpCalloutMock.class, new DC_HttpCalloutMockImploAuth());
    
    Test.startTest();
      
    ApexPages.StandardController sc = new ApexPages.StandardController(a2);
    DC_PushAccountToDSMController controller = new DC_PushAccountToDSMController(sc);
    System.assert(controller.subtitle != null); 
      
    controller.createDCRecordFromSFRecord();
    System.assert(controller.subtitle != null);
     
    controller.createDCRecordFromSFRecord();
    try{
      controller.createLink(-1);
    }catch(Exception e){}
    System.assert(controller.subtitle != null);

    controller.back();
    List<DC_CompanyService.Company> compList = new List<DC_CompanyService.Company>{new DC_CompanyService.Company()};
    DC_DoubleClickCompanyWrapper w = new DC_DoubleClickCompanyWrapper(objMapping, fldMappingList, compList);
    w.next();
    w.getField('name');
    w.getField('comment');
    w.getField('lastmodifieddatetime');
    
    w.getField('primaryContactid');
    w.getField('faxphone');
    
    w.getField('email');
    w.getField('address');
    w.getField('type_x');
    
    w.getField('appliedlabels');
    w.getField('appliedteamids');
    w.getField('enablesameadvertisercompetitiveexclusion');
    
    
    w.setField('name', 'test');
    w.setField('comment', 'test');
    w.setField('lastmodifieddatetime', System.now());
    
    w.setField('primaryContactid', '12345');
    w.setField('faxphone', '1111111111');
    w.setField('email', 'test@test.com');
    
    w.setField('address', 'test');
    w.setField('type_x', 'test');
    w.setField('appliedlabels', new List<DC_CompanyService.AppliedLabel>());
    w.setField('appliedteamids', new List<Long>());
    w.setField('enablesameadvertisercompetitiveexclusion', 'true');
    Test.stopTest();
  }
  @isTest
  static void testCompanyManager(){
    createTestData();
    
    a2.DC_DoubleClickId__c = '12345';
    update a2;
    DC_DoubleClickOAuthHandler oAuthHandler = new DC_DoubleClickOAuthHandler(null);
    
    test.startTest();
    Test.setMock(HttpCalloutMock.class, new DC_HttpCalloutMockImploAuth());
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplCompany());
    
    DC_CompanyMapper companyMapper = DC_MapperFactory.getInstance().getAccountMapper(); 
    DC_CompanyService.Company company = new DC_CompanyService.Company();     
    company.id = 12345;
    company.name = 'testCompany';
    
    DC_DoubleClickCompanyManager cManager = new DC_DoubleClickCompanyManager(oAuthHandler);
    cManager.getCompaniesUpdatedSinceLastSync();
    cManager.getDCRecords(companyMapper.wrap(company));
    cManager.updateDCRecords(companyMapper.wrap(company));
    //cManager.getCompany(12345);
    
    DC_GenericWrapper gCompWrap1 = companyMapper.wrap(a1);
    DC_GenericWrapper gCompWrap2 = companyMapper.wrap(a2);
    DC_DoubleClickCompanyWrapper wrapper = new DC_DoubleClickCompanyWrapper(objMapping, fldMappingList, new List<DC_CompanyService.Company>{company});
    wrapper.createWrapper(objMapping, fldMappingList, new List<DC_CompanyService.Company>{company});
    wrapper.getCurrentWrapped();
    wrapper.next();
    wrapper.getExistingRecords();
    wrapper.getNewRecords();
    wrapper.getField('primaryPhone');
    wrapper.getField('DoubleClickId');
    wrapper.getField('sfid');
    
    wrapper.setField('primaryPhone', '1111111111');
    wrapper.setField('DoubleClickId', '12345');
    wrapper.setField('sfid', a2.id);
    wrapper = new DC_DoubleClickCompanyWrapper(objMapping, fldMappingList, new List<Account>{a1});
    wrapper.createWrapper(objMapping, fldMappingList, new List<Account>{a1});
    try{
      wrapper.getNewRecords();
    }catch(exception e){}
    try{
      wrapper.getExistingRecords();
    }catch(exception e){}
    
    
    
    ApexPages.StandardController sc = new ApexPages.StandardController(a2);
    DC_PushAccountToDSMController controller = new DC_PushAccountToDSMController(sc);
    controller.getSelectedRecord(a2.id);
    system.debug('testing dc_reconrd list controller');
    controller.checkExistingDSMRecord();  
    DC_LinkRecordController linkCont = new DC_PushAccountToDSMController(sc, 'Account', 'Company');
    linkCont.createLinkSFRecordAndDCRecord(wrapper); 
    
    linkCont.isLink = true;
    System.assert(linkCont.subtitle != null);
    linkCont.isLink = false;
    linkCont.isUpdate = true;
    System.assert(linkCont.subtitle != null);
 
    test.stopTest();
    linkCont.isTestExce = true;
    linkCont.createDCRecordFromSFRecord();
  }
  
  
  static void createTestData(){
    
    DC_ConnectorApplicationSettings__c dcConAppSetting = new DC_ConnectorApplicationSettings__c();
    dcConAppSetting.DC_AllowAccountTriggerPushToDSM__c = true;
    insert dcConAppSetting;
    
    //updated  27-oct-2014
    //DC_Authentication__c objDC_Authentication = new DC_Authentication__c(DC_AccessToken__c ='1324324',DC_ExpiresIn__c = 3600 ,DC_RefreshToken__c = '454655656',DC_TokenType__c = 'Bearer');
    objDC_Authentication = new DC_Authentication__c(DC_AccessToken__c ='1324324',DC_ExpiresIn__c = 3600 ,DC_RefreshToken__c = '454655656',DC_TokenType__c = 'Bearer');
    insert objDC_Authentication;
    
    List<Account> accList = new List<Account>();
    a1 = new Account(name = 'test1');
    a1.DC_ReadyForDSM__c = true;
    a2 = new Account(name = 'test2');
    accList.add(a1);
    accList.add(a2);
    insert accList;
    
    objMapping = new DC_ObjectMapping__c(DC_DoubleClickObjectName__c='Company', DC_SalesforceObjectName__c = 'Account', name = 'account');
    insert objMapping;
    
    fldMappingList = new List<DC_FieldMapping__c>();
    DC_FieldMapping__c fldMapping1 = new DC_FieldMapping__c(DC_Object_Mapping__c = objMapping.id, DC_DoubleClickFieldName__c = 'id', DC_SalesforceFieldName__c = 'DC_DoubleClickId__c', Name = 'DoubleClickId');
    DC_FieldMapping__c fldMapping2 = new DC_FieldMapping__c(DC_Object_Mapping__c = objMapping.id, DC_DoubleClickFieldName__c = 'externalId', DC_SalesforceFieldName__c = 'Id', Name = 'sfid');
    
    fldMappingList.add(new DC_FieldMapping__c(DC_Object_Mapping__c = objMapping.id, DC_DoubleClickFieldName__c = 'name', DC_SalesforceFieldName__c = 'name', Name = 'name'));
    fldMappingList.add(new DC_FieldMapping__c(DC_Object_Mapping__c = objMapping.id, DC_DoubleClickFieldName__c = 'type_x', DC_SalesforceFieldName__c = 'type', Name = 'type_x'));
    fldMappingList.add(new DC_FieldMapping__c(DC_Object_Mapping__c = objMapping.id, DC_DoubleClickFieldName__c = 'address', DC_SalesforceFieldName__c = 'billingcity', Name = 'address'));
    fldMappingList.add(new DC_FieldMapping__c(DC_Object_Mapping__c = objMapping.id, DC_DoubleClickFieldName__c = 'email', DC_SalesforceFieldName__c = 'Description', Name = 'email'));
    fldMappingList.add(new DC_FieldMapping__c(DC_Object_Mapping__c = objMapping.id, DC_DoubleClickFieldName__c = 'faxphone', DC_SalesforceFieldName__c = 'Phone', Name = 'faxphone'));
    fldMappingList.add(new DC_FieldMapping__c(DC_Object_Mapping__c = objMapping.id, DC_DoubleClickFieldName__c = 'primaryphone', DC_SalesforceFieldName__c = 'Phone', Name = 'primaryphone'));
    fldMappingList.add(new DC_FieldMapping__c(DC_Object_Mapping__c = objMapping.id, DC_DoubleClickFieldName__c = 'primaryContactId', DC_SalesforceFieldName__c = 'DC_DoubleClickId__c', Name = 'primaryContactId'));
    fldMappingList.add(new DC_FieldMapping__c(DC_Object_Mapping__c = objMapping.id, DC_DoubleClickFieldName__c = 'lastmodifieddatetime', DC_SalesforceFieldName__c = 'lastmodifieddate', Name = 'lastmodifieddatetime'));
    fldMappingList.add(new DC_FieldMapping__c(DC_Object_Mapping__c = objMapping.id, DC_DoubleClickFieldName__c = 'comment', DC_SalesforceFieldName__c = 'Description', Name = 'comment'));
    fldMappingList.add(new DC_FieldMapping__c(DC_Object_Mapping__c = objMapping.id, DC_DoubleClickFieldName__c = 'appliedlabels', DC_SalesforceFieldName__c = 'Description', Name = 'appliedlabels'));
    fldMappingList.add(new DC_FieldMapping__c(DC_Object_Mapping__c = objMapping.id, DC_DoubleClickFieldName__c = 'appliedteamids', DC_SalesforceFieldName__c = 'Description', Name = 'appliedteamids'));
    fldMappingList.add(new DC_FieldMapping__c(DC_Object_Mapping__c = objMapping.id, DC_DoubleClickFieldName__c = 'enablesameadvertisercompetitiveexclusion', DC_SalesforceFieldName__c = 'DC_ReadyforDSM__c', Name = 'enablesameadvertisercompetitiveexclusion'));
    
    fldMappingList.add(fldMapping1); 
    fldMappingList.add(fldMapping2);
    insert fldMappingList;
  }
}