// 
// (c) 2014 Appirio
//
// T-254123 : batch class for pushing Opportunities to DSM that not exist yet
// 
// 4th March 2014    Anjali K (JDC)      Original
//
public with sharing class DC_PushOpportunityToDSMBatch extends DC_PushRecordToDSMProcess implements
	Database.Batchable<sObject>,
  Database.AllowsCallouts,
  Database.Stateful {
 	
 	/*private string query;
 	private boolean readyForDSM = true;
 	
 	public DC_PushOpportunityToDSMBatch() {}
 	public DC_PushOpportunityToDSMBatch(string soql) {
 	  query = soql;
 	}
 	
 	public DC_PushOpportunityToDSMBatch(boolean useReadyForDSM) {
 		readyForDSM = useReadyForDSM;
 	}
 	
	public Database.QueryLocator start( Database.BatchableContext bc ){
		string useQuery = (query == null || query == '') ? 
		  'Select Id from Opportunity where goog_dclk_dsm__DC_DoubleClickId__c = null AND goog_dclk_dsm__DC_ReadyForDSM__c = :readyForDSM' : query;
    
    return Database.getQueryLocator( useQuery );
  }
  
  public void execute( Database.BatchableContext bc, List<SObject> sObjectList ) { 
  	try {
  		DC_OpportunityProposalConnector.getInstance().syncSFOpportunitiesToDCProposals((List<Opportunity>)sObjectList);
  	} catch (Exception e) {
  		DC_SalesforceDML.getInstance().flush();
  	}
  	  
  }
  public void finish(Database.BatchableContext info) {  }    
	
	*/
	
	public DC_PushOpportunityToDSMBatch() {}
 	public DC_PushOpportunityToDSMBatch(string soql) {
 	  query = soql;
 	}
 	
 	public DC_PushOpportunityToDSMBatch(boolean useReadyForDSM) {
 		readyForDSM = useReadyForDSM;
 	}
 	
	
  
  public override string getContext() {
  	return 'Push Opportunity to DSM';
  }
  
  public override void executeSync(List<SObject> sObjectList){
  	DC_OpportunityProposalConnector.getInstance().syncSFOpportunitiesToDCProposals((List<Opportunity>)sObjectList);
  }
  public override String getDefaultQuery(){
  	String defaultQry = 'Select Id from Opportunity where goog_dclk_dsm__DC_DoubleClickId__c = null AND goog_dclk_dsm__DC_ReadyForDSM__c = :readyForDSM';
    return defaultQry;
  }
  
}