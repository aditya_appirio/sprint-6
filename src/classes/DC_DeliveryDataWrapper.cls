public with sharing class DC_DeliveryDataWrapper extends DC_ReportLineWrapper {
  public DC_DeliveryDataWrapper(goog_dclk_dsm__DC_ObjectMapping__c objectMapping, List<goog_dclk_dsm__DC_FieldMapping__c> fieldMappings, String report) {
    super(objectMapping, fieldMappings, report);
  }
}