public with sharing class DC_SFTeamDCTeamConnector extends DC_Connector {
  
  private static DC_SFTeamDCTeamConnector instance;
  private DC_TeamMapper teamMapper;
  
  public static DC_SFTeamDCTeamConnector getInstance() {
    if(instance == null) {
      instance = new DC_SFTeamDCTeamConnector();
    } 
    return instance;
  } 
   
  //constructor
  private DC_SFTeamDCTeamConnector(){
    super();
    teamMapper = DC_MapperFactory.getInstance().getTeamMapper();
  }
  
  // Salesforce user manager instance  
  private DC_SalesforceTeamManager sfTeamManager {
    get {
      if(sfTeamManager == null) {
        sfTeamManager = managerFactory.getSalesforceTeamManager();
      }
      return sfTeamManager;
    }
    set;
  }
    
    // DC user manager instance
  public DC_DoubleClickTeamManager dcTeamManager {
    get {
      if(dcTeamManager == null) {
        dcTeamManager = managerFactory.getTeamManager();
      }
      return dcTeamManager;
    }
    set;
  }
  
  public DC_Connector.DC_DoubleClickPageProcessingResult syncTeamsByPage(DateTime basis, long pageSize, long offset) {
    DC_Connector.DC_DoubleClickPageProcessingResult result = new DC_Connector.DC_DoubleClickPageProcessingResult();
    DC_GenericWrapper updatedTeams = dcTeamManager.getTeamsUpdatedSinceLastSync(basis, pageSize, offset); 
    //not needed for user as for user we need to just pull the users from dsm and upsert them into the DC Users object
        
    DC_GenericWrapper matchingTeams = sfTeamManager.getMatchingSalesforceRecords(updatedTeams);
    boolean isHandlerClass = teamMapper.updateFieldsOnAllObjectsWithCreate(updatedTeams, matchingTeams);
    
    result.dmlStatus = sfTeamManager.upsertTeams(matchingTeams); 
    result.totalResultSetSize = updatedTeams.totalResultSetSize;
    return result;
  }  
  
}