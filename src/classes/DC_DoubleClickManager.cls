// 
// (c) 2014 Appirio
//
// DC_DoubleClickManager implements DC_Manager
// DC Connector
//
// 20 Jan 2014    Anjali K (JDC)      Original
//
public abstract with sharing class DC_DoubleClickManager implements DC_Manager {

  protected DC_DoubleClickOAuthHandler authHandler;
  
  //Constructor
  //@param : DC_Object_Mapping and goog_dclk_dsm__DC_Field_Mapping__c array
  public DC_DoubleClickManager(DC_DoubleClickOAuthHandler authHandler){
    this.authHandler = authHandler;
  }
  public virtual DC_GenericWrapper getDCRecords(DC_GenericWrapper companies){
  	System.debug('>>>>getCompanies of DoubleClickManager');
  	return null;
  }
  public virtual void updateDCRecords(DC_GenericWrapper companies) {
  	System.debug('>>>>updateCompanies of DoubleClickManager');
  }
  public virtual DC_GenericWrapper createDCRecords(DC_GenericWrapper companies) {
  	System.debug('>>>>createCompanies of DoubleClickManager');
    return null;
  }
}