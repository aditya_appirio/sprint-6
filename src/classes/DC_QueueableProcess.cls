public abstract class DC_QueueableProcess {
  public DC_QueueableProcessParameter params;

  // abstract methods =====================================================  
  public abstract void initialize(DC_QueueableProcessParameter inParams);
  public abstract void execute();
  public abstract System.Type getParameterType();
  
  public abstract class DC_QueueableProcessParameter {
    public goog_dclk_dsm__DC_ProcessingQueue__c queueItem { get; set; } 
    public string serialize() {
      return JSON.serialize(this);
    }
  }
}