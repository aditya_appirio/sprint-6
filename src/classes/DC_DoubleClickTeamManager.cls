public with sharing class DC_DoubleClickTeamManager extends DC_DoubleClickManager {
  
  private DC_TeamMapper mapper {
    get {
      if(mapper == null) {
        mapper = DC_TeamMapper.getInstance();
      }
      
      return mapper;
    }
    set;
  }
  
  private DC_DoubleClickTeamDAO teamDAO {
    get {
      if(teamDAO == null) {
        teamDAO = new DC_DoubleClickTeamDAO(this.authHandler);
      }
        
      return teamDAO;
    }
    set;
	}

	public DC_DoubleClickTeamManager(DC_DoubleClickOAuthHandler authHandler){
    super(authHandler);
	}
	 
	public DC_GenericWrapper getTeamsUpdatedSinceLastSync(DateTime basis, long pageSize, long offset) {
		// isactive
  	return mapper.wrap(teamDAO.getTeamsByStatement('ORDER BY Id LIMIT ' + pageSize + ' OFFSET ' + offset));
  }
  /*  
  public DC_GenericWrapper getTeamsUpdatedSinceLastSync(Datetime lastSyncDate) {
  	return getTeamsUpdatedSinceLastSync(lastSyncDate, DC_Constants.Team_PAGE_SIZE, 0);
  } 
  
	public DC_GenericWrapper getTeam(Integer id) {
    return mapper.wrap(teamDAO.getTeam(id, true));
	}

	public void updateTeam(DC_GenericWrapper Team) {
	  teamDAO.updateTeam((DC_TeamService.Team)Team.getCurrentObject());
	}
  
  public DC_GenericWrapper getUnmatchedTeamsByName(String name) {
  	DC_TeamService.TeamPage cPage = teamDAO.getTeamsByStatement('WHERE name = \'' + name.replace('\'', '\'\'') + '\'' );
  	if(cPage != null){
  		return mapper.wrap(cPage.results);
  	}else{
  		return null;
  	}
  }
  */
  // TO DO: make sure we fetch pages of data 
  //Doubt: Not sure of this one. Do we limit the no. of Teams we are getting through getTeamsByStatement?
  /*
  public override DC_GenericWrapper getDCRecords(DC_GenericWrapper teams) {
    if (teams.size() == 0)
      return mapper.wrap(new DC_TeamService.Team[] {} );
    
    teams.gotoBeforeFirst();
    String ids = '';
    String separator = '';
    List<DC_TeamService.Team> result = new List<DC_TeamService.Team>(); 
    while(teams.hasNext()) {
      teams.next();
        
      ids += separator + teams.getField('DoubleClickId');
      separator = ',';  
    }
    if(teamDAO.getTeamsByStatement('WHERE id in (' + ids + ')') != null){
      return mapper.wrap(TeamDAO.getTeamsByStatement('WHERE id in (' + ids + ')').results);
    }
    return null;
  }
  
  public override DC_GenericWrapper createDCRecords(DC_GenericWrapper Teams) {
  	System.debug('Teams:::in123 ' + teams);
    DC_TeamService.Team[] apiTeams = new DC_TeamService.Team[Teams.size()];
        
    teams.gotoBeforeFirst();
    while(teams.hasNext()) {
      teams.next();

      DC_TeamService.Team team = (DC_TeamService.Team) teams.getCurrentObject();
      
      apiTeams[teams.getCurrentIndex()] = team;
    }
    return mapper.wrap(teamDAO.createTeams(apiTeams));
  }
    
  // update the provided Teams in DoubleClick
  // Teams  DC_GenericWrapper   wrapper containing Teams for update
  public override void updateDCRecords(DC_GenericWrapper teams) {
    DC_TeamService.Team[] apiTeams = new DC_TeamService.Team[teams.size()];
    
    teams.gotoBeforeFirst();
    
    while(teams.hasNext()) {
      teams.next();
      apiTeams[teams.getCurrentIndex()] = (DC_TeamService.Team) teams.getCurrentObject();
    }
    
    TeamDAO.updateTeams(apiTeams);
        
  }
  */
}