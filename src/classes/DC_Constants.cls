// 
// (c) 2014 Appirio
//
// Contains all string variable
// T-238071
// 17 Jan 2014   Anjali K (JDC)      Original
//Contains constant string
//
public with sharing class DC_Constants {
	public static final String APPLICATION_NAME = 'sfdc-dc-connector';
  public static final String CODE = 'code';
  public static final String OFFLINE = 'offline';
  public static final String FORCE = 'force';
  public static final String POST_METHOD = 'POST';
  public static final String AUTH_CODE = 'authorization_code';
  public static final String END_POINT = 'https://accounts.google.com/o/oauth2/token';
  public static final String SERVICE_END_POINT = goog_dclk_dsm__DC_ConnectorAuthenticationSettings__c.getOrgDefaults().goog_dclk_dsm__EndPoint_Url__c;
  
  public static final String WSDL_SOAP_LOCATION = goog_dclk_dsm__DC_ConnectorAuthenticationSettings__c.getOrgDefaults().goog_dclk_dsm__WSDL_Soap_Location__c;
  public static final String REFRESH_TOKEN = 'refresh_token';
  public static final String ACCESS_TOKEN = 'access_token';
  public static final String TOKEN_TYPE = 'token_type';
  public static final String EXPIRES_IN = 'expires_in';
  public static final String ERR_DESC = 'error_description';
  public static final String ERROR = 'error';
  public static final String AUTH_STR = 'Authorization';
  public static final String SALESFORCE_STR = 'Salesforce';
  public static final String DOUBLECLICK_STR = 'DoubleClick';
  public static final String MAPPING_STR = 'Mapping ';
  public static final String NOT_FOUND_STR = ' not found!';
  public static final String ARR_OUT_OF_BOUND_ERR = 'Array index out of bounds!!';
  public static final String FOR_FLD_STR = ' for field:';
  public static final String INVALID_PROPOSAL_MAPPING = 'Invalid Mapping for Proposal';
  public static final String INVALID_COMPANY_MAPPING = 'Invalid Mapping for Company';
  public static final String INVALID_USER_MAPPING = 'Invalid Mapping for User';
  public static final String INVALID_TEAM_MAPPING = 'Invalid Mapping for Team';
  public static final String LISTS_SIZE_MUST_MATCH = 'List sizes must match';
  public static final String UNIQUE_ERROR = 'UniqueError.NOT_UNIQUE';
  public static final String ACCOUNT_ALREADY_EXIST = 'Account already exists in DSM.';
  public static final String DUPLIATE_ACCOUNT_NAME_ERR = 'Two Companies cannot exist in DSM with the same name.  Please rename the Salesforce Account or the DSM Company before continuing.';
  public static final String ACC_COMP_LINK_SUCCESS_MSG = 'Your Salesforce Account has been linked successfully with its DSM Company';
  public static final String ACC_COMP_CREATE_SUCCESS_MSG = 'Your Salesforce Account has been created successfully in DSM';
  public static final String UNSUPPORTED_DATA_CONVERT_ERR = 'Unsupported data conversion';
  public static final String INVALID_TYPE_OF_FIELD = 'Error trying to set field value: value is of invalid type for field ';
  public static final String ACCOUNT_CONTEXT = 'Account';
  public static final String CONTACT_CONTEXT = 'Contact';
  public static final String USER_CONTEXT = 'DC User';
  public static final String USER_BATCH_CONTEXT = 'User Batch';
  public static final String TEAM_CONTEXT = 'DC Team';
  public static final String CUSTOM_FIELD_ERROR_STR = 'Custom field has not been created, or ID has not being retrieved from DoubleClick, please contact your administrator to finalize the mapping';
  public static final Integer PROPOSAL_PAGE_SIZE = 1;
  public static final Integer USER_PAGE_SIZE = 1;
  public static final Integer Team_PAGE_SIZE = 1;
  public static final String AUTH_DOUBLECLICK_SFDC_CONNECTOR = 'Click the Button below to Authorize the Google DoubleClick Salesforce Connector with DoubleClick.  You must have an account in DoubleClick to perform this action';
  public static final String AUTH_CONNECT_SUCCESS_MSG = 'You have successfully authorized the connector.';
  public static final String SCHEDULE_CREATED_MSG = 'Schedule Created';
  public static final String FIELD_MAPPINGS_INSERTED_MSG = 'Field mappings successfully created';
  public static final String OBJECT_MAPPINGS_INSERTED_MSG = 'Object mappings successfully created';
  public static final String DEEP_LINK_TO_ACCOUNTS_INSERTED_MSG = 'Deep link to accounts successfully created';
  public static final String DEEP_LINK_TO_CONTACTS_INSERTED_MSG = 'Deep link to contacts successfully created';
  public static final String DEEP_LINK_TO_PROPOSAL_INSERTED_MSG = 'Deep link to proposal successfully created';
  public static final String OBJECT_DEEP_LINK_INSERTED_MSG = 'Object deep link to successfully created';
  public static final String PROCESSING_QUEUE_INSERTED_MSG = 'Processing queue successfully created';
  public static final String QUEUE_ITEMS_INSERTED_MSG = 'Queue items inserted';

  public static final String SFDC_OPPORTUNITY_ID = 'SFDC_Opportunity_ID';
  
  public static String getInvalid_Mapping_Company(String userinput){
    return 'Invalid Mapping '+ userinput + ' for DoubleClick object Company';
  }
  public static String getInvalid_Mapping_Proposal(String userinput){
    return 'Invalid Mapping '+ userinput + ' for DoubleClick object Proposal';
  }
  public static String getInvalid_Mapping_ProposalLI(String userinput){
    return 'Invalid Mapping '+ userinput + ' for DoubleClick object Proposal Line Item';
  }
  public static String getInvalid_Mapping_Contact(String userinput){
    return 'Invalid Mapping '+ userinput + ' for DoubleClick object Contact';
  }
  public static String getInvalid_Mapping_User(String userinput){
    return 'Invalid Mapping '+ userinput + ' for DoubleClick object User';
  }
  
  public static String getCustomFldMalformError(String userinput){
    return 'Invalid mapping ' + userinput + ', custom field name is malformed it should be custom.<desired field name>';
  }

  // the following creates label dependencies to support packaging.
  public static final String[] labels = new String[] { Label.DC_Name, Label.DC_Email, Label.DC_workphone, Label.DC_Title, Label.DC_SF_Account, Label.DC_DoubleClickId, Label.DC_Address };
  
}