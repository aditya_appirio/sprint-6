// 
// (c) 2014 Appirio
//
// DC_DoubleClickUserDAO
// T-251378 
// 
// 20 Feb 2014    Anjali K (JDC)      Original
//
public with sharing class DC_DoubleClickUserDAO extends DC_DoubleClickDAO {

  protected override void tokenRefreshed() {
  	this.userService = null;
  }
  
  public DC_DoubleClickUserDAO(DC_DoubleClickOAuthHandler authHandler) {
    super(authHandler);
  }
  
  // get a configured doubleclick user interface
  private DC_UserService.UserServiceInterfacePort userService { 
    get {
      if(this.userService == null) {
        this.userService = DC_ServicesFactory.getUserService(this.authHandler);
      } 

      return this.userService;
    }
    set;
  } 
  
  public DC_UserService.Statement createStatement(String query) {
    DC_UserService.Statement result = new DC_UserService.Statement();

    result.query = query;  
    
    return result;
  }

  public DC_UserService.Statement createStatement(String query, DC_UserService.String_ValueMapEntry[] params) {
    DC_UserService.Statement result = createStatement(query);
    result.values = params;
    
    return result;
  }

  
  private class GetCurrentUserCommand implements DC_DoubleClickDAO.Command {
    public DC_UserService.User_x result;
    public DC_DoubleClickUserDAO context;
    public Integer id;
    
    public GetCurrentUserCommand(DC_DoubleClickUserDAO context) {
      this.context = context;
      this.id = id;
    }
    
    public void execute() {
      this.result = context.userService.getCurrentUser();
    }
  }

  // retrieve a DC user by the passed in integer id
  public DC_UserService.User_x getCurrentUser() {
    GetCurrentUserCommand cmd = new GetCurrentUserCommand(this);
    
    this.invokeAPI(cmd, true);
    
    return cmd.result;
  }  
  
  private class GetUserByStatementCommand implements DC_DoubleClickDAO.Command {
    public DC_UserService.UserPage result;
    public DC_DoubleClickUserDAO context;
    public DC_UserService.Statement statement;
    
    public GetUserByStatementCommand(DC_DoubleClickUserDAO context, DC_UserService.Statement statement) {
    	this.context = context;
    	this.statement = statement;
    } 
    
  	public void execute() {
			System.debug('statement::' + statement);
    	this.result = context.userService.getUsersByStatement(statement);
    	if (this.result == null || this.result.results == null || this.result.results.size() == 0) {
        return;        
      }
      System.debug('this.result.results:::' + this.result.results);
      for (DC_UserService.User_x user : this.result.results) {
      	System.debug('user.customFieldValues:::' + user.customFieldValues);
      	System.debug('user.dropDownCustomFieldValues:::' + user.dropDownCustomFieldValues);
        if ((user.customFieldValues == null || user.customFieldValues.size() == 0) && (user.dropDownCustomFieldValues == null || user.dropDownCustomFieldValues.size() == 0)) {
          continue;
        } 
        if(user.customFieldValues != null ){
	        for (integer i = 0; i < user.customFieldValues.size(); i++) {
	          System.debug('@@ Custom Field Text: ' + user.customFieldValues[i]);
	          System.debug('@@ Custom Field Type: ' + user.customFieldValues[i].BaseCustomFieldValue_Type);
	
	          user.customFieldValues[i] = copyCustomField(user.customFieldValues[i]);
	        }
        }
        if(user.dropDownCustomFieldValues != null){
	        for (integer i = 0; i < user.dropDownCustomFieldValues.size(); i++) {
	          System.debug('@@ Custom Field Text: ' + user.dropDownCustomFieldValues[i]);
	          System.debug('@@ Custom Field Type: ' + user.dropDownCustomFieldValues[i].BaseCustomFieldValue_Type);
	
	          user.dropDownCustomFieldValues[i] = copyCustomField(user.dropDownCustomFieldValues[i]);
	        } 
				}
        System.debug('@@ Copied Custom Fields: ' + user.customFieldValues);
    	}
		}
		public DC_UserService.BaseCustomFieldValue copyCustomField(DC_UserService.BaseCustomFieldValue inval) {
		  DC_UserService.BaseCustomFieldValue ret;
		  if (inval.BaseCustomFieldValue_Type == 'CustomFieldValue') {
		    DC_UserService.CustomFieldValue cfv = new DC_UserService.CustomFieldValue();
		    cfv.customFieldId = inval.customFieldId;
		    cfv.value = copyCustomFieldValue(inval.value); // copy value
		    System.debug('@@ Value after copy: ' + cfv.value);
		    ret = cfv;
		  } else if (inval.BaseCustomFieldValue_Type == 'DropDownCustomFieldValue') {
		  	DC_UserService.DropDownCustomFieldValue ddcfv = new DC_UserService.DropDownCustomFieldValue();
        ddcfv.customFieldId = inval.customFieldId;
        ddcfv.customFieldOptionId = inval.customFieldOptionId; // copy value
        ret = (DC_UserService.BaseCustomFieldValue)ddcfv;
		  }
		  return ret;
		}

		public DC_UserService.Value copyCustomFieldValue(DC_UserService.Value inval) {
		  DC_UserService.Value ret;
		  System.debug('@@ Service value: ' + inval.value);
	
	  	if (inval.value_type == 'BooleanValue') {
		    DC_UserService.BooleanValue bv = new DC_UserService.BooleanValue();
		    bv.value = boolean.valueOf(inval.value);
		    ret = bv;
	      } else if (inval.value_type == 'NumberValue') {
	        DC_UserService.NumberValue nv = new DC_UserService.NumberValue();
	        nv.value = inval.value;
	        ret = nv;
	      } else if (inval.value_type == 'TextValue') {
	        DC_UserService.TextValue tv = new DC_UserService.TextValue();
	        tv.value = inval.value;
	        System.debug('@@ TextValue: ' + tv.value);
	        ret = tv;
	      }
	
	  	return ret;
		}    
  }
  // retrieve a user using the passed in query string
  public DC_UserService.UserPage getUsersByStatement(String query) {
  	System.debug('query::' + query);
    DC_UserService.Statement statement = this.createStatement(query);
    
    GetUserByStatementCommand cmd = new GetUserByStatementCommand(this, statement);
    
    this.invokeAPI(cmd, true);
    System.debug('cmd.result::' + cmd.result);
    return cmd.result;
  }
  
  private class CreateUsersCommand implements DC_DoubleClickDAO.Command {
    public DC_DoubleClickUserDAO context;
    public DC_UserService.User_x[] users;
    public DC_UserService.User_x[] result;
    
    public CreateUsersCommand(DC_DoubleClickUserDAO context, DC_UserService.User_x[] users) {
      this.context = context;
      this.users = users;
    }
    
    public void execute() {
      this.result = context.userService.createUsers(this.users);
    }
  }
  
  // retrieve a user using the passed in query string
  public DC_UserService.User_x[] createUsers(DC_UserService.User_x[] users) {
    CreateUsersCommand cmd = new CreateUsersCommand(this, users);
    
    this.invokeAPI(cmd, true);
    
    return cmd.result;
  }
  
  private class UpdateUsersCommand implements DC_DoubleClickDAO.Command {
    public DC_DoubleClickUserDAO context;
    public DC_UserService.User_x[] users;
    
    public UpdateUsersCommand(DC_DoubleClickUserDAO context, DC_UserService.User_x[] users) {
      this.context = context;
      this.users = users;
    }
    
    public void execute() {
      context.userService.updateUsers(this.users);
    }
  }
  
  public void updateUsers(DC_UserService.User_x[] users) {
    UpdateUsersCommand cmd = new UpdateUsersCommand(this, users);
    
    this.invokeAPI(cmd, true);
  }




  // ** DEPRECATED for 201403 API ====================================
  /*


  private class UpdateUserCommand implements DC_DoubleClickDAO.Command {
    public DC_DoubleClickUserDAO context;
    public DC_UserService.User_x user;
    
    public UpdateUserCommand(DC_DoubleClickUserDAO context, DC_UserService.User_x user) {
      this.context = context;
      this.user = user;
    }
    
    public void execute() {
      context.userService.updateUser(this.user);
    }
  }

  public void updateUser(DC_UserService.User_x user) {
    UpdateUserCommand cmd = new UpdateUserCommand(this, user);
    
    this.invokeAPI(cmd, true);
  }

  private class GetUserCommand implements DC_DoubleClickDAO.Command {
    public DC_UserService.User_x result;
    public DC_DoubleClickUserDAO context;
    public Integer id;
    
    public GetUserCommand(DC_DoubleClickUserDAO context, Integer id) {
      this.context = context;
      this.id = id;
    }
    
    public void execute() {
      this.result = context.userService.getUser(id);
    }
  }

  // retrieve a DC user by the passed in integer id
  public DC_UserService.User_x getUser(Integer id, boolean retry) {
    GetUserCommand cmd = new GetUserCommand(this, id);
     
    this.invokeAPI(cmd, true);
    
    return cmd.result;
  }
  */
  // END DEPRECATED ====================================


}