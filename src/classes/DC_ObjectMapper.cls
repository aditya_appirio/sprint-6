// 
// (c) 2014 Appirio
//
// DC_ObjectMapper
// DC Connector
//
// 20 Jan 2014    Anjali K (JDC)      Original
//
public virtual with sharing class DC_ObjectMapper {
  private Map<Id, DC_FieldHandlerFactory.DC_FieldMappingCustomHandler> customHandlersByFieldMappingId = new Map<Id, DC_FieldHandlerFactory.DC_FieldMappingCustomHandler>();
      
  private List<goog_dclk_dsm__DC_ObjectMapping__c> objectMappings;
  private Map<String, goog_dclk_dsm__DC_ObjectMapping__c> objectMappingsMap;
  private List<goog_dclk_dsm__DC_FieldMapping__c> fieldMappings; 
  private Map<String, List<goog_dclk_dsm__DC_FieldMapping__c>> fieldMappingsMap;
  private Map<String, Map<String, String>> dcToSFFieldConversions;
  private Map<String, Map<String, String>> sfToDCFieldConversions;
  private static DC_ObjectMapper instance;
  private DC_CustomFieldManager customFieldManager;
	
	private DC_FieldHandlerFactory.DC_FieldMappingCustomHandler fldMappingCustHandler;
  // enforce singleton instance  
  public static DC_ObjectMapper getInstance() {
    if(instance == null) {
      instance = new DC_ObjectMapper();
    } 
    
    return instance;
  }
  
  private boolean isHandlerClassInMapping;
  
  public DC_FieldHandlerFactory.DC_FieldMappingCustomHandler getCustomMappingHandlerInstance(){
  	//if(fldMappingCustHandler == null) {
     // fldMappingCustHandler = DC_FieldHandlerFactory.getHandlerInstance(fieldMapping.goog_dclk_dsm__DC_HandlerClass__c);
    //} 
    return fldMappingCustHandler;
  }
  
  protected DC_ObjectMapper() {
    objectMappingsMap = new Map<String, goog_dclk_dsm__DC_ObjectMapping__c>();
    fieldMappingsMap = new Map<String, List<goog_dclk_dsm__DC_FieldMapping__c>>();
    dcToSFFieldConversions = new Map<String, Map<String, String>>();
    sfToDCFieldConversions = new Map<String, Map<String, String>>();
    customFieldManager = DC_ManagerFactory.getInstance().getCustomFieldManager();
    isHandlerClassInMapping = false;
  }
  
  public goog_dclk_dsm__DC_ObjectMapping__c getObjectMappingByName(String objectMappingName) {
    // TO DO- Done: Jaipur - this doesn't need to be a loop, please make goog_dclk_dsm__DC_ObjectMapping__c a unique field in 
    //Doubt: We can't make Name field unique in salesforce as its standard field, should we put any validation rule
    // If we can do this with validation rule that would be great, thanks!
    
    system.debug('==>>objectMappingName>>'+objectMappingName+'---'+objectMappingsMap);
    
    if(objectMappingsMap.get(objectMappingName) == null) {
      List<goog_dclk_dsm__DC_ObjectMapping__c> objectMappingList = [SELECT Id, goog_dclk_dsm__DC_DoubleClickObjectName__c, Name, 
                                                            goog_dclk_dsm__DC_SalesforceObjectName__c
                                                     FROM goog_dclk_dsm__DC_ObjectMapping__c 
                                                     WHERE Name = :objectMappingName
                                                     limit 1];
      if(objectMappingList != null && objectMappingList.size()>0)
        objectMappingsMap.put(objectMappingName, objectMappingList[0]);
      else
        throw new DC_ConnectorException(DC_Constants.MAPPING_STR + objectMappingName + DC_Constants.NOT_FOUND_STR);
    }
    
    return objectMappingsMap.get(objectMappingName);
  }
  
  // get the field mappings for the specified object
  // objectMappingName  String  The object for which to retrieve field mappings
  public List<goog_dclk_dsm__DC_FieldMapping__c> getFieldMappingsByName(String objectMappingName) {
    if(fieldMappingsMap.get(objectMappingName) == null) {
      fieldMappingsMap.put(objectMappingName, [SELECT Id, goog_dclk_dsm__DC_Object_Mapping__c, goog_dclk_dsm__DC_Object_Mapping__r.Name,
                                                      goog_dclk_dsm__DC_DoubleClickFieldName__c, Name, goog_dclk_dsm__DC_SalesforceFieldName__c,
                                                      goog_dclk_dsm__DC_Conversion__c, goog_dclk_dsm__DC_DoubleClickDefaultValue__c,
                                                      goog_dclk_dsm__DC_DoubleClickFieldMaxLength__c,
                                                      goog_dclk_dsm__DC_SalesforceDefaultValue__c,
                                                      goog_dclk_dsm__DC_SalesforceFieldMaxLength__c,
                                                      goog_dclk_dsm__DC_SyncDirection__c, 
                                                      goog_dclk_dsm__DC_DoubleClickCustomFieldID__c,
                                                      goog_dclk_dsm__DC_HandlerClass__c,
                                                      goog_dclk_dsm__DC_Object_Mapping__r.goog_dclk_dsm__DC_DoubleClickObjectName__c
                                               FROM goog_dclk_dsm__DC_FieldMapping__c
                                               WHERE goog_dclk_dsm__DC_Object_Mapping__r.Name =: objectMappingName AND goog_dclk_dsm__DC_Enabled__c = true]);

    }
    System.debug(':::fieldMappingsMap:::' + fieldMappingsMap);
    return fieldMappingsMap.get(objectMappingName);
  }

  // get the field conversion for the object and field mapping
  private String getFieldConversions(String objectMappingName, String fieldMappingName) {
  	System.debug(':::objectMappingName:::' + objectMappingName);
    List<goog_dclk_dsm__DC_FieldMapping__c> fieldMapping = getFieldMappingsByName(objectMappingName);
    System.debug(':::fieldMapping:::' + fieldMapping);
    System.debug(':::fieldMappingName:::' + fieldMappingName);
    //for(goog_dclk_dsm__DC_FieldMapping__c fm : fieldMappings) { @Anjali on 3rd feb commented this, I think we should use fieldMapping not fieldMappings
    
    for(goog_dclk_dsm__DC_FieldMapping__c fm : fieldMapping) {
      if(fm.Name.equalsIgnoreCase(fieldMappingName)) {
        return fm.goog_dclk_dsm__DC_Conversion__c;
      }
    }
    return null;
  }
  
  // populate the key value map based on JSON output
  // valuesMap  Map<String, String>   OUT parameter -- populated through parsing of JSON
  // parser     JSONParser            JSON parser with key value pairs 
  public void populateMap(Map<String, String> valuesMap, JSONParser parser) {
    String key;
    String value;
    while(parser.nextToken() != null && !parser.getText().equalsIgnoreCase('}')) {
      key = parser.getText();
      parser.nextToken();
      value = parser.getText();

      valuesMap.put(key, value);
    }
  }

  public Map<String, String> getSfToDcFieldConversions(String objectMappingName, String fieldMappingName) {
    String conversions = getFieldConversions(objectMappingName, fieldMappingName);
    String conversionMapKey = objectMappingName + '.' + fieldMappingName;
    Map<String, String> result = sfToDCFieldConversions != null && sfToDCFieldConversions.containsKey(conversionMapKey) ? sfToDCFieldConversions.get(conversionMapKey) : null;
		System.debug('conversions:::' + conversions);
    if(result == null) {
      if(conversions != null) {
        JSONParser parser = JSON.createParser(conversions);
				System.debug('parser:::' + parser + '==parser.getText()====' + parser.getText());
        while (parser.nextToken() != null && !parser.getText().equalsIgnoreCase('sfToDc')) {
        }

        if(parser.nextToken() != null) {
          result = new Map<String, String>();
          populateMap(result, parser);
        }

        sfToDCFieldConversions.put(conversionMapKey, result);
      }
    }

    return result;
  }

  public Map<String, String> getDCToSfFieldConversions(String objectMappingName, String fieldMappingName) {
    String conversions = getFieldConversions(objectMappingName, fieldMappingName);
    String conversionMapKey = objectMappingName + '.' + fieldMappingName;
    Map<String, String> result = dcToSFFieldConversions.get(conversionMapKey);

    if(result == null) {
      if(conversions != null) {
        JSONParser parser = JSON.createParser(conversions);

        while (parser.nextToken() != null && !parser.getText().equalsIgnoreCase('dcToSF')) {
        }

        if(parser.nextToken() != null) {
          result = new Map<String, String>();
          populateMap(result, parser);
        }

        dcToSFFieldConversions.put(conversionMapKey, result);
      }
    }

    return result;
  }
  
  public void closeCustomHandlers() {
    for (DC_FieldHandlerFactory.DC_FieldMappingCustomHandler handler : customHandlersByFieldMappingId.values()) {
      handler.finish();
    }

    customHandlersByFieldMappingId = new Map<Id, DC_FieldHandlerFactory.DC_FieldMappingCustomHandler>();
  }
  
  // Get comma separated fields of passed Object
  public String getCommaSeparatedSalesforceFieldList(String objectMappingName) {
    String result = '';
    String separator = '';
    
    Set<String> dedupeSet = new Set<String>();
    System.debug('objectMappingName::' + objectMappingName);
    for(goog_dclk_dsm__DC_FieldMapping__c fieldMapping : this.getFieldMappingsByName(objectMappingName)) {

      // get whatever needs to be added to the query to support the custom handlers
    	if (fieldMapping.goog_dclk_dsm__DC_HandlerClass__c != null) {
    		string qaddition = DC_FieldHandlerFactory.getHandlerQueryString(fieldMapping.goog_dclk_dsm__DC_HandlerClass__c);
    		
    		// if nothing to add, continue
    		if (qaddition == null || qaddition == '' || dedupeSet.contains(qaddition)) {
    		  continue;
    		}
    		  
    		result += separator + qaddition;
        separator = ',';
        dedupeSet.add(qaddition);  
        system.debug('=========result1======='+result);
    		
    	} else if(fieldMapping.goog_dclk_dsm__DC_SalesforceFieldName__c != null && !dedupeSet.contains(fieldMapping.goog_dclk_dsm__DC_SalesforceFieldName__c)) {
        result += separator + fieldMapping.goog_dclk_dsm__DC_SalesforceFieldName__c;
        separator = ',';
        dedupeSet.add(fieldMapping.goog_dclk_dsm__DC_SalesforceFieldName__c);
      }
    }
     system.debug('=========result2======='+result);
    return result;
  }
   
  public void updateFieldsOnAllObjects(DC_GenericWrapper source, DC_GenericWrapper target) {

		source.gotoBeforeFirst();
    while(source.hasNext()) {
      source.next();
      //for (DC_FieldHandlerFactory.DC_FieldMappingCustomHandler handler : customHandlersByFieldMappingId.values()) {
       // handler.nextRecord(source, target);
      //}

      // TODO US: build logic to validate last modified date before making the update
      // BR -- above may not apply to all objects
      if(target.gotoSfid((String)source.getField('sfid'))) {
        updateFields(source, target);
      }
    }
    
    closeCustomHandlers();
  }

  public void updateFieldsOnAllObjectsDCIdBased(DC_GenericWrapper source, DC_GenericWrapper target) {

    source.gotoBeforeFirst();
    while(source.hasNext()) {
      source.next();
      //for (DC_FieldHandlerFactory.DC_FieldMappingCustomHandler handler : customHandlersByFieldMappingId.values()) {
       // handler.nextRecord(source, target);
      //}

      // TODO US: build logic to validate last modified date before making the update
      // BR -- above may not apply to all objects
      if(target.gotoDCId((String)source.getField('DoubleClickId'))) {
        updateFields(source, target);
      }
    }
    
    closeCustomHandlers();
  }

  // update teh field on the target with the new value for the specified mapping
  public virtual void updateField(DC_GenericWrapper target, goog_dclk_dsm__DC_FieldMapping__c fieldMapping, Object newValue) {
    // Set value to the field
    target.setField(fieldMapping.Name, newValue);
    System.debug('target:::' + target);
  }

  // loop through the field mappings and apply source to target
  public void updateFields(DC_GenericWrapper source, DC_GenericWrapper target) {
    goog_dclk_dsm__DC_ObjectMapping__c objectMapping = source.objectMapping;
		//DC_FieldHandlerFactory.DC_FieldMappingCustomHandler fldMappingCustHandler;

    for(goog_dclk_dsm__DC_FieldMapping__c fieldMapping : source.fieldMappings) {
      boolean directional = fieldMapping.goog_dclk_dsm__DC_SyncDirection__c != null 
          && !fieldMapping.goog_dclk_dsm__DC_SyncDirection__c.equals('Not Integrated') 
          && (fieldMapping.goog_dclk_dsm__DC_SyncDirection__c.equals('Bi-Directional') 
              || (target.isSalesforce() && fieldMapping.goog_dclk_dsm__DC_SyncDirection__c.equals('To Salesforce Only')) 
              || (target.isDoubleClick() && fieldMapping.goog_dclk_dsm__DC_SyncDirection__c.equals('To DoubleClick Only')));
			System.debug('fieldMapping:::' + fieldMapping);
      // if this is a handler class, instantiate (if necessary) and call the necessary fields
      //if (fieldMapping.goog_dclk_dsm__DC_HandlerClass__c != null && (String)source.getField('DoubleClickId') != null) { 
      if (directional && fieldMapping.goog_dclk_dsm__DC_HandlerClass__c != null) { 
      	isHandlerClassInMapping = true;
        // if we don't yet have an instance for this fieldMapping, create one
      	if(!customHandlersByFieldMappingId.containsKey(fieldMapping.id)){
          System.debug('@@ custom mapping not found for field mapping id: ' + fieldMapping.id + '. Creating...');
          // instantiate the custom handler
          fldMappingCustHandler = DC_FieldHandlerFactory.getHandlerInstance(fieldMapping.goog_dclk_dsm__DC_HandlerClass__c);
          // initialize
          fldMappingCustHandler.initialize(source.objectMapping, source.fieldMappings, source, target);
      		customHandlersByFieldMappingId.put(fieldMapping.id, fldMappingCustHandler);
      	}
        customHandlersByFieldMappingId.get(fieldMapping.Id).processField(fieldMapping, source, target);
        // anjali: if handler class is not in our map, instantiate a new one using the factory and add it to the map. 
        // call processField on the handler class
       
        
      // Handle direction
      } else if(directional) {
  
        // Handle default values
        Object newValue;

        if(target.isSalesforce()) {
          if(fieldMapping.goog_dclk_dsm__DC_SalesforceDefaultValue__c != null && (fieldMapping.goog_dclk_dsm__DC_DoubleClickFieldName__c == null || source.getField(fieldMapping.Name) == null)) {
            newValue = fieldMapping.goog_dclk_dsm__DC_SalesforceDefaultValue__c;
          } else {
            newValue = source.getField(fieldMapping.Name);
          }
        } else {
          if(fieldMapping.goog_dclk_dsm__DC_DoubleClickDefaultValue__c != null && (fieldMapping.goog_dclk_dsm__DC_SalesforceFieldName__c == null || source.getField(fieldMapping.Name) == null)) {
            newValue = fieldMapping.goog_dclk_dsm__DC_DoubleClickDefaultValue__c;
          } else {
            newValue = source.getField(fieldMapping.Name);
          }
        }

        // Handle max sizes
        // BR: added newValue != null to avoid null reference       
        if(target.isSalesforce() && 
            fieldMapping.goog_dclk_dsm__DC_SalesforceFieldMaxLength__c != null && 
            newValue != null &&
            newValue instanceof String && 
            ((String)newValue).length() > fieldMapping.goog_dclk_dsm__DC_SalesforceFieldMaxLength__c) {
          newValue = ((String)newValue).substring(0, fieldMapping.goog_dclk_dsm__DC_SalesforceFieldMaxLength__c.intValue());
        } else if(target.isDoubleClick() && 
            fieldMapping.goog_dclk_dsm__DC_DoubleClickFieldMaxLength__c != null && 
            newValue != null &&
            newValue instanceof String && 
            ((String)newValue).length() > fieldMapping.goog_dclk_dsm__DC_DoubleClickFieldMaxLength__c) {
          newValue = ((String)newValue).substring(0, fieldMapping.goog_dclk_dsm__DC_DoubleClickFieldMaxLength__c.intValue());
        }

        // Handle field conversions
        Map<String, String> conversions = source.isSalesforce() ? 
          this.getSfToDcFieldConversions(objectMapping.Name, fieldMapping.Name) : 
          this.getDCToSfFieldConversions(objectMapping.Name, fieldMapping.Name);
		
        if(conversions != null) {
          newValue = conversions.get(String.valueOf(newValue));          
        } 
				System.debug('newValue ' + newValue);
        // Set value to the field
        System.debug('Updating field ' + fieldMapping.goog_dclk_dsm__DC_DoubleClickFieldName__c);

        updateField(target, fieldMapping, newValue);
      }
    }
    System.debug('END UPDATE FIELDS');
    //if(fldMappingCustHandler != null){
    //	fldMappingCustHandler.finish();
    //}
    
  }

  public virtual void linkSFRecordsAndDCRecords(DC_GenericWrapper target, DC_GenericWrapper source) {
    if(target.size() != source.size()) {
      throw new DC_ConnectorException(DC_Constants.LISTS_SIZE_MUST_MATCH);
    }

    target.gotoBeforeFirst();
    source.gotoBeforeFirst();

    while(target.hasNext()) {
      target.next();
      source.next();
      linkSFRecordAndDCRecord(target, source);
    }    
  }    

  // Links the current record with the current record in the wrappers
  public virtual void linkSFRecordAndDCRecord(DC_GenericWrapper target, DC_GenericWrapper source) {
    target.setField('DoubleClickId', source.getField('DoubleClickId'));
  }

  
  // Overrided method wrap : takes Account as param and call wrap(List<Account> accounts)
  public DC_GenericWrapper wrap(Account account) {
    List<Account> accountList = new List<Account>();
    accountList.add(account);
    DC_GenericWrapper result = wrap(accountList);
    
    return result;
  }
  public DC_GenericWrapper wrap(List<Account> accounts) {
    return new DC_SalesforceWrapper(this.getObjectMappingByName('account'), this.getFieldMappingsByName('account'), accounts);
  }

  public DC_GenericWrapper wrap(Opportunity opp) {
    List<Opportunity> oppList = new List<Opportunity>();
    oppList.add(opp);
    DC_GenericWrapper result = wrap(oppList);
    
    return result;
  }

  public DC_GenericWrapper wrap(List<Opportunity> oppList) {
    return new DC_SalesforceWrapper(this.getObjectMappingByName('Opportunity'), this.getFieldMappingsByName('Opportunity'), oppList);
  }
  
  public DC_GenericWrapper wrap(OpportunityLineItem opp) {
    List<OpportunityLineItem> oppList = new List<OpportunityLineItem>();
    oppList.add(opp);
    DC_GenericWrapper result = wrap(oppList);
    
    return result;
  }

  public DC_GenericWrapper wrap(List<OpportunityLineItem> oppList) {
    return new DC_SalesforceWrapper(this.getObjectMappingByName('OpportunityLineItem'), this.getFieldMappingsByName('OpportunityLineItem'), oppList);
  }
  
  public DC_GenericWrapper wrapForDeliveryData(List<OpportunityLineItem> oppList) {
    return new DC_SalesforceWrapper(this.getObjectMappingByName('DeliveryData'), this.getFieldMappingsByName('DeliveryData'), oppList);
  }

  public DC_GenericWrapper wrap(Contact contact) {
    List<Contact> contactList = new List<Contact>();
    contactList.add(contact);
    DC_GenericWrapper result = wrap(contactList);
    
    return result;
  }
  public DC_GenericWrapper wrap(List<Contact> contacts) {
    return new DC_SalesforceWrapper(this.getObjectMappingByName('contact'), this.getFieldMappingsByName('contact'), contacts);
  }
  
  public DC_GenericWrapper wrap(goog_dclk_dsm__DC_User__c user) {
    List<goog_dclk_dsm__DC_User__c> userList = new List<goog_dclk_dsm__DC_User__c>();
    userList.add(user);
    DC_GenericWrapper result = wrap(userList);
    return result;
  }
  
  public DC_GenericWrapper wrap(List<User> users) {
    return new DC_SalesforceWrapper(this.getObjectMappingByName('User Batch'), this.getFieldMappingsByName('User Batch'), users);
  }
  public DC_GenericWrapper wrap(User user) {
    return wrap(new List<User> {user});
  }      
  
  public DC_GenericWrapper wrap(List<goog_dclk_dsm__DC_Team__c> teams) {
    return new DC_SalesforceWrapper(this.getObjectMappingByName('team'), this.getFieldMappingsByName('team'), teams);
  }
  
  public DC_GenericWrapper wrap(goog_dclk_dsm__DC_Team__c team) {
    List<goog_dclk_dsm__DC_Team__c> teamList = new List<goog_dclk_dsm__DC_Team__c>();
    teamList.add(team);
    DC_GenericWrapper result = wrap(teamList);
    return result;
  }
  
  public DC_GenericWrapper wrap(List<goog_dclk_dsm__DC_User__c> users) {
    return new DC_SalesforceWrapper(this.getObjectMappingByName('user'), this.getFieldMappingsByName('user'), users);
  }
  
  public virtual DC_GenericWrapper createNewDCRecords(DC_GenericWrapper accounts) {
  	return null;
  }

  // Returns how many custom fields are still pending to be created for this object mapping
  public Long createCustomFields(String objectMappingName) {
    List<goog_dclk_dsm__DC_FieldMapping__c> customFields = new List<goog_dclk_dsm__DC_FieldMapping__c>();
    Integer created = 0; 
    Integer pending = 0;

    for(goog_dclk_dsm__DC_FieldMapping__c fieldMapping : getFieldMappingsByName(objectMappingName)) {
      if(fieldMapping.goog_dclk_dsm__DC_DoubleClickCustomFieldID__c == null 
      			&& fieldMapping.goog_dclk_dsm__DC_DoubleClickFieldName__c.startsWithIgnoreCase(Label.DC_Custom)) {
				if(fieldMapping.goog_dclk_dsm__DC_DoubleClickFieldName__c.split('\\.').size() > 1){
					pending++;
	        // This requires 2 callouts per field, so it has to be limited to 5 per execution so it doesn't hit the 10
	        // callouts per apex execution limit
	        if(created < 5) {
	          // TO DO : Done on 20th feb 2014, Jaipur - validate if the string is well formed, it should be custom.<field name> if there is no "." 
	          // or nothing after the "." throw a DC_ConnectorException with message: 
	          // "Invalid mapping <fieldMapping.goog_dclk_dsm__DC_DoubleClickFieldName__c>, custom field name is malformed it should be custom.<desired field name>"
	          String fieldName = fieldMapping.goog_dclk_dsm__DC_DoubleClickFieldName__c.split('\\.')[1];
	          fieldMapping.goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = customFieldManager.createOrGetCustomFieldId(fieldName, fieldMapping.goog_dclk_dsm__DC_Object_Mapping__r.goog_dclk_dsm__DC_DoubleClickObjectName__c);
	          customFields.add(fieldMapping);
	          
	          created++;
	        }
        }else{
        	throw new DC_ConnectorException(DC_Constants.getCustomFldMalformError(fieldMapping.goog_dclk_dsm__DC_DoubleClickFieldName__c));
        }
      }
    }

    if(created > 0) {
      update customFields;
    }

    return pending - created;
  }
  
  public boolean getHandlerClassInMapping(){
  	return isHandlerClassInMapping;
  }

}