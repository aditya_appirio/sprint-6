public with sharing class DC_DoubleClickTeamWrapper extends DC_DoubleClickWrapper {
	//constructor : passing values to DC_DoubleClickWrapper constructor
	public DC_DoubleClickTeamWrapper(goog_dclk_dsm__DC_ObjectMapping__c objectMapping, List<goog_dclk_dsm__DC_FieldMapping__c> fieldMappings, List<Object> objects) {
    super(objectMapping, fieldMappings, objects);
  }
  
  // Method to get current User
  private DC_TeamService.Team getCurrentTeam() {
    if(this.currentObject == null){
      throw new DC_ConnectorException(DC_Constants.ARR_OUT_OF_BOUND_ERR);
    } else if(!(this.currentObject instanceOf DC_TeamService.Team)){
        throw new DC_ConnectorException(DC_Constants.INVALID_TEAM_MAPPING);
    } else{
        return (DC_TeamService.Team) this.currentObject;
    } 
  }
  
  // Overridden getter for User fields 
  public override Object getField(String field) {
  	System.debug('field::' + field);
    String fieldName = getDoubleClickFieldName(field.toLowerCase());
    System.debug('fieldName::' + fieldName +'==' + fieldName.toLowerCase().equals('roleId'));
    if(fieldName.toLowerCase().equals('id')) {
      return getCurrentTeam().id;
    } else if(fieldName.equalsIgnoreCase('name')) {
        return getCurrentTeam().name;
    } else if(fieldName.equalsIgnoreCase('description')) {
        return getCurrentTeam().description;
    } else if(fieldName.equalsIgnoreCase('hasAllCompanies')) {
        return getCurrentTeam().hasAllCompanies;
    } else if(fieldName.equalsIgnoreCase('hasAllInventory')) {
        return getCurrentTeam().hasAllInventory;
    } else if(fieldName.equalsIgnoreCase('teamAccessType')) {
        return getCurrentTeam().teamAccessType;
    } else if(fieldName.equalsIgnoreCase('companyIds')) {
        return getCurrentTeam().companyIds;
    } else if(fieldName.equalsIgnoreCase('adUnitIds')) {
	      return getCurrentTeam().adUnitIds;
		} 
    else { 
      throw new DC_ConnectorException(DC_Constants.getInvalid_Mapping_User(field));
    }
  }
  
  // Overridden setter for User fields 
  public override void setField(String field, Object value) {
    String fieldName = getDoubleClickFieldName(field);
    try{
      if(fieldName.equalsIgnoreCase('id')) {
          getCurrentTeam().id = this.toInteger(value);  
      } else if(fieldName.equalsIgnoreCase('name')) {
          getCurrentTeam().name = this.toString(value);
      } else if(fieldName.equalsIgnoreCase('description')) {
          getCurrentTeam().description = this.toString(value); 
      } else if(fieldName.equalsIgnoreCase('hasAllCompanies')) {
          getCurrentTeam().hasAllCompanies = this.toBoolean(value);
      } else if(fieldName.equalsIgnoreCase('hasAllInventory')) {
          getCurrentTeam().hasAllInventory = this.toBoolean(value);
      } else if(fieldName.equalsIgnoreCase('teamAccessType')) {
          getCurrentTeam().teamAccessType = this.toString(value);
      } else if(fieldName.equalsIgnoreCase('companyIds')) {
          getCurrentTeam().companyIds = this.toLongArr(value);
      } else if(fieldName.equalsIgnoreCase('adUnitIds')) {
          getCurrentTeam().adUnitIds = this.toStringArr(value); 
      } 
      else {
        throw new DC_ConnectorException(DC_Constants.getInvalid_Mapping_User(field));
      }
    }
    catch(System.TypeException e){
      throw new DC_ConnectorException(e+DC_Constants.FOR_FLD_STR+field);
    }
  }  

  // Overridden for creating user wrapper
  public override DC_GenericWrapper createWrapper(goog_dclk_dsm__DC_ObjectMapping__c objectMapping, List<goog_dclk_dsm__DC_FieldMapping__c> fieldMappings, List<Object> objects) {
    return new DC_DoubleClickTeamWrapper(objectMapping, fieldMappings, objects);
  }  

  // Overridden for getting current user wrapper
  public override DC_GenericWrapper getCurrentWrapped() {
    return new DC_DoubleClickTeamWrapper(this.objectMapping, this.fieldMappings, new object[] { this.currentObject });
  }    

  // Overridden for getting existing user wrapper
  public override DC_GenericWrapper getExistingRecords() {
    throw new DC_ConnectorException('Not implemented.');
    /*this.gotoBeforeFirst();
    List<Object> result = new List<Object>();
    
    while(this.hasNext()) {
      this.next();
      if(this.getField('DoubleClickId') != null) {
        result.add(this.currentObject);
      }
    }
    
    return new DC_DoubleClickTeamWrapper(this.objectMapping, this.fieldMappings, result);
    */
  }   

  // Overridden for getting new user wrapper
  public override DC_GenericWrapper getNewRecords() {
    throw new DC_ConnectorException('Not implemented.');
    /*
    this.gotoBeforeFirst();
    List<Object> result = new List<Object>();
    
    while(this.hasNext()) {
      this.next();
      if(this.getField('DoubleClickId') == null) {
        result.add(this.currentObject);
      }
    } 
    
    return new DC_DoubleClickTeamWrapper(this.objectMapping, this.fieldMappings, result);
    */
  }  
 /* // method to get custom type text field value : 19th March Anjali
  private String getTextCustomFieldValue(Long fieldId) {
    if(getCurrentUser().customFieldValues != null) {

      for(DC_UserService.BaseCustomFieldValue fieldValue : getCurrentUser().customFieldValues) {
        
        if (fieldValue instanceOf DC_UserService.CustomFieldValue) {
          DC_UserService.CustomFieldValue curfield = (DC_UserService.CustomFieldValue)fieldValue;
          if(curfield.customFieldId == fieldId) {    
            if (curfield.value instanceOf DC_UserService.TextValue) {
              return ((DC_UserService.TextValue)curfield.value).value;
            }
          }
        }
      } 
    }
    return null;
  }

	// method to set custom type text field value
  private void setTextCustomFieldValue(Long fieldId, String value) {
    DC_UserService.CustomFieldValue field = new DC_UserService.CustomFieldValue();
    field.value = new DC_UserService.TextValue();
    field.customFieldId = fieldId;
    ((DC_UserService.TextValue)field.value).value = this.toString(value);

    if(getCurrentUser().customFieldValues == null) {
      getCurrentUser().customFieldValues = new List<DC_UserService.BaseCustomFieldValue>();
    } else {
      Integer idx = 0;
      for(DC_UserService.BaseCustomFieldValue fieldValue : getCurrentUser().customFieldValues) {
        if(fieldValue.customFieldId == fieldId) {
          getCurrentUser().customFieldValues.remove(idx);
          break;
        } 
        idx++;
      }
    }

    getCurrentUser().customFieldValues.add(field);
  }
  
  */

  /*

  

  

	

  */
}