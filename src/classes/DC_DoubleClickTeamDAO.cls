public with sharing class DC_DoubleClickTeamDAO extends DC_DoubleClickDAO {
	protected override void tokenRefreshed() {
  	this.teamService = null;
  }
  
  public DC_DoubleClickTeamDAO(DC_DoubleClickOAuthHandler authHandler) {
    super(authHandler);
  }
  
  // get a configured doubleclick Team interface
  private DC_TeamService.TeamServiceInterfacePort teamService {
    get {
      if(this.teamService == null) {
        this.teamService = DC_ServicesFactory.getTeamService(this.authHandler);
      }  

      return this.teamService;
    }
    set;
  }
  
  public DC_TeamService.Statement createStatement(String query) {
    DC_TeamService.Statement result = new DC_TeamService.Statement();

    result.query = query;
    
    return result;
  }

  public DC_TeamService.Statement createStatement(String query, DC_TeamService.String_ValueMapEntry[] params) {
    DC_TeamService.Statement result = createStatement(query);
    result.values = params;
    
    return result;
  }
  /*
  private class GetTeamCommand implements DC_DoubleClickDAO.Command {
  	public DC_TeamService.Team result;
  	public DC_DoubleClickTeamDAO context;
  	public Integer id;
  	
  	public GetTeamCommand(DC_DoubleClickTeamDAO context, Integer id) {
  	  this.context = context;
  	  this.id = id;
  	}
  	 
  	public void execute() {
  	  this.result = context.teamService.getTeam(id);
  	}
  }

  // retrieve a DC Team by the passed in integer id
  public DC_TeamService.Team getTeam(Integer id, boolean retry) {
  	GetTeamCommand cmd = new GetTeamCommand(this, id);
  	
  	this.invokeAPI(cmd, true);
  	
  	return cmd.result;
  }
  */
  
  private class GetTeamByStatementCommand implements DC_DoubleClickDAO.Command {
    public DC_TeamService.TeamPage result;
    public DC_DoubleClickTeamDAO context;
    public DC_TeamService.Statement statement;
    
    public GetTeamByStatementCommand(DC_DoubleClickTeamDAO context, DC_TeamService.Statement statement) {
    	this.context = context;
    	this.statement = statement;
    } 
    
  	public void execute() {
			System.debug('statement::' + statement);
    	this.result = context.TeamService.getTeamsByStatement(statement);
    	if (this.result == null || this.result.results == null || this.result.results.size() == 0) {
        return;        
      }
      /*for (DC_TeamService.Team team : this.result.results) {
        if ((team.customFieldValues == null || team.customFieldValues.size() == 0) && (team.dropDownCustomFieldValues == null || Team.dropDownCustomFieldValues.size() == 0)) {
          continue;
        } 
        if(team.customFieldValues != null ){
	        for (integer i = 0; i < team.customFieldValues.size(); i++) {
	          System.debug('@@ Custom Field Text: ' + team.customFieldValues[i]);
	          System.debug('@@ Custom Field Type: ' + team.customFieldValues[i].BaseCustomFieldValue_Type);
	
	          team.customFieldValues[i] = copyCustomField(team.customFieldValues[i]);
	        }
        }
        if(team.dropDownCustomFieldValues != null){
	        for (integer i = 0; i < team.dropDownCustomFieldValues.size(); i++) {
	          System.debug('@@ Custom Field Text: ' + team.dropDownCustomFieldValues[i]);
	          System.debug('@@ Custom Field Type: ' + team.dropDownCustomFieldValues[i].BaseCustomFieldValue_Type);
	
	          team.dropDownCustomFieldValues[i] = copyCustomField(team.dropDownCustomFieldValues[i]);
	        }
				}
        System.debug('@@ Copied Custom Fields: ' + team.customFieldValues);
    	}*/
		}
		/*public DC_TeamService.BaseCustomFieldValue copyCustomField(DC_TeamService.BaseCustomFieldValue inval) {
		  DC_TeamService.BaseCustomFieldValue ret;
		  if (inval.BaseCustomFieldValue_Type == 'CustomFieldValue') {
		    DC_TeamService.CustomFieldValue cfv = new DC_TeamService.CustomFieldValue();
		    cfv.customFieldId = inval.customFieldId;
		    cfv.value = copyCustomFieldValue(inval.value); // copy value
		    System.debug('@@ Value after copy: ' + cfv.value);
		    ret = cfv;
		  } else if (inval.BaseCustomFieldValue_Type == 'DropDownCustomFieldValue') {
		  	DC_TeamService.DropDownCustomFieldValue ddcfv = new DC_TeamService.DropDownCustomFieldValue();
        ddcfv.customFieldId = inval.customFieldId;
        ddcfv.customFieldOptionId = inval.customFieldOptionId; // copy value
        ret = (DC_TeamService.BaseCustomFieldValue)ddcfv;
		  }
		  return ret;
		}

		public DC_TeamService.Value copyCustomFieldValue(DC_TeamService.Value inval) {
		  DC_TeamService.Value ret;
	 
	  	if (inval.value_type == 'BooleanValue') {
		    DC_TeamService.BooleanValue bv = new DC_TeamService.BooleanValue();
		    bv.value = boolean.valueOf(inval.value);
		    ret = bv;
	      } else if (inval.value_type == 'NumberValue') {
	        DC_TeamService.NumberValue nv = new DC_TeamService.NumberValue();
	        nv.value = inval.value;
	        ret = nv;
	      } else if (inval.value_type == 'TextValue') {
	        DC_TeamService.TextValue tv = new DC_TeamService.TextValue();
	        tv.value = inval.value;
	        System.debug('@@ TextValue: ' + tv.value);
	        ret = tv;
	      }
	
	  	return ret;
		}    */
  }
  // retrieve a Team using the passed in query string
  public DC_TeamService.TeamPage getTeamsByStatement(String query) {
  	System.debug('query::' + query);
    DC_TeamService.Statement statement = this.createStatement(query);
    
    GetTeamByStatementCommand cmd = new GetTeamByStatementCommand(this, statement);
    
    this.invokeAPI(cmd, true);
    System.debug('cmd.result::' + cmd.result);
    return cmd.result;
  }
  /*
  private class CreateTeamsCommand implements DC_DoubleClickDAO.Command {
    public DC_DoubleClickTeamDAO context;
    public DC_TeamService.Team[] teams;
    public DC_TeamService.Team[] result;
    
    public CreateTeamsCommand(DC_DoubleClickTeamDAO context, DC_TeamService.Team[] Teams) {
      this.context = context;
      this.teams = teams;
    }
    
    public void execute() {
      this.result = context.teamService.createTeams(this.teams);
    }
  }
  
  // retrieve a Team using the passed in query string
  public DC_TeamService.Team[] createTeams(DC_TeamService.Team[] teams) {
    CreateTeamsCommand cmd = new CreateTeamsCommand(this, teams);
    
    this.invokeAPI(cmd, true);
    
    return cmd.result;
  }
  
  private class UpdateTeamsCommand implements DC_DoubleClickDAO.Command {
    public DC_DoubleClickTeamDAO context;
    public DC_TeamService.Team[] teams;
    
    public UpdateTeamsCommand(DC_DoubleClickTeamDAO context, DC_TeamService.Team[] teams) {
      this.context = context;
      this.teams = teams;
    }
    
    public void execute() {
      context.TeamService.updateTeams(this.teams);
    }
  }
  
  public void updateTeams(DC_TeamService.Team[] teams) {
    UpdateTeamsCommand cmd = new UpdateTeamsCommand(this, teams);
    
    this.invokeAPI(cmd, true);
  }

  private class UpdateTeamCommand implements DC_DoubleClickDAO.Command {
    public DC_DoubleClickTeamDAO context;
    public DC_TeamService.Team team;
    
    public UpdateTeamCommand(DC_DoubleClickTeamDAO context, DC_TeamService.Team teams) {
      this.context = context;
      this.team = team;
    }
    
    public void execute() {
      context.TeamService.updateTeam(this.team);
    }
  }

  public void updateTeam(DC_TeamService.Team team) {
    UpdateTeamCommand cmd = new UpdateTeamCommand(this, team);
    
    this.invokeAPI(cmd, true);
  }*/
}