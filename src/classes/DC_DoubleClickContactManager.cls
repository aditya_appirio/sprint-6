public with sharing class DC_DoubleClickContactManager extends DC_DoubleClickManager {
	
	private DC_ContactMapper mapper {
    get {
      if(mapper == null) {
        mapper = DC_ContactMapper.getInstance();
      }
      
      return mapper;
    }
    set;
  }
  
	private DC_DoubleClickContactDAO contactDAO {
    get {
        if(contactDAO == null) {
            contactDAO = new DC_DoubleClickContactDAO(this.authHandler);
        }
        
        return contactDAO;
    }
    set;
	}

	public DC_DoubleClickContactManager(DC_DoubleClickOAuthHandler authHandler){
    super(authHandler);
	}
    
	public DC_GenericWrapper getContactsUpdatedSinceLastSync() {
    //LastSyncDate__c lastSyncDate = goog_dclk_dsm__LastSyncDate__c.getOrgDefaults();
    //Datetime lastSync = System.now().addDays(-100);//Doubt: What value should populate here if custom setting doesn't return any value
    /*if(lastSyncDate !=null)
      lastSync = lastSyncDate.goog_dclk_dsm__LastSyncDateCompany__c;
    */

    return mapper.wrap(contactDAO.getContactsByStatement('WHERE lastModifiedDateTime >= \'' + contactDAO.formatDateForPQL(datetime.now()) + '\' LIMIT 10 OFFSET 0').results);
	} 
    

    

    


  // get contacts matched by name OR email to provide linking capability
  // String 
  public DC_GenericWrapper getMatchingContacts(String companyId, String name, String email) {
  	string statement = 'WHERE companyId = \'' + companyId + '\' AND ';
  	
  	// if an email was provided, add criteria to also look for matching email
    statement += (email == '') ? 
      'name = \'' + name.replace('\'', '\'\'') + '\'' 
      : '(name = \'' + name.replace('\'', '\'\'') + '\' OR email = \'' + email + '\')';
  	// wrap and return results
    return mapper.wrap(contactDAO.getContactsByStatement(statement).results);
  }
  
  // get contacts matched by name OR email to provide linking capability
  // String 
  public DC_GenericWrapper getMatchingContacts1(List<string> companyIds, List<String> names, List<String> emails) {
  	string statement = 'WHERE companyId in ' + companyIds + '  AND ';
  	// if an email was provided, add criteria to also look for matching email
    statement += (emails.size() == 0) ? 
      'name in ' + names  
      : '(name in ' + names + ' OR email in ' + emails + ')';
  	// wrap and return results
  	
  	System.debug('statement:::' + statement);
  	DC_GenericWrapper res = mapper.wrap(contactDAO.getContactsByStatement(statement).results);
  	System.debug('res:::' + res);
    return res;
  }
  
    // TO DO: make sure we fetch pages of data 
  //Doubt: Not sure of this one. Do we limit the no. of companies we are getting through getCompaniesByStatement?
  public override DC_GenericWrapper getDCRecords(DC_GenericWrapper contacts) {
    if (contacts.size() == 0)
      return mapper.wrap(new DC_ContactService.Contact[] {} );
          
    contacts.gotoBeforeFirst();
    String ids = '';
    String separator = '';
    List<DC_ContactService.Contact> result = new List<DC_ContactService.Contact>(); 
    
    while(contacts.hasNext()) {
        contacts.next();
        
      ids += separator + contacts.getField('DoubleClickId');
      separator = ',';  
    }

    return mapper.wrap(contactDAO.getContactsByStatement('WHERE id in (' + ids + ')').results);
  }
    
  public override DC_GenericWrapper createDCRecords(DC_GenericWrapper contacts) {
    DC_ContactService.Contact[] apiContacts = new DC_ContactService.Contact[contacts.size()];
        
    contacts.gotoBeforeFirst();
    while(contacts.hasNext()) {
      contacts.next();

      DC_ContactService.Contact contact = (DC_ContactService.Contact) contacts.getCurrentObject();
      
      //contact.type_x = 'ADVERTISER';
      
      apiContacts[contacts.getCurrentIndex()] = contact;
    }

    return mapper.wrap(contactDAO.createContacts(apiContacts));
  }
    
  // update the provided companies in DoubleClick
  // companies  DC_GenericWrapper   wrapper containing companies for update
  public override void updateDCRecords(DC_GenericWrapper contacts) {
    DC_ContactService.Contact[] apiContacts = new DC_ContactService.Contact[contacts.size()];
    
    contacts.gotoBeforeFirst();
    
    while(contacts.hasNext()) {
      contacts.next();
      apiContacts[contacts.getCurrentIndex()] = (DC_ContactService.Contact) contacts.getCurrentObject();
    }
    
    contactDAO.updateContacts(apiContacts);
      
  }
  
  public  Set<String> getDCRecordsIds(DC_GenericWrapper contacts) {
    if (contacts.size() == 0)
      return null;
    Set<String> dcContactIds = new Set<String>();      
    contacts.gotoBeforeFirst();
    String ids = '';
    String separator = '';
    List<DC_ContactService.Contact> result = new List<DC_ContactService.Contact>(); 
    
    while(contacts.hasNext()) {
        contacts.next();
      dcContactIds.add(String.valueOf(contacts.getField('DoubleClickId')));  
    }

    return dcContactIds;
  }

  // ** DEPRECATED for 201403 API ====================================
  /*
  public DC_GenericWrapper getContact(Integer id) {
    return mapper.wrap(contactDAO.getContact(id, true));
  }

  public void updateContact(DC_GenericWrapper contact) {
    contactDAO.updateContact((DC_ContactService.Contact)contact.getCurrentObject());
  }  
  */
  // END DEPRECATED ====================================

}