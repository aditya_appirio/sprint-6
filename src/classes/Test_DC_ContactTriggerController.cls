// 
// (c) 2014 Appirio
//
// Test_DC_ContactTriggerController : test class for DC_ContactTriggerController 
//
// 13 March 2014   Karun Kumar(JDC) Original
//
@isTest
private class Test_DC_ContactTriggerController {

  static testMethod void myUnitTest() {
    goog_dclk_dsm__DC_ConnectorApplicationSettings__c connAppSetting = DC_TestUtils.createConnAppSetting(1, false);
    connAppSetting.goog_dclk_dsm__DC_AllowContactTriggerPushToDSM__c = true;
    insert connAppSetting;
    
    //updated 27-oct-2014
  	goog_dclk_dsm__DC_Authentication__c objDC_Authentication = new goog_dclk_dsm__DC_Authentication__c(goog_dclk_dsm__DC_AccessToken__c ='1324324',goog_dclk_dsm__DC_ExpiresIn__c = 3600 ,goog_dclk_dsm__DC_RefreshToken__c = '454655656',goog_dclk_dsm__DC_TokenType__c = 'Bearer');
    insert objDC_Authentication;	
    
    goog_dclk_dsm__DC_ObjectMapping__c dcObjMapping1 = DC_TestUtils.createObjectMapping('Company', 'Account', false);
    goog_dclk_dsm__DC_ObjectMapping__c dcObjMapping2 = DC_TestUtils.createObjectMapping('Contact', 'Contact', false);
    insert new List<goog_dclk_dsm__DC_ObjectMapping__c>{dcObjMapping1, dcObjMapping2};
    
    goog_dclk_dsm__DC_FieldMapping__c fldMapping1 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping2.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId');
    insert new List<goog_dclk_dsm__DC_FieldMapping__c>{fldMapping1};
    
    system.debug('Setup success.');
    
    Account acc = DC_TestUtils.createAccount('testCompany', true);
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplContact());
    Test.startTest();
    
    Contact con = DC_TestUtils.createContact('fName', 'lName', acc.Id, true);
    con.MailingCity = 'Jaipur';
    con.MailingCountry = 'India';
    con.MailingState = 'Rajasthan';
    con.goog_dclk_dsm__DC_ReadyForDSM__c = true;
    con.Email = 'test@gmail.com';
    con.goog_dclk_dsm__dc_doubleclickid__c = '1';

    update con;
    system.debug('contact update success.');
    
    con = [Select goog_dclk_dsm__DC_ReadyForDSM__c, goog_dclk_dsm__DC_DoubleClickId__c, goog_dclk_dsm__DC_DSMRecord__c From Contact Where Id = :con.Id];
    system.debug('con :: '+ con);
    
    system.assertEquals(con.goog_dclk_dsm__DC_ReadyForDSM__c, true);
    system.assertEquals(con.goog_dclk_dsm__DC_DoubleClickId__c, '1');
    system.assert(con.goog_dclk_dsm__DC_DSMRecord__c.contains('Exists in DSM'));
    
  }
}