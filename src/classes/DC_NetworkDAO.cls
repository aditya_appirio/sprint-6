public with sharing class DC_NetworkDAO extends DC_DoubleClickDAO {

  public DC_NetworkDAO(DC_DoubleClickOAuthHandler authHandler) {
    super(authHandler);
  }
  
  // get a configured doubleclick company interface
  private DC_NetworkService.NetworkServiceInterfacePort networkService {
    get {
      if(this.networkService == null) {
        this.networkService = DC_ServicesFactory.getNetworkService(this.authHandler);
      } 

      return this.networkService;
    }
    set;
  }

  protected override void tokenRefreshed() {
    this.networkService = null;
  }

  private class GetNetworksCommand implements DC_DoubleClickDAO.Command {
    public DC_NetworkService.Network[] result;
    public DC_NetworkDAO context;
    
    public GetNetworksCommand(DC_NetworkDAO context) {
      this.context = context;
    }
    
    public void execute() {
      this.result = context.networkService.getAllNetworks();
    }
  }

  // retrieve a DC company by the passed in integer id
  public DC_NetworkService.Network[] getNetworks() {
    GetNetworksCommand cmd = new GetNetworksCommand(this);
    
    this.invokeAPI(cmd, true);
    
    return cmd.result;
  }

  private class GetCurrentNetworkCommand implements DC_DoubleClickDAO.Command {
    public DC_NetworkService.Network result;
    public DC_NetworkDAO context;
    
    public GetCurrentNetworkCommand(DC_NetworkDAO context) {
      this.context = context;
    }
    
    public void execute() {
      this.result = context.networkService.getCurrentNetwork();
    }
  }

  // retrieve a DC company by the passed in integer id
  public DC_NetworkService.Network getCurrentNetwork() {
    GetCurrentNetworkCommand cmd = new GetCurrentNetworkCommand(this);
    
    this.invokeAPI(cmd, true);
    
    return cmd.result;
  }

}