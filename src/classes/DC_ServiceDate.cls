// 
// Abstract class for creating a common service date
//
public abstract with sharing class DC_ServiceDate {
	public Integer year;
	public Integer month;
	public Integer day; 
	
	public void setYear(integer value) { 
		year = value;
	}
	
  public integer getYear() { 
    return year;
  }
  
  public void setMonth(integer value) { 
    month = value;
  }
  
  public integer getMonth() { 
    return month;
  } 
  
  public void setDay(integer value) { 
    day = value;
  }
  
  public integer getDay() { 
    return day;
  } 

  public Date getSalesforceDate() {
    // TODO US: Handle locale specific date formats
    return Date.parse(month + '/' + day + '/' + year);
  }

  public void setSalesforceDate(Date theDate) {
  	if (theDate == null)
      return;
      
    this.day = theDate.day();
    this.month = theDate.month();
    this.year = theDate.year();

  }
}