// 
// (c) 2014 Appirio
//
// Controller to link Salesforce Contacts and DoubleClick Contacts
// 
// 28 Jan 2014    Anjali K (JDC)      Original
//
public with sharing class DC_PushContactToDSMController extends DC_LinkRecordController {
	 private DC_ContactContactConnector connector1;
	 private static string contactLabel = Contact.sObjectType.getDescribe().getLabel();
  // standard apex page controller  
  public DC_PushContactToDSMController(ApexPages.Standardcontroller c) {
    super(c, contactLabel, Label.DC_DSM_Contact);
    //updated try and catch 21-oct-2014
    try{
    connector1=DC_ContactContactConnector.getInstance();
    }
    catch (Exception e) {
    		System.debug('e:::' + e);
	      if(!ApexPages.hasMessages()){
        	ApexPages.addMessages(e);
        	return;
      	}
  	}
  }	
	
	// selected record returned as typed contact record
	public Contact selectedContactRecord {
		get {
			return (Contact)selectedRecord;
		}
	}
	
	// query for the selected records	
	public override List<sObject> getSelectedRecord(Id recordId) {
		return [
		  SELECT id, name, email, goog_dclk_dsm__DC_AccountDoubleClickId__c, 
			       goog_dclk_dsm__DC_DoubleClickId__c, goog_dclk_dsm__DC_ReadyForDSM__c, Phone 
		    FROM Contact 
		   WHERE id = :recordId
		];
	}
	
  public override string getContext() {
    return 'Push Contact to DSM';
  }	
  
  // execute the connector's find matching contacts methods
  public override DC_GenericWrapper executeConnectorGetMatchingRecords() {
  	//updated try and catch 21-oct-2014
    try{
    return connector1.findMatchingContacts(selectedContactRecord);
    }
    catch (Exception e) {
    		System.debug('e:::' + e);
	      if(!ApexPages.hasMessages()){
        	ApexPages.addMessages(e);
        	return null;
      	}
      	return null;
  	}
  }	
	
	// execute the connector's sync existing records actions
	public override void executeConnectorSyncExisting() {
		//updated try and catch 21-oct-2014
	    try{
			connector1.syncSFContactsToDCContacts(new Contact[] { selectedContactRecord });
	    }
	    catch (Exception e) {
	    		System.debug('e:::' + e);
		      if(!ApexPages.hasMessages()){
	        	ApexPages.addMessages(e);
	        	return;
	      	}
	  	}
	}
	
	// execute the connector's sync method to create records
	public override void executeConnectorSync() {
		//updated try and catch 21-oct-2014
	    try{
		connector1.syncSFContactsToDCContacts(new Contact[] { selectedContactRecord }); 
	    }
		catch (Exception e) {
	    		System.debug('e:::' + e);
		      if(!ApexPages.hasMessages()){
	        	ApexPages.addMessages(e);
	        	return;
	      	}
	  	}
	}
	  
	//If SFDC Account exist in DSM but SFDC Account does not has value in goog_dclk_dsm__DC_DoubleClickId__c field then populate goog_dclk_dsm__DC_DoubleClickId__c with Company id
	public override void executeConnectorLink(DC_GenericWrapper wrapper){
	  // create link between account record and wrapper
	  System.debug('selectedContactRecord::' + selectedContactRecord);
	  //updated try and catch 21-oct-2014
	    try{
	  	connector1.linkSFContactsAndDCContacts(new List<Contact> {selectedContactRecord}, wrapper);
	    }
	    catch (Exception e) {
	    		System.debug('e:::' + e);
		      if(!ApexPages.hasMessages()){
	        	ApexPages.addMessages(e);
	        	return;
	      	}
	  	}
	}    	
	
	public override String getLinkMessage() {
		return Label.DC_SF_Contact_has_Matching_DC_Contact;
	}
	
  public override String getUniquenessViolationMessage() {
    return Label.DC_Contact_Uniqueness_Violation_Message;
  } 	
}