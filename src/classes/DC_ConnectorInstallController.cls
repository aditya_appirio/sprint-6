public with sharing class DC_ConnectorInstallController {
  public void registerCurrentUserTeamAssociation() {
    DC_DCUserUserConnector userConnector = DC_DCUserUserConnector.getInstance();
    userConnector.registerCurrentUserWithAllEntitiesTeam(); 
  } 
}