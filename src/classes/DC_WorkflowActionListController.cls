// 
// (c) 2014 Appirio
//
// Controller for component to do WF actions
// 
// 31st March 2014    Anjali K (JDC)      Original
// 17   October 2014  				      Updated     added a logic for User And Admin Authentication for Approve And Reject Proposal
public with sharing class DC_WorkflowActionListController {
	// api name of the configuration field that provides the field name 
	// to retrieve from the wrapper's record

  // generic wrapper with records to render
  private DC_GenericWrapper lWrapper;
  
  // local config context 
  private String lConfigContext;
  private DC_WorkflowManager lWFManager;
  private List<DC_WorkflowService.WorkflowRequest> rejectedWorkflows;
  private List<DC_WorkflowService.WorkflowRequest> approvedWorkflows;
  private Integer rejectedWFRequests;
	private Integer approvedWFRequests;
	public boolean isError{get;set;}
  
  public Opportunity opportunity {
  	get;
  	set;
  }

  public Opportunity currentOpportunity {
    get {
      if(currentOpportunity == null) {
        currentOpportunity = [Select Id, goog_dclk_dsm__DC_DoubleClickId__c from Opportunity where Id = :opportunity.Id];
      }

      return currentOpportunity;
    }
    set;
  }
  
  // records containing the field configuration
  public List<sObject> fieldConfig { get; set; }
  public List<SelectOption> wfActionOptions{get;set;}
  
  // property for the wrapper
  public DC_WorkflowManager wfManager { 
  	get {
  		return lWFManager;
  	}
  	set {
  		lWFManager = value;
  	}
  }
      
  // property for the wrapper
  public DC_GenericWrapper wrapper { 
  	get {
  		return lWrapper;
  	}
  	set {
  		lWrapper = value;
  		// if we have the config context as well, update the 
  		// list of value maps for rendering
      if (WFActionWrapperList == null && configContext != null)
        updateWFActionWrapperList();  		
  	}
  }
  
  // config context [custom setting] with the fields to 
  // be rendered
  public String configContext { 
  	get {
  		return lConfigContext;
  	} 
  	set {
  		lConfigContext = value;
      // if we have the wrapper, update the 
      // list of value maps for rendering
  		if (WFActionWrapperList == null && wrapper != null)
  		  updateWFActionWrapperList();
  	}
  }
  
  // rendered on the page 
  public List<WorkflowActionWrapper> WFActionWrapperList { get; set; }
  
  // update the linkactionwrapperlist with new link action wrappers (defined below) 
  // based on the generic wrapper
  public void updateWFActionWrapperList() {
  	if(wfActionOptions == null){
  		wfActionOptions = new List<SelectOption>();
		  wfActionOptions.add(new SelectOption(Label.DC_NotSelected,Label.DC_NotSelected));
		  wfActionOptions.add(new SelectOption(Label.DC_Approve,Label.DC_Approve));
		  wfActionOptions.add(new SelectOption(Label.DC_Reject,Label.DC_Reject));
  	}
  	WFActionWrapperList = new List<WorkflowActionWrapper>();
  	// retrieve field configuration from custom setting
  	fieldConfig = populateFieldConfig();

  	lwrapper.gotoBeforeFirst();
  	
  	while (lwrapper.hasNext()) {
  		lwrapper.next();
      // create new link action wrapper list  		
  		WFActionWrapperList.add(new WorkflowActionWrapper(wrapper, fieldConfig));
  	}
  }
  
  // retrieve field configuration from custom setting
  public List<sObject> populateFieldConfig() {
  	String query = 'SELECT Name, goog_dclk_dsm__DC_SortOrder__c, goog_dclk_dsm__DC_DeepLinkToDSMRecord__c FROM '+configContext+' ORDER BY goog_dclk_dsm__DC_SortOrder__c';
  	return Database.query(query);
  }
	
	//Function called on click of Perform Action button
	public PageReference performWFActions(){
		
		String actionText = '';
		
		try{
		
			if(WFActionWrapperList == null || WFActionWrapperList.size() == 0){
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No Workflow Request'));
				return null;
			}
			isError = false;
			rejectedWorkflows = new List<DC_WorkflowService.WorkflowRequest>();
			approvedWorkflows = new List<DC_WorkflowService.WorkflowRequest>();
			
			DC_DoubleClickWorkflowMapper mapper = DC_DoubleClickWorkflowMapper.getInstance();
			for(WorkflowActionWrapper wfAct : WFActionWrapperList){
				if(wfAct.selectedAction == Label.DC_Reject){ 
					rejectedWorkflows.add((DC_WorkflowService.WorkflowRequest)wfAct.objectX);
				}else if(wfAct.selectedAction == Label.DC_Approve){
					approvedWorkflows.add((DC_WorkflowService.WorkflowRequest)wfAct.objectX);
				}
			}
	    //If any wf request is approved
	    if(approvedWorkflows.size() > 0){
	      actionText = 'Approve Proposal';
	      approveWorkflowRequests(mapper.wrap(approvedWorkflows),actionText);
	    } else {
	      System.debug('Nothing to approve');
	    }
			//If any wf request is rejected
			if(rejectedWorkflows.size() > 0){
				actionText = 'Reject Proposal';
				rejectWorkflowRequests(mapper.wrap(rejectedWorkflows),actionText);
			} else {
	      System.debug('Nothing to reject');
	    }
	    
			if(currentOpportunity.goog_dclk_dsm__DC_DoubleClickId__c != null){
				DC_OpportunityProposalConnector connectorInstance = DC_OpportunityProposalConnector.getInstance();
				DC_GenericWrapper propsalWrapper = connectorInstance.getProposalFromOpportunity(currentOpportunity,actionText);
				DC_Connector.DC_DoubleClickPageProcessingResult res = connectorInstance.syncProposalsFromDCtoSF(propsalWrapper,actionText);
				connectorInstance.executeAllDml();
			}
			Pagereference pg = new Pagereference('/apex/DC_WorkflowActionsPage?id='+currentOpportunity.id);
			pg.setRedirect(true);
		
			return pg;
		}
		catch (Exception e) {
				isError = true;
	    		System.debug('e:::' + e);
		      if(!ApexPages.hasMessages()){
	        	ApexPages.addMessages(e);
	        	
	      	}
	      	return null;
	  	}
		
	}
	
	//If any wf request is rejected
	private void rejectWorkflowRequests(DC_GenericWrapper rejectedWorkflowsWrap,String actionText){
		
		DC_WorkflowManager wfManager1 = DC_ManagerFactory.getInstance().getWorkflowManager(actionText);
		rejectedWFRequests = wfManager1.rejectWorkflowRequests(rejectedWorkflowsWrap);
		
		if(rejectedWFRequests != rejectedWorkflows.size()){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, String.format(Label.DC_SF_Related_Record_Has_Matching_DC_Record, new String[]{ Label.DC_Rejected})));
			isError = true;
			return;
		}
	}
	
	//If any wf request is approved
	private void approveWorkflowRequests(DC_GenericWrapper approvedWorkflowsWrap,String actionText){

		DC_WorkflowManager wfManager1 = DC_ManagerFactory.getInstance().getWorkflowManager(actionText);
		approvedWFRequests = wfManager1.approveWorkflowRequests(approvedWorkflowsWrap);
		
		if(approvedWFRequests != approvedWorkflows.size()){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, String.format(Label.DC_SF_Related_Record_Has_Matching_DC_Record, new String[]{ Label.DC_Approved})));
			isError = true;
			return;
		}
	}
	
	
	
	
  // inner class for a record coming from the generic wrapper
  public class WorkflowActionWrapper {
    // this records index in the source DC_GenericWrapper instance
    private Integer index;
    public String selectedAction{get;set;}
    
    public Object objectX{get;set;}
    
    // map of values for the DC_GenericWrapper record provided
    public Map<String, Object> values { get; set; }
    
    // constructor
    // wrapper      DC_GenericWrapper           wrapper containing the specific record for this instance
    // fieldConfig  List<sObject>               field configuration records used to define which fields appear
    public WorkflowActionWrapper(DC_GenericWrapper wrapper, List<sObject> fieldConfig) {
      // identify the index of the selected record -- will be used for createLink call if selected
      index = wrapper.getCurrentIndex();
      selectedAction = Label.DC_NotSelected;
      this.objectX = wrapper.getCurrentObject();
       
      // shift the values from the record to the value map (values)
      wrapperCurrentRecordValuesToMap(wrapper, fieldConfig);
    }
		
    // take the current record's values from the wrapper and put them in the values map
    // wrapper      DC_GenericWrapper     wrapper with record and values
    // fieldConfig  List<sObject>         records containing the field information for value selection
	  private void wrapperCurrentRecordValuesToMap(DC_GenericWrapper wrapper, List<sObject> fieldConfig) {
	    values = new Map<String, Object>(); 
	    for (sObject configItem : fieldConfig) {
	    	string configName = (string)configItem.get('name');
	      values.put(configName, wrapper.getField(configName));
	    }
	  }
  }
}