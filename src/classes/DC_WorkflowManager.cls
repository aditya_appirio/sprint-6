public with sharing class DC_WorkflowManager extends DC_DoubleClickManager {

  private DC_WorkflowDAO dao {
    get {
      if(dao == null) {
        dao = new DC_WorkflowDAO(this.authHandler);
      }

      return dao;
    }
    set;
  }

  public DC_WorkflowManager(DC_DoubleClickOAuthHandler authHandler){
    super(authHandler);
  }
	
	 // opportunity mapper instance
  private DC_ProposalMapper opportunityMapper {
    get {
      if(opportunityMapper == null) {
        opportunityMapper = DC_MapperFactory.getInstance().getOpportunityMapper();
      }
      return opportunityMapper;
    }
    set;
  }
  
	public DC_GenericWrapper wrapOppty(Opportunity opp){
		return opportunityMapper.wrap(opp);
	}
  public DC_GenericWrapper getPendingWorkflowRequests(DC_GenericWrapper opp) {
    // TODO: Using status = 'PENDING_APPROVAL' in the query doesn't work, it basically doesn't return anything even if there are
    // items pending approval, below we have a workaround that removes the ones that are not pending, need to figure out why the 
    // query criteria didn't work and use the query instead
    
    //DC_WorkflowService.WorkflowRequest[] requests = dao.getWorkflowRequestByStatement('WHERE entityId = \'' + opp.getField('doubleclickid') + '\' and entityType = \'PROPOSAL\'').results;
    DC_WorkflowService.WorkflowRequest[] requests = new List<DC_WorkflowService.WorkflowRequest>();
    requests = dao.getWorkflowRequestByStatement('WHERE entityId = \'' + opp.getField('doubleclickid') + '\' and entityType = \'PROPOSAL\'' +' and type = \'WORKFLOW_APPROVAL_REQUEST\'').results;
    DC_WorkflowService.WorkflowRequest[] pendingRequests = new List<DC_WorkflowService.WorkflowRequest>();
		System.debug('requests:::' +requests );
		System.debug('requests.size():::' +requests.size() );
    for(DC_WorkflowService.WorkflowRequest request : requests) {
    	System.debug('request:::' +request );
      if(request.status != null && request.status.equalsIgnoreCase('PENDING_APPROVAL')) {
        pendingRequests.add(request);
      }
    }
		System.debug('pendingRequests:::' +pendingRequests );
    return DC_DoubleClickWorkflowMapper.getInstance().wrap(pendingRequests);
  }

  public Integer approveWorkflowRequests(DC_GenericWrapper requests) {
    String inStatement = '';
    String separator = '';
		System.debug('requests:::' +requests );
    requests.gotoBeforeFirst();
    while(requests.hasNext()) {
      requests.next();

      inStatement += separator + requests.getField('doubleclickid');

      separator = ', ';
    }
    //return dao.approveWorkflowRequests('WHERE id in (' + inStatement + ')');
    return dao.approveWorkflowRequests('WHERE id in (' + inStatement + ')' + ' and type = \'WORKFLOW_APPROVAL_REQUEST\'');
  }

  public Integer rejectWorkflowRequests(DC_GenericWrapper requests) {
    String inStatement = '';
    String separator = '';
		System.debug('reject requests:::' +requests );
    requests.gotoBeforeFirst();
    while(requests.hasNext()) {
      requests.next();

      inStatement += separator + requests.getField('doubleclickid');

      separator = ', ';
    }
	//return dao.approveWorkflowRequests('WHERE id in (' + inStatement + ')');
    return dao.rejectWorkflowRequests('WHERE id in (' + inStatement + ')' + ' and type = \'WORKFLOW_APPROVAL_REQUEST\'');
  }

}