/**
 */
@isTest
private class Test_DC_DoubleClickTeamManager {

	static testMethod void testDoubleClickTeamManager() {
		goog_dclk_dsm__DC_ObjectMapping__c dcobjMapping1 = DC_TestUtils.createObjectMapping('Team', 'Team', 'goog_dclk_dsm__DC_Team__c', true);
		List<goog_dclk_dsm__DC_FieldMapping__c> fldMappings = new List<goog_dclk_dsm__DC_FieldMapping__c>();
	  Test.startTest();
	  Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplTeamService());
	  
	  DC_DoubleClickOAuthHandler oAuthHandler = new DC_DoubleClickOAuthHandler(Userinfo.getUserId());     
	  DC_DoubleClickTeamManager dcTeamManager = new DC_DoubleClickTeamManager(oAuthHandler);  
	  
	  system.debug('Point 1');
	      
	  DC_GenericWrapper gwObj = dcTeamManager.getTeamsUpdatedSinceLastSync(DateTime.now(), 10, 0);
	  system.assertEquals(gwObj <> null, true);
	  system.debug('Point 2');
	  
	  Test.stopTest();
	}
}