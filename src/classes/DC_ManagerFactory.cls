// 
// (c) 2014 Appirio
//
// DC_ManagerFactory
// DC Connector
//
// 20 Jan 2014    Anjali K (JDC)      Original
// 15 Oct 2014    				      Updated        added User Authentication and Admin Authentication logic

public with sharing class DC_ManagerFactory {
  
  private static DC_ManagerFactory instance;
  private DC_DoubleClickOAuthHandler authHandler = new DC_DoubleClickOAuthHandler(null);
  private DC_DoubleClickOAuthHandler userAuthHandler = new DC_DoubleClickOAuthHandler(Userinfo.getUserId());
  private static boolean isUserContext = false;
  private Map<String,goog_dclk_dsm__DC_Access_for_Action__c> mapDCActionWithAccess =  goog_dclk_dsm__DC_Access_for_Action__c.getAll();
  
  // singleton enforcing getinstance
  public static DC_ManagerFactory getInstance() {
  	try {
	    if(ApexPages.CurrentPage() != null) {
	      	isUserContext = ApexPages.CurrentPage().getUrl().contains('/apex/') ? true : false;
	    } else {
	      isUserContext = false;
	    }
  	} catch (Exception ex) {
		// Not in the context of apex page
	  	isUserContext = false;
	}  
  	
    if(instance == null) {
      instance = new DC_ManagerFactory();
      
      if(!Test.isRunningTest() && isUserContext && instance.userAuthHandler.auth == null) {
      	//donothing
      }

    } 
    
    return instance;
  }
  
  //Constructor
  private DC_ManagerFactory(){
    
  }
  
  public void setAuthHandler(DC_DoubleClickOAuthHandler authHandler) {
      this.authHandler = authHandler;
  }

  // return an instance of the doubleclick company manager
  public DC_ReportManager getReportManager() {
    return new DC_ReportManager(this.authHandler);
  }

  // return an instance of the doubleclick company manager
  public DC_DoubleClickCompanyManager getCompanyManager() {
    if(isUserContext && (!mapDCActionWithAccess.isEmpty() && mapDCActionWithAccess.get('Push Account').goog_dclk_dsm__Access__c == 'User')){
        if(this.userAuthHandler.auth == null || (this.userAuthHandler.auth != null && this.userAuthHandler.auth.goog_dclk_dsm__DC_AccessToken__c == null)){
           throw new DC_ConnectorException('You are not authenticated to perform this operation against DoubleClick, please proceed to the DC Authorization page to perform the authentication process(User Authentication)');
        }
        else 
            return new DC_DoubleClickCompanyManager(this.userAuthHandler);
    }
    else{
      if(this.authHandler.auth == null || (this.authHandler.auth != null && this.authHandler.auth.goog_dclk_dsm__DC_AccessToken__c == null)){
            throw new DC_ConnectorException('You are not authenticated to perform this operation against DoubleClick, please proceed to the DC Authorization page to perform the authentication process(Admin Authentication)');
        }
        else
            return new DC_DoubleClickCompanyManager(this.authHandler); 
    }
  }
  
  // return an instance of the custom field manager
  public DC_CustomFieldManager getCustomFieldManager() {
    return new DC_CustomFieldManager(this.authHandler); 
  }
  
  // return an instance of the salesforce account manager
  public DC_SalesforceAccountManager getSalesforceAccountManager() {
    return new DC_SalesforceAccountManager();
  }
  
  //For Contact : 29 Jan: In Progress
  
  // return an instance of the doubleclick contact manager
  public DC_DoubleClickContactManager getContactManager() {
    if(isUserContext && mapDCActionWithAccess.get('Push Contact').goog_dclk_dsm__Access__c == 'User'){
        if(this.userAuthHandler.auth == null || (this.userAuthHandler.auth != null && this.userAuthHandler.auth.goog_dclk_dsm__DC_AccessToken__c == null)){
            throw new DC_ConnectorException('You are not authenticated to perform this operation against DoubleClick, please proceed to the DC Authorization page to perform the authentication process(User Authentication)'); 
        }
        else 
            return new DC_DoubleClickContactManager(this.userAuthHandler);
    }
    else{
            if(this.authHandler.auth == null || (this.authHandler.auth != null && this.authHandler.auth.goog_dclk_dsm__DC_AccessToken__c == null)){
                throw new DC_ConnectorException('You are not authenticated to perform this operation against DoubleClick, please proceed to the DC Authorization page to perform the authentication process(Admin Authentication)');
            }
            else
                return new DC_DoubleClickContactManager(this.authHandler);
        }
  }
  
  // return an instance of the salesforce contact manager
  public DC_SalesforceContactManager getSalesforceContactManager() {
    return new DC_SalesforceContactManager();
  }
  
  //For Proposal : 04 Feb: In Progress
  
  // return an instance of the doubleclick proposal manager
  public DC_DoubleClickProposalManager getProposalManager(String actionText) {
    System.debug('--actionText->' + actionText  + isUserContext);
    if(mapDCActionWithAccess.containsKey(actionText)  && mapDCActionWithAccess.get(ActionText).goog_dclk_dsm__Access__c == 'User'){
        if(this.userAuthHandler.auth == null || (this.userAuthHandler.auth != null && this.userAuthHandler.auth.goog_dclk_dsm__DC_AccessToken__c == null)){
            throw new DC_ConnectorException('You are not authenticated to perform this operation against DoubleClick, please proceed to the DC Authorization page to perform the authentication process(User Authentication)');
        }
        else {
        	System.debug('--->' + actionText + '<-action->' + this.userAuthHandler);
            return new DC_DoubleClickProposalManager(this.userAuthHandler); 
        }
    }
    else{
        if(this.authHandler.auth == null || (this.authHandler.auth != null && this.authHandler.auth.goog_dclk_dsm__DC_AccessToken__c == null)){
            throw new DC_ConnectorException('You are not authenticated to perform this operation against DoubleClick, please proceed to the DC Authorization page to perform the authentication process(Admin Authentication)');
        }
        else{
        	System.debug('--->' + actionText + '<-action->' + this.authHandler);
            return new DC_DoubleClickProposalManager(this.authHandler);
        }    
 
    }
  } 
  
  // return an instance of the doubleclick proposal line item manager
  public DC_DoubleClickProposalLineItemManager getproposalLIManager() {
    return new DC_DoubleClickProposalLineItemManager(this.authHandler); 
  }
  
  // return an instance of the salesforce opportunity manager
  public DC_SalesforceOpportunityManager getSalesforceOpportunityManager() {
    return new DC_SalesforceOpportunityManager();
  }
  // return an instance of the salesforce opportunity Line Item manager
  public DC_SalesforceOpportunityLineItemManager getSalesforceOpportunityLIManager() {
    return new DC_SalesforceOpportunityLineItemManager();
  }
  // return an instance of the salesforce dc User manager
  public DC_SalesforceUserManager getSalesforceUserManager() {
    return new DC_SalesforceUserManager();
  }
  // return an instance of the double click User manager
  public DC_DoubleClickUserManager getUserManager() {
    return new DC_DoubleClickUserManager(this.authHandler);
  }
  
  // return an instance of the double click network manager
  public DC_NetworkManager getNetworkManager() {
    return new DC_NetworkManager(this.authHandler);
  }

  // return an instance of the double click workflow manager
  public DC_WorkflowManager getWorkflowManager(String actionText) {
    if(isUserContext && (mapDCActionWithAccess.containsKey(actionText)  && mapDCActionWithAccess.get(ActionText).goog_dclk_dsm__Access__c == 'User')){
        if(this.userAuthHandler.auth == null || (this.userAuthHandler.auth != null && this.userAuthHandler.auth.goog_dclk_dsm__DC_AccessToken__c == null)){
            throw new DC_ConnectorException('You are not authenticated to perform this operation against DoubleClick, please proceed to the DC Authorization page to perform the authentication process(User Authentication)');
        }
        else 
            return new DC_WorkflowManager(this.userAuthHandler); 
    }
    else{
        if(this.authHandler.auth == null || (this.authHandler.auth != null && this.authHandler.auth.goog_dclk_dsm__DC_AccessToken__c == null)){    
            throw new DC_ConnectorException('You are not authenticated to perform this operation against DoubleClick, please proceed to the DC Authorization page to perform the authentication process(Admin Authentication)');
        }
        else
            return new DC_WorkflowManager(this.authHandler);
    }
    
  }
  
  // return an instance of the sf team manager
  public DC_SalesforceTeamManager getSalesforceTeamManager() {
    return new DC_SalesforceTeamManager();
  }
  // return an instance of the double click team manager
  public DC_DoubleClickTeamManager getTeamManager() {
    return new DC_DoubleClickTeamManager(this.authHandler);
  }
  // return an instance of the double click team manager
  public DC_DoubleClickUserTeamAssociationManager getUserTeamAssociationManager() {
    return new DC_DoubleClickUserTeamAssociationManager(this.authHandler);
  }
}