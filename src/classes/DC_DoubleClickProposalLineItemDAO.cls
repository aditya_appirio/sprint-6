// 
// (c) 2014 Appirio
//
// DC_DoubleClickProposalLineItemDAO
// T-249484 :   Create Proposal Lines classes
// 
// 13 Feb 2014    Ankit Goyal (JDC)      Copied from DC_DoubleClickProposalLineItemDAO
//
public with sharing class DC_DoubleClickProposalLineItemDAO extends DC_DoubleClickDAO{
  protected override void tokenRefreshed() {
    this.proposalLIService = null; 
  }
  
  //Construtor
  public DC_DoubleClickProposalLineItemDAO(DC_DoubleClickOAuthHandler authHandler) {
    super(authHandler);
  }
 
  // get a configured doubleclick opportunity interface
  private DC_ProposalLineItemService.ProposalLineItemServiceInterfacePort proposalLIService {
    get {
      if(this.proposalLIService == null) {
        this.proposalLIService = DC_ServicesFactory.getProposalLineItemService(this.authHandler);
      } 

      return this.proposalLIService;
    }
    set;
  }
  

  
  //Inner class for getting Proposal by statement
  private class GetProposalLIByStatementCommand implements DC_DoubleClickDAO.Command {
    public DC_ProposalLineItemService.ProposalLineItemPage result;
    public DC_DoubleClickProposalLineItemDAO context;
    public DC_ProposalLineItemService.Statement statement;
    
    public GetProposalLIByStatementCommand(DC_DoubleClickProposalLineItemDAO context, DC_ProposalLineItemService.Statement statement) {
      this.context = context;
      this.statement = statement;
    }
    
    public void execute() {
      this.result = context.proposalLIService.getProposalLineItemsByStatement(statement);
      System.debug('@@ Service: Total Result Set Size: ' + this.result.totalResultSetSize);
      
      if (this.result == null || this.result.results == null || this.result.results.size() == 0) {
        return;        
      }
      for (DC_ProposalLineItemService.ProposalLineItem proposalLI : this.result.results) {
      	System.debug('proposalLI.customFieldValues:::' + proposalLI.customFieldValues);
      	System.debug('proposalLI.dropDownCustomFieldValues:::' + proposalLI.dropDownCustomFieldValues);
        if ((proposalLI.customFieldValues == null || proposalLI.customFieldValues.size() == 0) && (proposalLI.dropDownCustomFieldValues == null || proposalLI.dropDownCustomFieldValues.size() == 0) ){
          continue;
        } 
        if (proposalLI.customFieldValues != null){
	        for (integer i = 0; i < proposalLI.customFieldValues.size(); i++) {
	          System.debug('@@ Custom Field Text: ' + proposalLI.customFieldValues[i]);
	          System.debug('@@ Custom Field Type: ' + proposalLI.customFieldValues[i].BaseCustomFieldValue_Type);
						System.debug('@@ Custom Field value: ' + proposalLI.customFieldValues[i].value);
	          proposalLI.customFieldValues[i] = copyCustomField(proposalLI.customFieldValues[i]);
	        }
        }
        if(proposalLI.dropDownCustomFieldValues != null){
	        for (integer i = 0; i < proposalLI.dropDownCustomFieldValues.size(); i++) {
	          System.debug('@@ Custom Field Text: ' + proposalLI.dropDownCustomFieldValues[i]);
	          System.debug('@@ Custom Field Type: ' + proposalLI.dropDownCustomFieldValues[i].BaseCustomFieldValue_Type);
	          proposalLI.dropDownCustomFieldValues[i] = copyCustomField(proposalLI.dropDownCustomFieldValues[i]);
	        }
				}
        
        System.debug('@@ Copied Custom Fields: ' + proposalLI.customFieldValues);
      }
    }
    public DC_ProposalLineItemService.BaseCustomFieldValue copyCustomField(DC_ProposalLineItemService.BaseCustomFieldValue inval) {
      DC_ProposalLineItemService.BaseCustomFieldValue ret;
      if (inval.BaseCustomFieldValue_Type == 'CustomFieldValue') {
        DC_ProposalLineItemService.CustomFieldValue cfv = new DC_ProposalLineItemService.CustomFieldValue();
        cfv.customFieldId = inval.customFieldId;
        cfv.value = copyCustomFieldValue(inval.value); // copy value
        System.debug('@@ Value after copy: ' + cfv.value);
        ret = cfv;
      } else if (inval.BaseCustomFieldValue_Type == 'DropDownCustomFieldValue') {
        DC_ProposalLineItemService.DropDownCustomFieldValue ddcfv = new DC_ProposalLineItemService.DropDownCustomFieldValue();
        ddcfv.customFieldId = inval.customFieldId;
        ddcfv.customFieldOptionId = inval.customFieldOptionId; // copy value
        ret = (DC_ProposalLineItemService.BaseCustomFieldValue)ddcfv;
      } 
      //ret.customFieldId = inval.customFieldId;
      //ret.value = inval.value;
      return ret;
    }

    public DC_ProposalLineItemService.Value copyCustomFieldValue(DC_ProposalLineItemService.Value inval) {
      DC_ProposalLineItemService.Value ret;
      System.debug('@@ Service value: ' + inval.value);

      if (inval.value_type == 'BooleanValue') {
        DC_ProposalLineItemService.BooleanValue bv = new DC_ProposalLineItemService.BooleanValue();
        bv.value = boolean.valueOf(inval.value);
        ret = bv;
      } else if (inval.value_type == 'NumberValue') {
        DC_ProposalLineItemService.NumberValue nv = new DC_ProposalLineItemService.NumberValue();
        nv.value = inval.value;
        ret = nv;
      } else if (inval.value_type == 'TextValue') {
        DC_ProposalLineItemService.TextValue tv = new DC_ProposalLineItemService.TextValue();
        tv.value = inval.value;
        System.debug('@@ TextValue: ' + tv.value);
        ret = tv;
      }

      return ret;
    } 
  }
  
  // retrieve a company using the passed in query string
  public DC_ProposalLineItemService.ProposalLineItemPage getProposalLineItemsByStatement(String query) {
    DC_ProposalLineItemService.Statement statement = this.createProposalLIStatement(query);
    System.debug('query:::' + query);
    GetProposalLIByStatementCommand cmd = new GetProposalLIByStatementCommand(this, statement);
    
    this.invokeAPI(cmd, true);
    
    return cmd.result;
  }
  
  //Inner class for creating Proposals
  private class CreateProposalsCommand implements DC_DoubleClickDAO.Command {
    public DC_DoubleClickProposalLineItemDAO context;
    public DC_ProposalLineItemService.ProposalLineItem[] proposals;
    public DC_ProposalLineItemService.ProposalLineItem[] result;
    
    public CreateProposalsCommand(DC_DoubleClickProposalLineItemDAO context, DC_ProposalLineItemService.ProposalLineItem[] proposals) {
      this.context = context;
      this.proposals = proposals;
    }
    
    public void execute() {
      this.result = context.proposalLIService.createProposalLineItems(this.proposals);
    }
  }
  
  // retrieve a company using the passed in query string
  public DC_ProposalLineItemService.ProposalLineItem[] createProposalLineItems(DC_ProposalLineItemService.ProposalLineItem[] proposals) {
    CreateProposalsCommand cmd = new CreateProposalsCommand(this, proposals);
    
    this.invokeAPI(cmd, true);
    
    return cmd.result;
  }
  
  //Inner class for updating Proposals
  private class UpdateProposalLIsCommand implements DC_DoubleClickDAO.Command {
    public DC_DoubleClickProposalLineItemDAO context;
    public DC_ProposalLineItemService.ProposalLineItem[] proposalLIs;
    
    public UpdateProposalLIsCommand(DC_DoubleClickProposalLineItemDAO context, DC_ProposalLineItemService.ProposalLineItem[] proposalLIs) {
      this.context = context;
      this.proposalLIs = proposalLIs;
    }
    
    public void execute() {
      context.proposalLIService.updateProposalLineItems(this.proposalLIs);
    }
  }
  
  //Command for updating Proposals
  public void updateProposalLineItems(DC_ProposalLineItemService.ProposalLineItem[] proposalLIs) {
    UpdateProposalLIsCommand cmd = new UpdateProposalLIsCommand(this, proposalLIs);
    
    this.invokeAPI(cmd, true);
  }
  

  
  public DC_ProposalLineItemService.Statement createProposalLIStatement(String query, DC_ProposalLineItemService.String_ValueMapEntry[] params) {
    DC_ProposalLineItemService.Statement result = createProposalLIStatement(query);
    result.values = params;
    
    return result;
  }
  
  public DC_ProposalLineItemService.Statement createProposalLIStatement(String query) {
    DC_ProposalLineItemService.Statement result = new DC_ProposalLineItemService.Statement();
    System.debug('query::' + query);
    result.query = query;
    
    return result;
  }

  // ** DEPRECATED for 201403 API ====================================
  /*

  //Inner class for updating Proposal
  private class UpdateProposalLICommand implements DC_DoubleClickDAO.Command {
    public DC_DoubleClickProposalLineItemDAO context;
    public DC_ProposalLineItemService.ProposalLineItem proposalLI;
    
    public UpdateProposalLICommand(DC_DoubleClickProposalLineItemDAO context, DC_ProposalLineItemService.ProposalLineItem proposalLI) {
      this.context = context;
      this.proposalLI = proposalLI;
    }
    
    public void execute() {
      context.proposalLIService.updateProposalLineItem(this.proposalLI);
    }
  }
  //Command for updating Proposal
  public void updateProposalLineItem(DC_ProposalLineItemService.ProposalLineItem proposalLI) {
    UpdateProposalLICommand cmd = new UpdateProposalLICommand(this, proposalLI);
    
    this.invokeAPI(cmd, true);
  }
  
  //Inner class for getting Proposal
  private class GetProposalLICommand implements DC_DoubleClickDAO.Command {
    public DC_ProposalLineItemService.ProposalLineItem result;
    public DC_DoubleClickProposalLineItemDAO context;
    public Integer id;
    
    public GetProposalLICommand(DC_DoubleClickProposalLineItemDAO context, Integer id) {
      this.context = context;
      this.id = id;
    }
    
    public void execute() {
      this.result = context.proposalLIService.getProposalLineItem(id);
    }
  }

  // retrieve a DC Proposal Line Item by the passed in integer id
  public DC_ProposalLineItemService.ProposalLineItem getProposalLineItem(Integer id, boolean retry) {
    GetProposalLICommand cmd = new GetProposalLICommand(this, id);
    
    this.invokeAPI(cmd, true);
    
    return cmd.result;
  }  
  */
  // END DEPRECATED ====================================


}