// 
// (c) 2014 Appirio
//
// DC_AccountCompanyConnector
//
// 12 Feb 2014    Ankit Goyal (JDC)      Original
// 16 Oct 2014    				         Updated     Added logic for User And Admin Auth.

public with sharing class DC_OpportunityProposalConnector extends DC_Connector {
  public static boolean connectorSyncUpdate = false;
  private static DC_OpportunityProposalConnector instance;
  
  //constructor
  private DC_OpportunityProposalConnector(){
    super();
  }
  public static DC_OpportunityProposalConnector getInstance() {
    if(instance == null) {
      instance = new DC_OpportunityProposalConnector();
    }  
    return instance;
  }
  
   // opportunity mapper instance
  private DC_ProposalMapper opportunityMapper {
    get {
      if(opportunityMapper == null) {
        opportunityMapper = DC_MapperFactory.getInstance().getOpportunityMapper();
      }
      return opportunityMapper;
    }
    set;
  }
  
  // doubleClick Manager Instance
  public DC_DoubleClickProposalManager proposalManager {
    get {
      if(proposalManager == null) {
        proposalManager = managerFactory.getProposalManager('Admin');
      }
      return proposalManager;  
    }
    set;
  }
  
  // doubleClick Manager Instance
  public DC_DoubleClickProposalManager proposalManagerWithAction {get;set;}
  public void getproposalManager(String strAction)
  {
    if(proposalManagerWithAction == null)
        proposalManagerWithAction = managerFactory.getProposalManager(strAction);
  }
  
  // salesforce opportunity manager instance
  private DC_SalesforceOpportunityManager opportunityManager {
    get {
      if(opportunityManager == null) {
        opportunityManager = managerFactory.getSalesforceOpportunityManager();
      }
      return opportunityManager;
    }
    set;
  }
  
  // Salesforce user manager instance  
  private DC_SalesforceUserManager userManager {
    get {
      if(userManager == null) {
        userManager = managerFactory.getSalesforceUserManager();
      }
      return userManager;
    }
    set;
  }
  
  
  
  public void syncExistingOpportunities(List<Opportunity> oppList) {
    DC_GenericWrapper oppsToSync = opportunityMapper.wrap(oppList);

    if(oppsToSync.size() > 0) {
      oppsToSync = oppsToSync.getExistingRecords();

      if(oppsToSync.size() > 0) {
        syncSFOppsToDCProposals(oppsToSync);
      }
    }
  }
  
  // returns retracted opportunities
  public List<Opportunity> syncSFOpportunitiesToDCProposals(List<Opportunity> oppList) {
    DC_GenericWrapper connectorQueriedOpportunities = opportunityManager.getOpportunities(oppList);

    DC_GenericWrapper retracted = syncSFOppsToDCProposals(connectorQueriedOpportunities);
    if (retracted != null) {
      return (List<Opportunity>)retracted.getObjects();
    }

    return new List<Opportunity>();
  }
  
  // returns retracted opportunities
  public List<Opportunity> syncSFOpportunitiesToDCProposals(List<Opportunity> oppList,String actionText) {
    DC_GenericWrapper connectorQueriedOpportunities = opportunityManager.getOpportunities(oppList);
 
    DC_GenericWrapper retracted = syncSFOppsToDCProposals(connectorQueriedOpportunities,actionText);
    if (retracted != null) {
      return (List<Opportunity>)retracted.getObjects();
    }
 
    return new List<Opportunity>();
  }
  
  public DC_GenericWrapper getProposalFromOpportunity(Opportunity opp){
  	DC_GenericWrapper connectorQueriedOpportunities = opportunityManager.getOpportunities(new List<Opportunity>{opp});
  	DC_GenericWrapper existingDCProposals = proposalManager.getProposals(connectorQueriedOpportunities);
  	return existingDCProposals;
  }
  
  public DC_GenericWrapper getProposalFromOpportunity(Opportunity opp,String actionText){
    getproposalManager(actionText);
    DC_GenericWrapper connectorQueriedOpportunities = opportunityManager.getOpportunities(new List<Opportunity>{opp});
    DC_GenericWrapper existingDCProposals = proposalManagerWithAction.getProposals(connectorQueriedOpportunities);
    return existingDCProposals;
  }
  
  // returns retracted opportunities
  public DC_GenericWrapper syncSFOppsToDCProposals(DC_GenericWrapper opps) {
    connectorSyncUpdate = true;
    // Find which opps are new records to be created in DC
    DC_GenericWrapper oppsToCreate = opps.getNewRecords();
    System.debug('oppsToCreate:' + oppsToCreate);
    // Find which opps are new records to be created in DC
    DC_GenericWrapper oppsToUpdate = opps.getExistingRecords();
    System.debug('oppsToUpdate:' + oppsToUpdate);    
    // Retrieve existing contacts from DC
    DC_GenericWrapper existingDCProposals = proposalManager.getProposals(oppsToUpdate);
		System.debug('existingDCProposals:' + existingDCProposals);
    // Create prepare new contacts 
    DC_GenericWrapper newDCProposals = opportunityMapper.createNewDCProposals(oppsToCreate);
    System.debug('newDCProposals:' + newDCProposals);
    DC_GenericWrapper retractedOpportunities;
    DC_GenericWrapper approvedProposals;
    if (existingDCProposals.size() > 0 && (approvedProposals = getApprovedProposals(existingDCProposals)).size() > 0) {
      proposalManager.retractProposals(approvedProposals);
      retractedOpportunities = opportunityManager.getOpportunities(approvedProposals);
    }
    // Update existing contacts 
    existingDCProposals.gotoBeforeFirst();
    oppsToUpdate.gotoBeforeFirst();
    while(existingDCProposals.hasNext()) {
      existingDCProposals.next();
      oppsToUpdate.next();
      
      opportunityMapper.updateFields(oppsToUpdate, existingDCProposals);
    } 
    opportunityMapper.closeCustomHandlers();
    
    existingDCProposals = proposalManager.updateProposals(existingDCProposals);

    if (existingDCProposals != null && oppsToUpdate != null) {
      existingDCProposals.gotoBeforeFirst();
      oppsToUpdate.gotoBeforeFirst();
      while(existingDCProposals.hasNext() && oppsToUpdate.hasNext()) {
        existingDCProposals.next();
        oppsToUpdate.next();
        
        //oppsToCreate.updateFields(newDCProposals);
        opportunityMapper.updateFields(existingDCProposals, oppsToUpdate);
      }
    }

    opportunityMapper.closeCustomHandlers();

    opportunityManager.updateOpportunities(oppsToUpdate);

    // Create new proposals in DoubleClick
    newDCProposals = proposalManager.createProposals(newDCProposals);
    
    // Update contacts with company id
    newDCProposals.gotoBeforeFirst();
    oppsToCreate.gotoBeforeFirst();
    while(newDCProposals.hasNext()) {
      newDCProposals.next();
      oppsToCreate.next();
      
      //oppsToCreate.updateFields(newDCProposals);
      opportunityMapper.updateFields(newDCProposals, oppsToCreate);
    }

    opportunityMapper.closeCustomHandlers();
		System.debug('test:::');
    oppsToCreate.gotoBeforeFirst();
    newDCProposals.gotoBeforeFirst();
    while(oppsToCreate.hasNext()) {
      oppsToCreate.next();
      newDCProposals.next();

      opportunityMapper.linkOpportunityAndProposal(oppsToCreate, newDCProposals);
    }

    opportunityMapper.closeCustomHandlers();
		System.debug('oppsToCreate:' + oppsToCreate);
    opportunityManager.updateOpportunities(oppsToCreate);

    this.executeAllDml();

    return retractedOpportunities;
  }
  
  // returns retracted opportunities
  //Overloaded method 
  public DC_GenericWrapper syncSFOppsToDCProposals(DC_GenericWrapper opps,String actionText) {
    
    getproposalManager(actionText);
    connectorSyncUpdate = true;
    // Find which opps are new records to be created in DC
    DC_GenericWrapper oppsToCreate = opps.getNewRecords();
    System.debug('oppsToCreate:' + oppsToCreate);
    // Find which opps are new records to be created in DC
    DC_GenericWrapper oppsToUpdate = opps.getExistingRecords();
    System.debug('oppsToUpdate:' + oppsToUpdate);    
    // Retrieve existing contacts from DC
    DC_GenericWrapper existingDCProposals = proposalManagerWithAction.getProposals(oppsToUpdate);
        System.debug('existingDCProposals:' + existingDCProposals);
    // Create prepare new contacts 
    DC_GenericWrapper newDCProposals = opportunityMapper.createNewDCProposals(oppsToCreate);
    System.debug('newDCProposals:' + newDCProposals);
    DC_GenericWrapper retractedOpportunities;
    DC_GenericWrapper approvedProposals;
    if (existingDCProposals.size() > 0 && (approvedProposals = getApprovedProposals(existingDCProposals)).size() > 0) {
      proposalManagerWithAction.retractProposals(approvedProposals);
      retractedOpportunities = opportunityManager.getOpportunities(approvedProposals);
    }
    // Update existing contacts 
    existingDCProposals.gotoBeforeFirst();
    oppsToUpdate.gotoBeforeFirst();
    while(existingDCProposals.hasNext()) {
      existingDCProposals.next();
      oppsToUpdate.next();
      
      opportunityMapper.updateFields(oppsToUpdate, existingDCProposals);
    } 
    opportunityMapper.closeCustomHandlers();
    
    existingDCProposals = proposalManagerWithAction.updateProposals(existingDCProposals);
 
    if (existingDCProposals != null && oppsToUpdate != null) {
      existingDCProposals.gotoBeforeFirst();
      oppsToUpdate.gotoBeforeFirst();
      while(existingDCProposals.hasNext() && oppsToUpdate.hasNext()) {
        existingDCProposals.next();
        oppsToUpdate.next();
        
        //oppsToCreate.updateFields(newDCProposals);
        opportunityMapper.updateFields(existingDCProposals, oppsToUpdate);
      }
    }
 
    opportunityMapper.closeCustomHandlers();
 
    opportunityManager.updateOpportunities(oppsToUpdate);
    
    // Create new proposals in DoubleClick
    newDCProposals = proposalManagerWithAction.createProposals(newDCProposals);
    
    // Update contacts with company id
    newDCProposals.gotoBeforeFirst();
    oppsToCreate.gotoBeforeFirst();
    while(newDCProposals.hasNext()) {
      newDCProposals.next();
      oppsToCreate.next();
      
      //oppsToCreate.updateFields(newDCProposals);
      opportunityMapper.updateFields(newDCProposals, oppsToCreate);
    }
 
    opportunityMapper.closeCustomHandlers();
        System.debug('test:::');
    oppsToCreate.gotoBeforeFirst();
    newDCProposals.gotoBeforeFirst();
    while(oppsToCreate.hasNext()) {
      oppsToCreate.next();
      newDCProposals.next();
 
      opportunityMapper.linkOpportunityAndProposal(oppsToCreate, newDCProposals);
    }
 
    opportunityMapper.closeCustomHandlers();
        System.debug('oppsToCreate:' + oppsToCreate);
    opportunityManager.updateOpportunities(oppsToCreate);
 
    this.executeAllDml();
 
    return retractedOpportunities;
  }

  public DC_Connector.DC_DoubleClickPageProcessingResult syncProposalsByPage(DateTime basis, long pageSize, long offset) {
    DC_GenericWrapper updatedProposals = proposalManager.getProposalsUpdatedSince(basis, pageSize, offset); 
    return syncProposalsFromDCtoSF(updatedProposals);
  }  
  
  public DC_Connector.DC_DoubleClickPageProcessingResult syncProposalsFromDCtoSF(DC_GenericWrapper updatedProposals) {
  	connectorSyncUpdate = true;
    DC_Connector.DC_DoubleClickPageProcessingResult result = new DC_Connector.DC_DoubleClickPageProcessingResult();
    System.debug('updatedProposals:' + updatedProposals);

    
    DC_GenericWrapper opportunitiesToUpdate = opportunityManager.getOpportunities(updatedProposals);
    System.debug('opportunitiesToUpdate:' + opportunitiesToUpdate);
    opportunityMapper.updateFieldsOnAllObjects(updatedProposals, opportunitiesToUpdate);
    System.debug('opportunitiesToUpdate:11 ::' + opportunitiesToUpdate);
    result.dmlStatus = opportunityManager.updateOpportunities(opportunitiesToUpdate);
    
    result.totalResultSetSize = updatedProposals.totalResultSetSize;
    
    return result; 
  } 

  //Overloaded method 
  public DC_Connector.DC_DoubleClickPageProcessingResult syncProposalsFromDCtoSF(DC_GenericWrapper updatedProposals,String actionText) {
    getproposalManager(actionText);
    connectorSyncUpdate = true;
    DC_Connector.DC_DoubleClickPageProcessingResult result = new DC_Connector.DC_DoubleClickPageProcessingResult();
    System.debug('updatedProposals:' + updatedProposals);
 
    
    DC_GenericWrapper opportunitiesToUpdate = opportunityManager.getOpportunities(updatedProposals);
    System.debug('opportunitiesToUpdate:' + opportunitiesToUpdate);
    opportunityMapper.updateFieldsOnAllObjects(updatedProposals, opportunitiesToUpdate);
    System.debug('opportunitiesToUpdate:11 ::' + opportunitiesToUpdate);
    result.dmlStatus = opportunityManager.updateOpportunities(opportunitiesToUpdate);
    
    result.totalResultSetSize = updatedProposals.totalResultSetSize;
    
    return result; 
  } 

  /*
  public long syncProposalsUpdatedSinceLastSync() {
  	integer proposalsToReturn = DC_Constants.PROPOSAL_PAGE_SIZE;
    // TO DO-Done: Jaipur We need to keep the last time this process ran in a system field, could be part of the custom setting,
    // could be a custom label, etc. Here, we should read the current value of that field and assign that to lastSyncDate. 
    // if the system field is null, skip the code that performs the synchronization, to the end where the current datetime
    // is assigned to the system field. NOTE: Each entity will need its own last sync date, so this particular one is the 
    // ProposalLastSyncDatetime
    Datetime lastSyncDate = DC_SyncDateManager.opportunityLastSyncDate;

    DC_GenericWrapper updatedProposals = proposalManager.getProposalsUpdatedSince(lastSyncDate);
    
    DC_GenericWrapper opportunitiesToUpdate = opportunityManager.getOpportunities(updatedProposals);

    opportunityMapper.updateFieldsOnAllObjects(updatedProposals, opportunitiesToUpdate);

    opportunityManager.updateOpportunities(opportunitiesToUpdate);

    // TO DO-Done: Jaipur here, before the method ends the current datetime should be assigned to the last synchronization datetime
    // field, and saved so that the next time this executes this date get's picked up as the last sync datetime.
    //cs_LastSyncDate.goog_dclk_dsm__LastSyncDateOpportunity__c=System.now();
    //DC_SalesforceDML.getInstance().registerUpdate(cs_LastSyncDate);
    
    DateTime lstdt = DC_ProposalMapper.getInstance().getLatestUpdateDate((DC_DoubleClickProposalWrapper)updatedProposals);
    System.Debug('Opty Last Sync Date: ' + lstdt);
    
    DC_SyncDateManager.opportunityLastSyncDate = lstdt;
    
    executeAllDml();
    
    System.debug(updatedProposals.size());
    return updatedProposals.totalResultSetSize;
  }
  */
  
  // take a body of wrappers, identify those that are approved
  public DC_GenericWrapper getApprovedProposals(DC_GenericWrapper wrapper) {
  	return opportunityMapper.getApprovedProposals(wrapper);
  }
    
  // find companies by matching on name of account that is provided
  // account  Account  account whose name will be used to find a matching companys
  public DC_GenericWrapper findMatchingProposals(Opportunity opp) {
    return proposalManager.getMatchingProposals(opp.goog_dclk_dsm__DC_AccountDoubleClickId__c, opp.Name);
  }
  
  // find companies by matching on name of account that is provided
  // account  Account  account whose name will be used to find a matching companys //Overloaded method 
  public DC_GenericWrapper findMatchingProposals(Opportunity opp,String ActionText) {
    getproposalManager(actionText);
    return proposalManagerWithAction.getMatchingProposals(opp.goog_dclk_dsm__DC_AccountDoubleClickId__c, opp.Name);
  }
  
   public DC_GenericWrapper findMatchingApprovedProposals(Opportunity opp) {
    return proposalManager.getMatchingApprovedProposals(opp.goog_dclk_dsm__DC_AccountDoubleClickId__c, opp.Name);
  }
  
  //Overloaded method 
  public DC_GenericWrapper findMatchingApprovedProposals(Opportunity opp,String ActionText) {
    getproposalManager(actionText);
    return proposalManagerWithAction.getMatchingApprovedProposals(opp.goog_dclk_dsm__DC_AccountDoubleClickId__c, opp.Name);
  }
  
  public Set<String> getUserIds(Map<String, Set<String>> userRoleDblClickIdMap){
  	Set<String> userDblClckIds = new Set<String>();
  	for(String role : userRoleDblClickIdMap.keySet()){
  		userDblClckIds.addAll(userRoleDblClickIdMap.get(role));
  	}
  	return userDblClckIds;
  }
 
  // link the Salesforce Oppty and the DoubleClick Proposal
  // opp  Opportunity           Salesforce Opportunity to link
  // dcProposal  DC_GenericWrapper wrapper containing DoubleClick Proposal to link against
  public void linkOpportunityAndProposal(Opportunity opp, DC_GenericWrapper dcProposal) {
    connectorSyncUpdate = true;
    DC_GenericWrapper wrappedOpportunity = opportunityMapper.wrap(opp);
    wrappedOpportunity.next();
    opportunityMapper.linkOpportunityAndProposal(wrappedOpportunity, dcProposal);

    proposalManager.updateProposals(opportunityMapper.wrap(
      (DC_ProposalService.Proposal)dcProposal.getCurrentObject()));
    opportunityManager.updateCurrent(wrappedOpportunity);
  }
	
  // link the Salesforce Oppty and the DoubleClick Proposal
  // opp  Opportunity           Salesforce Opportunity to link
  // dcProposal  DC_GenericWrapper wrapper containing DoubleClick Proposal to link against
  public void linkOpportunityAndProposal(Opportunity opp, DC_GenericWrapper dcProposal,String actionText) {
    getproposalManager(actionText);
    connectorSyncUpdate = true;
    DC_GenericWrapper wrappedOpportunity = opportunityMapper.wrap(opp);
    wrappedOpportunity.next();
    opportunityMapper.linkOpportunityAndProposal(wrappedOpportunity, dcProposal);
    proposalManagerWithAction.updateProposals(opportunityMapper.wrap(
      (DC_ProposalService.Proposal)dcProposal.getCurrentObject()));
    opportunityManager.updateCurrent(wrappedOpportunity);
  }
  
	public void submitProposalsForApproval(Opportunity currOpp){
        getproposalManager('Submit Proposal');
        DC_GenericWrapper connectorQueriedOpportunities = opportunityManager.getOpportunities(new List<Opportunity>{currOpp});
        DC_GenericWrapper proposals = proposalManagerWithAction.getProposals(connectorQueriedOpportunities);
        proposalManagerWithAction.submitProposalsForApproval(proposals);
	}
		
	public void retractProposals(Opportunity currOpp){
        getproposalManager('Retract Proposal');
        DC_GenericWrapper connectorQueriedOpportunities = opportunityManager.getOpportunities(new List<Opportunity>{currOpp});
        DC_GenericWrapper proposals = proposalManagerWithAction.getProposals(connectorQueriedOpportunities);
        proposalManagerWithAction.retractProposals(proposals);
	}
	
}