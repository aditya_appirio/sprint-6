// 
// (c) 2014 Appirio
//
// Custom field manager for working with custom field api
//
//
public class DC_CustomFieldManager extends DC_DoubleClickManager {
  
  /*private DC_ObjectMapper mapper {
    get {
      if(mapper == null) {
        mapper = DC_ObjectMapper.getInstance();
      }
      
      return mapper;
    }
    set;
  }*/ 

  private DC_CustomFieldDAO customFieldDAO {
    get {
      if(customFieldDAO == null) {
        customFieldDAO = new DC_CustomFieldDAO(this.authHandler);
      }
        
      return customFieldDAO;
    }
    set;
  }

  public DC_CustomFieldManager(DC_DoubleClickOAuthHandler authHandler){
    super(authHandler);
  }

  public Long getCustomFieldIdByName(String name) {
    DC_CustomFieldService.CustomField[] fields = customFieldDAO.getCustomFieldsByStatement('WHERE Name = \'' + name + '\'').results;

    if(fields != null && fields.size() > 0) {
      return fields[0].id;
    }

    return null;
  }

  public Long createCustomField(String name, String entityType) {
    DC_CustomFieldService.CustomField field = new DC_CustomFieldService.CustomField();

    field.name = name;
    field.entityType = entityType.toUpperCase();
    field.dataType = 'STRING';
    field.visibility = 'FULL';
    field = customFieldDAO.createCustomFields(new DC_CustomFieldService.CustomField[] {field})[0];
    return field.id;
  }

  public Long createOrGetCustomFieldId(String name, String entityType) {
    String parsedName = name;

    if(parsedName.startsWithIgnoreCase('custom.')) {
      parsedName = parsedName.substring(7);
    }

    Long result = getCustomFieldIdByName(parsedName);  	
  	
    if(result != null) {
      return result;
    }

    return createCustomField(parsedName, entityType);
  }

  public Long getProposalExternalIDFieldId() {
    goog_dclk_dsm__DC_ProposalExternalId__c proposalExtId = goog_dclk_dsm__DC_ProposalExternalId__c.getOrgDefaults();
    if(proposalExtId.goog_dclk_dsm__Id__c != null){
      return Long.valueOf(proposalExtId.goog_dclk_dsm__Id__c);
    } 
    DC_CustomFieldService.CustomFieldPage custfldPage = customFieldDAO.getCustomFieldsByStatement('WHERE Name = \''+DC_Constants.SFDC_OPPORTUNITY_ID+'\'');
    System.debug('customFieldDAO::' + customFieldDAO);
    DC_CustomFieldService.CustomField[] fields;
    if(custfldPage != null){
      fields = custfldPage.results;
    }
    if(fields != null && fields.size() > 0) {
      proposalExtId.goog_dclk_dsm__Id__c = String.valueOf(fields[0].id);
      insertCustomSetting(proposalExtId);
      return fields[0].id;
    } 
    else {
      DC_CustomFieldService.CustomField externalId = new DC_CustomFieldService.CustomField();

      externalId.name = DC_Constants.SFDC_OPPORTUNITY_ID;
      externalId.entityType = 'PROPOSAL';
      externalId.dataType = 'STRING';
      externalId.visibility = 'FULL';
      externalId = customFieldDAO.createCustomFields(new DC_CustomFieldService.CustomField[] {externalId})[0];
      System.debug('externalId::' + externalId);
      proposalExtId.goog_dclk_dsm__Id__c = String.valueOf(externalId.id);
      insertCustomSetting(proposalExtId);
      return externalId.id;
    }
  }
  private void insertCustomSetting(goog_dclk_dsm__DC_ProposalExternalId__c proposalExtId){
    DC_SalesforceDML.getInstance().registerInsert(proposalExtId);
  }
}