//
// Test_DC_PushOpportunityToDSMController : test class for DC_PushOpportunityToDSMController
// 
@isTest
private class Test_DC_PushOpportunityToDSMController {
    
    static Opportunity oppty, oppty1, oppty2, oppty3 ;
    static List<goog_dclk_dsm__DC_FieldMapping__c> fldMappingLst, companyFldMappingList;
    static goog_dclk_dsm__DC_ObjectMapping__c objMapping1, objMapping2 ;
    static Account a1;
    static goog_dclk_dsm__DC_Authentication__c objDC_Authentication;
    static Account acc3, acc4;
    
    @isTest
    static void myUnitTest() {
      createTestData();
      PageReference thePage = new PageReference('/apex/goog_dclk_dsm__DC_PushtoDSMProposalPage'); 
      Test.setCurrentPage(thePage); 
      ApexPages.StandardController sc = new ApexPages.StandardController(oppty);
      DC_PushOpportunityToDSMController controller = new DC_PushOpportunityToDSMController(sc);
      Test.startTest();
      Test.setMock(HttpCalloutMock.class, new DC_HttpCalloutMockImploAuth());
      Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplProposal());
      
      controller.getSelectedRecord(oppty.id);
      DC_GenericWrapper wrap = controller.executeConnectorGetMatchingRecords();
      System.assert(wrap != null);
      
      wrap.next();
      controller.executeConnectorLink(wrap);
     
      controller.executeConnectorSync();
      controller.getLinkMessage();
      controller.getUniquenessViolationMessage();
      controller.getContext();
      controller.checkExistingDSMRecord();
      
      a1.goog_dclk_dsm__DC_ReadyForDSM__c = false;
      update a1;
      controller.getSelectedRecord(oppty.id);
      
      sc = new ApexPages.StandardController(oppty2);
      controller = new DC_PushOpportunityToDSMController(sc);
      controller.getSelectedRecord(oppty2.id);
      system.debug('controller.selectedOpportunityRecord::' + controller.selectedOpportunityRecord);
      controller.pushAccountRecordToDSM();
      
      thePage = new PageReference('/apex/goog_dclk_dsm__DC_PushtoDSMProposalPage'); 
      Test.setCurrentPage(thePage); 
      sc = new ApexPages.StandardController(oppty2);
      controller = new DC_PushOpportunityToDSMController(sc);
      controller.getSelectedRecord(oppty2.id);
      system.debug('controller.selectedOpportunityRecord::' + controller.selectedOpportunityRecord);
      controller.pushAccountRecordToDSM();
      
      thePage = new PageReference('/apex/goog_dclk_dsm__DC_PushtoDSMProposalPage'); 
      Test.setCurrentPage(thePage); 
      sc = new ApexPages.StandardController(oppty2);
      controller = new DC_PushOpportunityToDSMController(sc);
      controller.getSelectedRecord(oppty3.id);
      system.debug('controller.selectedOpportunityRecord::' + controller.selectedOpportunityRecord);
      controller.pushAccountRecordToDSM();
      
      Test.stopTest();
    }
	
	@isTest
    static void ExceptionControllerUnitTest() {
      createTestDataWithoutAuth();
      PageReference thePage = new PageReference('/apex/goog_dclk_dsm__DC_PushtoDSMProposalPage'); 
      Test.setCurrentPage(thePage); 
      try{
      	 ApexPages.StandardController sc = new ApexPages.StandardController(oppty);
      	 DC_PushOpportunityToDSMController controller = new DC_PushOpportunityToDSMController(sc);
      	 Test.startTest();
	      Test.setMock(HttpCalloutMock.class, new DC_HttpCalloutMockImploAuth());
	      Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplProposal());
	      
	      controller.getSelectedRecord(oppty.id);
	      DC_GenericWrapper wrap = controller.executeConnectorGetMatchingRecords();
	      System.assert(wrap != null);
	      
	      wrap.next();
	      controller.executeConnectorLink(wrap);
	     
	      controller.executeConnectorSync();
	      controller.getLinkMessage();
	      controller.getUniquenessViolationMessage();
	      controller.getContext();
	      controller.checkExistingDSMRecord();
	      
	      a1.goog_dclk_dsm__DC_ReadyForDSM__c = false;
	      update a1;
	      controller.getSelectedRecord(oppty.id);
	      
	      sc = new ApexPages.StandardController(oppty2);
	      controller = new DC_PushOpportunityToDSMController(sc);
	      controller.getSelectedRecord(oppty2.id);
	      system.debug('controller.selectedOpportunityRecord::' + controller.selectedOpportunityRecord);
	      controller.pushAccountRecordToDSM();
	      
	      thePage = new PageReference('/apex/goog_dclk_dsm__DC_PushtoDSMProposalPage'); 
	      Test.setCurrentPage(thePage); 
	      sc = new ApexPages.StandardController(oppty2);
	      controller = new DC_PushOpportunityToDSMController(sc);
	      controller.getSelectedRecord(oppty2.id);
	      system.debug('controller.selectedOpportunityRecord::' + controller.selectedOpportunityRecord);
	      controller.pushAccountRecordToDSM();
	      
	      thePage = new PageReference('/apex/goog_dclk_dsm__DC_PushtoDSMProposalPage'); 
	      Test.setCurrentPage(thePage); 
	      sc = new ApexPages.StandardController(oppty2);
	      controller = new DC_PushOpportunityToDSMController(sc);
	      controller.getSelectedRecord(oppty3.id);
	      system.debug('controller.selectedOpportunityRecord::' + controller.selectedOpportunityRecord);
	      controller.pushAccountRecordToDSM();
	      
	      Test.stopTest();
      	 }
      	 catch(Exception ex){system.debug(ex);}
      
    }
    
    @isTest
    static void myUnitTest1() {
        
      createTestData();
      PageReference thePage = new PageReference('/apex/goog_dclk_dsm__DC_PushtoDSMProposalPage'); 
      Test.setCurrentPage(thePage); 
      ApexPages.StandardController sc = new ApexPages.StandardController(oppty);
      DC_PushOpportunityToDSMController controller = new DC_PushOpportunityToDSMController(sc);
      Test.startTest();
      Test.setMock(HttpCalloutMock.class, new DC_HttpCalloutMockImploAuth());
      Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplProposal());
      
      controller.getSelectedRecord(oppty.id);
      DC_GenericWrapper wrap = controller.executeConnectorGetMatchingRecords();
      System.assert(wrap != null);
      
      wrap.next();
      controller.executeConnectorSyncExisting();
      controller.getLinkMessage();
      controller.getUniquenessViolationMessage();
        
      DC_ProposalMapper proposalMapper = DC_MapperFactory.getInstance().getOpportunityMapper();
          
      DC_ProposalService.Proposal proposal = new DC_ProposalService.Proposal();
      proposal.name = 'testProposal';
      proposal.status = 'open';
      
      proposalMapper.wrap(proposal);
       
      DC_ProposalService.ProposalPage proposalPage = new DC_ProposalService.ProposalPage();
      proposalPage.results = new List<DC_ProposalService.Proposal>{proposal};     
      proposalPage.totalResultSetSize = 1;
      proposalPage.startIndex = 0;
          
      proposalMapper.wrap(proposalPage);
      proposalMapper.updateField(wrap, fldMappingLst[2], 1234);
      DC_DoubleClickProposalWrapper pwrap = new DC_DoubleClickProposalWrapper(objMapping1, fldMappingLst, new List<DC_ProposalService.Proposal>{proposal});
      try{
          proposalMapper.getLatestUpdateDate(pwrap);
      }catch(exception e){}
      proposalMapper.getApprovedProposals(wrap);

      Test.stopTest();
    }
    
    @isTest
    static void myUnitTest2() {
      createTestData();
      PageReference thePage = new PageReference('/apex/goog_dclk_dsm__DC_PushtoDSMProposalPage'); 
      Test.setCurrentPage(thePage); 
      ApexPages.StandardController sc = new ApexPages.StandardController(oppty);
      DC_PushOpportunityToDSMController controller = new DC_PushOpportunityToDSMController(sc);
      
      DC_DoubleClickOAuthHandler oAuthHandler = new DC_DoubleClickOAuthHandler(Userinfo.getUserId());
      test.startTest();
      Test.setMock(HttpCalloutMock.class, new DC_HttpCalloutMockImploAuth());
      Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplProposal());
      controller.getSelectedRecord(oppty.id);
      DC_GenericWrapper wrap = controller.executeConnectorGetMatchingRecords();
      DC_DoubleClickProposalManager pManager = new DC_DoubleClickProposalManager(oAuthHandler);
      try{
          pManager.getProposalsUpdatedSince(System.now());
      }catch(exception e){}
      
      DC_ProposalMapper opportunityMapper = DC_MapperFactory.getInstance().getOpportunityMapper(); 
      DC_ProposalService.Proposal proposal1 = new DC_ProposalService.Proposal();
      proposal1.name = 'testProposal1';
      proposal1.id = 12345;           
      pManager.getProposals(opportunityMapper.wrap(proposal1));
          
     // pManager.getProposal(123456);
      pManager.getMatchingApprovedProposals('123456', 'test');
      pManager.retractProposals(wrap);
      pManager.submitProposalsForApproval(wrap);
      test.stopTest();        
    }
    
  @isTest
  static void myUnitTest3() {    
    createTestData();   
    PageReference thePage = new PageReference('/apex/goog_dclk_dsm__DC_PushtoDSMProposalPage'); 
    Test.setCurrentPage(thePage); 
    DC_ProposalService.Proposal proposal = new DC_ProposalService.Proposal();
    proposal.name = 'testProposal1';
    proposal.status = 'open';
    proposal.id = 12345;
        
    DC_TestUtils.createOpportunityTeamMember(oppty3.id, 'Primary Trafficker', Userinfo.getUserId(), true);
        
        
    goog_dclk_dsm__DC_User__c dcUser = DC_TestUtils.createDCUser();
    
    DC_TestUtils.createDoubleClickTeamMember(oppty3.id, 'Primary Sales Rep', dcUser.id, true);
        
    ApexPages.StandardController sc = new ApexPages.StandardController(oppty1);
    DC_PushOpportunityToDSMController controller = new DC_PushOpportunityToDSMController(sc);
    
    DC_CompanyService.Company company = new DC_CompanyService.Company();         
    company.id = 123456;
    company.name = 'test4';
        
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new DC_HttpCalloutMockImploAuth());
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplCompany());
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplProposal());
    DC_CompanyMapper companyMapper = DC_MapperFactory.getInstance().getAccountMapper(); 

    controller.getSelectedRecord(oppty1.id);
    
    thePage = new PageReference('/apex/goog_dclk_dsm__DC_PushtoDSMProposalPage'); 
    Test.setCurrentPage(thePage); 
    sc = new ApexPages.StandardController(oppty3);
    controller = new DC_PushOpportunityToDSMController(sc);
    controller.getSelectedRecord(oppty3.id);
    controller.acknowledgeCompanyCreateAndContinue();
    
    controller.companyResultWrapper = companyMapper.wrap(company);
    System.debug('companyResultWrapper::test:' + controller.companyResultWrapper);
    controller.currentCreationContext = 'Company';
    try{
        controller.createLink(0);
    }catch(Exception e){}
    DC_GenericWrapper gwObj;
    try{
        gwObj = controller.executeConnectorGetMatchingRecords();
    }catch(Exception e){}
    try{
        gwObj = controller.executeConnectorGetMatchingApprovedRecords();
    }catch(Exception e){}
    try{
        controller.executeConnectorSyncExisting();
    }catch(Exception e){}
    String str = controller.getLinkMessage();
    str = controller.getUniquenessViolationMessage();
    controller.meetsOpportunityRequirements();
    
    Test.stopTest();
    
  }
   
   
  @isTest
  static  void myUnitTest4() {    
    createTestData();   
    
    DC_ProposalService.Proposal proposal = new DC_ProposalService.Proposal();
    proposal.name = 'testProposal1';
    proposal.status = 'open';
    proposal.id = 12345;
    
    acc3.goog_dclk_dsm__DC_DoubleClickId__c = '123456';
    update acc3;
    DC_TestUtils.createOpportunityTeamMember(oppty3.id, 'Primary Trafficker', Userinfo.getUserId(), true);
        
        
    goog_dclk_dsm__DC_User__c dcUser = DC_TestUtils.createDCUser();
    
    DC_TestUtils.createDoubleClickTeamMember(oppty3.id, 'Primary Sales Rep', dcUser.id, true);
    
    PageReference thePage = new PageReference('/apex/goog_dclk_dsm__DC_PushtoDSMProposalPage'); 
    Test.setCurrentPage(thePage); 
    
    ApexPages.StandardController sc = new ApexPages.StandardController(oppty3);
    DC_PushOpportunityToDSMController controller = new DC_PushOpportunityToDSMController(sc);
    
    DC_CompanyService.Company company = new DC_CompanyService.Company();         
    company.id = 123456;
    company.name = 'test4';
        
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new DC_HttpCalloutMockImploAuth());
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplCompany());
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplProposal());
    controller.getSelectedRecord(oppty3.id);
    controller.checkExistingDSMRecord();
    Test.stopTest();
    
  }
    
  static void createTestData(){
    a1 = new Account(name = 'test1');
    a1.goog_dclk_dsm__DC_ReadyForDSM__c = true;
    
  	objDC_Authentication = new goog_dclk_dsm__DC_Authentication__c(goog_dclk_dsm__DC_AccessToken__c ='1324324',goog_dclk_dsm__DC_ExpiresIn__c = 3600 ,goog_dclk_dsm__DC_RefreshToken__c = '454655656',goog_dclk_dsm__DC_TokenType__c = 'Bearer');
    insert objDC_Authentication;
    
    Account a2 = new Account(name = 'test2');
    a2.goog_dclk_dsm__DC_DoubleClickId__c = '12345';
    
    acc3 = new Account(name = 'test2');
    
    acc4 = new Account(name = 'testCompany');
    acc4.goog_dclk_dsm__DC_ReadyForDSM__c = true;
    insert new List<Account>{a1, a2, acc3, acc4};
    
    oppty = new Opportunity(name = 'testProposal1', AccountId = a1.id, StageName = 'Closed/Won', CloseDate = date.today());
    oppty.goog_dclk_dsm__DC_ProposalStatus__c = 'APPROVED';
    oppty1 = new Opportunity(name = 'testProposal1', AccountId = a2.id, StageName = 'Closed/Won', CloseDate = date.today());
    oppty1.goog_dclk_dsm__DC_ReadyForDSM__c = true;
    oppty2 = new Opportunity(name = 'testProposal1', AccountId = acc3.id, StageName = 'Closed/Won', CloseDate = date.today());
    oppty3 = new Opportunity(name = 'testProposal1', AccountId = acc4.id, StageName = 'Closed/Won', CloseDate = date.today());
    insert new List<Opportunity>{oppty, oppty1, oppty2, oppty3};
        
    objMapping1 = new goog_dclk_dsm__DC_ObjectMapping__c(goog_dclk_dsm__DC_DoubleClickObjectName__c='Proposal', goog_dclk_dsm__DC_SalesforceObjectName__c = 'Opportunity', name = 'opportunity');
    objMapping2 = new goog_dclk_dsm__DC_ObjectMapping__c(goog_dclk_dsm__DC_DoubleClickObjectName__c='Company', goog_dclk_dsm__DC_SalesforceObjectName__c = 'Account', name = 'account');
    insert new List<goog_dclk_dsm__DC_ObjectMapping__c>{objMapping1, objMapping2};
    
    goog_dclk_dsm__DC_FieldMapping__c fldMapping1 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping2.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping2 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping2.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'externalId', goog_dclk_dsm__DC_SalesforceFieldName__c = 'Id', Name = 'sfid');
    
    goog_dclk_dsm__DC_FieldMapping__c fldMapping3 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping4 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'custom.SFDC_Opportunity_ID', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Id', Name = 'sfid');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping5 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'probabilityOfClose', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Probability', Name = 'probability');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping6 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'status', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345,  Name = 'status');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping7 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id,   goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345,  Name = 'Team Members', goog_dclk_dsm__DC_HandlerClass__c = 'DC_CustomProposalTeamMemberHandler');
    
    insert new List<goog_dclk_dsm__DC_FieldMapping__c>{fldMapping1, fldMapping2, fldMapping3, fldMapping4, fldMapping5, fldMapping6, fldMapping7};
    fldMappingLst = new List<goog_dclk_dsm__DC_FieldMapping__c>();
    fldMappingLst.add(fldMapping3);
    fldMappingLst.add(fldMapping4);
    fldMappingLst.add(fldMapping5);
    fldMappingLst.add(fldMapping6);
    fldMappingLst.add(fldMapping7);
    companyFldMappingList = new List<goog_dclk_dsm__DC_FieldMapping__c>();
    companyFldMappingList.add(fldMapping1 );
    companyFldMappingList.add(fldMapping2);
  }
  
  static void createTestDataWithoutAuth(){
    a1 = new Account(name = 'test1');
    a1.goog_dclk_dsm__DC_ReadyForDSM__c = true;
    
    Account a2 = new Account(name = 'test2');
    a2.goog_dclk_dsm__DC_DoubleClickId__c = '12345';
    
    acc3 = new Account(name = 'test2');
    
    acc4 = new Account(name = 'testCompany');
    acc4.goog_dclk_dsm__DC_ReadyForDSM__c = true;
    insert new List<Account>{a1, a2, acc3, acc4};
    
    oppty = new Opportunity(name = 'testProposal1', AccountId = a1.id, StageName = 'Closed/Won', CloseDate = date.today());
    oppty.goog_dclk_dsm__DC_ProposalStatus__c = 'APPROVED';
    oppty1 = new Opportunity(name = 'testProposal1', AccountId = a2.id, StageName = 'Closed/Won', CloseDate = date.today());
    oppty1.goog_dclk_dsm__DC_ReadyForDSM__c = true;
    oppty2 = new Opportunity(name = 'testProposal1', AccountId = acc3.id, StageName = 'Closed/Won', CloseDate = date.today());
    oppty3 = new Opportunity(name = 'testProposal1', AccountId = acc4.id, StageName = 'Closed/Won', CloseDate = date.today());
    insert new List<Opportunity>{oppty, oppty1, oppty2, oppty3};
        
    objMapping1 = new goog_dclk_dsm__DC_ObjectMapping__c(goog_dclk_dsm__DC_DoubleClickObjectName__c='Proposal', goog_dclk_dsm__DC_SalesforceObjectName__c = 'Opportunity', name = 'opportunity');
    objMapping2 = new goog_dclk_dsm__DC_ObjectMapping__c(goog_dclk_dsm__DC_DoubleClickObjectName__c='Company', goog_dclk_dsm__DC_SalesforceObjectName__c = 'Account', name = 'account');
    insert new List<goog_dclk_dsm__DC_ObjectMapping__c>{objMapping1, objMapping2};
    
    goog_dclk_dsm__DC_FieldMapping__c fldMapping1 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping2.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping2 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping2.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'externalId', goog_dclk_dsm__DC_SalesforceFieldName__c = 'Id', Name = 'sfid');
    
    goog_dclk_dsm__DC_FieldMapping__c fldMapping3 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping4 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'custom.SFDC_Opportunity_ID', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Id', Name = 'sfid');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping5 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'probabilityToClose', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Probability', Name = 'probability');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping6 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'status', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345,  Name = 'status');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping7 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id,   goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345,  Name = 'Team Members', goog_dclk_dsm__DC_HandlerClass__c = 'DC_CustomProposalTeamMemberHandler');
    
    insert new List<goog_dclk_dsm__DC_FieldMapping__c>{fldMapping1, fldMapping2, fldMapping3, fldMapping4, fldMapping5, fldMapping6, fldMapping7};
    fldMappingLst = new List<goog_dclk_dsm__DC_FieldMapping__c>();
    fldMappingLst.add(fldMapping3);
    fldMappingLst.add(fldMapping4);
    fldMappingLst.add(fldMapping5);
    fldMappingLst.add(fldMapping6);
    fldMappingLst.add(fldMapping7);
    companyFldMappingList = new List<goog_dclk_dsm__DC_FieldMapping__c>();
    companyFldMappingList.add(fldMapping1 );
    companyFldMappingList.add(fldMapping2);
  }
}