public with sharing class DC_SalesforceWrapper extends DC_GenericWrapper {
	// BR: added field displaytype caching to avoid governor limit on describes
  private static Map<String, Schema.DisplayType> displayMapByFieldName = new Map<String, Schema.DisplayType>();
  
  public override Boolean isSalesforce() {
    return true;
  }

  public override Boolean isDoubleClick() {
    return false;
  }

  public DC_SalesforceWrapper(goog_dclk_dsm__DC_ObjectMapping__c objectMapping, List<goog_dclk_dsm__DC_FieldMapping__c> fieldMappings, List<Object> objects) {
    super(objectMapping, fieldMappings, objects);
  }
  
  private String getSalesforceFieldName(String fieldName) {
  	System.debug('fieldNameMap::' + fieldNameMap);
  	System.debug('fieldName::' + fieldName);
    goog_dclk_dsm__DC_FieldMapping__c mapping = this.fieldNameMap.get(fieldName.toLowerCase()); 
    if(mapping != null) {
    	System.debug('mapping.goog_dclk_dsm__DC_SalesforceFieldName__c::' + mapping.goog_dclk_dsm__DC_SalesforceFieldName__c);
      return mapping.goog_dclk_dsm__DC_SalesforceFieldName__c;
    } else {
      throw new DC_ConnectorException(DC_Constants.MAPPING_STR + fieldName + DC_Constants.NOT_FOUND_STR);
    }
  }

  public override Object getField(String field) {
  	System.debug('this.currentObject::' + this.currentObject);
    return ((SObject)this.currentObject).get(getSalesforceFieldName(field));
  }
  
  public override void setField(String field, Object value) {

    Schema.DisplayType fieldType = getSalesforceFieldType(field);
    System.debug('@@ Setting field ' + field + ' (type ' + fieldType.name() + ') to value ' + value);
    System.debug('Before Set Object Data: ' + this.currentObject);
        
    if (getSalesforceFieldName(field).equalsIgnoreCase('pricebookentryid') && ((sObject)this.currentObject).Id != null) {
    	return;
    }      
    
    if(fieldType == Schema.DisplayType.String){
    	((SObject)this.currentObject).put(getSalesforceFieldName(field), this.toString(value));
    }
    else if(fieldType == Schema.DisplayType.ID){
      ((SObject)this.currentObject).put(getSalesforceFieldName(field), this.toString(value));
    }
    else if(fieldType == Schema.DisplayType.Integer){
      ((SObject)this.currentObject).put(getSalesforceFieldName(field), this.toInteger(value));
    }
    else if(fieldType == Schema.DisplayType.Currency){ 
      ((SObject)this.currentObject).put(getSalesforceFieldName(field), this.toDecimal(value));
    }       
    else if(fieldType == Schema.DisplayType.Double){ 
      ((SObject)this.currentObject).put(getSalesforceFieldName(field), this.toDecimal(value));
    }    
    else if(fieldType == Schema.DisplayType.Boolean){
      ((SObject)this.currentObject).put(getSalesforceFieldName(field), this.toBoolean(value));
    }
    else if(fieldType == Schema.DisplayType.Email){
      ((SObject)this.currentObject).put(getSalesforceFieldName(field), this.toString(value));
    }
    else if(fieldType == Schema.DisplayType.Date){
      ((SObject)this.currentObject).put(getSalesforceFieldName(field), ((DC_ServiceDate)value).getSalesforceDate());
    }
    else if(fieldType == Schema.DisplayType.DateTime){
      ((SObject)this.currentObject).put(getSalesforceFieldName(field), ((DC_ServiceDateTime)value).getSalesforceDatetime());
    }
    else if(fieldType == Schema.DisplayType.Reference && getSalesforceFieldName(field).equalsIgnoreCase('OpportunityId') 
            && value instanceof long && ((sObject)this.currentObject).getSObjectType() == OpportunityLineItem.sObjectType) {
      System.debug('Adding Opportunity to Line Item...');
    	Opportunity o = new Opportunity(goog_dclk_dsm__DC_DoubleClickId__c = String.valueOf((long)value));
    	OpportunityLineItem li = (OpportunityLineItem)this.currentObject;
    	li.Opportunity = o;
    }
    else {
      ((SObject)this.currentObject).put(getSalesforceFieldName(field), value);
    }
    System.debug('After Set Object Data: ' + this.currentObject);
  }
  
   
  // Return Salesforce field type of the field exist in object from objectMapping
  private Schema.DisplayType getSalesforceFieldType(String fieldName){
  	// Added by BR: fieldName is the unmapped system agnostic name -- converting
  	String sfFieldName = '';
    try {
    	if (!displayMapByFieldName.containsKey(fieldName)) {
        sfFieldName = getSalesforceFieldName(fieldName);
        System.debug('sfFieldName::' + sfFieldName + '--objectMapping.goog_dclk_dsm__DC_SalesforceObjectName__c--'+ objectMapping.goog_dclk_dsm__DC_SalesforceObjectName__c);
        Schema.DescribeSObjectResult r = Schema.getGlobalDescribe().get(objectMapping.goog_dclk_dsm__DC_SalesforceObjectName__c).getDescribe();
        Schema.sObjectField field = r.fields.getMap().get(sfFieldName);
        System.debug('field::' + field);
        System.debug('field.getDescribe()::' + field.getDescribe());
        Schema.DisplayType fieldType = field.getDescribe().getType();
        
        displayMapByFieldName.put(fieldName, fieldType);
    	} 
      return displayMapByFieldName.get(fieldName);
    } catch (Exception e) {
    	// catching to provide additional info, but rethrowing so as to handle higher up in stack
    	System.debug(String.format('Exception Field Name: {0}, SF Equivelent: {1}', new String[] { fieldName, sfFieldName } ));
    	e.setMessage(e.getMessage() + '; Field Name: ' + fieldName + '; SF Field Name: ' + sfFieldName);
    	throw e;
    }
  }

  public override DC_GenericWrapper getNewRecords() {
    this.gotoBeforeFirst();
    List<Object> result = new List<Object>();
    
    while(this.hasNext()) {
      this.next();
      if(this.getField('DoubleClickId') == null) {
        result.add(this.currentObject);
      }
    }
    
    return new DC_SalesforceWrapper(this.objectMapping, this.fieldMappings, result);
  }
  
  public override DC_GenericWrapper getExistingRecords() {
    this.gotoBeforeFirst();
    List<Object> result = new List<Object>();
    
    while(this.hasNext()) {
      this.next();
      
      if(this.getField('DoubleClickId') != null) {
        result.add(this.currentObject);
      }
    }
    
    return new DC_SalesforceWrapper(this.objectMapping, this.fieldMappings, result);
  }
  
  public override DC_GenericWrapper getCurrentWrapped() {
    return new DC_SalesforceWrapper(this.objectMapping, this.fieldMappings, new object[] { this.currentObject });
  }  
  
  public override DC_GenericWrapper getUnmatchedRecords(String fieldName) {
    return null;
  }
    
  public override DC_GenericWrapper getMatchedRecords(String fieldName) {
    return null;
  }
  
}