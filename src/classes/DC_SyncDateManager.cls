//
//  Manages synchronization date tracking
//
//  Brandon Raines
//
public with sharing class DC_SyncDateManager {
	private static DC_SalesforceDML.DC_SalesforceDMLStatus dmlStatus;
	// prevent multiple registrations for dml -- once registered,
	// additional updates to the object will occur once executed 
	private static boolean outstandingDML {
		get {
			// dml status exists and hasn't been executed
			return dmlStatus != null && !dmlStatus.getExecuted();
		}
	}
	
	// the sync custom setting.  This is an org level custom setting.
	private static goog_dclk_dsm__DC_LastSync__c syncSetting {
		get {
			// if the sync setting is null or the sync setting id is null, create
			// note that getOrgDefaults will never return null 
      if (syncSetting == null && (syncSetting = goog_dclk_dsm__DC_LastSync__c.getOrgDefaults()).Id == null) {
        insertCustomSetting();
      }
       
			return syncSetting;
		}
		set;		
	}

  // set/get the opportunity last sync date from custom setting
  public static Long deliveryDataReportJobId {
    get {
    	System.debug('syncSetting::' + syncSetting);
      if (syncSetting.goog_dclk_dsm__DC_DeliveryDataReportJobId__c == null) {
        return null;
      }
        
      return syncSetting.goog_dclk_dsm__DC_DeliveryDataReportJobId__c.longValue();
    }
    set {
      syncSetting.goog_dclk_dsm__DC_DeliveryDataReportJobId__c = value;
      
      updateCustomSetting();  
    }
  }
	
  // set/get the opportunity last sync date from custom setting
  public static DateTime deliveryDataLastSyncDate {
    get {
      if (syncSetting.goog_dclk_dsm__DC_LastSyncDateDeliveryData__c == null) {
        return DateTime.now();      
      }
        
      return syncSetting.goog_dclk_dsm__DC_LastSyncDateDeliveryData__c;
    }
    set {
      syncSetting.goog_dclk_dsm__DC_LastSyncDateDeliveryData__c = value;
      
      updateCustomSetting();  
    }
  }

	// set/get the opportunity last sync date from custom setting
  public static DateTime opportunityLastSyncDate {
  	get {
  		if (syncSetting.goog_dclk_dsm__DC_lastSyncDateOpportunity__c == null) {
  			return DateTime.now();			
  		}
  		  
      return syncSetting.goog_dclk_dsm__DC_lastSyncDateOpportunity__c;
  	}
  	set {
  		syncSetting.goog_dclk_dsm__DC_lastSyncDateOpportunity__c = value;
  		
	    updateCustomSetting();	
  	}
  }
  
  // set the sync date to now
  public static void opportunitiesSynchronizedNow() {
  	opportunityLastSyncDate = DateTime.now();
  	updateCustomSetting();
  }
  
  // set/get the opportunity line item last sync date from custom setting
  public static DateTime opportunityLineitemLastSyncDate {
  	get {
  		if (syncSetting.goog_dclk_dsm__DC_LastSyncDateOpportunityLineItem__c == null) {
  			return DateTime.now();			
  		}
  		  
      return syncSetting.goog_dclk_dsm__DC_LastSyncDateOpportunityLineItem__c;
  	}
  	set {
  		syncSetting.goog_dclk_dsm__DC_LastSyncDateOpportunityLineItem__c = value;
  		
	    updateCustomSetting();	
  	}
  }
  
  // set the sync date to now
  public static void opportunityLineItemsSynchronizedNow() {
  	opportunityLineitemLastSyncDate = DateTime.now();
  	updateCustomSetting();
  }
  
  // set/get the team last sync date from custom setting
  public static DateTime teamLastSyncDate {
  	get {
  		if (syncSetting.goog_dclk_dsm__DC_LastSyncDateTeam__c == null) {
  			return DateTime.now();			
  		}
  		  
      return syncSetting.goog_dclk_dsm__DC_LastSyncDateTeam__c;
  	}
  	set {
  		syncSetting.goog_dclk_dsm__DC_LastSyncDateTeam__c = value;
  		
	    updateCustomSetting();	
  	}
  }
  
  // set the sync date to now
  public static void teamsSynchronizedNow() {
  	teamLastSyncDate = DateTime.now();
  	updateCustomSetting();
  }

  // prepare the custom setting for insert
  private static void insertCustomSetting() {
	  dmlStatus = DC_SalesforceDML.getInstance().registerInsert(syncSetting);
  }
  
  // prepare the custom setting for update
  private static void updateCustomSetting() {
	  if (!outstandingDML) {
	    dmlStatus = DC_SalesforceDML.getInstance().registerUpdate(syncSetting);
	  }	     
  }
}