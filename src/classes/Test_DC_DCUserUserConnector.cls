// 
// (c) 2014 Appirio
//
// Test_DC_DCUserUserConnector: test class for DC_DCUserUserConnector 
//
@isTest
private class Test_DC_DCUserUserConnector {
  
  static testMethod void testDCUserUserConnector() {
  	goog_dclk_dsm__DC_User__c dcUser = DC_TestUtils.createDCUser();
  	goog_dclk_dsm__DC_ObjectMapping__c dcObjMapping1 = new goog_dclk_dsm__DC_ObjectMapping__c();
  	dcObjMapping1.goog_dclk_dsm__DC_DoubleClickObjectName__c = 'User';
  	dcObjMapping1.Name = 'User';
  	dcObjMapping1.goog_dclk_dsm__DC_SalesforceObjectName__c = 'goog_dclk_dsm__DC_User__c';
  	
  	goog_dclk_dsm__DC_ObjectMapping__c dcObjMapping2 = new goog_dclk_dsm__DC_ObjectMapping__c();
  	dcObjMapping2.goog_dclk_dsm__DC_DoubleClickObjectName__c = 'User';
  	dcObjMapping2.Name = 'User Batch';
  	dcObjMapping2.goog_dclk_dsm__DC_SalesforceObjectName__c = 'User';
    insert new List<goog_dclk_dsm__DC_ObjectMapping__c>{dcObjMapping1, dcObjMapping2};
  	
  	List<goog_dclk_dsm__DC_FieldMapping__c> fldMappingList = new List<goog_dclk_dsm__DC_FieldMapping__c>();
  	fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'name', goog_dclk_dsm__DC_SalesforceFieldName__c = 'name', Name = 'name'));
    
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'email', goog_dclk_dsm__DC_SalesforceFieldName__c = 'email', Name = 'email'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'roleId', goog_dclk_dsm__DC_SalesforceFieldName__c = 'UserRoleId', Name = 'roleId'));
    
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'roleName', goog_dclk_dsm__DC_SalesforceFieldName__c = 'UserRole.Name', Name = 'roleName'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'preferredLocale', goog_dclk_dsm__DC_SalesforceFieldName__c = 'LocaleSidKey', Name = 'preferredLocale'));
    
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'UserRecord_Type', goog_dclk_dsm__DC_SalesforceFieldName__c = 'Username', Name = 'UserRecord_Type'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'lastModifiedDateTime', goog_dclk_dsm__DC_SalesforceFieldName__c = 'LastModifiedDate', Name = 'lastModifiedDateTime'));
    
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'ordersUiLocalTimeZoneId', goog_dclk_dsm__DC_SalesforceFieldName__c = 'TimeZoneSidKey', Name = 'ordersUiLocalTimeZoneId'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'isEmailNotificationAllowed', goog_dclk_dsm__DC_SalesforceFieldName__c = 'ReceivesAdminInfoEmails', Name = 'isEmailNotificationAllowed'));
    
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'custom.SFDC_User_ID', goog_dclk_dsm__DC_SalesforceFieldName__c = 'id', Name = 'sfid', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345));
    
    //fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping2.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = goog_dclk_dsm__'DC_DoubleClickId__c', Name = 'DoubleClickId'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping2.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'name', goog_dclk_dsm__DC_SalesforceFieldName__c = 'name', Name = 'name'));
    
  	insert fldMappingList;
  	
  	DC_UserService.User_x userX = new DC_UserService.User_x();    
	  userX.id = 12345;
	  userX.name = 'testUser';
  	User currUser = [Select id from User where id =:Userinfo.getUserId()];
  	test.startTest();
  	
  	Test.setMock(HttpCalloutMock.class, new DC_HttpCalloutMockImploAuth());
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplUserService());
    
  	DC_DCUserUserConnector dcCon = DC_DCUserUserConnector.getInstance();
  	DC_DoubleClickUserWrapper dcuserWrap = new DC_DoubleClickUserWrapper(dcObjMapping1,fldMappingList,new list<goog_dclk_dsm__DC_User__c>{dcUser});
  	DC_GenericWrapper matchingUserWrapperObj = dcCon.findMatchingUsers(dcUser);
  	DC_UserMapper userMapper = DC_MapperFactory.getInstance().getUserMapper();
  	DC_GenericWrapper dcGen =  userMapper.wrap(userX);
  	dcGen.next();
  	DC_Connector.DC_DoubleClickPageProcessingResult processingRes = dcCon.syncUsersByPage(DateTime.now(),5L,5L);
		//dcCon.syncSFUsersToDCUsers(new List<User>{currUser});
		dcCon.pushSalesforceUsersToNewDCUsers(new List<User>{currUser});
		dcCon.registerCurrentUserWithAllEntitiesTeam();
		
	  dcuserWrap = new DC_DoubleClickUserWrapper(dcObjMapping1,fldMappingList,new list<DC_UserService.User_x>{userX});
		dcuserWrap.next();
		//System.assertEquals(12345, dcuserWrap.getField('DoubleClickId'));
		System.assertEquals('testUser', dcuserWrap.getField('name'));
		System.assertEquals(null, dcuserWrap.getField('email'));
		dcuserWrap.getField('roleId');
		System.assertEquals(null, dcuserWrap.getField('roleName'));
		dcuserWrap.getField('preferredLocale');
		dcuserWrap.getField('UserRecord_Type');
		//dcuserWrap.getField('lastModifiedDateTime');
		dcuserWrap.getField('ordersUiLocalTimeZoneId');
		dcuserWrap.getField('isEmailNotificationAllowed');
		System.assertEquals(null, dcuserWrap.getField('sfid'));
		
		//dcuserWrap.setField('DoubleClickId', '12345');
		dcuserWrap.setField('email', 'a@a.com');
		dcuserWrap.setField('name', 'test');
		dcuserWrap.setField('roleId', '000000000000000');
		dcuserWrap.setField('roleName', 'Sales Planner');
		dcuserWrap.setField('preferredLocale', 'IST');
		dcuserWrap.setField('UserRecord_Type', 'Test');
		//dcuserWrap.setField('lastModifiedDateTime', System.now());
		dcuserWrap.setField('ordersUiLocalTimeZoneId', '000000000000000');
		dcuserWrap.setField('isEmailNotificationAllowed', true);
		dcuserWrap.setField('sfid', dcUser.id);
		
		DC_GenericWrapper newRecWrap = dcuserWrap.getNewRecords();
		DC_GenericWrapper existingRecWrap = dcuserWrap.getExistingRecords();
		DC_GenericWrapper currentRecWrap = dcuserWrap.getCurrentWrapped();
		dcuserWrap.createWrapper(dcObjMapping1,fldMappingList,new list<DC_UserService.User_x>{userX});
  	test.stopTest(); 
  	
  	System.assertEquals(matchingUserWrapperObj.size(), 2);
  	System.assertEquals(processingRes.totalResultSetSize, 2);
  	System.assertEquals(existingRecWrap.size(), 1);
  	System.assertEquals(currentRecWrap.size(), 1);
  	
  }
}