// 
// (c) 2014 Appirio
//
// DC_DoubleClickProposalManager
// T-245174 : Create Push to DSM functionality for Opportunities
// 
// 04 Feb 2014    Ankit Goyal (JDC)      Original
//
public with sharing class DC_SalesforceOpportunityManager extends DC_SalesforceManager {
  
  private DC_ProposalMapper mapper {
    get {
      if(mapper == null) {
        mapper = DC_ProposalMapper.getInstance();
      }
      return mapper;
    }
    set;
  }
  
  public DC_GenericWrapper getOpportunity(String id) {
     String fieldNames = this.mapper.getCommaSeparatedSalesforceFieldList('Opportunity');
     
     String query = 'SELECT ' + fieldNames + ' FROM Opportunity where id = \'' + id + '\'';
     
     return mapper.wrap((List<Opportunity>)Database.query(query));
  }
  
  //update Contact
  public DC_SalesforceDML.DC_SalesforceDMLStatus updateOpportunities(DC_GenericWrapper opps) {
    List<Opportunity> oppList = new List<Opportunity>();
    DC_SalesforceDML.DC_SalesforceDMLStatus status;
    
    for(Object o : opps.getObjects()) {
      oppList.add((Opportunity)o);
    }
    
    if(oppList.size() > 0) {
      status = DC_SalesforceDML.getInstance().registerUpdate(oppList);
    }
    
    return status;
  }

  public DC_GenericWrapper getOpportunities(List<Opportunity> opportunities) {
    if(opportunities == null || opportunities.size() == 0) {
      return null;
    }

    List<Id> ids = new List<Id>();

    for(Opportunity opp : opportunities) {
      ids.add(opp.Id);
    }

    return getOpportunities(ids);
  }

  public DC_GenericWrapper getOpportunities(List<Id> ids) {
    String fieldNames = this.mapper.getCommaSeparatedSalesforceFieldList('Opportunity');
/*
    String inStatement = '(';
    String separator = '';

    for(Id id : ids) {
      inStatement += separator + '\'' + id + '\'';
      separator = ',';
    }

    inStatement += ')';
*/

    System.Debug('Opportunity Ids: ' + ids +'fieldNames::' + fieldNames);
    String query = 'SELECT ' + fieldNames + ' FROM Opportunity where id in :ids';

    return mapper.wrap((List<Opportunity>)Database.query(query));
  }

  public DC_GenericWrapper getOpportunities(DC_GenericWrapper wrapper) {
    wrapper.gotoBeforeFirst();
    List<Id> opportunityIds = new List<Id>();
		System.debug('wrapper:::' + wrapper);
    while(wrapper.hasNext()) {
      wrapper.next();

      opportunityIds.add((String)wrapper.getField('sfid'));
    }

    return getOpportunities(opportunityIds);
  }
  
	
}