// 
// (c) 2014 Appirio
//
// DC_AccountCompanyConnector
//
// 12 Feb 2014    Ankit Goyal (JDC)      Original
//
public with sharing class DC_AccountCompanyConnector extends DC_Connector {
  public static boolean connectorSyncUpdate = false;

  private static DC_AccountCompanyConnector instance; 
  private DC_CompanyMapper companyMapper;
  
  public static DC_AccountCompanyConnector getInstance() {
    if(instance == null) {
      instance = new DC_AccountCompanyConnector();
    } 
    return instance;
  }
  
  //constructor
  private DC_AccountCompanyConnector(){
    super();
    companyMapper = DC_MapperFactory.getInstance().getAccountMapper();
  }
  
  // Salesforce account manager instance  
  private DC_SalesforceAccountManager accountManager {
    get {
      if(accountManager == null) {
        accountManager = managerFactory.getSalesforceAccountManager();
      }
      return accountManager;
    }
    set;
  }
  
    // DC company manager instance
  public DC_DoubleClickCompanyManager companyManager {
    get {
      if(companyManager == null) {
        companyManager = managerFactory.getCompanyManager();
      }
      return companyManager;
    }
    set;
  }
  
  // Sync the accounts passed with DoubleClick -- update only.  
  // Records that don't exist get ignored
  // accountList    List<Account>   Accounts to sync with DoubleClick
  public void syncExistingSF_Records(List<Account> accountList) {
    connectorSyncUpdate = true;
    DC_GenericWrapper accountsToSync = companyMapper.wrap(accountList);

    if(accountsToSync.size() > 0) {
      // get the existing records
      accountsToSync = accountsToSync.getExistingRecords();

      if(accountsToSync.size() > 0) {
        syncSFRecordsToDCRecords(accountsToSync, DC_Constants.ACCOUNT_CONTEXT);
      }
    } 
  }
  
  // Creates non-existent accounts as companies in DSM, otherwise updates existing
  // accountList List<account> accounts to create or update
  public DC_GenericWrapper syncSFAccountsToDCCompanies(List<Account> accountList) { 
    connectorSyncUpdate = true;
    DC_GenericWrapper connectorQueriedAccounts = accountManager.getAccounts(accountList);
    DC_GenericWrapper wrap = syncSFRecordsToDCRecords(connectorQueriedAccounts, DC_Constants.ACCOUNT_CONTEXT);
    return wrap;
  }
  
  // find companies by matching on name of account that is provided
  // account  Account  account whose name will be used to find a matching companys
  public DC_GenericWrapper findMatchingCompanies(Account account) {
    return companyManager.getUnmatchedCompaniesByName(account.Name);
  }

  // link the Salesforce Account and the DoubleClick Company
  // account  Account           Salesforce account to link
  // company  DC_GenericWrapper wrapper containing DoubleClick company to link against
  public void linkAccountsAndCompanies(List<Account> accounts, DC_GenericWrapper company) {
    connectorSyncUpdate = true;
    DC_GenericWrapper wrappedAccount = companyMapper.wrap(accounts);
    companyMapper.linkSFRecordsAndDCRecords(wrappedAccount, company);

    companyManager.updateDCRecords(company);
    accountManager.updateAll(wrappedAccount);
  }

  // link the Salesforce Account and the DoubleClick Company
  // account  Account           Salesforce account to link
  // company  DC_GenericWrapper wrapper containing DoubleClick company to link against
  /*public void linkAccountAndCompany(Account account, DC_GenericWrapper company) {
    DC_GenericWrapper wrappedAccount = companyMapper.wrap(account);
    wrappedAccount.next();
    companyMapper.linkSFRecordAndDCRecord(wrappedAccount, company);

    companyManager.updateCompany(company);
    accountManager.updateCurrent(wrappedAccount);
  }*/
  
}