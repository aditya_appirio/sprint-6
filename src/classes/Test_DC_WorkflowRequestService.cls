// 
// (c) 2014 Appirio
//
// Test_DC_WorkflowService : test class for DC_WorkflowService 
//

@isTest
private class Test_DC_WorkflowRequestService {

  static testMethod void myUnitTest() {
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplWorkflowService());
    Test.startTest(); 
    DC_WorkflowService.CommonError ce = new DC_WorkflowService.CommonError();
    System.assert(ce != null);
    
    DC_WorkflowService.performWorkflowRequestActionResponse_element v1 = new DC_WorkflowService.performWorkflowRequestActionResponse_element();
    DC_WorkflowService.WorkflowRequestPage wrp = new DC_WorkflowService.WorkflowRequestPage();
    DC_WorkflowService.WorkflowRequest wfReq = new DC_WorkflowService.WorkflowRequest();
    DC_WorkflowService.getWorkflowRequestsByStatementResponse_element v2 = new DC_WorkflowService.getWorkflowRequestsByStatementResponse_element();
    DC_WorkflowService.DateTimeValue dtTmVal = new DC_WorkflowService.DateTimeValue();
    DC_WorkflowService.Statement statement = new DC_WorkflowService.Statement();
    DC_WorkflowService.WorkflowRequestAction wra = new DC_WorkflowService.WorkflowRequestAction();
    DC_WorkflowService.PublisherQueryLanguageContextError pqlErr = new DC_WorkflowService.PublisherQueryLanguageContextError();
    DC_WorkflowService.WorkflowApprovalRequest wfApprovalReq = new DC_WorkflowService.WorkflowApprovalRequest();
    DC_WorkflowService.ApiError apiErr = new DC_WorkflowService.ApiError();
    DC_WorkflowService.Date_x dateX = new DC_WorkflowService.Date_x();
    DC_WorkflowService.SoapRequestHeader srh = new DC_WorkflowService.SoapRequestHeader();
    
    DC_WorkflowService.BooleanValue boolVal = new DC_WorkflowService.BooleanValue();
    DC_WorkflowService.ForecastError al = new DC_WorkflowService.ForecastError();
    DC_WorkflowService.QuotaError ue = new DC_WorkflowService.QuotaError();
    DC_WorkflowService.FeatureError fe = new DC_WorkflowService.FeatureError();
    DC_WorkflowService.AuthenticationError ae = new DC_WorkflowService.AuthenticationError();
    DC_WorkflowService.PermissionError pe = new DC_WorkflowService.PermissionError();
    DC_WorkflowService.PublisherQueryLanguageSyntaxError PQLSyntaxErr = new DC_WorkflowService.PublisherQueryLanguageSyntaxError();
    
    DC_WorkflowService.QuotaError quotaErr = new DC_WorkflowService.QuotaError();
    DC_WorkflowService.DateValue dtVal = new DC_WorkflowService.DateValue();
    DC_WorkflowService.ApplicationException appExcep = new DC_WorkflowService.ApplicationException();
    DC_WorkflowService.String_ValueMapEntry strValMapEntry = new DC_WorkflowService.String_ValueMapEntry();
    DC_WorkflowService.LineItemOperationError liOpErr = new DC_WorkflowService.LineItemOperationError();
    
    
    DC_WorkflowService.InternalApiError iae = new DC_WorkflowService.InternalApiError();
    DC_WorkflowService.Authentication au = new DC_WorkflowService.Authentication();
    DC_WorkflowService.SetValue sv = new DC_WorkflowService.SetValue();
    DC_WorkflowService.getWorkflowRequestsByStatement_element sle = new DC_WorkflowService.getWorkflowRequestsByStatement_element();
    DC_WorkflowService.ApproveWorkflowApprovalRequests req = new DC_WorkflowService.ApproveWorkflowApprovalRequests();
    DC_WorkflowService.NotNullError nne = new DC_WorkflowService.NotNullError();
    DC_WorkflowService.UpdateResult ur = new DC_WorkflowService.UpdateResult();
    DC_WorkflowService.SoapResponseHeader srh1 = new DC_WorkflowService.SoapResponseHeader();
    DC_WorkflowService.TextValue tv = new DC_WorkflowService.TextValue();
    DC_WorkflowService.NumberValue numVal = new DC_WorkflowService.NumberValue();
    //DC_WorkflowService.ClientLogin cl = new DC_WorkflowService.ClientLogin(); 
    DC_WorkflowService.DateTime_x dt = new DC_WorkflowService.DateTime_x();
    DC_WorkflowService.StatementError se = new DC_WorkflowService.StatementError();
    DC_WorkflowService.ApiException apiExce = new DC_WorkflowService.ApiException();
    DC_WorkflowService.Value val = new DC_WorkflowService.Value();
    DC_WorkflowService.ApiVersionError v4= new DC_WorkflowService.ApiVersionError();
    DC_WorkflowService.OAuth oAuth = new DC_WorkflowService.OAuth();
    DC_WorkflowService.ServerError serverErr = new DC_WorkflowService.ServerError();
    DC_WorkflowService.RejectWorkflowApprovalRequests v5 = new DC_WorkflowService.RejectWorkflowApprovalRequests();
    DC_WorkflowService.performWorkflowRequestAction_element v6 = new DC_WorkflowService.performWorkflowRequestAction_element();
    
    DC_WorkflowService.WorkflowRequestServiceInterfacePort port = new DC_WorkflowService.WorkflowRequestServiceInterfacePort();
    System.assert(port != null);
    port.performWorkflowRequestAction(wra, statement);
    statement.query = 'Where id in (12345)';
		port.getWorkflowRequestsByStatement(statement);
    Test.stopTest(); 
  }
}