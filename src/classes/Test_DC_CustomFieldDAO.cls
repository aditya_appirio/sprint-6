// 
// (c) 2014 Appirio
//
// Test_DC_CustomFieldDAO: test class for DC_CustomFieldDAO 
//
@isTest
private class Test_DC_CustomFieldDAO {

  static testMethod void myUnitTest() {
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplCustomFieldService());
    Test.startTest();
    DC_DoubleClickOAuthHandler oAuthHandler = new DC_DoubleClickOAuthHandler(Userinfo.getUserId());
    
    DC_CustomFieldDAO custfldDAO = new DC_CustomFieldDAO(oAuthHandler);
    String query = null;
    
    DC_CustomFieldService.String_ValueMapEntry[] params = new List<DC_CustomFieldService.String_ValueMapEntry>();
    DC_CustomFieldService.Statement statement = custfldDAO.createStatement(query, params);
    System.assert(statement != null);
    
    DC_CustomFieldService.CustomFieldPage custFldPage = custfldDAO.getCustomFieldsByStatement(query);
    System.assert(custFldPage != null);
    DC_CustomFieldService.CustomField[] customFields = new List<DC_CustomFieldService.CustomField>(); 
    DC_CustomFieldService.CustomField[] custFlds = custFldDAO.createCustomFields(customFields);
    System.assert(custFlds != null);
    System.assertEquals(custFlds[0].name, 'custFld2');
    Test.stopTest();
  }
}