// 
// (c) 2014 Appirio
//
// Test_DC_SalesforceOpportunityManager : test class for DC_SalesforceOpportunityManager 
//
// 14 feb 2014    Ankit Goyal (JDC)      Original
//
@isTest(seeAllData = true)
public without sharing class Test_DC_SalesforceOpportunityLIManager {
  static testMethod void testDC_SalesforceOpportunityLIManager() {
    Account acc = DC_TestUtils.createAccount('test', true);
    Opportunity opp = DC_TestUtils.createOpportunity('testOpp',System.today(),acc.Id,'Prospecting',true);
    //PricebookEntry PBE=[select id from PricebookEntry limit 1 ];
    Pricebook2 standardPB = [select id, isActive from Pricebook2 where isStandard=true limit 1];
    if (!standardPB.isActive) {
      standardPB.isActive = true;
      update standardPB;
    }
    
    Product2 prod = new Product2(Name = 'testProd', IsActive = true);
    insert prod;
    PricebookEntry pbe = new PricebookEntry(Pricebook2Id = standardPB.Id, Product2Id = prod.Id, UnitPrice = 10000, IsActive = true, UseStandardPrice = false);
    insert pbe;
    
    OpportunityLineItem oppLI = DC_TestUtils.createOpportunityLI(23, 10, opp.Id,pbe.Id, true);
    goog_dclk_dsm__DC_ObjectMapping__c dcObjMapping = DC_TestUtils.createObjectMapping('ProposalLineItem', 'OpportunityLineItem', true);
    goog_dclk_dsm__DC_FieldMapping__c dcFldMapping1 = DC_TestUtils.createFieldMapping(dcObjMapping.id,'id', 'goog_dclk_dsm__DC_DoubleClickId__c','doubleClickId' , false);
    goog_dclk_dsm__DC_FieldMapping__c dcFldMapping2 = DC_TestUtils.createFieldMapping(dcObjMapping.id,'externalId', 'Id', 'sfid' , false);
    
    goog_dclk_dsm__DC_ObjectMapping__c dcDelMapping = DC_TestUtils.createObjectMapping('DeliveryData', 'DeliveryDataReport', 'OpportunityLineItem', true);
    goog_dclk_dsm__DC_FieldMapping__c dcDelFldMapping1 = DC_TestUtils.createFieldMapping(dcDelMapping.id,'id', 'goog_dclk_dsm__DC_DoubleClickId__c','doubleClickId' , false);
    
    insert new List<goog_dclk_dsm__DC_FieldMapping__c>{dcFldMapping1, dcFldMapping2, dcDelFldMapping1};
    
    DC_SalesforceManager sm = new DC_SalesforceManager();
    DC_SalesforceOpportunityLineItemManager som = new DC_SalesforceOpportunityLineItemManager();
    som.getOpportunityLineItem(oppLI.id);

    som.getOpportunityLineItems(new List<Id>{oppLI.id});
    
    DC_GenericWrapper gw = som.getOpportunityLineItems(new List<OpportunityLineItem>{oppLI});
    som.getOpportunityLineItems(gw);
    som.updateOpportunityLineItems(gw);
    som.upsertOpportunityLineItems(gw);
    som.generateUpsertableRecords(gw);
    som.updateAll(gw);
    som.getOpportunityLineItemsForDeliveryData(new List<OpportunityLineItem>{oppLI});
    
    opp.goog_dclk_dsm__DC_DoubleClickId__c = '12345';
    update opp;
    Map<String, List<DC_ProposalLineItemService.ProposalLineItem>> proposalIdLIMap = new Map<String, List<DC_ProposalLineItemService.ProposalLineItem>>();
    proposalIdLIMap.put('12345', new List<DC_ProposalLineItemService.ProposalLineItem>());
    proposalIdLIMap.get('12345').add(new DC_ProposalLineItemService.ProposalLineItem());
    som.fetchOpptiesExistInSFDC(proposalIdLIMap);
  }
}