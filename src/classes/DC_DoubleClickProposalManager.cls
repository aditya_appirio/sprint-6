// 
// (c) 2014 Appirio
//
// DC_DoubleClickProposalManager
// T-245174 : Create Push to DSM functionality for Opportunities
// 
// 04 Feb 2014    Ankit Goyal (JDC)      Original
//
public with sharing class DC_DoubleClickProposalManager extends DC_DoubleClickManager{

  private DC_ProposalMapper mapper { 
    get { 
      if(mapper == null) {
        mapper = DC_ProposalMapper.getInstance();
      }
      
      return mapper;
    }
    set;
  }
  
  private DC_DoubleClickProposalDAO proposalDAO {
    get {
        if(proposalDAO == null) {
            proposalDAO = new DC_DoubleClickProposalDAO(this.authHandler);
        }
        
        return proposalDAO;
    }
    set;
  }

  public DC_DoubleClickProposalManager(DC_DoubleClickOAuthHandler authHandler){
    super(authHandler);
  }
  
  public DC_GenericWrapper getProposalsUpdatedSince(DateTime basis, long pageSize, long offset) {
  	return mapper.wrap(proposalDAO.getProposalsByStatement('WHERE lastModifiedDateTime > \'' + proposalDAO.formatDateForPQL(basis) + '\' ORDER BY Id LIMIT ' + pageSize + ' OFFSET ' + offset));
  }
    
  public DC_GenericWrapper getProposalsCurrentlyServing(long pageSize, long offset) {
    String dateTimeNow = proposalDAO.formatDateForPQL(Datetime.now());
    
    //return mapper.wrap(proposalDAO.getProposalsByStatement('WHERE status = \'APPROVED\' AND startDateTime <= \'' + dateTimeNow + '\' AND endDateTime >= \'' + dateTimeNow + '\' ORDER BY Id LIMIT ' + pageSize + ' OFFSET ' + offset));
    return mapper.wrap(proposalDAO.getProposalsByStatement('WHERE startDateTime <= \'' + dateTimeNow + '\' AND endDateTime >= \'' + dateTimeNow + '\' ORDER BY Id LIMIT ' + pageSize + ' OFFSET ' + offset));
  }

  public DC_GenericWrapper getProposalsUpdatedSince(Datetime lastSyncDate) {
  	return getProposalsUpdatedSince(lastSyncDate, DC_Constants.PROPOSAL_PAGE_SIZE, 0);
  } 
    

    
  // TO DO: make sure we fetch pages of data 
  //Doubt: Not sure of this one. Do we limit the no. of companies we are getting through getCompaniesByStatement?
  public DC_GenericWrapper getProposals(DC_GenericWrapper proposals) {
    if (proposals.size() == 0)
      return mapper.wrap(new DC_ProposalService.Proposal[] {} );
          
    proposals.gotoBeforeFirst();
    String ids = '';
    String separator = '';
    List<DC_ProposalService.Proposal> result = new List<DC_ProposalService.Proposal>(); 
    
    while(proposals.hasNext()) {
        proposals.next();
        
      ids += separator + proposals.getField('DoubleClickId');
      separator = ',';  
    }
    return mapper.wrap(proposalDAO.getProposalsByStatement('WHERE id in (' + ids + ')').results);
  }
    
  public DC_GenericWrapper createProposals(DC_GenericWrapper proposals) {
    DC_ProposalService.Proposal[] apiProposals = new DC_ProposalService.Proposal[proposals.size()];
        
    proposals.gotoBeforeFirst();
    while(proposals.hasNext()) {
      proposals.next();

      
      DC_ProposalService.Proposal proposal = (DC_ProposalService.Proposal) proposals.getCurrentObject();

      apiProposals[proposals.getCurrentIndex()] = proposal;
    }
    return mapper.wrap(proposalDAO.createProposals(apiProposals));
  }
    
  // update the provided companies in DoubleClick
  // companies  DC_GenericWrapper   wrapper containing companies for update
  public DC_GenericWrapper updateProposals(DC_GenericWrapper proposals) {
    DC_ProposalService.Proposal[] apiProposals = new DC_ProposalService.Proposal[proposals.size()];
    
    proposals.gotoBeforeFirst();
    
    while(proposals.hasNext()) {
      proposals.next();
      apiProposals[proposals.getCurrentIndex()] = (DC_ProposalService.Proposal) proposals.getCurrentObject();
    }
    
    return mapper.wrap(proposalDAO.updateProposals(apiProposals)); 
      
  }
    


  // get proposals matched by name OR email to provide linking capability
  // String 
  public DC_GenericWrapper getMatchingProposals(String companyId, String name) {
    string statement = String.format('WHERE name = \'\'{1}\'\'', new String[] { companyId, name.replace('\'', '\'\'') } );

    // wrap and return results
    return mapper.wrap(proposalDAO.getProposalsByStatement(statement).results);
  }
  
  // get approved proposals matched by name OR email 
  // String 
  public DC_GenericWrapper getMatchingApprovedProposals(String companyId, String name) {
    string statement = String.format('WHERE name = \'\'{1}\'\' and approvalStatus = \'false\'', new String[] { companyId, name.replace('\'', '\'\'') } );

    // wrap and return results
    return mapper.wrap(proposalDAO.getProposalsByStatement(statement).results);
  }
  
  
  public void retractProposals(DC_GenericWrapper proposals) {
  	proposalDAO.retractProposals((DC_ProposalService.Proposal[])proposals.getObjects());
  }

  public void submitProposalsForApproval(DC_GenericWrapper proposals) {
    proposalDAO.submitProposalsForApproval((DC_ProposalService.Proposal[])proposals.getObjects());
  }
 
  // ** DEPRECATED for 201403 API ====================================
  /*

  public DC_GenericWrapper getProposal(Integer id) {
    return mapper.wrap(proposalDAO.getProposal(id, true));
  }
  public void updateProposal(DC_GenericWrapper proposal) {
    proposalDAO.updateProposal((DC_ProposalService.Proposal)proposal.getCurrentObject());
  }
  */
  // END DEPRECATED ====================================

}