// 
// (c) 2014 Appirio
//
// T-266487 : batch class for pushing Account to SFDC that not exist yet
// 
// 26th March 2014    Anjali K (JDC)      Original
//
public with sharing class DC_PushAccountToDSMBatch extends DC_PushRecordToDSMProcess implements
																					Database.Batchable<sObject>,
																				  Database.AllowsCallouts,
																				  Database.Stateful {
	
 	public DC_PushAccountToDSMBatch() {}
 	public DC_PushAccountToDSMBatch(string soql) {
 	  query = soql;
 	}
 	
 	public DC_PushAccountToDSMBatch(boolean useReadyForDSM) {
 		readyForDSM = useReadyForDSM;
 	}
 	
  public override string getContext() {
  	return 'Push Account to DSM';
  }
  
  public override void execute( Database.BatchableContext bc, List<SObject> sObjectList ) {
  	for (integer i = sObjectList.size() - 1; i >= 0; i--) {
  		if (((Account)sObjectList[i]).goog_dclk_dsm__DoubleClick_Account_Teams__r == null
  		       || ((Account)sObjectList[i]).goog_dclk_dsm__DoubleClick_Account_Teams__r.size() == 0) {
  		  sObjectList.remove(i);       	
  		} 
  	}
  	super.execute(bc, sObjectList);
  }
  
  public override void executeSync(List<SObject> sObjectList){
  	DC_AccountCompanyConnector.getInstance().syncSFAccountsToDCCompanies((List<Account>)sObjectList);
  }
  public override String getDefaultQuery(){
    string queryColumnList = DC_ObjectMapper.getInstance().getCommaSeparatedSalesforceFieldList('Account');
    
    String defaultQry = 
           'SELECT ' + queryColumnList + ' ' + 
           'FROM Account ' +
           'WHERE goog_dclk_dsm__DC_DoubleClickId__c = null ' + 
           ((readyForDSM) ? ' AND goog_dclk_dsm__DC_ReadyForDSM__c = :readyForDSM' : '');
    return defaultQry;
  }  
	
}