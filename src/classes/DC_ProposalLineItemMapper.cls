// 
// (c) 2014 Appirio
//
// DC_ProposalLineItemMapper
// T-249484 : Create Proposal Lines classes
// 
// 13 Feb 2014    Ankit Goyal (JDC)      Copied from DC_ProposalLineItemMapper
//
public with sharing class DC_ProposalLineItemMapper extends DC_ObjectMapper {
  
  private static DC_ProposalLineItemMapper instance;
  
  //constructor
  private DC_ProposalLineItemMapper(){
    super();
  }
  public static DC_ProposalLineItemMapper getInstance() {
    if(instance == null) {
      instance = new DC_ProposalLineItemMapper();
    } 
    return instance;
  }
  
  // Overloaded method wrap : takes DC_ProposalLineItemService.ProposalLineItem as param and call wrap(List<DC_ProposalLineItemService.ProposalLineItem>)
  public DC_GenericWrapper wrap(DC_ProposalLineItemService.ProposalLineItem proposalLI) {
    List<DC_ProposalLineItemService.ProposalLineItem> proposalList = new List<DC_ProposalLineItemService.ProposalLineItem>();
    proposalList.add(proposalLI);
    DC_GenericWrapper result = wrap(proposalList);
    
    return result;
  }
  
  // Overloaded method wrap : takes List<DC_ProposalLineItemService.ProposalLineItem> as param and convert into DC_DoubleClickProposalWrapper instance
  public DC_GenericWrapper wrap(List<DC_ProposalLineItemService.ProposalLineItem> proposalLIs) {
    return new DC_DoubleClickProposalLineItemWrapper(this.getObjectMappingByName('OpportunityLineItem'), this.getFieldMappingsByName('OpportunityLineItem'), proposalLIs);
  }
  
  // Overloaded method wrap : takes List<DC_ProposalService.Proposal> as param and convert into DC_DoubleClickProposalWrapper instance
  public DC_GenericWrapper wrap(DC_ProposalLineItemService.ProposalLineItemPage page) {
    DC_GenericWrapper wrapper = new DC_DoubleClickProposalLineItemWrapper(this.getObjectMappingByName('OpportunityLineItem'), this.getFieldMappingsByName('OpportunityLineItem'), page.results);
    wrapper.totalResultSetSize = page.totalResultSetSize;
    return wrapper;
  }  

  /* // Overrided method wrap : takes Proposal as param and call wrap(List<Proposal> proposals)
  public DC_GenericWrapper wrap(Opportunity opp) {
    List<Opportunity> oppList = new List<Opportunity>();
    oppList.add(opp);
    DC_GenericWrapper result = wrap(oppList);
    
    return result;
  }
  
  // Overrided method wrap : takes List<Proposal> as param and convert into DC_SalesforceWrapper instance
  public DC_GenericWrapper wrap(List<Opportunity> oppList) {
    return new DC_SalesforceWrapper(this.getObjectMappingByName('Opportunity'), this.getFieldMappingsByName('Opportunity'), oppList);
  }
  */
  
  //Create new DC proposals for SF Opportunities
  public DC_GenericWrapper createNewDCProposalLIs(DC_GenericWrapper proposalLIs) {
    DC_GenericWrapper dc_proposalLIs = this.wrap(new List<DC_ProposalLineItemService.ProposalLineItem>());
    proposalLIs.gotoBeforeFirst();
    
    while(proposalLIs.hasNext()) {
      proposalLIs.next();
      dc_proposalLIs.add(new DC_ProposalLineItemService.ProposalLineItem());
      dc_proposalLIs.gotoLast();
      //dc_proposals.updateFields(proposals);
      updateFields(proposalLIs, dc_proposalLIs);
    } 
    return dc_proposalLIs;
  }

  // Links the current company with the current account in the wrappers
  public void linkOpportunityLIAndProposalLI(DC_GenericWrapper src, DC_GenericWrapper trg) {
      src.setField('DoubleClickId', trg.getField('DoubleClickId'));
      trg.setField('sfid', src.getField('sfid'));
  }

  public override void updateField(DC_GenericWrapper target, goog_dclk_dsm__DC_FieldMapping__c fieldMapping, Object newValue) {
    if(fieldMapping.name.equalsIgnoreCase('probability')) {
      if(newValue instanceof Double) {
        newValue = ((Double)newValue).round().intValue();
      }

      newValue = ((Integer)newValue / 10) * 10000;
    } 

    super.updateField(target, fieldMapping, newValue);
  }
  
  public void updateFieldsOnAllObjectsWithCreate(DC_GenericWrapper source, DC_GenericWrapper target) {
    source.gotoBeforeFirst();

    while(source.hasNext()) {
      source.next();

      // TODO US: build logic to validate last modified date before making the update
      if(target.gotoDCId(String.valueOf((long)source.getField('DoubleClickId')))) {
        updateFields(source, target);
      } else {
        addNewRecordFromSource(source, target);
      }
    }
  }
  
  public void addNewRecordFromSource(DC_GenericWrapper source, DC_GenericWrapper target) {
    object item;
    if (target instanceOf DC_SalesforceWrapper) {
      item = new OpportunityLineItem();
    } else {
      item = new DC_ProposalLineItemService.ProposalLineItem();
    } 
    
    target.add(item);
    target.gotoLast();    
    
    updateFields(source, target);
  }  
	
	// get only those Proposal line items whose proposal ids are connected to SFDC Oppties
	public Map<String, List<DC_ProposalLineItemService.ProposalLineItem>> getExistingOpportunitiesProposalLI(DC_GenericWrapper wrapper){
		DC_GenericWrapper retwrap ;
    wrapper.gotoBeforeFirst();
    //Map of Proposal id to list of proposal LIs that are connected to Proposal id
    Map<String, List<DC_ProposalLineItemService.ProposalLineItem>> proposalIdLIMap = new Map<String, List<DC_ProposalLineItemService.ProposalLineItem>>();
    while (wrapper.hasNext()) {
    	
      wrapper.next();
      if (wrapper.getField('proposalId') != null) {
      	string proposalId = string.valueOf((long)wrapper.getField('proposalId'));
      	if(!proposalIdLIMap.containsKey(proposalId)){ 
      		proposalIdLIMap.put(proposalId, new List<DC_ProposalLineItemService.ProposalLineItem>());
      	}
      	proposalIdLIMap.get(proposalId).add((DC_ProposalLineItemService.ProposalLineItem)wrapper.getCurrentObject());
      }
    }
    return proposalIdLIMap;
	}
	 
	public List<DC_GenericWrapper> removeArchivedProposalLines(DC_GenericWrapper wrapper){
		DC_GenericWrapper archiveWrap = this.wrap(new List<DC_ProposalLineItemService.ProposalLineItem>());
		DC_GenericWrapper nonArchiveWrap = this.wrap(new List<DC_ProposalLineItemService.ProposalLineItem>());
		List<DC_GenericWrapper> wrapList = new List<DC_GenericWrapper>();
    wrapper.gotoBeforeFirst();
    while (wrapper.hasNext()) {
      wrapper.next();
      if (wrapper.getField('isArchived') != null && boolean.valueOf(wrapper.getField('isArchived')) == true) {
      	archiveWrap.add(wrapper.getCurrentObject());
      }else{
      	nonArchiveWrap.add(wrapper.getCurrentObject());
      }
    }
    wrapList.add(archiveWrap);
    wrapList.add(nonArchiveWrap);
    return wrapList;
	}
	
}