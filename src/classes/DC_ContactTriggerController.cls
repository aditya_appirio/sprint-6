// 
// (c) 2014 Appirio
//
// Controller for trigger syncing of Contacts to DCLK
//
public with sharing class DC_ContactTriggerController {
  
  // handle after update contacts action
  // updatedContacts  List<Contact>  contacts that were updated
  public static void performExistingContactSync(List<Contact> updatedContacts) {
    List<Id> contactIds = new List<Id>();
    
    for(Contact contact : updatedContacts) {
      if (contact.goog_dclk_dsm__DC_DoubleClickId__c != null) {
        contactIds.add(contact.Id);
      }
    }

    if (contactIds.size() > 0) {
      DC_ContactTriggerController.syncExistingContacts(contactIds);
    }
  }
  
  // sync existing contacts with DoubleClick
  // note that this is called as an async @future call as triggers cannot perform callouts
  // contactIds   List<Id>  ids of contacts to sync.
  @future(callout=true)
  public static void syncExistingContacts(List<Id> contactIds) {
    DC_ContactContactConnector connector = DC_ContactContactConnector.getInstance();
    
    connector.syncSFContactsToDCContacts([SELECT Id from Contact where id in :contactIds]);
  }
}