//
// queueable delivery data process, retrieves delivery data from doubleclick and updates salesforce
// 
public with sharing class DC_DeliveryDataProcess extends DC_QueueableProcess {
  public override void initialize(DC_QueueableProcessParameter inParams) {
    params = inParams;
  }
  
  public override void execute() {
  	try {
      DC_DeliveryDataConnector.getInstance().syncDeliveryData();
      DC_SalesforceDML.getInstance().flush();
  	} catch (Exception e) {
  		DC_ConnectorLogger.log(params.queueItem.goog_dclk_dsm__DC_Process__c, e, params.queueItem.Id);
  		DC_QueueDispatch.queueItemCompleted(params.queueItem, DC_QueueDispatch.DC_ExecutionStatus.Error, 'Errors encountered during delivery data processing');
  		return;
  	}
    DC_QueueDispatch.queueItemCompleted(params.queueItem, DC_QueueDispatch.DC_ExecutionStatus.Success, 'The process completed successfully.');
    DC_SalesforceDML.getInstance().flush();
  }  
  
  public override System.Type getParameterType(){
  	return DC_DeliveryDataProcessParameters.class;
  }
  
  public class DC_DeliveryDataProcessParameters extends DC_QueueableProcessParameter {}
}