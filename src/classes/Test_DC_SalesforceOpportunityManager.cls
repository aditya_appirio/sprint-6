// 
// (c) 2014 Appirio
//
// Test_DC_SalesforceOpportunityManager : test class for DC_SalesforceOpportunityManager 
//
// 14 feb 2014    Ankit Goyal (JDC)      Original
//
@isTest
public with sharing class Test_DC_SalesforceOpportunityManager {
  static testMethod void testDC_SalesforceOpportunityManager() {
    Account acc = DC_TestUtils.createAccount('test', true);
    Opportunity opp = DC_TestUtils.createOpportunity('testOpp',System.now().date(),acc.Id,'Prospecting',true);
    goog_dclk_dsm__DC_ObjectMapping__c dcObjMapping = DC_TestUtils.createObjectMapping('Proposal', 'Opportunity', true);
    goog_dclk_dsm__DC_FieldMapping__c dcFldMapping1 = DC_TestUtils.createFieldMapping(dcObjMapping.id,'Name', 'Name', 'name' , false);
    goog_dclk_dsm__DC_FieldMapping__c dcFldMapping2 = DC_TestUtils.createFieldMapping(dcObjMapping.id,'externalId', 'Id', 'sfid' , false);
    insert new List<goog_dclk_dsm__DC_FieldMapping__c>{dcFldMapping1, dcFldMapping2};
    DC_SalesforceManager sm = new DC_SalesforceManager();
    DC_SalesforceOpportunityManager som = new DC_SalesforceOpportunityManager();
    som.getOpportunity(opp.id);

    DC_GenericWrapper gw = som.getOpportunities(new List<Id>{opp.id});
    som.getOpportunities(gw);
    gw = som.getOpportunities(new List<Opportunity>{opp});
    
    som.updateOpportunities(gw);
    System.assertEquals(null, som.getOpportunities(new List<Opportunity>()));
  }
}