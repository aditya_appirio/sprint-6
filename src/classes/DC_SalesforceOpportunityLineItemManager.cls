// 
// (c) 2014 Appirio
//
// DC_SalesforceOpportunityLineItemManager
// T-249484 : Create Proposal Lines classes
// 
// 13 Feb 2014    Ankit Goyal (JDC)      Copied from DC_SalesforceOpportunityManager
//
public with sharing class DC_SalesforceOpportunityLineItemManager extends DC_SalesforceManager {
  
  private DC_ProposalLineItemMapper mapper {
    get {
      if(mapper == null) {
        mapper = DC_ProposalLineItemMapper.getInstance();
      }
      return mapper;
    }
    set;
  }
  
  public DC_GenericWrapper getOpportunityLineItem(String id) {
     String fieldNames = this.mapper.getCommaSeparatedSalesforceFieldList('OpportunityLineItem');
     
     String query = 'SELECT ' + fieldNames + ' FROM OpportunityLineItem where id = \'' + id + '\'';
     
     system.debug('==========query==========='+query);
     return mapper.wrap((List<OpportunityLineItem>)Database.query(query));
  }
  
  //update OpportunityLineItem
  public void updateOpportunityLineItems(DC_GenericWrapper opps) {
    List<OpportunityLineItem> oppList = new List<OpportunityLineItem>();
    
    for(Object o : opps.getObjects()) {
      oppList.add((OpportunityLineItem)o);
    }

    if(oppList.size() > 0) {
      DC_SalesforceDML.getInstance().registerUpdate(oppList);
    }
  }
  
  
  //update OpportunityLineItem
  public DC_SalesforceDML.DC_SalesforceDMLStatus upsertOpportunityLineItems(DC_GenericWrapper opps) {
    List<OpportunityLineItem> oppList = new List<OpportunityLineItem>();
    
    for(Object o : opps.getObjects()) {
      oppList.add((OpportunityLineItem)o);
    }

    DC_SalesforceDML.DC_SalesforceDMLStatus status;
    if(oppList.size() > 0) {
      status = DC_SalesforceDML.getInstance().registerUpsert(oppList);
    }

    return status;
  }  
  
  //delete OpportunityLineItem
  public DC_SalesforceDML.DC_SalesforceDMLStatus deleteOpportunityLineItems(DC_GenericWrapper opps) {
    List<OpportunityLineItem> oppList = new List<OpportunityLineItem>();
    
    for(Object o : opps.getObjects()) {
      oppList.add((OpportunityLineItem)o);
    }

    DC_SalesforceDML.DC_SalesforceDMLStatus status;
    if(oppList.size() > 0) {
    	System.debug('@@ registering delete for: ' + oppList);
      status = DC_SalesforceDML.getInstance().registerDelete(oppList);
    }

    return status;
  }  

  public DC_GenericWrapper getOpportunityLineItems(List<OpportunityLineItem> opportunities) {
    if(opportunities == null || opportunities.size() == 0) {
      return null;
    }

    List<Id> ids = new List<Id>();

    for(OpportunityLineItem opp : opportunities) {
      ids.add(opp.Id);
    }

    return getOpportunityLineItems(ids);
  }

  public DC_GenericWrapper getOpportunityLineItems(List<Id> ids) {
    String fieldNames = this.mapper.getCommaSeparatedSalesforceFieldList('OpportunityLineItem');
/*
    String inStatement = '(';
    String separator = '';

    for(Id id : ids) {
      inStatement += separator + '\'' + id + '\'';
      separator = ',';
    }

    inStatement += ')';
*/
    String query = 'SELECT ' + fieldNames + ' FROM OpportunityLineItem where id in :ids';

    return mapper.wrap((List<OpportunityLineItem>)Database.query(query));
  }

  public DC_GenericWrapper getOpportunityLineItemsForDeliveryData(List<OpportunityLineItem> ids) {
    String fieldNames = this.mapper.getCommaSeparatedSalesforceFieldList('DeliveryData');

    String query = 'SELECT ' + fieldNames + ' FROM OpportunityLineItem where id in :ids';

    return mapper.wrapForDeliveryData((List<OpportunityLineItem>)Database.query(query));
  }

  public DC_GenericWrapper getOpportunityLineItems(DC_GenericWrapper wrapper) {
  	String fieldNames = this.mapper.getCommaSeparatedSalesforceFieldList('OpportunityLineItem');
    wrapper.gotoBeforeFirst();
    List<String> dcIds = new List<String>();
    
    string opLineId;
    while(wrapper.hasNext()) {
      wrapper.next();
      
      dcIds.add(String.valueOf((long)wrapper.getField('doubleclickid')));
    }

    String query = 'SELECT ' + fieldNames + ' FROM OpportunityLineItem where goog_dclk_dsm__dc_doubleclickid__c in :dcIds';

    return mapper.wrap((List<OpportunityLineItem>)Database.query(query));
  }

  public DC_GenericWrapper generateUpsertableRecords(DC_GenericWrapper wrapper) {
    
    wrapper.gotoBeforeFirst();
    List<String> dcIds = new List<String>();
    
    string opLineId;
    while(wrapper.hasNext()) {
      wrapper.next();
      
      dcIds.add(String.valueOf((long)wrapper.getField('doubleclickid')));
    }
    return mapper.wrap([SELECT id, goog_dclk_dsm__dc_doubleclickid__c, goog_dclk_dsm__DC_isArchived__c, PricebookEntryId FROM OpportunityLineItem WHERE goog_dclk_dsm__dc_doubleclickid__c IN :dcIds]);
  }
  
  // Fetch proposals that are in sfdc means if opp.goog_dclk_dsm__DC_DoubleClickId__c != null
	public DC_GenericWrapper fetchOpptiesExistInSFDC(Map<String, List<DC_ProposalLineItemService.ProposalLineItem>> proposalIdLIMap){
		DC_GenericWrapper retwrap = mapper.wrap(new List<DC_ProposalLineItemService.ProposalLineItem>());
		for(Opportunity opp : [Select id, goog_dclk_dsm__DC_DoubleClickId__c from Opportunity where goog_dclk_dsm__DC_DoubleClickId__c in :proposalIdLIMap.keySet()]){
			retwrap.addAll(proposalIdLIMap.get(opp.goog_dclk_dsm__DC_DoubleClickId__c));
		}
		return retwrap;
	}
	
}