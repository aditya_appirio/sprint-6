// 
// (c) 2014 Appirio
//
// Test_DCProductLineItemService : test class for DC_ProposalLineItemService 
//
// 24 Jan 2014    Ankit Goyal (JDC)      Original
//
@isTest
public with sharing class Test_DCProductLineItemService {
  static testMethod void testDC_ProposalLineItemService() { 
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplProductLineItem());
    Test.startTest();
    
    DC_ProposalLineItemService.TechnologyTargetingError var1 = new DC_ProposalLineItemService.TechnologyTargetingError();
    DC_ProposalLineItemService.InternalApiError var2 = new DC_ProposalLineItemService.InternalApiError();
    DC_ProposalLineItemService.ProposalLineItemError var3 = new DC_ProposalLineItemService.ProposalLineItemError();
    DC_ProposalLineItemService.VideoPositionTarget var4 = new DC_ProposalLineItemService.VideoPositionTarget();
    DC_ProposalLineItemService.BandwidthGroup var5 = new DC_ProposalLineItemService.BandwidthGroup();
    DC_ProposalLineItemService.SetValue var6 = new DC_ProposalLineItemService.SetValue();
    DC_ProposalLineItemService.UserDomainTargeting var7 = new DC_ProposalLineItemService.UserDomainTargeting();
    DC_ProposalLineItemService.InventoryTargetingError var8 = new DC_ProposalLineItemService.InventoryTargetingError();
    DC_ProposalLineItemService.DayPartTargeting var9 = new DC_ProposalLineItemService.DayPartTargeting();
    DC_ProposalLineItemService.BooleanValue var10 = new DC_ProposalLineItemService.BooleanValue();
    DC_ProposalLineItemService.RequiredError var11 = new DC_ProposalLineItemService.RequiredError();
    DC_ProposalLineItemService.CustomCriteriaLeaf var12 = new DC_ProposalLineItemService.CustomCriteriaLeaf();
    DC_ProposalLineItemService.FeatureError var13 = new DC_ProposalLineItemService.FeatureError();
    DC_ProposalLineItemService.AuthenticationError var14 = new DC_ProposalLineItemService.AuthenticationError();
    DC_ProposalLineItemService.PermissionError var15 = new DC_ProposalLineItemService.PermissionError();
    DC_ProposalLineItemService.DeviceManufacturerTargeting var16 = new DC_ProposalLineItemService.DeviceManufacturerTargeting();
    DC_ProposalLineItemService.InventoryTargeting var17 = new DC_ProposalLineItemService.InventoryTargeting();
    DC_ProposalLineItemService.MobileDeviceSubmodelTargeting var18 = new DC_ProposalLineItemService.MobileDeviceSubmodelTargeting();
    DC_ProposalLineItemService.OperatingSystemTargeting var19 = new DC_ProposalLineItemService.OperatingSystemTargeting();
    DC_ProposalLineItemService.MobileDevice md = new DC_ProposalLineItemService.MobileDevice();
    DC_ProposalLineItemService.BandwidthGroupTargeting var21 = new DC_ProposalLineItemService.BandwidthGroupTargeting();
    DC_ProposalLineItemService.CustomCriteria var22 = new DC_ProposalLineItemService.CustomCriteria();
    DC_ProposalLineItemService.BaseCustomFieldValue var23 = new DC_ProposalLineItemService.BaseCustomFieldValue();
    DC_ProposalLineItemService.updateProposalLineItem_element var24 = new DC_ProposalLineItemService.updateProposalLineItem_element();
    DC_ProposalLineItemService.NumberValue var25 = new DC_ProposalLineItemService.NumberValue();
    DC_ProposalLineItemService.GeoTargeting var26 = new DC_ProposalLineItemService.GeoTargeting();
    DC_ProposalLineItemService.TimeOfDay var27 = new DC_ProposalLineItemService.TimeOfDay();
    DC_ProposalLineItemService.SoapResponseHeader var28 = new DC_ProposalLineItemService.SoapResponseHeader();
    DC_ProposalLineItemService.CustomFieldValue var29 = new DC_ProposalLineItemService.CustomFieldValue();
    DC_ProposalLineItemService.updateProposalLineItemsResponse_element var30 = new DC_ProposalLineItemService.updateProposalLineItemsResponse_element();
    DC_ProposalLineItemService.CustomCriteriaSet var31 = new DC_ProposalLineItemService.CustomCriteriaSet();
    DC_ProposalLineItemService.VideoPosition var32 = new DC_ProposalLineItemService.VideoPosition();
    DC_ProposalLineItemService.NotNullError var33 = new DC_ProposalLineItemService.NotNullError();
    DC_ProposalLineItemService.AudienceSegmentCriteria var34 = new DC_ProposalLineItemService.AudienceSegmentCriteria();
    DC_ProposalLineItemService.createProposalLineItemResponse_element var35 = new DC_ProposalLineItemService.createProposalLineItemResponse_element();
    DC_ProposalLineItemService.BrowserLanguageTargeting var36 = new DC_ProposalLineItemService.BrowserLanguageTargeting();
    DC_ProposalLineItemService.DateTimeValue var37 = new DC_ProposalLineItemService.DateTimeValue();
    DC_ProposalLineItemService.AdUnitTargeting var38 = new DC_ProposalLineItemService.AdUnitTargeting();
    DC_ProposalLineItemService.PublisherQueryLanguageContextError var39 = new DC_ProposalLineItemService.PublisherQueryLanguageContextError();
    DC_ProposalLineItemService.ProposalLineItemActionError var40 = new DC_ProposalLineItemService.ProposalLineItemActionError();
    DC_ProposalLineItemService.Date_x var41 = new DC_ProposalLineItemService.Date_x();
    DC_ProposalLineItemService.SoapRequestHeader var42 = new DC_ProposalLineItemService.SoapRequestHeader();
    DC_ProposalLineItemService.TeamError var43 = new DC_ProposalLineItemService.TeamError();
    DC_ProposalLineItemService.ProductError var44 = new DC_ProposalLineItemService.ProductError();
    DC_ProposalLineItemService.DeviceCategoryTargeting var45 = new DC_ProposalLineItemService.DeviceCategoryTargeting();
    DC_ProposalLineItemService.ApiException var46 = new DC_ProposalLineItemService.ApiException();
    DC_ProposalLineItemService.OperatingSystemVersionTargeting var47 = new DC_ProposalLineItemService.OperatingSystemVersionTargeting();
    DC_ProposalLineItemService.ProposalLineItemPage var48 = new DC_ProposalLineItemService.ProposalLineItemPage();
    DC_ProposalLineItemService.MobileCarrierTargeting var49 = new DC_ProposalLineItemService.MobileCarrierTargeting();
    DC_ProposalLineItemService.ApplicationException var50 = new DC_ProposalLineItemService.ApplicationException();
        
    DC_ProposalLineItemService.GenericTargetingError var52 = new DC_ProposalLineItemService.GenericTargetingError();
    DC_ProposalLineItemService.ApiVersionError var53 = new DC_ProposalLineItemService.ApiVersionError();
    DC_ProposalLineItemService.Money var54 = new DC_ProposalLineItemService.Money();
    DC_ProposalLineItemService.ContentTargeting var55 = new DC_ProposalLineItemService.ContentTargeting();
    DC_ProposalLineItemService.ServerError var56 = new DC_ProposalLineItemService.ServerError();
    DC_ProposalLineItemService.DateTime_x var57 = new DC_ProposalLineItemService.DateTime_x();
    DC_ProposalLineItemService.FrequencyCapError var58 = new DC_ProposalLineItemService.FrequencyCapError();
    DC_ProposalLineItemService.DeviceCapabilityTargeting var59 = new DC_ProposalLineItemService.DeviceCapabilityTargeting();
    DC_ProposalLineItemService.getProposalLineItem_element var60 = new DC_ProposalLineItemService.getProposalLineItem_element();
    DC_ProposalLineItemService.BillingError var61 = new DC_ProposalLineItemService.BillingError();
    DC_ProposalLineItemService.ProposalLineItemAction pliAction = new DC_ProposalLineItemService.ProposalLineItemAction();
    DC_ProposalLineItemService.DeviceCategory var63 = new DC_ProposalLineItemService.DeviceCategory();
    DC_ProposalLineItemService.createProposalLineItemsResponse_element var64 = new DC_ProposalLineItemService.createProposalLineItemsResponse_element();
    DC_ProposalLineItemService.DeviceManufacturer var65 = new DC_ProposalLineItemService.DeviceManufacturer();
    DC_ProposalLineItemService.ProposalError var66 = new DC_ProposalLineItemService.ProposalError();
    DC_ProposalLineItemService.BrowserLanguage var67 = new DC_ProposalLineItemService.BrowserLanguage();
    DC_ProposalLineItemService.MobileDeviceSubmodel var68 = new DC_ProposalLineItemService.MobileDeviceSubmodel();
    DC_ProposalLineItemService.TechnologyTargeting var69 = new DC_ProposalLineItemService.TechnologyTargeting();
    DC_ProposalLineItemService.CustomCriteriaNode var70 = new DC_ProposalLineItemService.CustomCriteriaNode();
    DC_ProposalLineItemService.Authentication var71 = new DC_ProposalLineItemService.Authentication();
    DC_ProposalLineItemService.StringLengthError var72 = new DC_ProposalLineItemService.StringLengthError();
    DC_ProposalLineItemService.UserDomainTargetingError var73 = new DC_ProposalLineItemService.UserDomainTargetingError();
    DC_ProposalLineItemService.AppliedLabel var74 = new DC_ProposalLineItemService.AppliedLabel();
    DC_ProposalLineItemService.UniqueError var75 = new DC_ProposalLineItemService.UniqueError();
    DC_ProposalLineItemService.updateProposalLineItems_element var76 = new DC_ProposalLineItemService.updateProposalLineItems_element();
    DC_ProposalLineItemService.DeliveryData var77 = new DC_ProposalLineItemService.DeliveryData();
    DC_ProposalLineItemService.OperatingSystem var78 = new DC_ProposalLineItemService.OperatingSystem();
    DC_ProposalLineItemService.PublisherQueryLanguageSyntaxError var79 = new DC_ProposalLineItemService.PublisherQueryLanguageSyntaxError();
    DC_ProposalLineItemService.String_ValueMapEntry var80 = new DC_ProposalLineItemService.String_ValueMapEntry();
    DC_ProposalLineItemService.Size var81 = new DC_ProposalLineItemService.Size();
    DC_ProposalLineItemService.Value var82 = new DC_ProposalLineItemService.Value();
    DC_ProposalLineItemService.DropDownCustomFieldValue var83 = new DC_ProposalLineItemService.DropDownCustomFieldValue();
    DC_ProposalLineItemService.PrecisionError var84 = new DC_ProposalLineItemService.PrecisionError();
    DC_ProposalLineItemService.OAuth var85 = new DC_ProposalLineItemService.OAuth();
    DC_ProposalLineItemService.ContentMetadataKeyHierarchyTargeting var86 = new DC_ProposalLineItemService.ContentMetadataKeyHierarchyTargeting();
    DC_ProposalLineItemService.updateProposalLineItemResponse_element var87 = new DC_ProposalLineItemService.updateProposalLineItemResponse_element();
    DC_ProposalLineItemService.VideoPositionTargeting var88 = new DC_ProposalLineItemService.VideoPositionTargeting();
    DC_ProposalLineItemService.RequiredNumberError var89 = new DC_ProposalLineItemService.RequiredNumberError();
    DC_ProposalLineItemService.ReservationDetailsError var90 = new DC_ProposalLineItemService.ReservationDetailsError();
    DC_ProposalLineItemService.ProposalLineItemPremium plip = new DC_ProposalLineItemService.ProposalLineItemPremium();
    DC_ProposalLineItemService.getProposalLineItemsByStatement_element var92 = new DC_ProposalLineItemService.getProposalLineItemsByStatement_element();
    DC_ProposalLineItemService.Technology var93 = new DC_ProposalLineItemService.Technology();
    DC_ProposalLineItemService.VideoPositionWithinPod var94 = new DC_ProposalLineItemService.VideoPositionWithinPod();
    DC_ProposalLineItemService.Statement stat1 = new DC_ProposalLineItemService.Statement();
    DC_ProposalLineItemService.ApiError var96 = new DC_ProposalLineItemService.ApiError();
    DC_ProposalLineItemService.Targeting var97 = new DC_ProposalLineItemService.Targeting();
    DC_ProposalLineItemService.BrowserTargeting var98 = new DC_ProposalLineItemService.BrowserTargeting();
    DC_ProposalLineItemService.getProposalLineItemsByStatementResponse_element var99 = new DC_ProposalLineItemService.getProposalLineItemsByStatementResponse_element();
    DC_ProposalLineItemService.Browser var100 = new DC_ProposalLineItemService.Browser();
    DC_ProposalLineItemService.ProposalLineItem var101 = new DC_ProposalLineItemService.ProposalLineItem();
    DC_ProposalLineItemService.CustomFieldValueError var102 = new DC_ProposalLineItemService.CustomFieldValueError();
    DC_ProposalLineItemService.performProposalLineItemAction_element var103 = new DC_ProposalLineItemService.performProposalLineItemAction_element();
    DC_ProposalLineItemService.DayPartTargetingError var104 = new DC_ProposalLineItemService.DayPartTargetingError();
    DC_ProposalLineItemService.QuotaError var105 = new DC_ProposalLineItemService.QuotaError();
    DC_ProposalLineItemService.GeoTargetingError var106 = new DC_ProposalLineItemService.GeoTargetingError();
    DC_ProposalLineItemService.DateValue var107 = new DC_ProposalLineItemService.DateValue();
    DC_ProposalLineItemService.getProposalLineItemResponse_element var108 = new DC_ProposalLineItemService.getProposalLineItemResponse_element();
    DC_ProposalLineItemService.MobileDeviceTargeting var109 = new DC_ProposalLineItemService.MobileDeviceTargeting();
    DC_ProposalLineItemService.RangeError var110 = new DC_ProposalLineItemService.RangeError();
    DC_ProposalLineItemService.createProposalLineItems_element var111 = new DC_ProposalLineItemService.createProposalLineItems_element();
    DC_ProposalLineItemService.LabelEntityAssociationError var112 = new DC_ProposalLineItemService.LabelEntityAssociationError();
    DC_ProposalLineItemService.Location var113 = new DC_ProposalLineItemService.Location();
    DC_ProposalLineItemService.DeviceCapability var114 = new DC_ProposalLineItemService.DeviceCapability();
    DC_ProposalLineItemService.FrequencyCap var115 = new DC_ProposalLineItemService.FrequencyCap();
    DC_ProposalLineItemService.ArchiveProposalLineItems var116 = new DC_ProposalLineItemService.ArchiveProposalLineItems();
    DC_ProposalLineItemService.DeliveryIndicator var117 = new DC_ProposalLineItemService.DeliveryIndicator();
    DC_ProposalLineItemService.StatementError var118 = new DC_ProposalLineItemService.StatementError();
    DC_ProposalLineItemService.CreativePlaceholder var119 = new DC_ProposalLineItemService.CreativePlaceholder();
    DC_ProposalLineItemService.ClientLogin var120 = new DC_ProposalLineItemService.ClientLogin();
    DC_ProposalLineItemService.CustomTargetingError var121 = new DC_ProposalLineItemService.CustomTargetingError();
    DC_ProposalLineItemService.TextValue var122 = new DC_ProposalLineItemService.TextValue();
    DC_ProposalLineItemService.MobileCarrier var123 = new DC_ProposalLineItemService.MobileCarrier();
    DC_ProposalLineItemService.performProposalLineItemActionResponse_element var124 = new DC_ProposalLineItemService.performProposalLineItemActionResponse_element();
    DC_ProposalLineItemService.DayPart var125 = new DC_ProposalLineItemService.DayPart();
    DC_ProposalLineItemService.UpdateResult var126 = new DC_ProposalLineItemService.UpdateResult();
    DC_ProposalLineItemService.OperatingSystemVersion var127 = new DC_ProposalLineItemService.OperatingSystemVersion();
    DC_ProposalLineItemService.UnarchiveProposalLineItems upli = new DC_ProposalLineItemService.UnarchiveProposalLineItems();
    DC_ProposalLineItemService.createProposalLineItem_element cplie = new DC_ProposalLineItemService.createProposalLineItem_element();
    
    DC_ProposalLineItemService.ProposalLineItemServiceInterfacePort plisPort = new DC_ProposalLineItemService.ProposalLineItemServiceInterfacePort();
    

    //getProposalLineItemsByStatement
    
    //DC_ProposalLineItemService.ProposalLineItem proposalLineItem=  plisPort.updateProposalLineItem(new DC_ProposalLineItemService.ProposalLineItem());
    //DC_ProposalLineItemService.ProposalLineItem proposalLineItem2 = plisPort.getProposalLineItem(Long.valueOf('12345'));
    //plisPort.createProposalLineItem(new DC_ProposalLineItemService.ProposalLineItem());
    plisPort.createProposalLineItems(new List<DC_ProposalLineItemService.ProposalLineItem>{new DC_ProposalLineItemService.ProposalLineItem()});
    plisPort.updateProposalLineItems(new List<DC_ProposalLineItemService.ProposalLineItem>{new DC_ProposalLineItemService.ProposalLineItem()});
    plisPort.performProposalLineItemAction(pliAction, stat1);
    plisPort.getProposalLineItemsByStatement(stat1);
    Test.stopTest();
  }
}