// 
// (c) 2014 Appirio
//
// Test_DC_DoubleClickProposalLIManager: test class for DC_DoubleClickProposalLIManager 
//
@isTest
private class Test_DC_DoubleClickProposalLIManager {

  static testMethod void myUnitTest() { 
    goog_dclk_dsm__DC_ObjectMapping__c objMapping1 = new goog_dclk_dsm__DC_ObjectMapping__c(goog_dclk_dsm__DC_DoubleClickObjectName__c='PROPOSAL_LINE_ITEM', goog_dclk_dsm__DC_SalesforceObjectName__c = 'OpportunityLineItem', name = 'OpportunityLineItem');
    insert objMapping1;
        
    goog_dclk_dsm__DC_FieldMapping__c fldMapping1 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping2 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'custom.SFDC_OpportunityLineItem_ID', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Id', Name = 'sfid');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping3 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'proposalId', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'OpportunityId', Name = 'ProposalId');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping4 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'probabilityToClose', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'probability', Name = 'Probability');
    insert new List<goog_dclk_dsm__DC_FieldMapping__c>{fldMapping1, fldMapping2, fldMapping3, fldMapping4};
    
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new DC_HttpCalloutMockImploAuth());
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplProductLineItem());
    DC_DoubleClickOAuthHandler authHandler  = new DC_DoubleClickOAuthHandler(userinfo.getUserId());
    DC_DoubleClickProposalLineItemManager plimanager = new DC_DoubleClickProposalLineItemManager(authHandler);
    DC_GenericWrapper gw = plimanager.getProposalLineItemsUpdatedSince(DateTime.now());
    System.assert(gw.size() > 0);
    
    DC_ProposalMapper propMapper = DC_MapperFactory.getInstance().getOpportunityMapper();
    DC_GenericWrapper dcGen; 
    /*=  plimanager.getProposalLineItem(1);
    System.assert(dcGen.size() > 0);
    List<DC_ProposalLineItemService.ProposalLineItem> pliList = (List<DC_ProposalLineItemService.ProposalLineItem>)dcGen.objects;
    System.assertEquals(pliList[0].name, 'testProposalLI1');
    */
    dcGen = plimanager.createProposalLineItems(gw);
    
    List<DC_ProposalLineItemService.ProposalLineItem> pliList = (List<DC_ProposalLineItemService.ProposalLineItem>)dcGen.objects;
    System.assertEquals(pliList[0].name, 'testProposalLI');
    
    plimanager.updateProposalLineItems(dcGen);
    test.stopTest();
  }
}