// 
// (c) 2014 Appirio
//
// Test_DC_DoubleClickCompanyDAO: test class for DC_DoubleClickCompanyDAO 
//
@isTest
private class Test_DC_DoubleClickCompanyDAO {

  static testMethod void myUnitTest() {
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplCompany());
    Test.startTest();
    DC_DoubleClickOAuthHandler oAuthHandler = new DC_DoubleClickOAuthHandler(Userinfo.getUserId());
    
    DC_DoubleClickCompanyDAO companyDAO = new DC_DoubleClickCompanyDAO(oAuthHandler);
    String query = null;
    
    DC_CompanyService.String_ValueMapEntry[] params = new List<DC_CompanyService.String_ValueMapEntry>();
    DC_CompanyService.Statement statement = companyDAO.createStatement(query, params);
    System.assert(statement != null); 
     
    DC_CompanyService.Company company = new DC_CompanyService.Company();
    company.email = 'test@test.com';
    company.id = 123456;
    company.name = 'testComp';
    company.primaryPhone = '111-111-1111';
    company.type_x = 'Advertiser';
    
    DC_CompanyService.Company[] companies = companyDAO.createCompanies(new List<DC_CompanyService.Company>{company});
    
    system.assert(companies != null);
    system.assertEquals(companies.size() , 1);
    system.assertEquals(companies[0].name , 'testCompany');
    
    
    //companyDAO.updateCompany(company);
    
    companyDAO.updateCompanies(new List<DC_CompanyService.Company>{company});
    
    query = 'WHERE Name = \'testComp\'';
    DC_CompanyService.CompanyPage compPage = companyDAO.getCompaniesByStatement(query);
    System.assert(compPage != null);
    System.assertEquals(compPage.totalResultSetSize, 1);
    //DC_CompanyService.Company comp = companyDAO.getCompany(123456, true);
    //System.assert(comp != null);
    //System.assertEquals(comp.Name, 'testCompany');
    
    Test.stopTest();
  }
}