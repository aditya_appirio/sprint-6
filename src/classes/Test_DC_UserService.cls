// 
// (c) 2014 Appirio
//
// Test_DC_UserService : test class for DC_UserService 
//
@isTest
private class Test_DC_UserService {

  static testMethod void testDCUserService() { 
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplUserService());
    Test.startTest(); 
    DC_UserService.CommonError ce = new DC_UserService.CommonError();
    System.assert(ce != null);
    
    DC_UserService.getCurrentUserResponse_element v1 = new DC_UserService.getCurrentUserResponse_element();
    DC_UserService.performUserActionResponse_element wrp = new DC_UserService.performUserActionResponse_element();
    DC_UserService.getUsersByStatementResponse_element wfReq = new DC_UserService.getUsersByStatementResponse_element();
    //DC_UserService.updateUser_element v2 = new DC_UserService.updateUser_element();
    DC_UserService.DateTimeValue dtTmVal = new DC_UserService.DateTimeValue();
    DC_UserService.Statement statement = new DC_UserService.Statement();
    DC_UserService.createUsers_element wra = new DC_UserService.createUsers_element();
    DC_UserService.PublisherQueryLanguageContextError pqlErr = new DC_UserService.PublisherQueryLanguageContextError();
    DC_UserService.getAllRolesResponse_element wfApprovalReq = new DC_UserService.getAllRolesResponse_element();
    DC_UserService.ApiError apiErr = new DC_UserService.ApiError();
    DC_UserService.Date_x dateX = new DC_UserService.Date_x();
    DC_UserService.DropDownCustomFieldValue ddCustFldVal = new DC_UserService.DropDownCustomFieldValue();
    
    DC_UserService.BooleanValue boolVal = new DC_UserService.BooleanValue();
    //DC_UserService.getUser_element usrEle = new DC_UserService.getUser_element();
    DC_UserService.QuotaError ue = new DC_UserService.QuotaError();
    DC_UserService.FeatureError fe = new DC_UserService.FeatureError();
    DC_UserService.AuthenticationError ae = new DC_UserService.AuthenticationError();
    DC_UserService.PermissionError pe = new DC_UserService.PermissionError();
    DC_UserService.PublisherQueryLanguageSyntaxError PQLSyntaxErr = new DC_UserService.PublisherQueryLanguageSyntaxError();
    
    DC_UserService.UserPage quotaErr = new DC_UserService.UserPage();
    DC_UserService.DateValue dtVal = new DC_UserService.DateValue();
    DC_UserService.ApplicationException appExcep = new DC_UserService.ApplicationException();
    DC_UserService.String_ValueMapEntry strValMapEntry = new DC_UserService.String_ValueMapEntry();
    //DC_UserService.createUser_element liOpErr = new DC_UserService.createUser_element();
    
    DC_UserService.InternalApiError iae = new DC_UserService.InternalApiError();
    DC_UserService.Authentication au = new DC_UserService.Authentication();
    DC_UserService.SetValue sv = new DC_UserService.SetValue();
    DC_UserService.performUserAction_element sle = new DC_UserService.performUserAction_element();
    DC_UserService.createUsersResponse_element req = new DC_UserService.createUsersResponse_element();
    DC_UserService.NotNullError nne = new DC_UserService.NotNullError();
    DC_UserService.UpdateResult ur = new DC_UserService.UpdateResult();
    DC_UserService.BaseCustomFieldValue baseCustFldVal = new DC_UserService.BaseCustomFieldValue();
    DC_UserService.updateUsersResponse_element uur = new DC_UserService.updateUsersResponse_element();
    DC_UserService.NumberValue numVal = new DC_UserService.NumberValue();
    DC_UserService.TypeError cl = new DC_UserService.TypeError();
    DC_UserService.DateTime_x dt = new DC_UserService.DateTime_x();
    DC_UserService.StatementError se = new DC_UserService.StatementError();
    DC_UserService.ApiException apiExce = new DC_UserService.ApiException();
    DC_UserService.Value val = new DC_UserService.Value();
    DC_UserService.ApiVersionError v4= new DC_UserService.ApiVersionError();
    DC_UserService.OAuth oAuth = new DC_UserService.OAuth();
    DC_UserService.UserAction usrAction = new DC_UserService.UserAction();
    DC_UserService.updateUsers_element v5 = new DC_UserService.updateUsers_element();
    //DC_UserService.createUserResponse_element v6 = new DC_UserService.createUserResponse_element();
    DC_UserService.SoapResponseHeader srh1 = new DC_UserService.SoapResponseHeader();
    //DC_UserService.getUserResponse_element usrResEle  = new DC_UserService.getUserResponse_element();
    DC_UserService.User_x usr = new DC_UserService.User_x();
    DC_UserService.CustomFieldValueError custFldValErr = new DC_UserService.CustomFieldValueError();
    DC_UserService.getAllRoles_element getRoleEle = new DC_UserService.getAllRoles_element();
    //DC_UserService.updateUserResponse_element usrRes = new DC_UserService.updateUserResponse_element();
    DC_UserService.UserRecord usrRec = new DC_UserService.UserRecord();
    DC_UserService.ParseError parseErr = new DC_UserService.ParseError();
    DC_UserService.InvalidEmailError invalidEmailErr = new DC_UserService.InvalidEmailError();
    DC_UserService.getCurrentUser_element getCurrUsrEle = new DC_UserService.getCurrentUser_element();
    DC_UserService.Role role = new DC_UserService.Role();
    //DC_UserService.ClientLogin clientLogin = new DC_UserService.ClientLogin();
    DC_UserService.DeactivateUsers deactivateUsrs = new DC_UserService.DeactivateUsers();
    DC_UserService.ActivateUsers activateUsrs = new DC_UserService.ActivateUsers();
    DC_UserService.RequiredError reqErr = new DC_UserService.RequiredError();
    DC_UserService.CustomFieldValue custFldVal = new DC_UserService.CustomFieldValue();
    DC_UserService.UniqueError uniqueErr = new DC_UserService.UniqueError();
    DC_UserService.UserServiceInterfacePort port = new DC_UserService.UserServiceInterfacePort();
      System.assert(port != null);
      //port.getUser(1235415245);
      //port.createUser(usr);
      port.performUserAction(usrAction, statement);
      try{
        port.getUsersByStatement(statement);
      }catch(exception e){}
      port.getCurrentUser();
      port.createUsers(new List<DC_UserService.User_x>{usr});
      port.getAllRoles();
      port.updateUsers(new List<DC_UserService.User_x>{usr});
      //port.updateUser(usr);
    DC_UserService.SoapRequestHeader srh = new DC_UserService.SoapRequestHeader();
    DC_UserService.TeamError teamErr = new DC_UserService.TeamError();
    DC_UserService.getUsersByStatement_element getUsrStatement = new DC_UserService.getUsersByStatement_element();
    DC_UserService.ServerError serverErr = new DC_UserService.ServerError();
    DC_UserService.TextValue txtVal = new DC_UserService.TextValue();
    Test.stopTest(); 
  }
}