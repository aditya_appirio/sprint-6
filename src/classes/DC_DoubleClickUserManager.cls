// 
// (c) 2014 Appirio
//
// DC_DoubleClickUserManager
// T-251378 
//
// 20 Feb 2014    Anjali K (JDC)      Original
//
public with sharing class DC_DoubleClickUserManager extends DC_DoubleClickManager {
  
  private DC_UserMapper mapper {
    get {
      if(mapper == null) {
        mapper = DC_UserMapper.getInstance();
      }
      
      return mapper;
    }
    set;
  }
  
  private DC_DoubleClickUserDAO userDAO {
    get {
      if(userDAO == null) {
        userDAO = new DC_DoubleClickUserDAO(this.authHandler);
      }
        
      return userDAO;
    }
    set;
	}

	public DC_DoubleClickUserManager(DC_DoubleClickOAuthHandler authHandler){
    super(authHandler);
	}
	 
	public DC_GenericWrapper getUsersUpdatedSinceLastSync(DateTime basis, long pageSize, long offset) {
		// isactive
  	return mapper.wrap(userDAO.getUsersByStatement('WHERE status=\'Active\' ORDER BY ID LIMIT ' + pageSize + ' OFFSET ' + offset));
  }
    
  public DC_GenericWrapper getUsersUpdatedSinceLastSync(Datetime lastSyncDate) {
  	return getUsersUpdatedSinceLastSync(lastSyncDate, DC_Constants.USER_PAGE_SIZE, 0);
  } 



  public DC_GenericWrapper getUnmatchedUsersByName(String name) {
  	DC_UserService.UserPage cPage = userDAO.getUsersByStatement('WHERE name = \'' + name.replace('\'', '\'\'') + '\' and externalId = \'\'' );
  	if(cPage != null){
  		return mapper.wrap(cPage.results);
  	}else{
  		return null;
  	}
  }
  
  public DC_GenericWrapper getCurrentUser() {
  	return mapper.wrap(userDAO.getCurrentUser());
  }
  
  // TO DO: make sure we fetch pages of data 
  //Doubt: Not sure of this one. Do we limit the no. of users we are getting through getUsersByStatement?
  public override DC_GenericWrapper getDCRecords(DC_GenericWrapper users) {
    if (users.size() == 0)
      return mapper.wrap(new DC_UserService.User_x[] {} );
    
    users.gotoBeforeFirst();
    String ids = '';
    String separator = '';
    List<DC_UserService.User_x> result = new List<DC_UserService.User_x>(); 
    while(users.hasNext()) {
      users.next();
        
      ids += separator + users.getField('DoubleClickId');
      separator = ',';  
    }
    if(userDAO.getUsersByStatement('WHERE id in (' + ids + ')') != null){
      return mapper.wrap(userDAO.getUsersByStatement('WHERE id in (' + ids + ')').results);
    }
    return null;
  }
    
  public override DC_GenericWrapper createDCRecords(DC_GenericWrapper users) {
  	System.debug('users:::in123 ' + users);
    DC_UserService.User_x[] apiUsers = new DC_UserService.User_x[users.size()];
        
    users.gotoBeforeFirst();
    while(users.hasNext()) {
      users.next();

      DC_UserService.User_x user = (DC_UserService.User_x) users.getCurrentObject();
      
      apiUsers[users.getCurrentIndex()] = user;
    }
    return mapper.wrap(userDAO.createUsers(apiUsers));
  }
    
  // update the provided users in DoubleClick
  // users  DC_GenericWrapper   wrapper containing users for update
  public override void updateDCRecords(DC_GenericWrapper users) {
    DC_UserService.User_x[] apiUsers = new DC_UserService.User_x[users.size()];
    
    users.gotoBeforeFirst();
    
    while(users.hasNext()) {
      users.next();
      apiUsers[users.getCurrentIndex()] = (DC_UserService.User_x) users.getCurrentObject();
    }
    
    userDAO.updateUsers(apiUsers);
        
  }


  // ** DEPRECATED for 201403 API ====================================
  /*
  public DC_GenericWrapper getUser(Integer id) {
    return mapper.wrap(userDAO.getUser(id, true));
  }


  public void updateUser(DC_GenericWrapper user) { 
    userDAO.updateUser((DC_UserService.User_x)user.getCurrentObject());
  }  
  */
  // END DEPRECATED ====================================


}