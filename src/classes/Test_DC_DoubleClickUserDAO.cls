// 
// (c) 2014 Appirio
//
// Test_DC_DoubleClickUserDAO : test class for DC_DoubleClickUserDAO 
// 
@isTest
private class Test_DC_DoubleClickUserDAO {

  static testMethod void testDCDoubleClickDAO() {
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplUserService());
    Test.startTest();
    DC_DoubleClickOAuthHandler oAuthHandler = new DC_DoubleClickOAuthHandler(Userinfo.getUserId());
    
    DC_DoubleClickUserDAO userDAO = new DC_DoubleClickUserDAO(oAuthHandler);
    String query = null;
    
    DC_UserService.String_ValueMapEntry[] params = new List<DC_UserService.String_ValueMapEntry>();
    DC_UserService.Statement statement = userDAO.createStatement(query, params);
    System.assert(statement != null);
     
    DC_UserService.User_x userX = new DC_UserService.User_x();
    userX.email = 'test@test.com';
    userX.id = 123456;
    userX.name = 'testComp';
    
    DC_UserService.User_x[] users = userDAO.createUsers(new List<DC_UserService.User_x>{userX});
    
    
    //userDAO.updateUser(userX);
    
    userDAO.updateUsers(new List<DC_UserService.User_x>{userX});
    
    query = 'WHERE Name = \'testUser\'';
    userDAO.getUsersByStatement(query);
    
    userDAO.getCurrentUser();
    //userDAO.getUser(123456, true);
    Test.stopTest();
  }
}