// 
// (c) 2014 Appirio
// 
// Test_DC_DoubleClickProposalDAO: test class for DC_DoubleClickProposalDAO 
//
@isTest
private class Test_DC_DoubleClickProposalDAO {

  static testMethod void testDoubleClickProposalDAO() {
    goog_dclk_dsm__DC_ObjectMapping__c objMapping1 = new goog_dclk_dsm__DC_ObjectMapping__c(goog_dclk_dsm__DC_DoubleClickObjectName__c='Proposal', goog_dclk_dsm__DC_SalesforceObjectName__c = 'Opportunity', name = 'opportunity');
    insert objMapping1;
    goog_dclk_dsm__DC_FieldMapping__c fldMapping3 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping4 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'custom.SFDC_Opportunity_ID', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Id', Name = 'sfid');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping5 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'probabilityToClose', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Probability', Name = 'probability');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping6 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'status', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345,  Name = 'status');
    insert new List<goog_dclk_dsm__DC_FieldMapping__c>{fldMapping3, fldMapping4, fldMapping5, fldMapping6};
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new DC_HttpCalloutMockImploAuth());
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplProposal());
    DC_DoubleClickOAuthHandler oAuthHandler = new DC_DoubleClickOAuthHandler(Userinfo.getUserId());
    
    DC_DoubleClickProposalDAO proposalDAO = new DC_DoubleClickProposalDAO(oAuthHandler);
    String query = null;
    
    DC_ProposalService.String_ValueMapEntry[] params = new List<DC_ProposalService.String_ValueMapEntry>();
    DC_ProposalService.Statement statement = proposalDAO.createProposalStatement(query, params);
    System.assert(statement != null);
    System.assertEquals(statement.query, query);
     
    DC_ProposalService.Proposal proposal = new DC_ProposalService.Proposal();
    proposal.name = 'testProposal';
    proposal.status = 'open';
    
    DC_ProposalService.Proposal[] proposals = proposalDAO.createProposals(new List<DC_ProposalService.Proposal>{proposal});
    system.assert(proposals != null);
    system.assertEquals(proposals.size() , 1);
    system.assertEquals(proposals[0].name , 'testProposal');
     
    proposalDAO.updateProposals(new List<DC_ProposalService.Proposal>{proposal}); 
    
    query = 'WHERE Name = \'testProposal\'';
    DC_ProposalService.ProposalPage proposalPage = proposalDAO.getProposalsByStatement(query);
    System.assert(proposalPage != null);
    System.assertEquals(proposalPage.totalResultSetSize, 2);
    
    
    DC_DoubleClickProposalWrapper pwrap = new DC_DoubleClickProposalWrapper(objMapping1, new List<goog_dclk_dsm__DC_FieldMapping__c>{fldMapping3, fldMapping4, fldMapping5, fldMapping6}, new List<DC_ProposalService.Proposal>{proposal});
    DC_DoubleClickProposalManager pManager = new DC_DoubleClickProposalManager(oAuthHandler);
    DC_GenericWrapper gw = pManager.getProposalsUpdatedSince(System.now());
    System.assert(gw.size() > 0);
    
    
    List<DC_ProposalService.Proposal> proposalList = (List<DC_ProposalService.Proposal>)gw.objects;
    System.assertEquals(2, gw.objects.size());
    System.assertEquals('testProposal', proposalList[0].name); 
    
    gw = pManager.getMatchingApprovedProposals('123456', 'test');
    System.assert(gw.objects.size() > 0);
    
    pManager.getProposals(pwrap);
    pManager.retractProposals(pwrap);
    
    //DC_DoubleClickManager class coverage
    pManager.getDCRecords(pwrap);
    pManager.updateDCRecords(pwrap);
    pManager.createDCRecords(pwrap);
    //DC_DoubleClickManager class coverage
    Test.stopTest();
    try{
      pManager.submitProposalsForApproval(pwrap);
    }catch(exception e){}
  }
  
}