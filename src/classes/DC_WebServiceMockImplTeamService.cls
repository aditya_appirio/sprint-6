// 
// (c) 2014 Appirio
//
// implements WebServiceMock for webServicecallout test classes 
//
// 24 Jan 2014    Anjali K (JDC)      Original
@isTest
global class DC_WebServiceMockImplTeamService implements WebServiceMock{
  global void doInvoke(Object stub, Object request, Map<String, Object> response,
                    String endpoint, String soapAction, String requestName,
                    String responseNS, String responseName, String responseType){
      DC_TeamService.Team team = new DC_TeamService.Team();     
      team.id = 12345;
      team.name = 'testTeam';
      
      DC_TeamService.Team team1 = new DC_TeamService.Team();     
      team1.name = 'testTeam1';
      
      DC_TeamService.TeamPage teamPage = new DC_TeamService.TeamPage();
      teamPage.results = new List<DC_TeamService.Team>{team, team1};
      teamPage.totalResultSetSize = 2;
      teamPage.startIndex = 0;  
                      
      if(request instanceof DC_TeamService.getTeam_element){
          DC_TeamService.getTeamResponse_element resp = new DC_TeamService.getTeamResponse_element();
          resp.rval = team;
          response.put('response_x', resp);
      }else if(request instanceof DC_TeamService.createTeam_element){
          DC_TeamService.createTeamResponse_element resp = new DC_TeamService.createTeamResponse_element();
          resp.rval = team1;
          response.put('response_x', resp);
          
      }else if(request instanceof DC_TeamService.getTeamsByStatement_element){
          DC_TeamService.getTeamsByStatementResponse_element resp = new DC_TeamService.getTeamsByStatementResponse_element();
          resp.rval = teamPage;
          response.put('response_x', resp);
          //response.put('response', new DC_TeamService.getTeamsByStatementResponse_element()); 
      }else if(request instanceof DC_TeamService.getCurrentTeam_element)
          response.put('response_x', new DC_TeamService.getCurrentTeamResponse_element());
      else if(request instanceof DC_TeamService.createTeams_element){
          DC_TeamService.createTeamsResponse_element resp = new DC_TeamService.createTeamsResponse_element();
          resp.rval = new List<DC_TeamService.Team>{team1};
          response.put('response_x', resp);
      }else if(request instanceof DC_TeamService.updateTeams_element) {
          response.put('response_x', new DC_TeamService.updateTeamsResponse_element());
      }    
      else if(request instanceof DC_TeamService.updateTeam_element) {
          response.put('response_x', new DC_TeamService.updateTeamResponse_element());
      } else {
      	throw new DC_TestUtils.DC_MethodNotSupportedInMockException();
      }
          
          
      
      return;
  }
}