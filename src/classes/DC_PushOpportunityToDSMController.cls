// 
// (c) 2014 Appirio
//
// Controller to link Salesforce Opportunities and DoubleClick Proposals
// 
// 28 Jan 2014    Anjali K (JDC)      Original
// 16 Oct 2014    				      Updated        added User Authentication and Admin Authentication logic for 'Push Proposal' , 'Submit Proposal' , 'Retract Proposal'

public with sharing class DC_PushOpportunityToDSMController extends DC_LinkRecordController {
	private final string CREATION_CONTEXT_COMPANY = 'Company';
	private final string CREATION_CONTEXT_OPPORTUNITY = 'Opportunity';
	
  public String currentCreationContext { get; set; }
  public DC_GenericWrapper companyResultWrapper { get; set; }
  public boolean needCompanyCreateAcknowledgement { get; set; }
  private DC_OpportunityProposalConnector connector2 { get; set; } 
  private DC_AccountCompanyConnector connector1 { get; set; } 
  private static string opportunityLabel = Opportunity.sObjectType.getDescribe().getLabel();
   
  public override string getContext() {
  	return 'Push Opportunity to DSM';
  }
   
  // standard apex page controller  
  public DC_PushOpportunityToDSMController(ApexPages.Standardcontroller c) {
    super(c, opportunityLabel, Label.DC_DSM_Proposal);
    currentCreationContext = CREATION_CONTEXT_COMPANY;
    
    //updated 21-oct-2014 try and catch
    try{
	    connector1=DC_AccountCompanyConnector.getInstance();
	    connector2=DC_OpportunityProposalConnector.getInstance();
    }
    catch (Exception e) {
    		System.debug('e:::' + e);
	      if(!ApexPages.hasMessages()){
        	ApexPages.addMessages(e);
        	return;
      	}
  	}
    needCompanyCreateAcknowledgement = false;
  } 
  
  // selected record returned as typed Opportunity record
  public Opportunity selectedOpportunityRecord {
    get {
      return (Opportunity)selectedRecord;
    }
  }
  
  // query for the selected records 
  public override List<sObject> getSelectedRecord(Id recordId) {
    return [
      SELECT id, name, goog_dclk_dsm__DC_AccountDoubleClickId__c, 
             goog_dclk_dsm__DC_DoubleClickId__c, goog_dclk_dsm__DC_ReadyForDSM__c,
             AccountId, Account.goog_dclk_dsm__DC_ReadyForDSM__c,
             Account.Name, goog_dclk_dsm__DC_ProposalStatus__c,
             (SELECT userid, teammemberrole FROM OpportunityTeamMembers WHERE teammemberrole IN ('Primary Sales Rep', 'Primary Trafficker')),
             (SELECT goog_dclk_dsm__dc_role__c FROM goog_dclk_dsm__DoubleClick_Team_Members1__r WHERE goog_dclk_dsm__dc_role__c IN ('Primary Sales Rep', 'Primary Trafficker'))
        FROM Opportunity 
       WHERE id = :recordId
    ];
  }
  
  private boolean accountExistsInDSM() {
  	System.debug('selectedOpportunityRecord::' + selectedOpportunityRecord);
  	return (selectedOpportunityRecord.goog_dclk_dsm__DC_AccountDoubleClickId__c != null);
  } 
      
      
  //Check if record already exists in DSM.  NOTE: this is called as a page action in the VF page as DML cannot occur from the constructor  
  // 3 possible scenarios:
  // - record already linked (determined by examining double click id on record) -- perform update
  // - record not linked, but one matches in DSM -- offer ability to link
  // - record not linked, and no matches -- create record
  public override void checkExistingDSMRecord(){
  	// need to check Account status in DSM.  If non-existent, create.  No UPDATE 
    
    //updated 21-oct-2014 try and catch
    try{
	    if (!meetsOpportunityRequirements()) {
	      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, String.format(Label.DC_Opportunity_Needs_Primary_Trafficker_and_Primary_Sales_Rep, new String[]{sfRecordLabel})));
	      return;
	    }
	
	    DC_GenericWrapper createdCompany;
	  	if (!accountExistsInDSM()) {
	  		createdCompany = pushAccountRecordToDSM();
	      // in this case, linking is likely occurring, or an exception.  
	      // either way, there wasn't a company, and there still isn't a company
	      if (createdCompany == null)
	        return;
	  	}
	  	
	  	if (needCompanyCreateAcknowledgement) {
	  		connector1.executeAllDml();
	  		return;
	  	}
	  	 
	  	
	  	// if the account is in dsm, then go ahead and attempt to create the opportunity
		  if (accountExistsInDSM() || createdCompany != null) {
		    currentCreationContext = CREATION_CONTEXT_OPPORTUNITY;
		    super.checkExistingDSMRecord();
		  }  	
	
	    connector2.executeAllDml();
     }
     catch (Exception e) {
    		System.debug('e:::' + e);
	      if(!ApexPages.hasMessages()){
        	ApexPages.addMessages(e);
        	return;
      	}
     }  	
  }    
  
  public DC_GenericWrapper pushAccountRecordToDSM() {
  	System.debug('selectedOpportunityRecord.Account.goog_dclk_dsm__DC_ReadyForDSM__c::' + selectedOpportunityRecord.Account.goog_dclk_dsm__DC_ReadyForDSM__c);
    if (!selectedOpportunityRecord.Account.goog_dclk_dsm__DC_ReadyForDSM__c) {
    	if(ApexPages.currentPage() != null){
      	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, String.format(Label.DC_Related_Record_Not_Ready_for_DSM, new String[]{Label.DC_SF_Account,Label.DC_DSM_Company,sfRecordLabel,dcRecordLabel })));
    	}else{
    		DC_ConnectorLogger.log(getContext(), new DC_ConnectorException(String.format(Label.DC_Related_Record_Not_Ready_for_DSM, new String[]{Label.DC_SF_Account,Label.DC_DSM_Company,sfRecordLabel,dcRecordLabel })));
    	}
    	return null;     
    } 

    Account newAccount = new Account(id = selectedOpportunityRecord.AccountId, name = selectedOpportunityRecord.Account.Name);
    
    // find any matching companies from dc    
    companyResultWrapper = connector1.findMatchingCompanies(newAccount);
    System.debug('companyResultWrapper:::' + companyResultWrapper);
    // if no companies, create
    if (companyResultWrapper == null || companyResultWrapper.size() == 0) {
    	DC_GenericWrapper createdCompany;
    	try{
    	   createdCompany = connector1.syncSFAccountsToDCCompanies(new List<Account> { newAccount });
    	   System.debug('createdCompany:::' + createdCompany);
    	}
    	catch(System.CalloutException ce){
    		string msg = ce.getMessage();
      System.debug('msg:::' + msg);
	      // if this is a uniqueness exception, provide additional information on how to resolve
	      if (msg.indexOf(DC_Constants.UNIQUE_ERROR) != -1) {
	      	if(ApexPages.currentPage() != null){
	        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, String.format(getUniquenessViolationMessage(), new String[]{ Label.DC_SF_Account, dcRecordLabel})));
	      	}else{
	      		DC_ConnectorLogger.log(getContext(), new DC_ConnectorException(String.format(getUniquenessViolationMessage(), new String[]{ Label.DC_SF_Account, dcRecordLabel})));
	      	}	
	      	escapeMessages = false;
	      } else {
	      	// otherwise display the exception
	      	if(ApexPages.currentPage() != null){
	        	ApexPages.addMessages(ce);
	      	}else{
	      		DC_ConnectorLogger.log(getContext(), ce);
	      	}	
	        
	      }
	      isException = true;
	      return null;
    	}
    	catch (Exception e) {
    		System.debug('e:::' + e);
	      if(ApexPages.currentPage() != null){
        	ApexPages.addMessages(e);
      	}
	      DC_ConnectorLogger.log(getContext(), e);
	      isException = true;
	      return null;
	    }
	    if(ApexPages.currentPage() != null){
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, String.format(Label.DC_Related_SF_Record_Not_Exist,new String[]{ Label.DC_SF_Account, sfRecordLabel, Label.DC_DSM_Company})));  
	    }
    	needCompanyCreateAcknowledgement = true;
    	return createdCompany;
    } else {
    	if(ApexPages.currentPage() != null){
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, String.format(Label.DC_SF_Related_Record_Has_Matching_DC_Record, new String[]{Label.DC_SF_Account,Label.DC_DSM_Company, sfRecordLabel+'(s)',dcRecordLabel+'(s)'})));
    	}
    	return null;
    }
  }
  
  public void acknowledgeCompanyCreateAndContinue() {
  	needCompanyCreateAcknowledgement = false;
  	selectedRecord = getSelectedRecord(selectedRecord.Id)[0];
    // if the account is in dsm, then go ahead and attempt to create the opportunity
	  currentCreationContext = CREATION_CONTEXT_OPPORTUNITY;
	  super.checkExistingDSMRecord();
    	
  } 
  
  // action called by "link" action on list 
  public override void createLink(integer selectedRecordIndex) {
  	System.debug('companyResultWrapper::in:' + companyResultWrapper);
  	if (currentCreationContext == CREATION_CONTEXT_COMPANY) {
  		System.debug('companyResultWrapper::in:' + companyResultWrapper);
	    companyResultWrapper.gotoObject(selectedRecordIndex);
	    connector1.linkAccountsAndCompanies(new List<Account>{new Account(id = selectedOpportunityRecord.AccountId)}, companyResultWrapper);
	    if(!ApexPages.hasMessages()){
      	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, String.format(Label.DC_Record_Linking_Successful,new String[]{ Label.DC_SF_Account, Label.DC_DSM_Company})));
	    }
	    needCompanyCreateAcknowledgement = true;
  	} else {
  		super.createLink(selectedRecordIndex);
  	}
  	connector1.executeAllDml();
  }      

  // check to see if necessary requirements are met -- primary trafficker and primary sales rep
  public boolean meetsOpportunityRequirements() {
    Set<String> roles = new Set<String>();
    Opportunity o = selectedOpportunityRecord;
    List<Id> dcUserIds = new List<Id>();
    for (OpportunityTeamMember otm : o.OpportunityTeamMembers) {
      dcUserIds.add(otm.userId);
    }

    Set<String> sfUserIds = new Set<String>();
    for (goog_dclk_dsm__DC_User__c u : [SELECT goog_dclk_dsm__DC_SalesforceUserRecord__r.Id FROM goog_dclk_dsm__DC_User__c WHERE goog_dclk_dsm__DC_SalesforceUserRecord__r.Id IN :dcUserIds]) {
      sfUserIds.add(u.goog_dclk_dsm__DC_SalesforceUserRecord__r.Id);
    }

    for (OpportunityTeamMember otm : o.OpportunityTeamMembers) {
      if (sfuserids.contains(otm.userId)) {
        roles.add(otm.TeamMemberRole);
      }
    }

    for (goog_dclk_dsm__DC_DoubleClickTeamMember__c dctms : o.goog_dclk_dsm__DoubleClick_Team_Members1__r) {
      roles.add(dctms.goog_dclk_dsm__dc_role__c);
    }

    return roles.contains('Primary Trafficker') && roles.contains('Primary Sales Rep');
  }
  
  // execute the connector's find matching Opportunitys methods
  public override DC_GenericWrapper executeConnectorGetMatchingRecords() {
  	//updated 16-oct-2014
    return connector2.findMatchingProposals(selectedOpportunityRecord,'Push Proposal');
  }
  
  // execute the connector's find matching Opportunitys methods
  public DC_GenericWrapper executeConnectorGetMatchingApprovedRecords() {
  	//updated 16-oct-2014
    return connector2.findMatchingApprovedProposals(selectedOpportunityRecord,'Push Proposal');
  }
  
  // execute the connector's sync existing records actions
  public override void executeConnectorSyncExisting() {
  	//updated 21-oct-2014 try catch
  	try{
    	List<Opportunity> retracted = connector2.syncSFOpportunitiesToDCProposals(new Opportunity[] { selectedOpportunityRecord },'Push Proposal');
	    if (retracted.size() > 0) {
	      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 
	        String.format(Label.DC_Approval_Retracted, new String[]{ sfRecordLabel, dcRecordLabel})));
	    } 
  	}
  	catch (Exception e) {
    		System.debug('e:::' + e);
	      if(!ApexPages.hasMessages()){
        	ApexPages.addMessages(e);
        	return;
      	}
  	}
  }
  
  // execute the connector's sync method to create records
  public override void executeConnectorSync() {
  	//updated 21-oct-2014 try and catch
  	try{
    connector2.syncSFOpportunitiesToDCProposals(new Opportunity[] { selectedOpportunityRecord },'Push Proposal');
    }
    catch (Exception e) {
    		System.debug('e:::' + e);
	      if(!ApexPages.hasMessages()){
        	ApexPages.addMessages(e);
        	return;
      	}
  	}
  }
    
  //If SFDC Account exist in DSM but SFDC Account does not has value in goog_dclk_dsm__DC_DoubleClickId__c field then populate goog_dclk_dsm__DC_DoubleClickId__c with Company id
  public override void executeConnectorLink(DC_GenericWrapper wrapper){
  	//updated 16-oct-2014
    // create link between account record and wrapper
    //updated 21-oct-2014 try and catch
    try{
    connector2.linkOpportunityAndProposal(selectedOpportunityRecord, wrapper,'Push Proposal');
    }
    catch (Exception e) {
    		System.debug('e:::' + e);
	      if(ApexPages.hasMessages() != null){
        	ApexPages.addMessages(e);
      	}
  	}
  }     
  
  public override String getLinkMessage() {
    return Label.DC_SF_Opportunity_has_Matching_DC_Proposal;
  }

  public override String getUniquenessViolationMessage() {
    return Label.DC_Proposal_Uniqueness_Violation_Message;
  } 
}