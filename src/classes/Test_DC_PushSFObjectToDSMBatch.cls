/**
 */
@isTest
private class Test_DC_PushSFObjectToDSMBatch {
	
	static Account a1;
	
	static testMethod void testDC_PushOpportunityToDSMBatch() {
	createTestData();
	Opportunity oppty1 = new Opportunity(name = 'testProposal1', AccountId = a1.id, StageName = 'Closed/Won', CloseDate = date.today());
    oppty1.goog_dclk_dsm__DC_ReadyForDSM__c = true;
    insert oppty1;
    
    //updated  27-oct-2014
  	goog_dclk_dsm__DC_Authentication__c objDC_Authentication = new goog_dclk_dsm__DC_Authentication__c(goog_dclk_dsm__DC_AccessToken__c ='1324324',goog_dclk_dsm__DC_ExpiresIn__c = 3600 ,goog_dclk_dsm__DC_RefreshToken__c = '454655656',goog_dclk_dsm__DC_TokenType__c = 'Bearer');
    insert objDC_Authentication;
	    
    goog_dclk_dsm__DC_ObjectMapping__c objMapping1 = new goog_dclk_dsm__DC_ObjectMapping__c(goog_dclk_dsm__DC_DoubleClickObjectName__c='Proposal', goog_dclk_dsm__DC_SalesforceObjectName__c = 'Opportunity', name = 'opportunity');
    insert new List<goog_dclk_dsm__DC_ObjectMapping__c>{objMapping1};
    
    goog_dclk_dsm__DC_FieldMapping__c fldMapping1 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId');
    goog_dclk_dsm__DC_FieldMapping__c fldMapping2 = new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'custom.SFDC_Opportunity_ID', goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345, goog_dclk_dsm__DC_SalesforceFieldName__c = 'Id', Name = 'sfid');
    insert new List<goog_dclk_dsm__DC_FieldMapping__c>{fldMapping1, fldMapping2};
    
    Test.startTest();
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplProposal());
    DC_PushOpportunityToDSMBatch opptyBatch = new DC_PushOpportunityToDSMBatch();
    
    String query = 'Select Id from Opportunity where goog_dclk_dsm__DC_DoubleClickId__c = null AND goog_dclk_dsm__DC_ReadyForDSM__c = true';
    opptyBatch = new DC_PushOpportunityToDSMBatch(query);
    opptyBatch.getContext();
    opptyBatch.executeSync(new List<Opportunity>{oppty1});
    opptyBatch = new DC_PushOpportunityToDSMBatch(true);
    opptyBatch.getDefaultQuery();
    Test.stopTest();
	}
	@isTest
	static void testDC_PushContactToDSMBatch() {
	createTestData();
	Contact cnct = new Contact(AccountId = a1.id, FirstName = 'fname1', LastName = 'lname1');
    insert cnct; 
    goog_dclk_dsm__DC_ObjectMapping__c objMapping1 = new goog_dclk_dsm__DC_ObjectMapping__c(goog_dclk_dsm__DC_DoubleClickObjectName__c='Contact', goog_dclk_dsm__DC_SalesforceObjectName__c = 'Contact', name = 'contact');
    insert new List<goog_dclk_dsm__DC_ObjectMapping__c>{objMapping1};
    //updated 27-oct-2014
  	goog_dclk_dsm__DC_Authentication__c objDC_Authentication = new goog_dclk_dsm__DC_Authentication__c(goog_dclk_dsm__DC_AccessToken__c ='1324324',goog_dclk_dsm__DC_ExpiresIn__c = 3600 ,goog_dclk_dsm__DC_RefreshToken__c = '454655656',goog_dclk_dsm__DC_TokenType__c = 'Bearer');
    insert objDC_Authentication;
	    
    List<goog_dclk_dsm__DC_FieldMapping__c> fldMappingList = new List<goog_dclk_dsm__DC_FieldMapping__c>();
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'name', goog_dclk_dsm__DC_SalesforceFieldName__c = 'FirstName', Name = 'name'));
    insert fldMappingList;
    
    Test.startTest();
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplContact());
    DC_PushContactToDSMBatch cnctBatch = new DC_PushContactToDSMBatch();
    
    String query = 'Select Id from Contact where goog_dclk_dsm__DC_DoubleClickId__c = null AND goog_dclk_dsm__DC_ReadyForDSM__c = true';
    cnctBatch = new DC_PushContactToDSMBatch(query);
    cnctBatch.getContext();
    cnctBatch.executeSync(new List<Contact>{cnct});
    cnctBatch = new DC_PushContactToDSMBatch(true);
    cnctBatch.getDefaultQuery();
    Test.stopTest();
	}
	
	static testMethod void testDC_PushAccountToDSMBatch() {
	createTestData();
	goog_dclk_dsm__DC_ObjectMapping__c objMapping1 = new goog_dclk_dsm__DC_ObjectMapping__c(goog_dclk_dsm__DC_DoubleClickObjectName__c='Company', goog_dclk_dsm__DC_SalesforceObjectName__c = 'Account', name = 'account');
    insert new List<goog_dclk_dsm__DC_ObjectMapping__c>{objMapping1};
    //updated 27-oct-2014
  	goog_dclk_dsm__DC_Authentication__c objDC_Authentication = new goog_dclk_dsm__DC_Authentication__c(goog_dclk_dsm__DC_AccessToken__c ='1324324',goog_dclk_dsm__DC_ExpiresIn__c = 3600 ,goog_dclk_dsm__DC_RefreshToken__c = '454655656',goog_dclk_dsm__DC_TokenType__c = 'Bearer');
    insert objDC_Authentication;
	    
    List<goog_dclk_dsm__DC_FieldMapping__c> fldMappingList = new List<goog_dclk_dsm__DC_FieldMapping__c>();
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = objMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'externalId', goog_dclk_dsm__DC_SalesforceFieldName__c = 'id', Name = 'sfid'));
    insert fldMappingList;
    
    Test.startTest();
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplCompany());
    DC_PushAccountToDSMBatch accBatch = new DC_PushAccountToDSMBatch();
    
    String query = 'Select Id from Account where goog_dclk_dsm__DC_DoubleClickId__c = null AND goog_dclk_dsm__DC_ReadyForDSM__c = true';
    accBatch = new DC_PushAccountToDSMBatch(query);
    accBatch.getContext();
    accBatch.executeSync(new List<Account>{a1});
    accBatch = new DC_PushAccountToDSMBatch(true);
    Test.stopTest();
	}
	
	static testMethod void testDC_PushUserToDSMBatch() {
		
	//updated 27-oct-2014
  	goog_dclk_dsm__DC_Authentication__c objDC_Authentication = new goog_dclk_dsm__DC_Authentication__c(goog_dclk_dsm__DC_AccessToken__c ='1324324',goog_dclk_dsm__DC_ExpiresIn__c = 3600 ,goog_dclk_dsm__DC_RefreshToken__c = '454655656',goog_dclk_dsm__DC_TokenType__c = 'Bearer');
    insert objDC_Authentication;
    
	User u = [Select id from User where id =:Userinfo.getUserId()];
	
	goog_dclk_dsm__DC_ObjectMapping__c dcObjMapping1 = new goog_dclk_dsm__DC_ObjectMapping__c();
  	dcObjMapping1.goog_dclk_dsm__DC_DoubleClickObjectName__c = 'User';
  	dcObjMapping1.Name = 'User';
  	dcObjMapping1.goog_dclk_dsm__DC_SalesforceObjectName__c = 'goog_dclk_dsm__DC_User__c';
  	
  	goog_dclk_dsm__DC_ObjectMapping__c dcObjMapping2 = new goog_dclk_dsm__DC_ObjectMapping__c();
  	dcObjMapping2.goog_dclk_dsm__DC_DoubleClickObjectName__c = 'User';
  	dcObjMapping2.Name = 'User Batch';
  	dcObjMapping2.goog_dclk_dsm__DC_SalesforceObjectName__c = 'User';
  	insert new List<goog_dclk_dsm__DC_ObjectMapping__c>{dcObjMapping1, dcObjMapping2};
  	
    List<goog_dclk_dsm__DC_FieldMapping__c> fldMappingList = new List<goog_dclk_dsm__DC_FieldMapping__c>();
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping2.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping2.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'name', goog_dclk_dsm__DC_SalesforceFieldName__c = 'name', Name = 'name'));
    
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'id', goog_dclk_dsm__DC_SalesforceFieldName__c = 'goog_dclk_dsm__DC_DoubleClickId__c', Name = 'DoubleClickId'));
    fldMappingList.add(new goog_dclk_dsm__DC_FieldMapping__c(goog_dclk_dsm__DC_Object_Mapping__c = dcObjMapping1.id, goog_dclk_dsm__DC_DoubleClickFieldName__c = 'externalId', goog_dclk_dsm__DC_SalesforceFieldName__c = 'id', Name = 'sfid'));
    insert fldMappingList;
    
    Test.startTest();
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplUserService());
    DC_PushUserToDSMBatch userBatch = new DC_PushUserToDSMBatch();
    
    String query = 'Select Id from User limit 1';
    userBatch = new DC_PushUserToDSMBatch(query);
    userBatch.getContext();
    userBatch.executeSync(new List<User>{u});
    userBatch = new DC_PushUserToDSMBatch(true);
    Test.stopTest();
	}
	static void createTestData(){
		a1 = new Account(name = 'test1');
	    a1.goog_dclk_dsm__DC_ReadyForDSM__c = true;
	    insert a1;
	}
}