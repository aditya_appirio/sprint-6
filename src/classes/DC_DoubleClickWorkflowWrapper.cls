public with sharing class DC_DoubleClickWorkflowWrapper extends DC_DoubleClickWrapper { 

  public DC_DoubleClickWorkflowWrapper(goog_dclk_dsm__DC_ObjectMapping__c objectMapping, List<goog_dclk_dsm__DC_FieldMapping__c> fieldMappings, List<Object> objects) {
    super(objectMapping, fieldMappings, objects);
  }

  public DC_WorkflowService.WorkflowRequest getCurrentWorkflowRequest() {
    if(this.currentObject == null){
      throw new DC_ConnectorException(DC_Constants.ARR_OUT_OF_BOUND_ERR);
    } else if(!(this.currentObject instanceOf DC_WorkflowService.WorkflowRequest)){
        // TODO: Jaipur please create constant with message for invalid workflow request mapping
        throw new DC_ConnectorException(DC_Constants.INVALID_COMPANY_MAPPING);
    } else{
        return (DC_WorkflowService.WorkflowRequest) this.currentObject;
    }
  }

  public override Object getField(String field) {
  	System.debug('field::' + field);
    String fieldName = getDoubleClickFieldName(field);
  	System.debug('fieldName::' + fieldName);
    if(fieldName != null && fieldName.equalsIgnoreCase('id')) {
      return this.getCurrentWorkflowRequest().id;
    } else if (fieldName != null && fieldName.equalsIgnoreCase('workflowRuleName')) {
      return this.getCurrentWorkflowRequest().workflowRuleName;
    } else if (fieldName != null && fieldName.equalsIgnoreCase('entityId')) {
      return this.getCurrentWorkflowRequest().entityId;
    } else if (fieldName != null && fieldName.equalsIgnoreCase('entityType')) {
      return this.getCurrentWorkflowRequest().entityType;
    } else if (fieldName != null && fieldName.equalsIgnoreCase('status')) {
      return this.getCurrentWorkflowRequest().status;
    } else {
      // TODO: Jaipur please create method to generate message for workflow request
      throw new DC_ConnectorException(DC_Constants.getInvalid_Mapping_Company(field));
    }
  }

  public override void setField(String field, Object value) {
    String fieldName = getDoubleClickFieldName(field);

    if(fieldName.equalsIgnoreCase('id')) {
      this.getCurrentWorkflowRequest().id = toLong(value);
    } else if (fieldName.equalsIgnoreCase('workflowRuleName')) {
      this.getCurrentWorkflowRequest().workflowRuleName = toString(value);
      this.getCurrentWorkflowRequest().type_x = 'WORKFLOW_APPROVAL_REQUEST';
    } else if (fieldName.equalsIgnoreCase('entityId')) {
      this.getCurrentWorkflowRequest().entityId = toLong(value);
    } else if (fieldName.equalsIgnoreCase('entityType')) {
      this.getCurrentWorkflowRequest().entityType = toString(value);
    } else if (fieldName.equalsIgnoreCase('status')) {
      this.getCurrentWorkflowRequest().status = toString(value);
    } else {
      // TODO: Jaipur please create method to generate message for workflow request
      throw new DC_ConnectorException(DC_Constants.getInvalid_Mapping_Company(field));
    }
  }

  public override DC_GenericWrapper getNewRecords() {
    this.gotoBeforeFirst();
    List<Object> result = new List<Object>();
    
    while(this.hasNext()) {
      this.next();
      
      if(this.getField('DoubleClickId') == null) {
        result.add(this.currentObject);
      }
    }
    
    return new DC_DoubleClickWorkflowWrapper(this.objectMapping, this.fieldMappings, result);
  }
  
  public override DC_GenericWrapper getExistingRecords() {
    this.gotoBeforeFirst();
    List<Object> result = new List<Object>();
    
    while(this.hasNext()) {
      this.next();
      
      if(this.getField('DoubleClickId') != null) {
        result.add(this.currentObject);
      }
    }
    
    return new DC_DoubleClickWorkflowWrapper(this.objectMapping, this.fieldMappings, result);
  }

  public override DC_GenericWrapper getCurrentWrapped() {
    return new DC_DoubleClickWorkflowWrapper(this.objectMapping, this.fieldMappings, new object[] { this.currentObject });
  }
   
  public override DC_GenericWrapper createWrapper(goog_dclk_dsm__DC_ObjectMapping__c objectMapping, List<goog_dclk_dsm__DC_FieldMapping__c> fieldMappings, List<Object> objects) {
    return new DC_DoubleClickWorkflowWrapper(objectMapping, fieldMappings, objects);
  }

}