// 
// (c) 2014 Appirio
//
// Controller for managing account updates in DCLK based on trigger
//
//
public with sharing class DC_AccountTriggerController {

  // handle after update accounts action
  // updateAccounts  List<Account>  accounts that were updated
  public static void performExistingAccountSync(List<Account> updatedAccounts) {
    List<Id> accountIds = new List<Id>();
    
    for(Account account : updatedAccounts) { 
      if (account.goog_dclk_dsm__DC_DoubleClickId__c != null) {
        accountIds.add(account.Id);
      }
    }
 
    if (accountIds.size() > 0) {
      DC_AccountTriggerController.syncExistingAccounts(accountIds);
    }
  } 
 
  // sync existing accounts with DoubleClick
  // note that this is called as an async @future call as triggers cannot perform callouts
  // accountIds   List<Id>  ids of accounts to sync.
  @future(callout=true)
  public static void syncExistingAccounts(List<Id> accountIds) {
  	
    DC_AccountCompanyConnector connector = DC_AccountCompanyConnector.getInstance();
    
    connector.syncSFAccountsToDCCompanies([SELECT Id from Account where id in :accountIds]);
  }
  
}