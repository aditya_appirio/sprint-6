public with sharing class DC_QueueableProcessMock extends DC_QueueableBatchProcess{
	public DC_QueueableProcessMock(){
		
	}
	
	public override void initialize(DC_QueueableProcessParameter inParams){
		
	}
  public override long getRecordsToProcessPerPage(){
  	return 1;
  }
  public override DateTime getBasisDate(){
  	return dateTime.now();
  }
  public override string getResultString(){
  	return 'result';
  }
  public override void updateSyncDate(DateTime dt){
  	
  }
  public override DC_Connector.DC_DoubleClickPageProcessingResult processRecords(long pageSize, long pageNo){
  	DC_SalesforceDML instance = DC_SalesforceDML.getInstance();
  	Account a = new Account(name = 'test');
  	DC_SalesforceDML instancedml = DC_SalesforceDML.getInstance();
		DC_SalesforceDML.DC_SalesforceDMLStatus dmlStatus = instance.registerInsert(a);
  	DC_Connector.DC_DoubleClickPageProcessingResult res = new DC_Connector.DC_DoubleClickPageProcessingResult();
  	res.totalResultSetSize = 2;
  	res.dmlStatus = dmlStatus;
  	return res;
  }
  public override void execute(){
  	
  }
  public override System.Type getParameterType(){
  	return null;
  }
	public class DC_RetrieveUsersParams extends DC_QueueableProcess.DC_QueueableProcessParameter { }
}