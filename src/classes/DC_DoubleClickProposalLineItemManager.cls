// 
// (c) 2014 Appirio
//
// DC_DoubleClickProposalLineItemManager
// T-249484 : Create Proposal Lines classes 
// 
// 13 Feb 2014    Ankit Goyal (JDC)      Copied from DC_DoubleClickProposalManager
//
public with sharing class DC_DoubleClickProposalLineItemManager extends DC_DoubleClickManager{

  private DC_ProposalLineItemMapper mapper {
    get {
      if(mapper == null) {
        mapper = DC_ProposalLineItemMapper.getInstance();
      }
      
      return mapper;
    }
    set;
  } 
  
  private DC_DoubleClickProposalLineItemDAO proposalLI_DAO {
    get {
        if(proposalLI_DAO == null) { 
            proposalLI_DAO = new DC_DoubleClickProposalLineItemDAO(this.authHandler);
        } 
        
        return proposalLI_DAO;
    }
    set;
  }

  public DC_DoubleClickProposalLineItemManager(DC_DoubleClickOAuthHandler authHandler){
    super(authHandler);
  }
    
  public DC_GenericWrapper getProposalLineItemsUpdatedSince(Datetime lastSyncDate) {
    return mapper.wrap(proposalLI_DAO.getProposalLineItemsByStatement('WHERE lastModifiedDateTime >= \'' + proposalLI_DAO.formatDateForPQL(lastSyncDate) + '\' ORDER BY ID LIMIT 10 OFFSET 0').results);
  } 
    

    
  // TO DO: make sure we fetch pages of data 
  //Doubt: Not sure of this one. Do we limit the no. of companies we are getting through getCompaniesByStatement?
  public DC_GenericWrapper getProposalLineItems(DC_GenericWrapper proposals) {
    if (proposals.size() == 0)
      return mapper.wrap(new DC_ProposalLineItemService.ProposalLineItem[] {} );
          
    proposals.gotoBeforeFirst();
    String ids = '';
    String separator = '';
    List<DC_ProposalLineItemService.ProposalLineItem> result = new List<DC_ProposalLineItemService.ProposalLineItem>(); 
    
    while(proposals.hasNext()) {
        proposals.next();
        
      ids += separator + proposals.getField('DoubleClickId');
      separator = ',';  
    }
    return mapper.wrap(proposalLI_DAO.getProposalLineItemsByStatement('WHERE id in (' + ids + ')').results);
  }
    
  public DC_GenericWrapper createProposalLineItems(DC_GenericWrapper proposals) {
    DC_ProposalLineItemService.ProposalLineItem[] apiProposals = new DC_ProposalLineItemService.ProposalLineItem[proposals.size()];
        
    proposals.gotoBeforeFirst();
    while(proposals.hasNext()) {
      proposals.next();

      DC_ProposalLineItemService.ProposalLineItem proposal = (DC_ProposalLineItemService.ProposalLineItem) proposals.getCurrentObject();
      
      //proposal.type_x = 'ADVERTISER';

      apiProposals[proposals.getCurrentIndex()] = proposal;
    }
    return mapper.wrap(proposalLI_DAO.createProposalLineItems(apiProposals));
  }
    
  // update the provided companies in DoubleClick
  // companies  DC_GenericWrapper   wrapper containing companies for update
  public void updateProposalLineItems(DC_GenericWrapper proposals) {
    DC_ProposalLineItemService.ProposalLineItem[] apiProposals = new DC_ProposalLineItemService.ProposalLineItem[proposals.size()];
    
    proposals.gotoBeforeFirst();
    
    while(proposals.hasNext()) {
      proposals.next();
      apiProposals[proposals.getCurrentIndex()] = (DC_ProposalLineItemService.ProposalLineItem) proposals.getCurrentObject();
    }
    
    proposalLI_DAO.updateProposalLineItems(apiProposals);
      
  }


  // get proposals matched by name OR email to provide linking capability
  // String 
  public DC_GenericWrapper getMatchingProposalLineItems(String companyId, String name) {
    string statement = String.format('WHERE name = \'\'{1}\'\'', new String[] { companyId, name.replace('\'', '\'\'') } );

    // wrap and return results
    return mapper.wrap(proposalLI_DAO.getProposalLineItemsByStatement(statement).results);
  }
  
  public DC_GenericWrapper getProposalLinesUpdatedSince(DateTime basis, long pageSize, long offset) { 
    return mapper.wrap(proposalLI_DAO.getProposalLineItemsByStatement('WHERE lastModifiedDateTime > \'' + proposalLI_DAO.formatDateForPQL(basis) + '\' ORDER BY Id LIMIT ' + pageSize + ' OFFSET ' + offset));
  }
  // ** DEPRECATED for 201403 API ====================================
  /*
    
  public void updateProposalLineItem(DC_GenericWrapper proposal) {
    proposalLI_DAO.updateProposalLineItem((DC_ProposalLineItemService.ProposalLineItem)proposal.getCurrentObject());
  }

  public DC_GenericWrapper getProposalLineItem(Integer id) {
    return mapper.wrap(proposalLI_DAO.getProposalLineItem(id, true)); 
  }	
  */
  // END DEPRECATED ====================================

}