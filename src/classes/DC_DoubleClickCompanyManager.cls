// 
// (c) 2014 Appirio
//
// Called from DC_ConnectorAdminPageController
// DC Connector
// T-238071
// 17 Jan 2014   Anjali K (JDC)      Original
//

public with sharing class DC_DoubleClickCompanyManager extends DC_DoubleClickManager {
  
  private DC_CompanyMapper mapper {
    get {
      if(mapper == null) {
        mapper = DC_CompanyMapper.getInstance();
      }
      
      return mapper;
    }
    set;
  }
  
  private DC_DoubleClickCompanyDAO companyDAO {
    get {
      if(companyDAO == null) {
        companyDAO = new DC_DoubleClickCompanyDAO(this.authHandler);
      }
        
      return companyDAO;
    }
    set;
  }

  public DC_DoubleClickCompanyManager(DC_DoubleClickOAuthHandler authHandler){
    super(authHandler);
  }
    
  public DC_GenericWrapper getCompaniesUpdatedSinceLastSync() {
    
    //Datetime lastSync = DC_SyncDateManager.companyLastSyncDate;
    
    return mapper.wrap(companyDAO.getCompaniesByStatement('WHERE lastModifiedDateTime >= \'' + companyDAO.formatDateForPQL(DateTime.now()) + '\' ORDER BY ID LIMIT 10 OFFSET 0').results);
  }
    


  public DC_GenericWrapper getUnmatchedCompaniesByName(String name) {
    DC_CompanyService.CompanyPage cPage =companyDAO.getCompaniesByStatement('WHERE name = \'' + name.replace('\'', '\'\'') + '\' and externalId = \'\'' );

    if(cPage != null){
    	System.debug(':::mapper.wrap(cPage.results):::' + mapper.wrap(cPage.results));
      return mapper.wrap(cPage.results);
    }else{
      return null;
    }
  }
  
  //Doubt: Not sure of this one. Do we limit the no. of companies we are getting through getCompaniesByStatement?
  public override DC_GenericWrapper getDCRecords(DC_GenericWrapper companies) {
    System.debug('>>>>getCompanies of Comp manager');
    if (companies.size() == 0)
      return mapper.wrap(new DC_CompanyService.Company[] {} );
    
    companies.gotoBeforeFirst();
    String ids = '';
    String separator = '';
    List<DC_CompanyService.Company> result = new List<DC_CompanyService.Company>(); 
    while(companies.hasNext()) {
      companies.next();
        
      ids += separator + companies.getField('DoubleClickId');
      separator = ',';  
    }
    if(companyDAO.getCompaniesByStatement('WHERE id in (' + ids + ')') != null){
      return mapper.wrap(companyDAO.getCompaniesByStatement('WHERE id in (' + ids + ')').results);
    }
    return null;
  }
    
  public override DC_GenericWrapper createDCRecords(DC_GenericWrapper companies) {
    DC_CompanyService.Company[] apiCompanies = new DC_CompanyService.Company[companies.size()];
        
    companies.gotoBeforeFirst();
    while(companies.hasNext()) {
      companies.next();

      DC_CompanyService.Company company = (DC_CompanyService.Company) companies.getCurrentObject();
      
      apiCompanies[companies.getCurrentIndex()] = company;
    }
    return mapper.wrap(companyDAO.createCompanies(apiCompanies));
  }
    
  // update the provided companies in DoubleClick
  // companies  DC_GenericWrapper   wrapper containing companies for update
  public override void updateDCRecords(DC_GenericWrapper companies) {
    DC_CompanyService.Company[] apiCompanies = new DC_CompanyService.Company[companies.size()];
    
    companies.gotoBeforeFirst();
    
    while(companies.hasNext()) {
      companies.next();
      apiCompanies[companies.getCurrentIndex()] = (DC_CompanyService.Company) companies.getCurrentObject();
    }
    
    companyDAO.updateCompanies(apiCompanies);
  }

  // ** DEPRECATED for 201403 API ====================================
  /*
  public void updateCompany(DC_GenericWrapper company) {
    companyDAO.updateCompany((DC_CompanyService.Company)company.getCurrentObject());
  }
  
  public DC_GenericWrapper getCompany(Integer id) {
    return mapper.wrap(companyDAO.getCompany(id, true));
  }
  */
  // END DEPRECATED ====================================

}