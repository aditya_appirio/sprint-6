// 
// (c) 2014 Appirio
//
// Test_DC_DoubleClickContactDAO: test class for DC_DoubleClickContactDAO 
//
@isTest
private class Test_DC_DoubleClickContactDAO {

  static testMethod void testDoubleClickContactDAO() {
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplContact());
    Test.startTest();
    DC_DoubleClickOAuthHandler oAuthHandler = new DC_DoubleClickOAuthHandler(Userinfo.getUserId());
    
    DC_DoubleClickContactDAO contactDAO = new DC_DoubleClickContactDAO(oAuthHandler);
    String query = null;
     
    DC_ContactService.Contact contact = new DC_ContactService.Contact();
    contact.companyId = 123456;
    contact.Title = 'test';
    contact.email = 'test@test.com';
    contact.id = 123456;
    contact.name = 'testContact';
    DC_ContactService.Contact[] contacts = contactDAO.createContacts(new List<DC_ContactService.Contact>{contact});
    
    system.assert(contacts != null);
    system.assertEquals(1, contacts.size());
    system.assertEquals('fname1 lname1', contacts[0].name);
    
    System.assert(contact != null);
    
    contactDAO.updateContacts(new List<DC_ContactService.Contact>{contact});
    
    query = 'WHERE Name = \'testContact\'';
    DC_ContactService.ContactPage conPage = contactDAO.getContactsByStatement(query);
    
    System.assert(conPage != null);
    System.assertEquals(conPage.totalResultSetSize, 1);
    
    DC_DoubleClickDAO dao = new DC_DoubleClickContactDAO(oAuthHandler);
    DC_ContactService.String_ValueMapEntry[] params = new List<DC_ContactService.String_ValueMapEntry>();
    DC_ContactService.Statement statement = dao.createContactStatement(query, params );
		System.assertEquals(statement.query, query);
    Test.stopTest();
  }
}