// 
// (c) 2014 Appirio
//
// WebServiceMockImpl_Proposal : implements WebServiceMock for webServicecallout test classes 
//
// 24 Jan 2014    Anjali K (JDC)      Original
//
@IsTest 
global class DC_WebServiceMockImplProposal implements WebServiceMock{
  global void doInvoke(Object stub, Object request, Map<String, Object> response,
                    String endpoint, String soapAction, String requestName,
                    String responseNS, String responseName, String responseType){
      
      DC_CustomFieldService.CustomField custFld = new DC_CustomFieldService.CustomField();
      custFld.name = 'custFld1';
      custFld.id = 12345;
      custFld.CustomField_Type = 'Integer';
      
      DC_CustomFieldService.CustomField custFld1 = new DC_CustomFieldService.CustomField();
      custFld1.name = 'custFld2';
      custFld1.CustomField_Type = 'Integer';
      
      DC_CustomFieldService.CustomFieldPage custFldPage = new DC_CustomFieldService.CustomFieldPage();
      custFldPage.startIndex = 0;
      custFldPage.results = new List<DC_CustomFieldService.CustomField>{custFld, custFld1};
      custFldPage.totalResultSetSize = 2;
      
      DC_CustomFieldService.CustomFieldOption custFldOption = new DC_CustomFieldService.CustomFieldOption();
      custFldOption.displayName = 'custFldOpt1';
      custFldOption.id = 12345;
      custFldOption.customFieldId = 12345;
      
      DC_CustomFieldService.CustomFieldOption custFldOption1 = new DC_CustomFieldService.CustomFieldOption();
      custFldOption1.displayName = 'custFldOpt2';
      custFldOption1.customFieldId = 12345;
      
      DC_ProposalService.Value bVal = new DC_ProposalService.BooleanValue();
	    bVal.value = 'true';
	    bVal.value_type = 'BooleanValue';
	    
	    DC_ProposalService.Value textVal = new DC_ProposalService.TextValue();
	    textVal.value = 'test';
	    textVal.value_type = 'TextValue';
	    DC_ProposalService.BaseCustomFieldValue custFldValue = new DC_ProposalService.CustomFieldValue();
	    custFldValue.customFieldId = 12345;
	    custFldValue.BaseCustomFieldValue_Type = 'CustomFieldValue';
	    custFldValue.value = bVal;
	    
	    DC_ProposalService.BaseCustomFieldValue dropDownCustFldValue = new DC_ProposalService.DropDownCustomFieldValue();
	    dropDownCustFldValue.customFieldId = 123456;
	    dropDownCustFldValue.BaseCustomFieldValue_Type = 'DropDownCustomFieldValue';
	    dropDownCustFldValue.value = textVal;
	    
      DC_CompanyService.Company company = new DC_CompanyService.Company();     
	    company.id = 12345;
	    company.name = 'testCompany';
	    company.externalId = '';
	    
	    DC_CompanyService.CompanyPage companyPage = new DC_CompanyService.CompanyPage();
	    companyPage.results = new List<DC_CompanyService.Company>{company};
	    companyPage.totalResultSetSize = 1;
	    companyPage.startIndex = 0; 
	    
      DC_ProposalService.Date_x dateX = new DC_ProposalService.Date_x();
	    dateX.year = Date.today().year();
	    dateX.month = Date.today().month();
	    dateX.day = Date.today().addDays(10).day();
	    
	    DC_ProposalService.DateTime_x dt = new DC_ProposalService.DateTime_x();
	    dt.date_x = dateX;
	    
      DC_ProposalService.Proposal proposal = new DC_ProposalService.Proposal();
      proposal.name = 'testProposal';
      proposal.lastModifiedDateTime = dt;
      proposal.appliedTeamIds = new List<Long>{12345};
      proposal.customFieldValues = new List<DC_ProposalService.BaseCustomFieldValue>{custFldValue};
       
      DC_ProposalService.Proposal proposal1 = new DC_ProposalService.Proposal();
      proposal1.name = 'testProposal1';
      proposal1.id = 12345;
      proposal1.lastModifiedDateTime = dt;
	    proposal1.startDateTime = dt;
	    proposal1.endDateTime = dt;
      proposal1.appliedTeamIds = new List<Long>{12345};
      proposal1.customFieldValues = new List<DC_ProposalService.BaseCustomFieldValue>{dropDownCustFldValue};
      
      DC_ProposalService.ProposalPage proposalPage = new DC_ProposalService.ProposalPage();
      proposalPage.results = new List<DC_ProposalService.Proposal>{proposal, proposal1};    
      proposalPage.totalResultSetSize = 2;
      proposalPage.startIndex = 0;
      
      
      DC_ReportService.ReportQuery rQuery = new DC_ReportService.ReportQuery();
    
	    DC_ReportService.ReportJob report = new DC_ReportService.ReportJob();     
	    report.id = 1663432659;
	    report.reportJobStatus = 'New';
	    report.reportQuery = rQuery;
    
      
      /*if(request instanceof DC_ProposalService.updateProposal_element){
        DC_ProposalService.updateProposalResponse_element resp = new DC_ProposalService.updateProposalResponse_element();
        resp.rval = proposal1;
        response.put('response_x', resp);
        
          //response.put('response_x', new DC_ProposalService.updateProposalResponse_element());
      }else*/
      /* if(request instanceof DC_ProposalService.createProposal_element){
        DC_ProposalService.createProposalResponse_element resp = new DC_ProposalService.createProposalResponse_element();
        resp.rval = proposal;
        response.put('response_x', resp);
        
          //response.put('response_x', new DC_ProposalService.createProposalResponse_element()); 
      }else*/
      
       if(request instanceof DC_ProposalService.updateProposals_element){
        DC_ProposalService.updateProposalsResponse_element resp = new DC_ProposalService.updateProposalsResponse_element();
        resp.rval = new List<DC_ProposalService.Proposal>{proposal1};
        response.put('response_x', resp);
        
          //response.put('response_x', new DC_ProposalService.updateProposalsResponse_element()); 
      }else if(request instanceof DC_ProposalService.createProposals_element){
        DC_ProposalService.createProposalsResponse_element resp = new DC_ProposalService.createProposalsResponse_element();
        resp.rval = new List<DC_ProposalService.Proposal>{proposal};
        response.put('response_x', resp);
          //response.put('response_x', new DC_ProposalService.createProposalsResponse_element()); 
      }else if(request instanceof DC_ProposalService.getProposalsByStatement_element){
        DC_ProposalService.getProposalsByStatementResponse_element resp = new DC_ProposalService.getProposalsByStatementResponse_element();
        resp.rval = proposalPage;
        response.put('response_x', resp);
          //response.put('response_x', new DC_ProposalService.getProposalsByStatementResponse_element());
      }else 
      /*if(request instanceof DC_ProposalService.getProposal_element){
        DC_ProposalService.getProposalResponse_element resp = new DC_ProposalService.getProposalResponse_element();
        resp.rval = proposal1;
        response.put('response_x', resp);
          //response.put('response_x', new DC_ProposalService.getProposalResponse_element());
      }else*/
       if(request instanceof DC_ProposalService.performProposalAction_element){
          response.put('response_x', new DC_ProposalService.performProposalActionResponse_element());
      }
      else if(request instanceof DC_CustomFieldService.updateCustomFieldOptions_element){
          DC_CustomFieldService.updateCustomFieldOptionsResponse_element resp = new DC_CustomFieldService.updateCustomFieldOptionsResponse_element();
          resp.rval = new List<DC_CustomFieldService.CustomFieldOption>{custFldOption};
          response.put('response_x', resp);
          //response.put('response_x', new DC_CustomFieldService.updateCustomFieldOptionsResponse_element());
      }else 
      /*if(request instanceof DC_CustomFieldService.getCustomField_element){
          DC_CustomFieldService.getCustomFieldResponse_element resp = new DC_CustomFieldService.getCustomFieldResponse_element();
          resp.rval = custFld;
          response.put('response_x', resp);
          
          //.put('response_x', new DC_CustomFieldService.getCustomFieldResponse_element()); 
      }else*/
      /* if(request instanceof DC_CustomFieldService.updateCustomFieldOption_element){
          DC_CustomFieldService.updateCustomFieldOptionResponse_element resp = new DC_CustomFieldService.updateCustomFieldOptionResponse_element();
          resp.rval = custFldOption;
          response.put('response_x', resp);
          
          //response.put('response_x', new DC_CustomFieldService.updateCustomFieldOptionResponse_element()); 
      }else if(request instanceof DC_CustomFieldService.createCustomFieldOption_element){
          DC_CustomFieldService.createCustomFieldOptionResponse_element resp = new DC_CustomFieldService.createCustomFieldOptionResponse_element();
          resp.rval = custFldOption1;
          response.put('response_x', resp);
          
          //response.put('response_x', new DC_CustomFieldService.createCustomFieldOptionResponse_element()); 
      }else*/ if(request instanceof DC_CustomFieldService.getCustomFieldsByStatement_element){
          DC_CustomFieldService.getCustomFieldsByStatementResponse_element resp = new DC_CustomFieldService.getCustomFieldsByStatementResponse_element();
          resp.rval = custFldPage;
          response.put('response_x', resp);
          
          //response.put('response_x', new DC_CustomFieldService.getCustomFieldsByStatementResponse_element());
      }/*else if(request instanceof DC_CustomFieldService.updateCustomField_element){
          DC_CustomFieldService.updateCustomFieldResponse_element resp = new DC_CustomFieldService.updateCustomFieldResponse_element();
          resp.rval = custFld;
          response.put('response_x', resp);
          
          //response.put('response_x', new DC_CustomFieldService.updateCustomFieldResponse_element());
      }else */if(request instanceof DC_CustomFieldService.createCustomFieldOptions_element){
          DC_CustomFieldService.createCustomFieldOptionsResponse_element resp = new DC_CustomFieldService.createCustomFieldOptionsResponse_element();
          resp.rval = new List<DC_CustomFieldService.CustomFieldOption>{custFldOption1};
          response.put('response_x', resp);
          
          //response.put('response_x', new DC_CustomFieldService.createCustomFieldOptionsResponse_element());
      }else if(request instanceof DC_CustomFieldService.getCustomFieldOption_element){
          DC_CustomFieldService.getCustomFieldOptionResponse_element resp = new DC_CustomFieldService.getCustomFieldOptionResponse_element();
          resp.rval = custFldOption;
          response.put('response_x', resp);
          
          //response.put('response_x', new DC_CustomFieldService.getCustomFieldOptionResponse_element());    
      
      }else if(request instanceof DC_CustomFieldService.createCustomFields_element){
          DC_CustomFieldService.createCustomFieldsResponse_element resp = new DC_CustomFieldService.createCustomFieldsResponse_element();
          resp.rval = new List<DC_CustomFieldService.CustomField>{custFld1};
          response.put('response_x', resp);
          
          //response.put('response_x', new DC_CustomFieldService.createCustomFieldsResponse_element());
      }else if(request instanceof DC_CustomFieldService.updateCustomFields_element){
          DC_CustomFieldService.updateCustomFieldsResponse_element resp = new DC_CustomFieldService.updateCustomFieldsResponse_element();
          resp.rval = new List<DC_CustomFieldService.CustomField>{custFld};
          response.put('response_x', resp);
          
          //response.put('response_x', new DC_CustomFieldService.updateCustomFieldsResponse_element());
      }else /*if(request instanceof DC_CustomFieldService.createCustomField_element){
          DC_CustomFieldService.createCustomFieldResponse_element resp = new DC_CustomFieldService.createCustomFieldResponse_element();
          resp.rval = custFld1;
          response.put('response_x', resp);
          //response.put('response_x', new DC_CustomFieldService.createCustomFieldResponse_element()); 
      }else */if(request instanceof DC_CustomFieldService.performCustomFieldAction_element)
          response.put('response_x', new DC_CustomFieldService.performCustomFieldActionResponse_element());  
      else if(request instanceof DC_ReportService.getReportDownloadURL_element){
	        DC_ReportService.getReportDownloadURLResponse_element resp = new DC_ReportService.getReportDownloadURLResponse_element();
	        resp.rval = 'sfid,DoubleClickId\nsfid,DoubleClickId\nsfid,DoubleClickId';
	        response.put('response_x', resp);
	        //response.put('response_x', new DC_CompanyService.getCompanyResponse_element());
	    }else if(request instanceof DC_ReportService.getReportJob_element){
	        DC_ReportService.getReportJobResponse_element resp = new DC_ReportService.getReportJobResponse_element();
	        resp.rval = report;
	        response.put('response_x', resp);
	        //response.put('response_x', new DC_CompanyService.updateCompanyResponse_element()); 
	    }
	    else if(request instanceof DC_ReportService.getReportDownloadUrlWithOptions_element){
	        DC_ReportService.getReportDownloadUrlWithOptionsResponse_element resp = new DC_ReportService.getReportDownloadUrlWithOptionsResponse_element();
	        resp.rval = 'sfid,DoubleClickId\nsfid,DoubleClickId\nsfid,DoubleClickId';
	        response.put('response_x', resp);
	        //response.put('response_x', new DC_CompanyService.updateCompaniesResponse_element()); 
	    }else if(request instanceof DC_ReportService.runReportJob_element){
	        DC_ReportService.runReportJobResponse_element resp = new DC_ReportService.runReportJobResponse_element();
	        resp.rval = report;
	        response.put('response_x', resp);
	        //response.put('response_x', new DC_CompanyService.createCompanyResponse_element()); 
	    }else if(request instanceof DC_CompanyService.getCompaniesByStatement_element){
        DC_CompanyService.getCompaniesByStatementResponse_element resp = new DC_CompanyService.getCompaniesByStatementResponse_element();
        resp.rval = companyPage;
        response.put('response_x', resp);
        //response.put('response_x', new DC_CompanyService.getCompaniesByStatementResponse_element());
    }
      return;
  }
}