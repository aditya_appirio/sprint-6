//  
// (c) 2014 Appirio
//
// Test_DC_ConnectorAdminPageController : test class for DC_ConnectorAdminPageController 
//
// 3rd feb 2014    Anjali K (JDC)      Original
//
@isTest
private class Test_DC_ConnectorAdminPageController {

  static testMethod void testDC_GenerateSeedData() {
    DC_ConnectorAdminPageController controller = new DC_ConnectorAdminPageController();

    // Test seed data generation
    controller.populateSeedData();
    System.assert([SELECT count() from goog_dclk_dsm__DC_FieldMapping__c] == 85);
    System.assert([SELECT count() from goog_dclk_dsm__DC_ObjectMapping__c] == 9);
    System.assert([SELECT count() from goog_dclk_dsm__DC_LinkAccountFields__c] == 3);
    System.assert([SELECT count() from goog_dclk_dsm__DC_LinkContactFields__c] == 5);
    System.assert([SELECT count() from goog_dclk_dsm__DC_LinkProposalFields__c] == 2);
    System.assert([SELECT count() from goog_dclk_dsm__DC_ObjectDeepLink__c] == 1);

	/* TODO check why these assertions aren't working
    System.assert([SELECT count() from goog_dclk_dsm__DC_ConnectorApplicationSettings__c] == 1);
    System.assert([SELECT count() from goog_dclk_dsm__DC_ProcessingQueue__c] == 5);
    System.assert([SELECT count() from goog_dclk_dsm__DC_QueueItems__c] == 5);
  	*/
  }
  @isTest
  static void testCreatePricebook(){
  	DC_ConnectorAdminPageController controller = new DC_ConnectorAdminPageController();
  	controller.createPricebook();
  }

  static testMethod void testDC_ConnectorAdminPage() {
  	
  	goog_dclk_dsm__DC_ConnectorAuthenticationSettings__c connSetting = DC_TestUtils.createConnectorSetting(
      '802187318127-fvdeo6krkkt0tbrmqd0u378sudior617.apps.googleusercontent.com',
        'sfdc-dc-connector', 'iGwU4g1MYxG6DNHnYeKlFYPE', '38192739', '6931', 
        'https://c.na11.visual.force.com/apex/DC_ConnectorAdminPage', 
        'https://www.google.com/apis/ads/publisher', true,'https://ads.google.com/apis/ads/publisher/v201408');
  	
  	goog_dclk_dsm__DC_Authentication__c auth = DC_TestUtils.createDCAuthentication('ya29.1.AADtN_WfW_S3p2Raw6ZCsveGTL0goy-T4lXQnjlnjDr97bue0MmKtBXHT-2cTac', 
      '1/VbGziuloIkBV7SZS7rP5wlRDzkVMX-UIf_l3wpVZm9Q', 3600, 'Bearer', false);
      auth.SetupOwnerId = userinfo.getuserId();
      insert auth;
    system.debug('@@@auth' +auth); 
    
    
    PageReference thePage = new PageReference('/apex/goog_dclk_dsm__DC_ConnectorAdminPage'); 
    Test.setCurrentPage(thePage); 
    ApexPages.currentPage().getParameters().put('code', '1234'); 
    
    DC_ConnectorAdminPageController controller = new DC_ConnectorAdminPageController();
    System.assert(controller.code != null);
    controller.saveSettings();
    System.assert(controller.error == null);
    
    controller.saveSettings();
    
    thePage = new PageReference('/apex/goog_dclk_dsm__DC_ConnectorAdminPage'); 
    Test.setCurrentPage(thePage); 
    ApexPages.currentPage().getParameters().put('error', 'testErr'); 
    
    controller = new DC_ConnectorAdminPageController();
    System.assert(controller.code == null);
    System.assert(controller.error != null);

    controller.schedulesPerHour = 5;
    controller.createSchedule();

    System.assert([SELECT count() from CronTrigger] == 5);

    System.assert(controller.manageSchedule() != null);
    DC_DoubleClickOAuthHandler authhandler;
    controller.authHandler = new DC_DoubleClickOAuthHandler(userinfo.getuserId());
    
    ApexPages.currentPage().getParameters().put('state', 'UserContext');
    controller.searchStateAndRedirect();
    controller.performSaveSetting();
    controller.saveSettings();
    
    ApexPages.currentPage().getParameters().put('state', 'AdminContext');
    controller.searchStateAndRedirect();
    controller.performSaveSetting();
    controller.saveSettings();
    
    
    goog_dclk_dsm__DC_ObjectMapping__c mapping = new goog_dclk_dsm__DC_ObjectMapping__c();
    mapping.goog_dclk_dsm__DC_DoubleClickObjectName__c = 'Proposal';
    mapping.goog_dclk_dsm__DC_SalesforceObjectName__c  = 'Opportunity';
    mapping.Name = 'Opportunity';

    insert mapping;

    goog_dclk_dsm__DC_FieldMapping__c fieldMapping = new goog_dclk_dsm__DC_FieldMapping__c();

    fieldMapping.goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = null;
    fieldMapping.goog_dclk_dsm__DC_DoubleClickFieldName__c = 'custom.test1';
    fieldMapping.goog_dclk_dsm__DC_Enabled__c = true;
    fieldMapping.goog_dclk_dsm__DC_Object_Mapping__c = mapping.Id;
    fieldMapping.Name = 'sfid1';

    insert fieldMapping;    

    fieldMapping = new goog_dclk_dsm__DC_FieldMapping__c();

    fieldMapping.goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345;
    fieldMapping.goog_dclk_dsm__DC_DoubleClickFieldName__c = 'custom.test2';
    fieldMapping.goog_dclk_dsm__DC_Enabled__c = true;
    fieldMapping.goog_dclk_dsm__DC_Object_Mapping__c = mapping.Id;
    fieldMapping.Name = 'sfid2';

    insert fieldMapping;    

    fieldMapping = new goog_dclk_dsm__DC_FieldMapping__c();

    fieldMapping.goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = null;
    fieldMapping.goog_dclk_dsm__DC_DoubleClickFieldName__c = 'custom.test';
    fieldMapping.goog_dclk_dsm__DC_Enabled__c = true;
    fieldMapping.goog_dclk_dsm__DC_Object_Mapping__c = mapping.Id;
    fieldMapping.Name = 'sfid3'; 

    insert fieldMapping;    
    goog_dclk_dsm__DC_User__c  dcUser = DC_TestUtils.createDCUser();
           
    Test.startTest();
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplCustomFieldService());
    Test.setMock(HttpCalloutMock.class, new     DC_HttpCalloutMockImploAuth());
                            
    System.assert(controller.getTotalCountCustomFields() == 3);

    System.assert(controller.getPendingCountCustomFields() == 2);
    
    Test.stopTest();
    
  }
  
  static testMethod void testDC_ConnectorAdminPage2() {
    goog_dclk_dsm__DC_ConnectorAuthenticationSettings__c connSetting = DC_TestUtils.createConnectorSetting(
      '802187318127-fvdeo6krkkt0tbrmqd0u378sudior617.apps.googleusercontent.com',
        'sfdc-dc-connector', 'iGwU4g1MYxG6DNHnYeKlFYPE', '38192739', '6931', 
        'https://c.na11.visual.force.com/apex/DC_ConnectorAdminPage', 
        'https://www.google.com/apis/ads/publisher', true,'https://ads.google.com/apis/ads/publisher/v201408');
        
    goog_dclk_dsm__DC_Authentication__c auth = DC_TestUtils.createDCAuthentication('ya29.1.AADtN_WfW_S3p2Raw6ZCsveGTL0goy-T4lXQnjlnjDr97bue0MmKtBXHT-2cTac', 
      '1/VbGziuloIkBV7SZS7rP5wlRDzkVMX-UIf_l3wpVZm9Q', 3600, 'Bearer', false);
      auth.SetupOwnerId = userinfo.getuserId();
      insert auth;
    system.debug('@@@auth' +auth); 
    
    DC_ConnectorAdminPageController controller = new DC_ConnectorAdminPageController();
    
    controller = new DC_ConnectorAdminPageController();
  
    DC_DoubleClickOAuthHandler authhandler;
    controller.authHandler = new DC_DoubleClickOAuthHandler(userinfo.getuserId());
    
    controller.runUserImport();
    controller.runTeamServiceImport();
    
    controller.getLstAccessType();
    controller.saveDCAccessSettings();
    
    goog_dclk_dsm__DC_ProcessingQueue__c processQueue1 = new goog_dclk_dsm__DC_ProcessingQueue__c();
    processQueue1.goog_dclk_dsm__DC_Process__c = 'User Import';
    
    goog_dclk_dsm__DC_ProcessingQueue__c processQueue2 = new goog_dclk_dsm__DC_ProcessingQueue__c();
    processQueue2.goog_dclk_dsm__DC_Process__c = 'User Import';
    
    List<goog_dclk_dsm__DC_ProcessingQueue__c> lstUserImportProcessingQueue = new List<goog_dclk_dsm__DC_ProcessingQueue__c>();
    lstUserImportProcessingQueue.add(processQueue1);
    lstUserImportProcessingQueue.add(processQueue2);
    insert lstUserImportProcessingQueue;

    controller.runUserImport();
    
    goog_dclk_dsm__DC_ProcessingQueue__c processQueue3 = new goog_dclk_dsm__DC_ProcessingQueue__c();
    processQueue3.goog_dclk_dsm__DC_Process__c = 'Team Service';
    
    goog_dclk_dsm__DC_ProcessingQueue__c processQueue4 = new goog_dclk_dsm__DC_ProcessingQueue__c();
    processQueue4.goog_dclk_dsm__DC_Process__c = 'Team Service';
    
    List<goog_dclk_dsm__DC_ProcessingQueue__c> lstTeamServiceProcessingQueue = new List<goog_dclk_dsm__DC_ProcessingQueue__c>();
    lstTeamServiceProcessingQueue.add(processQueue3);
    lstTeamServiceProcessingQueue.add(processQueue4);
    insert lstTeamServiceProcessingQueue;
    
    controller.runTeamServiceImport();
    
    controller.getLstAccessType();
    controller.saveDCAccessSettings();
     
    goog_dclk_dsm__DC_ObjectMapping__c mapping = new goog_dclk_dsm__DC_ObjectMapping__c();
    mapping.goog_dclk_dsm__DC_DoubleClickObjectName__c = 'Proposal';
    mapping.goog_dclk_dsm__DC_SalesforceObjectName__c  = 'Opportunity';
    mapping.Name = 'Opportunity';

    insert mapping;

    goog_dclk_dsm__DC_FieldMapping__c fieldMapping = new goog_dclk_dsm__DC_FieldMapping__c();

    fieldMapping.goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = null;
    fieldMapping.goog_dclk_dsm__DC_DoubleClickFieldName__c = 'custom.test1';
    fieldMapping.goog_dclk_dsm__DC_Enabled__c = true;
    fieldMapping.goog_dclk_dsm__DC_Object_Mapping__c = mapping.Id;
    fieldMapping.Name = 'sfid1';

    insert fieldMapping;    

    fieldMapping = new goog_dclk_dsm__DC_FieldMapping__c();

    fieldMapping.goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = 12345;
    fieldMapping.goog_dclk_dsm__DC_DoubleClickFieldName__c = 'custom.test2';
    fieldMapping.goog_dclk_dsm__DC_Enabled__c = true;
    fieldMapping.goog_dclk_dsm__DC_Object_Mapping__c = mapping.Id;
    fieldMapping.Name = 'sfid2';

    insert fieldMapping;    

    fieldMapping = new goog_dclk_dsm__DC_FieldMapping__c();

    fieldMapping.goog_dclk_dsm__DC_DoubleClickCustomFieldID__c = null;
    fieldMapping.goog_dclk_dsm__DC_DoubleClickFieldName__c = 'custom.test';
    fieldMapping.goog_dclk_dsm__DC_Enabled__c = true;
    fieldMapping.goog_dclk_dsm__DC_Object_Mapping__c = mapping.Id;
    fieldMapping.Name = 'sfid3'; 

    insert fieldMapping;    
   
           
    Test.startTest();
    Test.setMock(WebServiceMock.class, new DC_WebServiceMockImplCustomFieldService());
    Test.setMock(HttpCalloutMock.class, new     DC_HttpCalloutMockImploAuth());

    controller.createCustomFields();

    System.assert(controller.getPendingCountCustomFields() == 0);
    
    Test.stopTest();
    
    
  }
}